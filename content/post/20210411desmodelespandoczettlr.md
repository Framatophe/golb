---
title: "Lettres et articles avec Zettlr ou Pandoc"
date: "2021-04-11"
lastmod: "2021-04-16"
author: "Christophe Masutti"
image: "/images/postimages/cm.png"
description: "Trois modèles Zettlr / Pandoc pour articles académiques, rapports et lettres simples. Une tentative pédagogique pour apprendre à se servir de Zettlr et pandoc."
tags: ["Markdown", "bidouille", "pandoc", "éditeur", "traitement de texte", "Zettlr"]
categories:
- Logiciel libre
---


Utiliser le célèbre convertisseur de document [pandoc](https://pandoc.org/) signifie manier (un tout petit peu) la ligne de commande. Rien de sorcier, pour peu d'être assez ouvert d'esprit pour comprendre qu'en matière de traitement de texte tout n'est pas censé se régler au clic de souris. Cela dit, le logiciel [Zettlr](https://www.zettlr.com/) permettent de s'affranchir de la ligne de commande tout en utilisant la puissance de pandoc. Zettlr propose une interface optimale pour écrire du texte en [markdown](https://fr.wikipedia.org/wiki/Markdown) et exporter dans différents formats. Il suffit d'aller voir dans les préférences avancées de Zettlr pour constater que ce dernier utilise une sorte de ligne de commande universelle avec pandoc pour réaliser ses exports. Pour ce qui concerne l'export en PDF, Zettlr utilise aussi LaTeX qu'il faudra installer sur la machine au préalable, tout comme pandoc.

J'ai écrit un petit [billet sur Zettlr](https://golb.statium.link/post/20200218zettlr/). Vous pouvez aller le lire pour mieux comprendre ce que je vais raconter ci-dessous. 

Si vous n'avez pas envie de lire ce billet et souhaitez directement vous servir des modèles que je propose, vous trouverez l'ensemble sur [ce dépôt GIT](https://framagit.org/Framatophe/modeles-pandoc-zettlr). Il y a un fichier `README` suffisant.

## Pandoc et LaTeX

Qu'en est-il de la mise en page&nbsp;? C'est là que les avantages combinés de pandoc et de LaTeX entrent en jeu&nbsp;: il est possible, en quelque sorte, de «&nbsp;programmer&nbsp;» la mise en page en élaborant des modèles qui utilisent différentes variables renseignées dans le fichier source en markdown. Bien entendu, on peut créer des modèles pour tout type d'export avec pandoc et d'ailleurs ce dernier utilise une batterie de modèles par défaut, mais ces derniers ne sont pas toujours adaptés à la sortie finale&nbsp;: qu'on utilise Zettlr ou non (ou n'importe quel éditeur de texte), il faut savoir compléter son fichier markdown avec des informations destinées à être taitées par pandoc.

Pour information, si pandoc s'utilise en ligne de commande, c'est parce qu'il y a [toute une batterie](https://pandoc.org/MANUAL.html#variables-set-by-pandoc) d'options et de commandes que pandoc est capable d'exécuter. Pour éviter de les entrer systématiquement en ligne de commande, on peut les indiquer simplement dans l'en-tête du fichier markdown (ou dans un fichier séparé). Pour mieux comprendre de quoi il s'agit, vous pouvez vous reporter [à ce document](https://framagit.org/Framatophe/ApercudePandoc/-/blob/master/sortie.pdf).

Pour donner un exemple trivial&nbsp;: si vous voulez que votre document markdown une fois exporté comporte un titre, un nom d'auteur et une date, il vous suffit de les indiquer de cette manière dans l'en-tête au début du document (on les appellera des «&nbsp;métadonnées&nbsp;»)&nbsp;:

    ``
    % Le titre
    % Prénom Nom
    % 17/02/2021
    ``

Ou bien, en utilisant le format YAML&nbsp;:

    ``
    ---
    title: "Le titre"
    date: "17 février 2021"
    author: "Prénom Nom"
    ...

    ``

Ainsi, pandoc les interprétera et, en utilisant un modèle par défaut ou un modèle de votre cru, reportera ces variables aux emplacements voulus dans la mise en page.

Si vous voulez écrire un article académique et le sortir en PDF en utilisant pandoc qui lui-même utilisera LaTeX, il vous faut donc réunir les éléments suivants :

- un modèle LaTeX (ou se contenter des modèles LaTEX par défaut de pandoc),
- des métadonnées à entrer après s'être bien renseigné sur la manuel d'utilisation de pandoc,
- un fichier de bibliographie,
- un fichier de style de présentation de la bibliographie.

En comptant les options courantes de pandoc que vous souhaitez utiliser, vous risquez de vous retrouver avec une ligne de commande assez longue&hellip; pour éviter cela, vous pouvez automatiser le tout avec un ``makefile`` que vous pourrez réutiliser plus tard pour d'autres documents, mais bigre&hellip; cela commence à devenir bien complexe pour qui n'a pas forcément envie d'y passer trop de temps, à moins de concevoir une fois pour toute une chaîne éditoriale que vous adapterez au fil du temps (c'est une option).


## Zettlr et les modèles

Un grand avantage de Zettlr est de vous décharger de plusieurs de ces contraintes, en plus d'offrir une interface vraiment pratique entièrement destinée à l'écriture. Pour réaliser des exports, il vous faudra seulement installer pandoc. Et pour réaliser des exports PDF, il vous faudra installer LaTeX (je préconise une installation complète, par exemple ``Texlive-full``).

Le fichier et le [style CSL](https://citationstyles.org/) de bibliographie&nbsp;? il vous suffit de les renseigner dans les préférences de Zettlr une fois pour toutes.

Le modèle de sortie PDF&nbsp;? idem, vous le renseignez dans les préférences d'export PDF.

Et pour exporter&nbsp;? il vous suffira de cliquer sur l'icône PDF.

Tout cela demeure assez simple, sauf que si vous rédigez un article sans chercher à travailler la question des métadonnées, votre sortie PDF risque de vous décevoir&nbsp;: pandoc utilisera le modèle par défaut, c'est-à-dire la classe ``[article]``de LaTeX, qui en soi est une excellente mise en page, mais ne colle pas forcément à vos souhaits.

Réaliser des modèles pour pandoc est chose assez facile. En effet, si vous comprenez des notions informatiques basiques comme «&nbsp;pour...si...ou...alors&nbsp;», la [syntaxe des modèles pandoc](https://pandoc.org/MANUAL.html#template-syntax) ne posera guère de problème. Il faudra néanmoins maîtriser LaTeX si vous voulez créer un modèle LaTeX.


### Mes modèles article, rapport et lettre

Pour vous éviter d'y passer trop de temps, je vous propose d'utiliser mes modèles élaboré pour des articles, des rapports ou des lettres simples.

J'ai pris le parti de réaliser un modèle LaTeX destiné à chaque fois à la sortie voulue, là où le modèle pandoc par défaut permet de créer un article, une présentation ou un livre en indiquant simplement la classe voulue. Ce modèle par défaut inclu donc de nombreuses variables mais à trop vouloir être généraliste, je pense qu'on loupe l'essentiel. 

Pour utiliser mes modèles, il vous suffit de réutiliser les fichiers markdown renseigner les différents éléments présents dans les en-têtes YAML de ces fichiers. Si vous voulez modifier les modèles eux-mêmes, il vous suffit de les éditer&nbsp;: j'ai tâché de commenter et de bien séparer les éléments importants.

Ces modèles utilisent KOMA-script, la classe ``scrartcl`` pour les article, ``scrreprt`` pour les rapports et ``scrlttr2`` pour les lettres. Si vous connaissez LaTeX, vous comprendrez que KOMA-script est un  excellent choix pour des documents européens (il fut créé au départ pour des documents de langue allemande, mais il fut rapidement adapté à différentes pratiques d'écriture, dont le Français).

Pour résumer très simplement&nbsp;:

- si vous écrivez une lettre, renseignez dans Zettlr le chemin vers le modèle dans les préférences d'export PDF,
- si vous écrivez un article ou un rapport, renseignez dans Zettlr le chemin vers le modèle dans les préférences d'export PDF et notez que le modèle utilise les styles CSL pour la bibliographie (pas de biblatex ou de natbib, donc),
- si vous ne voulez pas utiliser Zettlr, et uniquement pandoc, je vous ai indiqué dans le ``README`` les lignes de commande à utiliser (ce sont celles qu'utilise Zettlr, en fait).

Vous trouverez l'ensemble sur [ce dépôt GIT](https://framagit.org/Framatophe/modeles-pandoc-zettlr).








