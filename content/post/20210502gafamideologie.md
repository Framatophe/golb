---
title: "Les médias sociaux ne sont pas des espaces démocratiques"
date: 2021-05-08
author: "Christophe Masutti"
image: "/images/postimages/cm.png"
description: "Les médias sociaux des GAFAM sont les instruments d'un fascisme néolibéral. À l'inverse, les instances du Fediverse sont les supports utilitaires où des pratiques d'interlocution non-concurrentielles peuvent s'accomplir et s'inventer dans une grande diversité."
tags: ["Libres propos", "Politique", "surveillance"]
categories:
- Libres propos
---

**[Ce billet a été publié sur le Framablog à [cette adresse](https://framablog.org/2021/05/15/les-medias-sociaux-ne-sont-pas-des-espaces-democratiques/), le 15 mai 2021.]**

Lors d'une récente interview avec deux autres framasoftiennes à propos du Fediverse et des réseaux sociaux dits «&nbsp;alternatifs&nbsp;», une  question nous fut posée&nbsp;:

> dans la mesure où les instances de service de micro-blogging (type Mastodon) ou de vidéo (comme Peertube) peuvent afficher des «&nbsp;lignes éditoriales&nbsp;» très différentes les unes des autres, comment gérer la modération en choisissant de se fédérer ou non avec une instance peuplée de fachos ou comment se comporter vis-à-vis d'une instance communautaire et exclusive qui choisit délibérément de ne pas être fédérée ou très peu&nbsp;?

De manière assez libérale et pour peu que les conditions d'utilisation du service soient clairement définies dans chaque instance, on peut répondre simplement que la modération demande plus ou moins de travail, que chaque instance est tout à fait libre d'adopter sa propre politique éditoriale, et qu'il s'agit de choix individuels (ceux du propriétaire du serveur qui héberge l'instance) autant que de choix collectifs (si l'hébergeur entretient des relations diplomatiques avec les membres de son instance). C'est une évidence.

La difficulté consistait plutôt à expliquer pourquoi, dans la conception même des logiciels (Mastodon ou Peertube, en l'occurrence) ou dans les clauses de la licence d'utilisation, il n'y a pas de moyen mis en place par l'éditeur du logiciel (Framasoft pour Peertube, par exemple) afin de limiter cette possibilité d'enfermement de communautés d'utilisateurs dans de grandes bulles de filtres en particulier si elles tombent dans l'illégalité. Est-il légitime de faire circuler un logiciel qui permet à
[&#35;lesgens](https://www.cnrtl.fr/definition/gens) de se réunir et d'échanger dans un entre-soi homogène tout en prétendant que le
Fediverse est un dispositif d'ouverture et d'accès égalitaire&nbsp;?

Une autre façon de poser la question pourrait être la suivante&nbsp;: comment est-il possible qu'un logiciel libre puisse permettre à des fachos d'ouvrir leurs propres instance de microblogging en déversant impunément sur le réseau leurs flots de haine et de frustrations&nbsp;?[^1]

Bien sûr nous avons répondu à ces questions, mais à mon avis de manière trop vague. C'est qu'en réalité, il y a plusieurs niveaux de compréhension que je vais tâcher de décrire ici. Il y a trois aspects&nbsp;:

1.  l'*éthique* du logiciel libre n'inclut pas la destination morale des logiciels libres, tant que la loyauté des usages est respectée, et la première clause des 4 libertés du logiciel libre implique la liberté d'usage&nbsp;: sélectionner les utilisateurs finaux en fonction de leurs orientations politique, sexuelles, etc. contrevient fondamentalement à cette clause...
2.  ... mais du point de vue *technique*, on peut en discuter car la conception du logiciel pourrait permettre de repousser ces limites éthiques[^2],
3.  et la responsabilité *juridique* des hébergeurs implique que ces instances fachos sont de toute façon contraintes par l'arsenal
    juridique adapté&nbsp;; ce à quoi on pourra toujours rétorquer que cela n'empêche pas les fachos de se réunir dans une cave (mieux&nbsp;: un local poubelle) à l'abri des regards.

Mais est-ce suffisant&nbsp;? se réfugier derrière une prétendue neutralité de la technique (qui n'est jamais neutre), les limites éthiques ou la loi, ce n'est pas une bonne solution. Il faut se poser la question&nbsp;: que fait-on concrètement non pour interdire certains usages du Fediverse, mais pour en limiter l'impact social négatif&nbsp;?

La principale réponse, c'est que le modèle économique du Fediverse ne repose pas sur la valorisation lucrative des données, et que se détacher des modèles centralisés implique une remise en question de ce que sont les «&nbsp;réseaux&nbsp;» sociaux. La vocation d'un dispositif technologique comme le Fediverse n'est pas d'éliminer les pensées fascistes et leur expression, pas plus que la vocation des plateformes Twitter et Facebook n'est de diffuser des modèles démocratiques, malgré leur prétention à cet objectif. La démocratie, les échanges d'idées, et de manière générale les interactions sociales ne se décrètent pas par des modèles technologiques, pas plus qu'elles ne s'y résument. Prétendre le contraire serait les restreindre à des modèles et des choix imposés (et on voit bien que la technique ne peut être neutre). Si Facebook, Twitter et consorts ont la prétention d'être les gardiens de la liberté d'expression, c'est bien davantage pour exploiter les données personnelles à des fins lucratives que pour mettre en place un débat démocratique.

Exit le vieux rêve du *global village*&nbsp;? En fait, cette vieille idée
de Marshall McLuhan ne correspond pas à ce que la plupart des gens en
ont retenu. En 1978, lorsque Murray Turoff et Roxanne Hiltz publient
*The Network Nation*, ils conceptualisent vraiment ce qu'on entend par «
Communication médiée par ordinateur&nbsp;»&nbsp;: échanges de contenus (volumes et
vitesse), communication sociale-émotionnelle (les émoticônes), réduction
des distances et isolement, communication synchrone et asynchrone,
retombées scientifiques, usages domestiques de la communication en
ligne, etc. Récompensés en 1994 par l'EFF Pioneer Award, Murray Turoff
et Roxanne Hiltz sont aujourd'hui considérés comme les «&nbsp;parents&nbsp;» des
systèmes de forums et de chat massivement utilisés aujourd'hui. Ce qu'on
a retenu de leurs travaux, et par la suite des nombreuses applications,
c'est que l'avenir du débat démocratique, des processus de décision
collective (M. Turoff travaillait pour des institutions publiques) ou de
la recherche de consensus, reposent pour l'essentiel sur les
technologies de communication. C'est vrai en un sens, mais M. Turoff
mettait en garde[^3]&nbsp;:

> *Dans la mesure où les communications humaines sont le mécanisme par
> lequel les valeurs sont transmises, tout changement significatif dans
> la technologie de cette communication est susceptible de permettre ou
> même de générer des changements de valeur.*

Communiquer avec des ordinateurs, bâtir un système informatisé de
communication sociale-émotionnelle ne change pas seulement
l'organisation sociale, mais dans la mesure où l'ordinateur se fait de
plus en plus le support exclusif des communications (et les prédictions
de Turoff s'avéreront très exactes), la communication en réseau fini par
déterminer nos valeurs. 

Aujourd'hui, communiquer dans un espace unique
globalisé, centralisé et ouvert à tous les vents signifie que nous
devons nous protéger individuellement contre les atteintes morales et
psychiques de celleux qui s'immiscent dans nos échanges. Cela signifie
que nos écrits puissent être utilisés et instrumentalisés plus tard à
des fins non souhaitées. Cela signifie qu'au lieu du consensus et du
débat démocratique nous avons en réalité affaire à des séries de *buzz*
et des cancans. Cela signifie une mise en concurrence farouche entre des
contenus discursifs de qualité et de légitimités inégales mais
prétendument équivalents, entre une casserole qui braille *La donna è
mobile* et la version Pavarotti, entre une conférence du Collège de
France et un historien révisionniste amateur dans sa cuisine, entre des
contenus journalistiques et des *fake news*, entre des débats argumentés
et des plateaux-télé nauséabonds. 

Tout cela ne relève en aucun cas du
consensus et encore moins du débat, mais de l'annulation des chaînes de
valeurs (quelles qu'elles soient) au profit d'une mise en concurrence de
contenus à des fins lucratives et de captation de l'attention. Le
village global est devenu une poubelle globale, et ce n'est pas
brillant.

Dans cette perspective, le Fediverse cherche à inverser la tendance. Non
par la technologie (le protocole ActivityPub ou autre), mais par le fait
qu'il incite à réfléchir sur la manière dont nous voulons conduire nos
débats et donc faire circuler l'information.

On pourrait aussi bien
affirmer qu'il est normal de se voir fermer les portes (ou du moins être
exclu de fait) d'une instance féministe si on est soi-même un homme, ou
d'une instance syndicaliste si on est un patron, ou encore d'une
instance d'un parti politique si on est d'un autre parti. C'est un
comportement tout à fait normal et éminemment social de faire partie
d'un groupe d'affinités, avec ses expériences communes, pour parler de
ce qui nous regroupe, d'actions, de stratégies ou simplement un partage
d'expériences et de subjectivités, sans que ceux qui n'ont pas les mêmes
affinités ou subjectivités puissent s'y joindre. De manière ponctuelle
on peut se réunir à l'exclusion d'autre groupes, pour en sortir à titre
individuel et rejoindre d'autre groupes encore, plus ouverts, tout comme
on peut alterner entre l'intimité d'un salon et un hall de gare.

Dans
[ce texte](https://framablog.org/2021/01/26/le-fediverse-et-lavenir-des-reseaux-decentralises/)
paru sur le Framablog, A. Mansoux et R. R. Abbing montrent que le
Fediverse est une critique de l'ouverture. Ils ont raison. Là où les
médias sociaux centralisés impliquaient une ouverture en faveur d'une
croissance lucrative du nombre d'utilisateurs, le Fediverse se fout
royalement de ce nombre, pourvu qu'il puisse mettre en place des chaînes
de confiance.

Un premier mouvement d'approche consiste à se débarrasser
d'une conception complètement biaisée d'Internet qui fait passer cet
ensemble de réseaux pour une sorte de substrat technique sur lequel
poussent des services ouverts aux publics de manière égalitaire.
Évidemment ce n'est pas le cas, et surtout parce que les réseaux ne se
ressemblent pas, certains sont privés et chiffrés (surtout dans les
milieux professionnels), d'autres restreints, d'autres plus ouverts ou
complètement ouverts. Tous dépendent de protocoles bien différents. Et
concernant les médias sociaux, il n'y a aucune raison pour qu'une
solution technique soit conçue pour empêcher la première forme de
modération, à savoir le choix des utilisateurs. Dans la mesure où c'est
le propriétaire de l'instance (du serveur) qui reste *in fine*
responsable des contenus, il est bien normal qu'il puisse maîtriser
l'effort de modération qui lui incombe. Depuis les années 1980 et les
groupes *usenet*, les réseaux sociaux se sont toujours définis selon des
groupes d'affinités et des règles de modération clairement énoncées.

À l'inverse, avec des conditions générales d'utilisation le plus souvent
obscures ou déloyales, les services centralisés tels Twitter, Youtube ou
Facebook ont un modèle économique tel qu'il leur est nécessaire de
drainer un maximum d'utilisateurs. En déléguant le choix de filtrage à
chaque utilisateur, ces médias sociaux ont proposé une représentation
faussée de leurs services&nbsp;:

1.  Faire croire que c'est à chaque utilisateur de choisir les contenus qu'il veut voir alors que le système repose sur l'économie de l'attention et donc sur la multiplication de contenus marchands (la publicité) et la mise en concurrence de contenus censés capter l'attention. Ces contenus sont ceux qui totalisent plus ou moins d'audience selon les orientations initiales de l'utilisateur. Ainsi on se voit proposer des contenus qui ne correspondent pas forcément à nos goûts mais qui captent notre attention parce de leur nature attrayante ou choquante provoquent des émotions.
2.  Faire croire qu'ils sont des espaces démocratiques. Ils réduisent la démocratie à la seule idée d'expression libre de chacun (lorsque Trump s'est fait virer de Facebook les politiques se sont sentis outragés&hellip; comme si Facebook était un espace public, alors qu'il s'agit d'une entreprise privée).

Les médias sociaux *mainstream* sont tout sauf des espaces où serait
censée s'exercer la démocratie bien qu'ils aient été considérés comme
tels, dans une sorte de confusion entre le brouhaha débridé des contenus
et la liberté d'expression. Lors du «&nbsp;printemps arabe&nbsp;» de 2010, par
exemple, on peut dire que les révoltes ont beaucoup reposé sur la
capacité des réseaux sociaux à faire circuler l'information. Mais il a
suffi aux gouvernements de censurer les accès à ces services centralisés
pour brider les révolutions. Ils se servent encore aujourd'hui de cette
censure pour mener des négociations diplomatiques qui tantôt cherchent à
attirer l'attention pour obtenir des avantages auprès des puissances
hégémoniques tout en prenant la «&nbsp;démocratie&nbsp;» en otage, et tantôt
obligent les GAFAM à se plier à la censure tout en facilitant la
répression. La collaboration est le sport collectif des GAFAM. En
Turquie, [Amnesty International s'en inquiète](https://www.amnesty.org/fr/latest/news/2021/01/turkey-facebook-and-other-companies-in-danger-of-becoming-an-instrument-of-state-censorship/)
et les exemples concrets ne manquent pas comme [au Vietnam récemment](https://www.lefigaro.fr/secteur/high-tech/vietnam-facebook-et-google-des-zones-sans-droits-de-l-homme-denonce-amnesty-20201201).

Si les médias sociaux comme Twitter et Facebook sont devenus des leviers
politiques, c'est justement parce qu'ils se sont présentés comme des
supports technologiques à la démocratie. Car tout dépend aussi de ce
qu'on entend par «&nbsp;démocratie&nbsp;». Un mot largement privé de son sens
initial comme le montre si bien F. Dupuis-Déri[^4]. Toujours est-il que, de manière très réductrice, on
tient pour acquis qu'une démocratie s'exerce selon deux conditions&nbsp;: que
l'information circule et que le débat public soit possible.

Même en réduisant la démocratie au schéma techno-structurel que lui imposent les
acteurs hégémoniques des médias sociaux, la question est de savoir s'il
permettent la conjonction de ces conditions. La réponse est non. Ce
n'est pas leur raison d'être.

Alors qu'Internet et le Web ont été
élaborés au départ pour être des dispositifs égalitaires en émission et
réception de pair à pair, la centralisation des accès soumet l'émission
aux conditions de l'hébergeur du service. Là où ce dernier pourrait se
contenter d'un modèle marchand basique consistant à faire payer l'accès
et relayer à l'aveugle les contenus (ce que fait La Poste, encadrée par
la loi sur les postes et télécommunications), la salubrité et la
fiabilité du service sont fragilisés par la responsabilisation de
l'hébergeur par rapport à ces contenus et la nécessité pour l'hébergeur
à adopter un modèle économique de rentabilité qui repose sur la
captation des données des utilisateurs à des fins de marketing pour
prétendre à une prétendue gratuité du service[^5]. Cela implique que les contenus échangés ne sont et ne seront jamais indépendants de toute forme de censure unilatéralement décidée (quoi qu'en pensent les politiques qui entendent légiférer sur
l'emploi des dispositifs qui relèveront toujours de la propriété
privée), et jamais indépendants des impératifs financiers qui justifient
l'économie de surveillance, les atteintes à notre vie privée et le
formatage comportemental qui en découlent.

Paradoxalement, le rêve d'un
espace public ouvert est tout aussi inatteignable pour les médias
sociaux dits «&nbsp;alternatifs&nbsp;», où pour des raisons de responsabilité
légale et de choix de politique éditoriale, chaque instance met en place
des règles de modération qui pourront toujours être considérées par les
utilisateurs comme abusives ou au moins discutables. La différence,
c'est que sur des réseaux comme le Fediverse (ou les [instances usenet](https://www.cairn.info/les-maitres-du-reseau--9782707135216-page-39.htm)
qui reposent sur NNTP), le modèle économique n'est pas celui de
l'exploitation lucrative des données et n'enferme pas l'utilisateur sur
une instance en particulier. Il est aussi possible d'ouvrir sa propre
instance à soi, être le seul utilisateur, et néanmoins se fédérer avec
les autres.

De même sur chaque instance, les règles d'usage pourraient
être discutées à tout moment entre les utilisateurs et les responsables
de l'instance, de manière à créer des consensus. En somme, le Fediverse
permet le débat, même s'il est restreint à une communauté
d'utilisateurs, là où la centralisation ne fait qu'imposer un état de
fait tout en tâchant d'y soumettre le plus grand nombre. Mais dans un
pays comme le Vietnam où l'essentiel du trafic Internet passe par
Facebook, les utilisateurs ont-ils vraiment le choix&nbsp;?

Ce sont bien la centralisation et l'exploitation des données qui font des réseaux
sociaux comme Facebook, YouTube et Twitter des instruments extrêmement
sensibles à la censure d'État, au service des gouvernements
totalitaires, et parties prenantes du fascisme néolibéral.

L'affaire Cambridge Analytica a bien montré combien le débat démocratique sur les
médias sociaux relève de l'imaginaire, au contraire fortement soumis aux
effets de fragmentation discursive. Avant de nous demander quelles
idéologies elles permettent de véhiculer nous devons interroger
l'idéologie des GAFAM. Ce que je soutiens, c'est que la structure même
des services des GAFAM ne permet de véhiculer vers les masses que des
idéologies qui correspondent à leurs modèles économiques, c'est-à-dire
compatibles avec le profit néolibéral.

En reprenant des méthodes d'analyse des années 1970-80, le marketing psychographique et la socio-démographie[^6], Cambridge Analytica illustre parfaitement les trente dernières années de perfectionnement de l'analyse des données comportementales des individus en utilisant le *big data*. Ce qui intéresse le marketing, ce ne sont plus les causes,
les déterminants des choix des individus, mais la possibilité de prédire
ces choix, peu importent les causes. La différence, c'est que lorsqu'on
applique ces principes, on segmente la population par stéréotypage dont
la granularité est d'autant plus fine que vous disposez d'un maximum de
données. Si vous voulez influencer une décision, dans un milieu où il
est possible à la fois de pomper des données et d'en injecter (dans les
médias sociaux, donc), il suffit de voir quels sont les paramètres à
changer. À l'échelle de millions d'individus, changer le cours d'une
élection présidentielle devient tout à fait possible derrière un écran.
 
C'est la raison pour laquelle les politiques du moment ont surréagi face
au bannissement de Trump des plateformes comme Twitter et Facebook (voir
[ici](https://www.lexpress.fr/actualites/1/monde/trump-banni-de-twitter-l-ue-plaide-pour-un-controle-democratique-breton_2142441.html)
ou
[là](https://www.bloomberg.com/news/articles/2021-01-11/merkel-sees-closing-trump-s-social-media-accounts-problematic)).
Si ces plateformes ont le pouvoir de faire taire le président des
États-Unis, c'est que leur capacité de caisse de résonance accrédite
l'idée qu'elles sont les principaux espaces médiatiques réellement
utiles aux démarches électoralistes. En effet, nulle part ailleurs il
n'est possible de s'adresser en masse, simultanément et de manière
segmentée (ciblée) aux populations. Ce faisant, le discours politique ne
s'adresse plus à des groupes d'affinité (un parti parle à ses
sympathisants), il ne cherche pas le consensus dans un espace censé
servir d'agora géante où aurait lieu le débat public. Rien de tout cela.
Le discours politique s'adresse désormais et en permanence à chaque
segment électoral de manière assez fragmentée pour que chacun puisse y
trouver ce qu'il désire, orienter et conforter ses choix en fonction de
ce que les algorithmes qui scandent les contenus pourront présenter (ou
pas). Dans cette dynamique, seul un trumpisme ultra-libéral pourra
triompher, nulle place pour un débat démocratique, seules triomphent les
polémiques, la démagogie réactionnaire et ce que les gauches ont tant de
mal à identifier[^7]&nbsp;: le fascisme.

Face à
cela, et sans préjuger de ce qu'il deviendra, le Fediverse propose une
porte de sortie sans toutefois remettre au goût du jour les vieilles
représentations du *village global*. J'aime à le voir comme une
multiplication d'espaces (d'instances) plus où moins clos (ou plus ou
moins ouverts, c'est selon) mais fortement identifiés et qui s'affirment
les uns par rapport aux autres, dans leurs différences ou leurs
ressemblances, en somme dans leurs diversités.

C'est justement cette diversité qui est à la base du débat et de la recherche de consensus, mais sans en constituer l'alpha et l'oméga. Les instances du Fediverse,
sont des espaces communs d'immeubles qui communiquent entre eux, ou pas,
ni plus ni moins. Ils sont des lieux où l'on se regroupe et où peuvent
se bâtir des collectifs éphémères ou non. Ils sont les supports
utilitaires où des pratiques d'interlocution non-concurrentielles
peuvent s'accomplir et s'inventer&nbsp;: microblog, blog, organiseur
d'événement, partage de vidéo, partage de contenus audio, et toute
application dont l'objectif consiste à outiller la liberté d'expression
et non la remplacer.

Angélisme&nbsp;? Peut-être. En tout cas, c'est ma
manière de voir le Fediverse aujourd'hui. L'avenir nous dira ce que les
utilisateurs en feront.

## Notes

[^1]: On peut se référer au passage de la plateforme suprémaciste Gab aux réseaux du Fediverse, mais qui finalement fut bloquée par la plupart des instances du réseau.

[^2]: Par exemple, sans remplacer les outils de modération par du *machine learning* plus ou moins efficace, on peut rendre visible davantage les procédures de reports de contenus haineux, mais à condition d'avoir une équipe de modérateurs prête à réceptionner le flux&nbsp;: les limites deviennent humaines.

[^3]: Turoff, Murray, and Starr Roxane Hiltz. 1994. The Network Nation: Human Communication via Computer. Cambridge: MIT Press, p. 401.

[^4]: Dupuis-Déri, Francis. *Démocratie, histoire politique d'un mot: aux
    États-Unis et en France*. Montréal (Québec), Canada: Lux, 2013.

[^5]: Et même si le service était payant, l'adhésion supposerait un consentement autrement plus poussé à l'exploitation des données personnelles sous prétexte d'une qualité de service et d'un meilleur ciblage marketing ou de propagande. Pire encore s'il disposait d'une offre premium ou de niveaux d'abonnements qui segmenteraient encore davantage les utilisateurs.

[^6]: J'en parlerai dans un article à venir au sujet du courtage de données et de la société Acxiom.

[^7]: &hellip;pas faute d'en connaître les symptômes depuis longtemps. Comme ce texte de Jacques Ellul paru dans la revue *Esprit* en 1937, intitulé «&nbsp;[Le fascisme fils du libéralisme](https://lesamisdebartleby.wordpress.com/2020/03/26/jacques-ellul-le-fascisme-fils-du-liberalisme/)&nbsp;», dont voici un extrait&nbsp;: «&nbsp;[Le fascisme] s'adresse au sentiment et non à l'intelligence, il n'est pas un effort vers un ordre réel mais vers un ordre fictif de la réalité. Il est précédé par tout un courant de tendances vers le fascisme : dans tous les pays nous retrouvons ces mesures de police et de violence, ce désir de restreindre les droits du parlement au profit du gouvernement, décrets-lois et pleins pouvoirs, affolement systématique obtenu par une lente pression des journaux sur la mentalité courante, attaques contre tout ce qui est pensée dissidente et expression de cette pensée, limitation de liberté de parole et de droit de réunion, restriction du droit de grève et de manifester, etc. Toutes ces mesures de fait constituent déjà le fascisme.&nbsp;».
