---
title: "En finir avec Gérard d'Alsace"
date: 2021-12-29
author: "Christophe Masutti"
image: "/images/postimages/cm.png"
description: "Une tour Gérard d'Alsace a-t-elle vraiment existé à Gérardmer&nbsp;? D'où Gérardmer tire-t-elle son nom&nbsp;? Dans ce billet je propose d'en finir avec Gérard d'Alsace, mais pour cela il faut exhumer d'anciennes sources des XVIIe et XVIIIe siècle. Avec le récit de Dom Ruinart à travers les Vosges, nous verrons d'où provient cette légende."
tags: ["Libres propos", "Histoire", "Vosges"]
categories:
- Libres propos
---


Il y a quelques années, je m'étais penché sur l'histoire du nom de la ville de Gérardmer. Mon approche consistait à utiliser l'historiographie et confonter les interprétations pour conclure que si l'appellation en *mé* relevait des différentes transformations linguistiques locales, le patronyme *Gérard* ne pouvait provenir avec certitude du Duc Gérard d'Alsace comme le veut pourtant le folklore local. À la coutume j'opposais le manque de fouilles archéologiques et surtout l'absence de précautions méthodologiques de la part des auteurs&nbsp;; pour preuve je mentionnais la source de la confusion, à savoir le récit de Dom Ruinart, moine bénédictin rémois, décrivant sa journée du 2 octobre 1696&hellip; Dans cet article, je commenterai le récit de Dom Ruinart pour mieux mesurer sa place dans l'historiographie gérômoise. Pour cela, il me faudra auparavant exhumer les controverses à propos de Gérard d'Alsace et sa place dans l'histoire de Gérardmer.

**Màj. 30/08/2022 :** ce billet a fait l'objet d'un remaniement sous forme d'article [publié en ligne](https://www.philomatique-vosgienne.org/uploads/Publications%20en%20ligne/Masutti-nom-de-gerardmer.pdf) désormais sur le site de la Société Philomatique Vosgienne.

## L'hypothèse d'une tour

La solution m'a été soufflée par un gérômois fort connu, M. Pascal Claude, alors que je travaillais sur la réédition du livre de Louis Géhin *[Gérardmer à travers les âges](https://framadrive.org/s/9D2XmPpZDFPS7EE)*. M. Claude[^pclaude] avait trouvé un extrait des oeuvres de Dom Ruinart qui, si on le lit trop précipitamment, mentionne à un château là où la Jamagne (Ruisseau venant de Gérardmer) se jette dans la Vologne (nous verrons plus loin qu'il n'en est rien). M. Claude suggérait alors que la «&nbsp;Tour Gérard d'Alsace&nbsp;» dont fait mention la tradition ne pouvait justement pas être située à Gérardmer mais du côté d'Arches. 

Cette hypothèse de travail est la bonne car elle pose directement les conditions de l'existence ou non d'une «&nbsp;Tour Gérard d'Alsace&nbsp;» à Gérardmer. Cette tour supposée est depuis longtemps dans la culture populaire  une partie fondamentale de l'explication du patronyme Gérard dans le nom de la ville.

Une partie seulement, puisque dans un mouvement quelque peu circulaire du raisonnement, la signification du suffixe en *mé*  serait une seconde clé&nbsp;: le *mé* ou *mansus* en latin. Cette question est importante. Alors que *mer* doit son étymologie à *mare* désignant l'étendue d'eau que l'on retrouve dans le nom de Longemer ou Retournemer (qui se prononcent *mère* même si le patois les prononce indifférement *mé* ou *mô*), le *mé* peut avoir une signification romane qui renvoie à la propriété, la tenure&nbsp;: *mansus* en latin tardif, *mas* en langue d’oc, *meix* en langue d’oil, et *moué*, *mé* ou *mô* en patois du pays vosgien. Or, si l'inteprétation est attribuée au sens latin, c'est-à-dire en référence à une propriété, la démarche  consiste à rechercher les traces d'un édifice qui puisse l'attester physiquement à défaut d'une trace écrite. 

En somme, s'il y a une «&nbsp;tour Gérard d'Alsace&nbsp;» ce serait parce qu'il y a une «&nbsp;propriété&nbsp;» d'un certain Gérard. Et comme on s'y attend, le duc Gérard d’Alsace (1030-1070) qui, comme son nom ne l’indique pas, était Duc de Lorraine[^maisonalsace] devrait donc être ce fameux Gérard, heureux détenteur d'un *mansus* à Gérardmer. De qui d'autre pouvait-il s'agir&nbsp;? Par cette attribution qui se justifie elle-même, la tradition locale affirmait ainsi avec force le rattachement Lorrain de la ville depuis une époque fort ancienne. 

Et cela, même si le premier écrit connu qui atteste officiellement l'appartenance de Gérardmer au Duché de Lorraine date de 1285, bien longtemps après l'époque de Gérard d'Alsace. Comme l'écrit Louis Géhin, il s'agit d'«&nbsp;un acte de Mai 1285, par lequel le duc Ferry III concéda à Conrad Wernher, sire de Hadstatt, à son fils et à leurs héritiers, en fief et augmentation de fiefs, que le dit Hadstatt tenait déjà de lui, la moitié de la ville de La Bresse, qu’il les a associés dans les lieux appelés *Géramer*et *Longemer* en telle manière que lui et eux doivent faire une *ville neuve* dans ces lieux, où ils auront chacun moitié.&nbsp;»[^precision]

Il reste que l'existence d'une «&nbsp;Tour Gérard d'Alsace&nbsp;» à Gérardmer a toujours été imputée par les différents auteurs à une tradition, une légende issue de la culture populaire&hellip; sauf dans l'étude exhaustive la plus récente, celle de Marc Georgel, parue en 1958. Comparons-les.

Henri Lepage en 1877, dans sa «&nbsp;Notice Historique et Descriptive de Gérardmer&nbsp;»[^sourcelepage] écrit ceci&nbsp;:

> La tradition veut également que Gérard d’Alsace, [&hellip;] ait [&hellip;] fait de Gérardmer un rendez-vous pour la chasse et la pêche&nbsp;; elle ajoute qu’il aurait fait édifier une tour (3), près du ruisseau de la Jamagne, pour perpétuer le souvenir de son séjour dans ces lieux déserts&nbsp;; le lac d’où sort cette rivière ce serait dès lors appelé Gerardi mare, mer de Gérard, et par inversion Gérard-mer.
> 
> (En note de bas de page -- 3&nbsp;:  Cette tour s’élevait, dit-on, sur une petite éminence, au milieu de la prairie du Champ, à l’endroit où se voit aujourd’hui l’église du Calvaire, et on en aurait retrouvé les fondations.

On notera les précautions qu'emploie H. Lepage&nbsp;: «&nbsp;la tradition veut&nbsp;», «&nbsp;dit-on&nbsp;», et l'usage du conditionnel. 

Dans la même veine, un peu plus tard en 1893, Louis Géhin mentionne la construction de la première église de Gérardmer en 1540, et la situe sur l'emplacement «&nbsp;prétendu&nbsp;» de cette Tour&nbsp;:

> Dès l’année 1540, les habitants de Gérardmer élevèrent, sur le bord de la Jamagne, non loin de l’emplacement prétendu de la Tour de Gérard d’Alsace, une chapelle dédiée à saint Gérard et saint Barthélemy[^pretendu].


Et pourtant, en 1958, Marc Georgel ne prend plus aucune précaution&nbsp;! Dans une somme impressionnante sur la *La vie rurale et le folklore dans le canton de Gérardmer*  il écrit de manière péremptoire&nbsp;:

> Chacun sait maintenant (depuis les nombreux ouvrages qui traitent de Gérardmer) que le duc Gérard se fit construire une tour près du «&nbsp;Champ&nbsp;», lieu-dit actuellement «&nbsp;Le Calvaire&nbsp;», sur la rive droite de la Jamagne, à quelques centaines de mètres du lac qui s'appelait encore au XVIe siècle «&nbsp;le lac Major&nbsp;» (une preuve de plus que le nom de la ville de Gérardmer doit son origine à la «&nbsp;tour&nbsp;» de Gérard et non pas au lac[^memare]). Quelle était la destination de cette construction de Gérard&nbsp;? Tour de guet pour assurer plus facilement la garde de la petite agglomération de Champ&nbsp;? Villa saisonnière&nbsp;? La plupart des auteurs émettent l'hypothèse d'une sorte de pavillon de chasse.

Renouant ainsi avec la culture populaire, M. Georgel sacrifie la rigueur méthodologique à l'imagination des contes et légendes des Vosges. L'appellation du Lac en «&nbsp;lac Major&nbsp;» (dont il ne cite pas la source) ne prouve rien en soi. Quant aux hypothèses qu'il soulève à propos de la destination d'une telle construction sont quelque peu sujettes à caution&nbsp;:

- une tour de garde suppose&hellip; des gardes, donc des salaires et une économie locale  suffisante, ce qui, au XI<sup>e</sup> siècle, est fortement improbable en ces vallées,
- un pavillon de chasse est plausible si l'on part du principe qu'effectivement depuis Charlemagne la noblesse allait chasser dans ces vallées&hellip; sauf que dans ce cas, il s'agirait d'un campement établi à la hâte, peut-être pour y revenir d'une saison à l'autre, soumis au gré du climat, sans fondations&hellip; sans traces tangibles, donc.
- quant à une villégiature&hellip; Marc Georgel a sans doute tendance à calquer la dynamique touristique de Gérardmer florissante depuis le XIX<sup>e</sup> siècle en la rendant parfaitement anachronique.

Pour conclure cette première partie, la cause doit être entendue&nbsp;: nous devons comprendre les origines de cette histoire de «&nbsp;Tour Gérard d'Alsace&nbsp;». Entendons-nous bien&nbsp;: ce que nous allons démontrer n'est pas l'origine du folklore local à ce propos, qui peut remonter à une époque très lointaine, mais l'origine de la controverse qui permet de comprendre pour quelle raison il y a effectivement un débat à ce propos. Répondant à cette question, nous pourront montrer «&nbsp;d'où vient l'erreur&nbsp;».


[^maisonalsace]: De la maison d’Alsace, alors que la Haute Lorraine est inféodée au Saint Empire Germanique.

[^precision]: Le nom de Gérardmer est attesté par écrit pour la première fois en 1285 dans cet acte d'attribution de fief par le Duc de Lorraine Ferry III, soit 245 ans après que Gérard d’Alsace ai reçu le titre de Duc de Lorraine. Si l'on se réfère à l'acte de Ferry III, retranscrit *in extenso* par Louis Géhin, c'est bien une *Ville Neuve* que Ferry III fonde&nbsp;: non que que le hameau n'existât point encore à cette époque mais la réalité administrative est alors officielle et en aucun cas l'acte mentionne l'existence d'un édifice ducal préexistant. Par ailleurs, dans cet acte de Ferry III, c'est la forme *Geramer* qui est employée, sans le *r*&nbsp;: l'interprétation de ce fait peut varier, la première consiste à accuser la faute du copiste, la seconde consiste à se demander si le patronyme Gérard n'était pas une information négligeable à cette époque au point que même dans un acte officiel de l'autorité ducale on puisse en oublier cette référence en écrivant indistinctement *Geramer* à la place de *Gerarmer*. Le hameau n'est pas nouvellement habité, il est déjà ancien, parfaitement identifié par les parties, et se distingue bien de Longemer. Ceci aura son importance dans la suite de notre propos. Voir Louis Géhin, *[Gérardmer à travers les âges](https://framadrive.org/s/9D2XmPpZDFPS7EE)*, 1877.



[^pclaude]: Voir Pascal Claude, «&nbsp;Le mystère de la tour Gérard d’Alsace&nbsp;», dans Daniel Voinson, *La chapelle du Calvaire*, Gérardmer, 2013, p. 11-13.

[^pretendu]: Louis Géhin, *[Gérardmer à travers les âges](https://framadrive.org/s/9D2XmPpZDFPS7EE). Histoire complète de Gérardmer depuis ses origines jusqu’au commencement du XIX^e siècle*, Extrait du *Bulletin de la société philomatique vosgienne*, Saint Dié, Impr. Hubert. 1893.


[^sourcelepage]: Henri Lepage «&nbsp;Notice Historique et Descriptive de Gérardmer&nbsp;», dans *Annales de la société d’émulation du département des Vosges*, 1877, pp.130-232.

[^memare]: Nous préciserons plus loin&nbsp;: le suffixe en *mé* peut aussi bien provenir du latin *mansus* que de *mare*. La pronconciation du suffixe, à la différence de Longe*mère* ou Retourne*mère* force à retenir la première solution&hellip; sauf que le patois prononce indifféremment *mer*&nbsp;: *meix*, *moix*, *mé*, *mère*. D'autres exemples sont troublants&nbsp;: selon P. Marichal, on trouve à partir de 1285 plusieurs orthographes pour Gérardmer tels Geramer, Gerameix, ou Geroltsee, Giraulmoix&hellip; Alors&nbsp;: lac ou maison&nbsp;? la question n'est pas tranchée. Voir Paul Marichal, *Dictionnaire topographique du département des Vosges, comprenant les noms de lieux anciens et modernes*, Paris&nbsp;: Imprimerie nationale, 1941.




## Histoires croisées de Gérardmer et Longemer&nbsp;: un patronyme indécidable

Durant de nombreux siècles, la vallée des lacs de Gérardmer et Xonrupt-Longemer était habitée de manière sporadique, avec des populations provenant tantôt des frontières germaniques et tantôt des autorités administratives lorraines, qu'il s'agisse de l'autorité ducale ou de l'autorité des abbesses de Remiremont. Ce double patronat est attesté en 970 pour ces «&nbsp;bans de la montagne&nbsp;» dont la zone de Gérardmer faisait partie. L'essentiel de l'économie locale étant composée de foresterie, d'élevage et de produit d'élevage marcaire (sur les chaumes), et un peu de pisciculture. 

Les routes qui relient Gérardmer aux centres économiques lorrains sont praticables assez tôt. Les voies principales sont connues&nbsp;: Gérardmer-Bruyères en suivant le cours de la Vologne, Gérardmer-Remiremont en passant par le Col de Sapois, Gérardmer-Saint-Dié via le Col de Martimpré. Evidemment, pour rejoindre l'Alsace, Gérardmer ne figurait pas parmi les étapes des voyageurs lorrains, ceux-ci préférant passer par le col de Bussang, la vallée de Senones / territoire de Salm (col du Hantz) ou plus loin le col de Saverne. En somme, la vallée de Gérardmer était plus une destination qu'une étape.
 
Comme nous l'avons vu dans la première partie, si nous nous interrogeons sur l'origine du nom de Gérardmer, il faut pour en comprendre l'importance situer cette question dans le folklore local.

Les autorités lorraines étant lointaines, il reste que la culture locale attribue certains lieux-dits aux grands personnages qui ont fréquenté les contreforts vosgiens&nbsp;:  Charlemagne et ses parties de chasse ou plus tard les ducs de Lorraine constructeurs de châteaux. Ces coutumes sont importantes à la fois parce qu'elles attestent  de l'appartenance culturelle et juridique des lieux mais aussi pour des raisons spirituelles. Ainsi le moine Richer de Senones, au XIII<sup>e</sup> siècle dans sa chronique  mentionne la fondation d'une chapelle à Longemer en 1056 par Bilon, un serviteur de l'illustre Gérard d'Alsace[^chroricher]&nbsp;:

> Anno Domini m<sup>o</sup> lvi<sup>o</sup> quidam Bilonus, Gerardi ducis servus, in saltu Vosagi qui Longum mare dicitur, locuns et capellam in honore beati Bartholomei privus edificavit.

> L’an du Seigneur 1056, un certain personnage du nom de Bilon, serviteur du duc Gérard, construisit une chapelle en l’honneur de saint Barthélémy, dans une forêt de la Vosges, qu’on appelle Longe-mer (trad. L. Géhin).

[^chroricher]: Voir la [Chronique de Senones, par Richer -- BNF Gallica](https://gallica.bnf.fr/ark:/12148/btv1b10035704r/f34.item)


![Extrait de la Chronique de Richer](/images/Chronique_de_Senones_par_Richer__bilon.png)

<!-- ![Extrait de la Chronique de Richer](/images/Chronique_de_Senones_par_Richer__bilon.png) -->

Si l'édification de la chapelle en question était surtout un ermitage (comme il y en aura plus d'un dans la vallée) la confusion entre les lieux (Longemer et Gérardmer[^confulongmer]) a très certainement joué en faveur du double patronage de Saint Barthélémy et Saint Gérard, qui fut longtemps l'attribut de la nouvelle église du hameau de Gérardmer au XVI<sup>e</sup> siècle.

[^confulongmer]: En 1707, dans son *Histoire Ecclesiastique de Toul* le Père Benoit mentionne que la Vologne prend sa source au lac de Gérardmer. Il s'agit en fait du lac de Longemer (et en réalité au Haut-Chitelet)&nbsp;: l'erreur sur place n'est pas possible étant donné que l'affluent venant du lac de Gérardmer, la Jamagne, a un débit bien moindre. D'ailleurs en 1696, Dom Ruinart la mentionne sans la nommer comme nous le verrons plus loin. Voir Père Benoit de Toul, *Histoire ecclesiastique et politique de la Ville et du Diocèse de Toul*, Toul, A. Laurent Imprimeur, 1707, p. 54 ([URL Archive.org](https://archive.org/details/bub_gb_OIS6ROavsYkC)).

Cette question du patronage de l'église a toute son importance. Elle croise les histoires communes de Longemer et de Gérardmer. Cette approche doit être privilégiée pour comprendre les liens entre Gérardmer et son patronyme, car elle est l'objet d'une controverse célèbre.

En 1878, M. Arthur Benoît, correspondant de la Société d'émulation des Vosges,  reprend les écrits du P. Hugo d'Étival et ceux du Père Benoît Picart, Capucin de Toul (ou Benoît de Toul). Au tout début du XVIII<sup>e</sup> siècle, ces deux personnages hauts en couleurs étaient entrés dans une course politique dont le Duc Léopold de Lorraine devait en être l'arbitre. Le duel s'était cristallisé autour de l'histoire de la Maison de Lorraine que le P. Benoît Picart avait étudié et et dont il avait tiré un ouvrage (*L'origine de la très illustre Maison de Lorraine*) qui déplu finalement au Duc Leopold. Pour plaire à ce dernier le Père Hugo d'Etival eu la prétention d'écrire, sous un pseudonyme et une fausse maison d'édition, un traité sur la généalogie de la Maison de Lorraine. Répondant à cette supercherie, le Père Benoît Picard publia aussitôt deux tomes critiques du livre du P. Hugo, sous le titre de *Supplément à l'histoire de la maison de Lorraine*[^largeetpol]. 

Dans cette dispute, la question de l'attribution du patronyme au nom de Gérardmer ne fut pas épargnée et c'est justement à partir de l'histoire de Bilon à Longemer que l'on pose les prémisses du raisonnement.

En 1711, le père Hugo abbé d'Etival, mentionnant Bilon à l'image de Richer de Senones, suggère que c'est en l'honneur du Duc Gérard d'Alsace que Gérardmer porterait ce nom[^sourceAbenoit]&nbsp;:

> C'est apparemment du Duc Gérard que le village de Gerardmer à présent Geromé en Vosges, a emprunté son nom. Herculanus[^herculanus] dit que, dans ce lieu se retira vers l'an 1065, Bilon officier de la cour de Gerard Duc de Lorraine et qu'il dressa une chapelle en l'honneur de S. Barthelemy, sur les bords du lac, appelé alors Longue-mer et qui est la source de la rivière de Vologne. Ce courtisant pénitent, ou les peuples d'alentour, auraient-ils changé le nom de ce lac, pour éterniser la mémoire du Duc&nbsp;?

{{< figure src="/images/charles-louis-hugo-extrait.png" title="Extrait du Traité de Charles-Louis Hugo d'Étival" >}}

<!-- ![Extrait du Traité de Charles-Louis Hugo d'Étival](/images/charles-louis-hugo-extrait.png) -->


[^sourceAbenoit]: Charles-Louis Hugo, *Traité  historique et critique sur l'origine et la généalogie de la maison de Lorraine avec les chartes servant de preuves*, Berlin, Ulric Liebpert impr., 1711. Voir Arthur Benoit «&nbsp;Les origines de Gérardmer, d'après le P. Benoît Picart de Toul&nbsp;», Annales de la Société d'Émulation du Département des Vosges, Épinal, Collot, 1878, p. 249-252. [URL -- Gallica BNF](https://gallica.bnf.fr/ark:/12148/bpt6k333662/f249.item) (p. 250). 

[^herculanus]: Il s'agit de Jean Herquel (Herculanus) chanoine de Saint-Dié, mort en 1572.

[^largeetpol]: Sur ce sujet, voir Albert Denis, «&nbsp;Le R. P. Benoît Picart. Historien de Toul (1663-1720)&nbsp;», Bulletin de la Société Lorraine des Études Locales dans l'enseignement public, vol. 2, num. 5, 1930, p. 10-11. [URL](https://gallica.bnf.fr/ark:/12148/bpt6k6107650k/f12.item.r=benoit%20picart)

Et en 1712, le P. Benoît de Toul corrige le P. Hugo et écrit[^sourcepicart]&nbsp;:

> J'ai cru autrefois que le village de Gérardmer empruntait son nom au Duc Gérard, mais après plusieurs recherches que j'ai fait, pour l'éclaircissement de l'histoire de Toul et de Metz à laquelle je m'applique actuellement, je dis à présent que le Duc Gérard, suivi de Bilon, l'un de ses officiers, assista à la translation de l'évêque Saint Gérard faite à Toul le 22 octobre 1051. Cet officier touché de la sainteté de nos cérémonies et des miracles que le Bon Dieu fit paraître sur le tombeau de ce saint, et qui ont été écrits par un auteur contemporain, se retira dans les Vosges et fit bâtir une chapelle en l'honneur de Saint Gérard et de Saint Barthélémy, laquelle, à cause des biens qu'il y annexa, fut érigée en bénéfice dans l'église paroissiale&nbsp;; dont ces deux saints devinrent les patrons et donnèrent lieu d'appeler les habitations proches du lac&nbsp;: Gerardme, sancti gerardi mare.


[^sourcepicart]: P. Benoît Picart, *Supplément à l'Histoire de la Maison de Lorraine, avec des remarques sur le Traité historique et critique de l'origine et la généalogie de cette illustre maison*, Toul, Rollin, 1712, p. 46.

On saluera la tentative du P. Benoît de fournir à l'appui de son propos deux «&nbsp;preuves&nbsp;», à l'image de la rigueur habituelle qui le caractérisait (d'après ses commentateurs) mais aussi sans doute motivé par le fait de pouvoir à peu de frais contredire le P. Hugo. Néanmoins, si ces documents sont deux titres attestés des chanoinesses de Remiremont datant de 1449 et 1455, leur portée est très faible. Pour reprendre le commentaire qu'en fait M. Arthur Benoît (qui reproduit les textes en question dans son article), le premier prouve seulement qu'une chapelle existait à Longemer et dédié au deux saints Gérard et Barthélémy, et pour le second la chapelle ne porte plus que le patronage de Barthélémy.

À l'image de cette controverse, la recherche de l'attribution du patronyme a eu une postérité plutôt riche. L'essentiel des études  s'accordent au moins sur un point&nbsp;: il ne s'agit que d'avis et d'opinions qui n'ont jamais été solidement étayés par des écrits tangibles. Les historiens du XVIII<sup>e</sup> siècle avaient donc cette lourde charge de rechercher les titres, chartes et patentes qui auraient pu, une fois pour toute, résoudre cette question&hellip; en vain. 

Les cartographes eux mêmes s'y perdaient depuis longtemps. Par exemple, Thierry Alix, président de la Chambre des Comptes du Duché de Lorraine, fait élaborer la carte des Hautes Chaumes entre 1575 et 1578. On y trace parfaitement les trois lacs de Gérardmer, Longemer et Retournemer, mais on attribue au village au bord du premier le nom de Saint Barthélémy (c'est le patronage attesté administrativement et non le nom vernaculaire qui l'a emporté)[^sourcealix].

{{< figure src="/images/carteAlix2.jpg" title="Cartes des Hautes Chaumes, par T. Alix" >}}

<!-- ![Cartes des Hautes Chaumes, par T. Alix](/images/carteAlix2.jpg) -->

[^sourcealix]: Voir la carte sur le site des [Archives de Meurthe et Moselle](https://archives.meurthe-et-moselle.fr/d%C3%A9couvrir-nos-richesses/tr%C3%A9sor-darchives/les-hautes-chaumes-des-vosges-carte-par-thierry-alix-1576).

## Pourquoi une tour Gérard d'Alsace à Gérardmer&nbsp;?

Les archives des Vosges furent fouillées à maintes reprises à la recherche de tout indice permettant d'attribuer à Gérardmer le patronyme de Gérard d'Alsace. Les auteurs régionaux avaient à leur disposition tout l'héritage des abbayes, à commencer par la chronique de [Richer](https://fr.wikipedia.org/wiki/Richer_le_Lorrain) de (l'abbaye de) Senones (mort en 1266), l'histoire de Jean Herquel (Herculanus) chanoine de Saint-Dié (mort en 1572), les écrits de [Jean Ruyr]((https://fr.wikipedia.org/wiki/Jean_Ruyr)) chanoine de Saint-Dié (1560-1645), les nombreux textes  de [Augustin (Dom) Calmet](https://fr.wikipedia.org/wiki/Augustin_Calmet) moine de Senones (1672-1757), les histoires de [P. Benoît Picart de Toul](https://fr.wikipedia.org/wiki/Beno%C3%AEt_Picart) (1663-1720), les écrits de [Charles-Louis Hugo d'Étival](https://www.persee.fr/doc/bcrh_0770-6707_1898_num_67_8_2250) (1667-1739), et la liste est longue.

En fin de compte, autant l'histoire de Bilon à Longemer trouve ses origines dans des textes du clergé forts anciens et fait l'objet de débats au détour desquels on s'interroge effectivement sur le patronyme de Gérardmer[^archeobilon], autant nous ne trouvons aucune mention claire du prétendu château de Gérard d'Alsace à Gérardmer. 

Pour comprendre comment on en vint à supposer l'existence d'un tel édifice, il faut attendre le XIX<sup>e</sup> siècle et l'étude d'un médecin amateur d'histoire, pionner du genre qui occupera longuement la bourgeoisie locale férue d'histoire régionale. Il s'agit du docteur Jean-Baptiste Jacquot qui publia à Strasbourg sa thèse de médecine en 1826, assortie d'une notice historique sur Gérardmer[^sourcejacquot]. C'est dans cette notice que l'on trouve pour la première fois dans la littérature régionale la mention d'un château ducal à Gérardmer.

Jean-Baptiste Jacquot avait déniché aux archives un texte du moine bénédictin champenois Dom (Thierry) Ruinart, au titre d'un récit de voyage à la toute fin du XVII<sup>e</sup> siècle, durant lequel il était de passage dans les Vosges&nbsp;: le *Voyage d'Alsace et de Lorraine* effectué en 1696 et publié à titre posthume en 1724[^sourceruinart].

Notons toutefois&nbsp;: cette chronique de Dom Ruinart est fort connue depuis longue date des alsaciens, et sa première  traduction en français fut publiée en 1829 à Strasbourg aussi[^matter]. C'est sans doute la raison pour laquelle J.-B. Jacquot s'est attardé sur ce document, facilement identifiable. 


En lisant le texte en latin, J.-B. Jacquot, trouve un passage édifiant et selon lui de nature à éclairer la question du nom de Gérardmer. Dom Ruinart aurait mentionné le «&nbsp;vieux château (*castellum*) des ducs de Lorraine&nbsp;» rencontré au moment de franchir la Vologne «&nbsp;qui, réunie au ruisseau qui coule du lac de Gérardmer… Au sommet de la montagne qui domine cette rivière…&nbsp;». 

La traduction est incomplète mais pour J.-B. Jacquot, cela ne fait aucun doute&nbsp;: il s’agit de cette propriété du duc Gérard d’Alsace que Dom Ruinart, de passage à Gérardmer, aurait aperçu et mentionnée dans son compte-rendu. Un tel château serait situé dans les environs où le ruisseau de Gérardmer rencontre la Vologne, c'est-à-dire&hellip; à Gérardmer même. Le temps en aurait simplement effacé les traces.

Trop rapide, trop hâtif&nbsp;? les lecteurs qui le suivront sur ce point ne reviendront finalement jamais au texte source de Dom Ruinart. Si bien qu'on a longtemps tenue pour acquise l'affirmation de J.-B. Jacquot (sauf dans les publications des différentes sociétés intellectuelles Lorraines et Vosgiennes qui mentionnent toujours la tradition). Il nous faut donc aller voir le texte de Dom Ruinart *en entier*. 


[^archeobilon]: Voir Louis-Antoine-Nicolas Richard, dit Richard des Vosges, «&nbsp;Notice sur un squelette retrouvé&hellip;&nbsp;», reproduite dans le *Bulletin de la société philomatique vosgienne*, vol. 21, 1895-96, p. 53 *sq*.

[^sourcejacquot]: Jean-Baptiste Jacquot, *Essai de topographie physique et médicale du canton de Gérardmer. Précédé d'une notice historique*, (dissertation à la faculté  de  médecine  de  Strasbourg, pour le grade de docteur en médecine, Strasbourg, impr. Levrault, 1826.

[^sourceruinart]: Voir Ouvrages posthumes de D. Jean Mabillon et de D. Thierri Ruinart, tome III. Cocnernant la vie d'Urbain II, les Preuves et le Voyage d'Alsace et de Lorraine, par D. T. Ruinart, Paris, Vincent Thuillier éditeur, 1724, [URL Gallica](https://gallica.bnf.fr/ark:/12148/bpt6k108508z/f1.item)

[^matter]: Il ne s'agissait alors que d'une partie du récit de voyage, celle concernant l'Alsace. Jacques Matter (trad.), *Voyage littéraire en Alsace par Dom Ruinart*, Strasbourg, Levrault, 1829.



## Dom Ruinart, aventurier mal compris

On possède de Dom Ruinart plusieurs écrits consultables sur le site Gallica de la BNF. Le plus célèbre d'entre eux pour les études régionales reste le *Voyage en Alsace* traduit du latin par Jaques Matter en 1829 mais qui ne contient qu'une partie seulement du récit car il arrête la traduction au moment du retour en Lorraine au col de Bussang. Le*Voyage d'Alsace et de Lorraine* complet, lui, est paru en 1724 dans le recueil des *Oeuvres posthumes de Dom Jean Mabillon et Dom Thierry Ruinart*, Tome 3. Louis Jouve en a proposé une traduction exhaustive et plus moderne en 1881[^jouve].

[^jouve]: Louis Jouve, *Voyages anciens et modernes dans les Vosges, 1500-1870*, Epinal, Durand et fils, 1881 [URL Gallica](https://gallica.bnf.fr/ark:/12148/bpt6k102398s/f2.item.r=ruinart%20lorraine). 


Dans la pure tradition du voyage d'étude qui fit le rayonnement des intellectuels européens à travers toute l'époque médiévale et bien au-delà, Dom Ruinart se lance lui aussi en 1696 dans un périple qui le mène de Paris jusqu'en Lorraine en passant par sa région champenoise natale, avec une itinérance importante en Alsace. Il fait halte d'un monastère à l'autre et lors de ses séjours, il parcours les environs visitant divers établissements, églises, chapelles et autres édifices d'intérêt. À défaut, il les cite et tâche d'en établir l'historique. Pour une compréhension contemporaine nous pouvons mentionner les grandes étapes&nbsp;: Paris - Lagny - Meaux - Reuil - Orbais l'Abbaye - Sainte-Menehould - Verdun - Toul - Nancy - Lunéville - Baccarat - Moyenmoutier - Senones - (Nieder-)Haslach - Molsheim - Marmoutier - Marlenheim -  Wangen - Saverne - Strasbourg - Illkirch - Sélestat - Colmar - Munster - Soultzbach - Murbach - Guebwiller - Bussang - Remiremont - Champ-le-Duc - Bruyères - Moyenmoutier - Baccarat - Nancy - Pont-à-Mousson - Metz - Toul - Commercy - Verdun - Châlons - Reims - Lagny - Paris.

La lecture de ce récit est passionnante tant il recèle de nombreuses informations sur l'art, les usages monastiques et les connaissances en cette fin du XVII<sup>e</sup> siècle. Il recèle aussi de haut faits. On notera en particulier le passage dangereux des crêtes vosgiennes entre Moyenmoutier et Haslach. Sur le retour en Lorraine, la journée du 2 octobre 1696 qui nous intéresse ici n'est pas aussi spectaculaire même si nous pouvons saluer l'endurance certaine des voyageurs qui entreprennent ce jour-là un périple d'environ 62 kilomètres à cheval.

Venant d'Alsace, via Guebwiller, après avoir franchi le Col de Bussang et séjourné quelques jours chez les chanoinesses de Remiremont, Dom Ruinart entreprend un trajet jusque Moyenmoutier. C'est dans l'extrait qui va suivre que J.-B. Jacquot a cru voir mentionné l'existence d'un château du Duc de Lorraine à Gérardmer. Or, il n'en est rien. Pour comprendre son erreur, il nous faut lire le texte et compléter par quelques informations géographiques et historiques. Toute l'interprétation réside dans la possibilité de retracer exactement le parcours sur la base du récit[^ruinart]&nbsp;:

{{< figure src="/images/extrait-ruinart1696.png" title="Extrait du récit de Dom Ruinart en 1696" >}}

<!-- ![Extrait du récit de Dom Ruinart en 1696](/images/extrait-ruinart1696.png) -->

> Le 2 octobre, nous traversâmes la Vologne, qui, réunie au ruisseau sorti du lac de Gérardmer, nourrit de petites huîtres renfermant des perles. Sur le sommet de la montagne qui domine la rivière, se dresse le vieux château qu'habitaient les ducs de Lorraine, quand ils faisaient pêcher des perles. De là nous allâmes à Champ, remarquable par son ancienne Église, dont on attribue la construction à Charlemagne, et après avoir traversé Bruyères, nous entrâmes dans les forêts. Nous franchîmes la montagne au bas de laquelle Renaud, évêque de Toul, fut assassiné avec une cruauté inouïe par Maherus, prévôt de Saint-Dié, qui avait été chassé du siège épiscopal de Toul. Le soleil venait de se coucher quand nous arrivâmes à Moyenmoutier, laissant à droite la ville de Saint-Dié et à gauche l'abbaye de l'ordre des prémontrés, que nous visitâmes le landemain[^oubli].

[^ruinart]: Le texte original de Dom Ruinart dans le recueil des oeuvres posthumes se trouve sur le site [Gallica de la BNF](https://gallica.bnf.fr/ark:/12148/bpt6k108508z/f486.item) (le lien ci-contre renvoie à la page du passage dont il est question). Pour la traduction, voir Louis Jouve, *op. cit.*.


[^oubli]: Louis Jouve oublie de traduire&nbsp;: l'église du monastère est merveilleusement décorée.



À l'énoncé des lieux par Dom Ruinart, et sans avoir une idée précise de la géographie vosgienne, le fait de mentionner un ruisseau affluent de la Vologne et venant de Gérardmer peut induire en erreur et situer l'action (là où Dom Ruinart fanchi la Vologne) au Nord-ouest de Gérardmer, dans la vallée de Kichompré, à l'endroit où la Jamagne venant du lac de Gérardmer se jette dans la Vologne provenant, elle, du lac de Longemer.

Cependant, si nous nous en tenions à ce seul énoncé (et le texte renferme bien d'autres informations), il serait bien étonnant depuis ce lieu d'y voir une montagne dominante plus que les autres où serait situé un château ou même des ruines. L'encaissement des lieux ne permet pas d'identifier un sommet plus qu'un autre et, on en conviendra, le lieu lui-même est déjà fort éloigné du bourg de Gérardmer, qui plus est du centre où la chapelle Saint-Barthélémy est censée recouvrir les ruines de la soit-disant tour de Gérard d'Alsace&hellip; qui ne serait donc pas située sur une montagne, contrairement à ce que dit le texte, donc&hellip; on en perd son latin[^facile].

[^facile]: Oui, celle-là, elle était facile.

Par ailleurs, en supposant que le trajet de Remiremont à Champ(-Le-Duc) et Bruyères passe par Gérardmer, il faut comprendre que Dom Ruinart préfère de loin les chemins les plus rapides, autrement dit, aménagés ou les plus empruntés (la leçon subie du côté de la vallée de la Bruche en Alsace lui aura appris cela). Donc le trajet depuis Remiremont devrait nécessairement passer par le Col de Sapois, qui est la route principale (on ne remonte pas à l'époque la Vallée de Cleurie même s'il devait bien y avoir quelques sentiers jusqu'au Tholy pour rejoindre le chemin de Gérardmer provenant d'Arches).

Décidément, Dom Ruinart n'était pas homme à franchir les montagnes sur des chemins difficiles alors que l'objectif du voyage est de rejoindre Moyenmoutier en se contentant, la majeure partie du trajet, de suivre les fonds de vallées. Quant à visiter Gérardmer&hellip; nous sommes en 1696 et l'attrait touristique des lieux n'était pas aussi irrésistible qu'aujourd'hui.

L'hypothèse du trajet via Gérardmer doit définitivement être abandonnée à l'énoncé des autres indices.

Le premier&nbsp;: les huîtres perlières de la Vologne. Si Dom Ruinart mentionne Gérardmer c'est par érudition afin de préciser que la Vologne est une rivière de montagne bien particulière&nbsp;: ses affluents lui apportent divers éléments enrichissants qui permettent la culture de molusques, de grandes moules perlières[^protecmoule]. Cette particularité zoologique se retrouve dans d'autres vallées mais les bords de la Vologne avaient généré une activité économique suffisante pour que les Ducs de Lorraine y trouvent l'intérêt d'y établir un château servant de comptoir dédié à cette activité[^moules]. On s'accorde pour délimiter la zone où l'on rencontre le plus souvent ces molusques entre la zone d'affluence du Neuné près de Laveline-devant-Bruyères et le village de Jarménil, là où la Vologne se jette dans la Moselle. 

Quant au château, il s'agit de Château-Sur-Perles situé entre Docelles et Cheniménil. La fondation du château par le duché de Lorraine est attestée. En effet, l'activité perlière dans cette région était clairement sous la responsabilité (et le profit) du duché de Lorraine ainsi qu'en témoignent les livres de comptes jusqu'à une époque tardive. Les Archives de Meurthe-et-Moselle tiennent le registre des lettres patentes de René II, duc de Lorraine (1473-1508). Elles recensent à Cheniménil l'autorisation d'y construire un château en 1474[^patentes].

Ceci nous permet d'affirmer que Dom Ruinart et ses compagnons franchissent la Vologne juste avant Arches en venant de Remiremont, à l'emplacement de l'actuel village de Jarménil, avant de remonter la rivière où ils aperçoivent très peu de temps après à Cheniménil le Château des ducs de Lorraine fondé par René II.

[^protecmoule]: On notera que récemment, en 2018, la société d'histoire naturelle et d'ethnographie de Colmar a alerté les autorités à propos de la protection des moules perlières de la Vologne et du massif des Vosges en général. Il ne resterait que deux espèces en voie d'extinction.


[^patentes]: Voir le registre par nom de lieux à [cette adresse](http://archivesenligne.archives.cg54.fr/s/2/lettres-patentes-de-rene-ii/?), rechercher «&nbsp;Cheniménil&nbsp;».


[^moules]: Chabrol (Marie), «Les perles de la Vologne, trésor des ducs de Lorraine», *Le Pays lorrain*, Vol. 94, num. 2, 2013, pp. 115-122. Pour une étude plus ancienne et néanmoins exhaustive, voir D. A. Godron, «&nbsp;Les Perles de la Vologne et le Château-sur-Perle&nbsp;», Mémoires de l'Académie de Stanislas, 1869-1870, p. 10-30. [URL Gallica](https://gallica.bnf.fr/ark:/12148/bpt6k33584j/f77.item.r=perles).

Le second indice concerne le village de Champ, aujourd'hui nommé Champ-Le-Duc[^champduc] et le passage par Bruyères. Cette dernière ville figure à l'époque parmi les places de marché les plus actives. C'est par Bruyères que convergent de nombreux chemins, à cheval entre différentes prévôtés (Arches et Saint-Dié surtout). Toujours est-il qu'après avoir aperçu le château de Cheniménil, le chemin est tout tracé vers Bruyères et, de là par le massif forestier, un passage via le Col du Haut-Jacques pour redescendre ensuite au pied du massif de la Madeleine, là où Matthieu de Lorraine, alias Maherus, tendit une [ambuscade funeste](https://fr.wikipedia.org/wiki/Matthieu_de_Lorraine) à Renaud de Senlis en 1217 (le château de Maherus, ou château de Clermont, se situait au lieu-dit la Chaise du Roi).

[^champduc]: Champ se rapporte à la ville de Champ-le-Duc ainsi qu'elle éteit dénomée depuis les chroniques racontant la vie de Charlemagne. Voir  [histoire ecclesiastique&hellip; p. 85 du PDF]


C'est une étape difficile pour Dom Ruinart et ses accompagnants&nbsp;: pas moins de 62 kilomètres séparent Remiremont de Moyenmoutier par les chemins les plus directs passant (pour reprendre des noms indentifiables aujourd'hui) par Jarménil, Cheniménil, Lépange, Champ-le-Duc, Bruyères, le Col du Haut-Jacques, Saint-dié, Étival (abbaye des chanoines de l'ordre de Prémontré), Moyenmoutier. On peut estimer un départ de grand matin pour arriver après la tombée de la nuit, tout en faisant une halte restauratrice à Champ-Le-Duc, soit à mi-chemin.

On peut voir sur [cette carte](https://umap.openstreetmap.fr/fr/map/parcours-de-dom-ruinart-le-02-octobre-1696_697885) le trajet tel que je l'ai estimé au regard des éléments du récit.

Pour se faire une idée de la représentation cartographique de l'époque, on peut aussi se reporter à [cette carte du diocèse de Toul](https://gallica.bnf.fr/ark:/12148/btv1b5973266z/f1.item.zoom), par Guillaume De l'Isle, 1707, composée à l'occasion de la publication de l'*Histoire ecclésiastique* du diocèse par P. Benoît de Toul.


## Conclusion

Il n'y a jamais eu de château ou de tour construite à l'initiative du duc Gérard d'Alsace à Gérardmer. De manière générale, aucune mention ultérieure à son règne dans les livres de patentes n'autorise la construction d'un château à Gérardmer sous l'autorité du duché de Lorraine. Encore moins sous l'autorité des chanoinesses de Remiremont. Les preuves archéologiques et archivistiques d'une telle construction sont inexistantes (jusqu'à aujourd'hui).

À rebours de la coutume locale, on peut même affirmer qu'aucun auteur n'a pouvé l'existence une telle construction. Les précautions d'usage n'ont cependant pas toujours été prises&hellip; tout en confrontant sans cesse la tradition du souvenir commémoratif de Gérard d'Alsace à la réalité des faits. 

Nous avons montré qu'une erreur d'interprétation du texte de Dom Ruinart était à la source d'une méprise qui fit long feu. C'est la raison pour laquelle les plus rigoureux à l'instar d'Henri Lepage ou Louis Géhin se sont toujours référé à la *tradition locale*&nbsp;: il était important en effet de préciser cette particularité culturelle sans jamais l'affirmer comme une réalité. En revanche la répétition de cette tradition relatée dans les publications a provoqué certainement un effet d'amplification auquel a fini par succomber Marc Georgel qui affirma que «&nbsp;chacun sait maintenant (depuis les nombreux ouvrages qui traitent de Gérardmer) que le duc Gérard se fit construire une tour&nbsp;»&hellip; 

Mais cette fameuse tradition locale est-elle pour autant dépréciée&nbsp;? C'est une question que nous ne parviendrons pas à résoudre car elle est sans objet. Après tout, la légende demeure parfaitement logique. Les ducs de Lorraine ont contribué plus que significativement à la dynamique économique des Hautes Vosges et toutes les affaires juridiques de Gérardmer furent longtemps réglées par leur représentants ou directement à la cour du Duché. Si l'un ou l'autre Gérard, illustre ou parfaitement inconnu, a fini par donner son nom à Gérardmer, la tradition a construit une histoire autour de ce nom, une histoire qui a rassemblé la communauté villageoise autour d'une identité commune, celle de l'appartenance à la Lorraine. Cette construction permettait aussi une certaine indépendance des montagnards, loin des centres de pouvoir et des institutions, surtout avant le XVII<sup>e</sup> siècle. Sans marque physique clairement identifiée sur le sol gérômois, les habitants pouvaient toujours se réclamer de l'autorité ducale&hellip; ou pas, selon l'intérêt du moment. Et cela est sans doute bien plus important qu'une vieille tour en ruine.

{{< figure src="/images/carte-g-de-lisle-1707.jpg" title="Carte du diocèse de Toul, 1707" >}}

<!-- ![Carte du diocèse de Toul, 1707](/images/carte-g-de-lisle-1707.jpg) -->


## Notes









