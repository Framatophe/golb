
---
title: "Fuckapital"
date: 2019-06-07
author: "Christophe Masutti"
image: "/images/postimages/discussion.jpg"
description: "Il n'est pas toujours facile d'expliquer en quoi consiste le capitalisme de surveillance. C'est encore moins facile si l'approche que l'on choisit n'est pas tout à fait celle qui a le vent en poupe. Doit-on pour autant se contenter d'une critique édulcorée du capitalisme de surveillance&nbsp;? Elle n'offre qu'une alternative molle entre un capitalisme immoral et un capitalisme supportable. Essayons de ne pas avoir à choisir entre la peste et le choléra."
tags: ["Libres propos", "Histoire", "Capitalisme de surveillance", "Publication", "capitalisme"]
categories:
- Libres propos
---

Il n'est pas toujours facile d'expliquer en quoi consiste le capitalisme de surveillance. C'est encore moins facile si l'approche que l'on choisit n'est pas tout à fait celle qui a le vent en poupe. Doit-on pour autant se contenter d'une critique édulcorée du capitalisme de surveillance&nbsp;? Elle n'offre qu'une alternative molle entre un capitalisme immoral et un capitalisme supportable. Essayons de ne pas avoir à choisir entre la peste et le choléra.

<!--more-->

## Fuckapital. Dialogue à propos du capitalisme de surveillance

-- Alors ça recommence, tu vas encore casser du sucre sur le dos de Madame Zuboff&nbsp;?

-- Maiiiis nooon. Que vas-tu imaginer&nbsp;? Son travail sur le capitalisme de surveillance est exceptionnel. Et c'est justement pour cette raison qu'il est important d'un déceler les *a priori* et voir avec lesquels on peut ne pas être d'accord.

-- Quoi par exemple&nbsp;?

-- La manière dont elle définit le capitalisme de surveillance.

-- Oui, enfin… dans ce cas tu remets en cause tout son bouquin, [The Age of Surveillance Capitalism](https://www.publicaffairsbooks.com/titles/shoshana-zuboff/the-age-of-surveillance-capitalism/9781610395694/).

-- Non, non. J'ai déjà eu l'[occasion d'en parler](https://golb.statium.link/post/20181115definitionscapitalismesurveillance/). Si elle ne cite pas les premiers chercheurs qui ont proposé cette notion ([John Bellamy Foster et Robert W. McChesney](https://monthlyreview.org/2014/07/01/surveillance-capitalism/)), c'est parce que sa conception à elle est bien différente. Elle définit le capitalisme de surveillance à la manière d'un dictionnaire, avec une liste très bien faite détaillant sa polysémie. À lire cette définition, tout son livre est synthétisé (on salue au passage le talent). Mais ce n'est pas cela le problème. Je veux dire&nbsp;: le *capitalisme* de surveillance. C'est-à-dire la manière dont elle fait le lien entre les mécanismes du capitalisme et ceux du capitalisme de surveillance. En effet, pour elle, le capitalisme de surveillance est une évolution (néfaste) du capitalisme. Pour être plus exact, le capitalisme de surveillance est, selon elle, *un exercice* du capitalisme qui ne devrait pas fonctionner. Son livre est tout sauf une critique du capitalisme *tel qu'il s'exerce aujourd'hui*. 

-- Tu veux dire que, finalement, elle «&nbsp;détache&nbsp;» le capitalisme de surveillance du capitalisme tout court&nbsp;? Comme si c'était une erreur de parcours&nbsp;?

-- Exactement. D'où la question&nbsp;: est-ce que le capitalisme de surveillance est une erreur de parcours du capitalisme&nbsp;? Si oui, cela veut dire qu'il reste à le réglementer, comme on a (tenté de) [réglementer les monopoles](https://fr.wikipedia.org/wiki/Loi_antitrust). Ce serait un peu comme on distingue le capitalisme «&nbsp;sauvage&nbsp;» et le capitalisme «&nbsp;apprivoisé&nbsp;», selon les orientations politiques d'un pays.

-- Oui, je vois. Mais… C'est bien cela, non&nbsp;? Regarde quelqu'un comme Hal Varian de chez Google, souvent cité par S. Zuboff. Ce type pense que pour améliorer notre «&nbsp;expérience utilisateur&nbsp;», il faut pomper toute notre vie privée, en réalité pour revendre ces données aux plus offrants et s'enrichir au passage. Ou bien ces allumés (doués) comme le germano-américain [Peter Thiel](https://fr.wikipedia.org/wiki/Peter_Thiel), qui se disent libertariens. Si on les écoutait il n'y aurait presque plus d'État pour protéger nos libertés…

-- Effectivement, Zuboff montre bien qu'il y a un grand danger dans ce *laisser-faire*. Cela va même beaucoup plus loin, puisque que grâce aux technologies dont ils ont le monopole, ces grands acteurs économiques que sont les GAFAM, sont capables non seulement de tout savoir de nous mais surtout d'influencer nos comportements de telle manière que le marché lui-même est modelé pour calibrer nos comportements, nos savoirs, et nos vies en général. Pour Zuboff, cela met en danger la démocratie libérale, c'est-à-dire cette démocratie basée sur les droits et les libertés civils. Mais justement, c'est-là tout le paradoxe qu'elle ne pointe pas&nbsp;: c'est bien grâce à ce libéralisme qu'il est possible de vivre dans une économie capitaliste. Dès lors, si le capitalisme de surveillance est un excès du capitalisme, et donc du libéralisme, le remettre en cause revient à questionner les raisons pour lesquelles, à un moment donné, la démocratie libérale a échoué. Cela revient à critiquer les politiques libérales des 10, 20, 30, 50 dernières années. Pour cette raison il faut non seulement en faire l'histoire mais aussi la critique et au moins tenter de proposer une porte de sortie. On a beau lire le livre de Zuboff, on reste carrément coincé.

-- Je te vois venir&nbsp;: évidement tu arrives en grand sauveur… tu vas dévoiler la vérité. On connaît la chanson.

-- Pas moi&nbsp;! John Bellamy Foster et Robert W. McChesney ont déjà pointé la direction dans laquelle il faut absolument réfléchir&nbsp;: déconstruire les mécanismes du capitalisme de surveillance, c'est obligatoirement en faire une histoire pour comprendre comment le libéralisme a créé des bulles hégémoniques (à la fois politiques et économiques) à la source d'un modèle de domination dont les principaux instruments sont les technologies d'information et de la communication. Eux le font sur l'histoire politique. Moi, [je compte proposer](https://golb.statium.link/post/20181012archeologiekapsurannonce/) une approche plutôt axée sur l'histoire des technologies, mais pas uniquement.

-- Mais attends une minute. J'ai vu une [conférence de Zuboff](https://www.youtube.com/watch?v=uJwf6oLvc2Q) donnée à [Boston](http://www.lowellinstitute.org/event/1000/) en janvier 2019 à la suite de la sortie de son livre. Je l'ai trouvée passionnante. Elle expliquait comment les *pratiques* du capitalisme de surveillance sont bien celles du capitalisme en général. Elle ne le détache pas de l'histoire. 

-- Vas-y, développe.

-- Elle parlait de la tradition capitaliste. La manière dont évolue le capitalisme en s'accaparant des choses qui «&nbsp;vivent&nbsp;» en dehors du marché pour les introduire dans le marché, c'est-à-dire transformer en quelque chose qui peut être vendu et acheté.

-- Heu.. si on réduit le capitalisme à un marché d'échange, dans ce cas, l'humanité à toujours vécu dans le capitalisme. C'est un peu léger. À moins que tu suggères par là que le capitalisme implique de tout transformer en biens marchands, y compris ce qui est censé y échapper comme les biens communs, les services publics, la santé, l'environnement, etc. Et dans ce cas, oui, c'est bien le cas, mais on enfonce des portes ouvertes.

-- Certes. C'est même très orienté de penser les choses ainsi. Mais attends. Elle précise tout de même. Le capitalisme *industriel* procède de cette manière et accapare la nature pour la transformer en biens destinés à l'échange. De la même manière encore, notre force physique (un élément naturel) s'est trouvé valorisé sur le marché en tant que force de travail. Et encore de la même manière, ce qu'elle appelle notre *expérience humaine privée* se retrouve sur le marché du capitalisme de surveillance. Il y a ainsi un marché de nos données comportementales, avec des calculs prédictifs, des paris sur nos comportements futurs, bref, on marchande nos âmes, tu vois ce que je veux dire&nbsp;?

-- Mouais… Quoique… il faudrait encore savoir ce qu'on entend par capitalisme, dans ce cas. L'un des gros problèmes des libéraux, c'est qu'ils pensent que le capitalisme a toujours existé. Oups… non, ce n'est pas correct de ma part. Disons qu'il y a plusieurs acceptions du capitalisme et que leurs histoires sont relatives. Par exemple, la propriété des biens de production est un principe qui ne date pas uniquement du XIX<sup>e</sup> siècle&nbsp;; la création de biens marchands est une activité qui date d'aussi loin que l'économie elle-même&nbsp;; et pour ce qui concerne l'accumulation de capital, tout dépend de savoir si on se place dans un capitalisme modéré par l'État (dans ce cas, s'il y a une monarchie, n'importe qui ne peut pas accumuler le capital, n'est-ce pas) ou si on se place dans un régime libéral, auquel cas, soit c'est la fête du slip soit il y a tout de même des gardes-fous, voire des contradictions, selon les orientations politiques du moment.

-- D'accord, mais quel rapport avec le capitalisme de surveillance d'aujourd'hui&nbsp;?

-- Attends, je n'ai pas fini là dessus. Il faut lire l'économiste Bruno Amable, par exemple, qui distingue géographiquement et culturellement différents modèles du capitalisme dans le monde (modèle européen, modèle asiatique, modèle méditerranéen, ou bien modèle socio-démocrate et modèle néolibéral). Et comme il y a plusieurs modèles du capitalisme, il y a aussi plusieurs manières d'envisager là où s'exerce le capitalisme selon les secteurs économiques. Par exemple le capitalisme financier, le capitalisme industriel ou bien… le capitalisme de surveillance, donc. Ce sont des *formes* du capitalisme.

-- Tu veux dire que Zuboff dresse une filiation du capitalisme de surveillance avec un capitalisme fantasmé? qui n'existe pas en réalité&nbsp;?

-- Je veux dire que *le capitalisme* en général n'est pas ce qu'en dit Zuboff, mais ce n'est pas le plus important. Ce que fait Zuboff, c'est qu'elle sort le capitalisme de surveillance de l'histoire elle-même. Ses références historiques ne concernent que l'histoire fleuve du capitalisme dont elle imprime les mécanismes sur des pratiques (qu'on appelle la surveillance, la dataveillance, l'uberveillance, etc.) d'aujourd'hui alors que ces mêmes pratiques ont *forcément* une origine qui ne date pas d'hier. Elles ont elles aussi une histoire. Et dès lors qu'elles ont une histoire, le capitalisme auquel elles se rattachent n'est pas uniforme. Et attention, je n'oppose pas pour autant une approche matérialiste, mais c'est trop  long pour t'expliquer maintenant.

-- C'est pour cela que tu vas en faire une archéologie, parce que cette histoire nous est cachée&nbsp;?

-- En tout cas les vestiges ne sont pas directement visibles, il faut creuser. Par exemple, le capitalisme des années 1950-1960-1970 est un capitalisme qui a vu naître le consumérisme de masse, des technologies de marketing, et en même temps il se trouvait hyper-régulé par l'État, si bien que les pratiques de surveillance sont en réalité des pratiques qui naissent avec les institutions, l'économie institutionnelle&nbsp;: les secteurs bancaires, assurantiels, et les institutions publiques aussi. L'hégémonie des grosses entreprises monopolistiques est d'abord une hégémonie qui relève de l'économie politique, en particulier celle des États-Unis, mais pas uniquement. Par la suite, la libéralisation des secteurs a créé d'autres formes encore de ce capitalisme de surveillance. Et dans toutes ces histoires, il faut analyser quelles technologies sont à l'œuvre et quelles sont les tensions sociales qu'elles créent.

-- Pfou… c'est nébuleux, tu peux donner un exemple&nbsp;?

-- Prenons l'exemple de la valorisation de l'information en capital. C'est un principe qui existe depuis fort longtemps. Si on prend plus exactement l'information personnelle qui relève de la vie privée ou ce que Zuboff appelle notre expérience humaine privée. Qu'est-ce que c'est&nbsp;? jamais la même chose. Ce n'est pas une donnée naturelle dont le capitalisme se serait emparé pour la «&nbsp;mettre&nbsp;» sur le marché. D'abord ce qu'on appelle «&nbsp;vie privée&nbsp;» est une notion qui est définie dans le droit américain de manière très malléable depuis la fin du XIX<sup>e</sup> siècle, et cette notion n'a d'existence juridique en Europe dans un sens approchant celui d'aujourd'hui que depuis la seconde moitié du XX<sup>e</sup> siècle, de plus, selon les pays ce n'est pas du tout la même chose. Dans ce cas, quel est vraiment le statut des modèles mathématiques puis informatiques des analyses marketing, d'abord théorisés dans les années 1940 et 1950, puis informatisés dans la seconde moitié des années 1960&nbsp;? Par exemple, en 1966, DEMON (*Decision Mapping via Optimum Go-No Networks*), dédié à l'analyse prédictive concernant «&nbsp;la commercialisation de nouveaux biens de consommation avec un cycle d’achat court&nbsp;». Ces systèmes utilisaient bien des informations personnelles pour modéliser des comportements d'achats et prédire les états du marché. Ils se sont largement améliorés avec l'émergence des systèmes de gestion de base de données dans les années 1980 puis du *machine learning* des années 2000 à aujourd'hui. Ces technologies ont une histoire, la vie privée a une histoire, le capitalisme de surveillance a une histoire. Les évolutions de chacune de ces parties sont concomitantes, et s'articulent différemment selon les périodes. 

-- Tout cela n'est manifestement pas né avec Google.

-- Évidemment. Il n'y a pas des «&nbsp;capitalistes de la surveillance&nbsp;» qui se sont décidés un beau jour de s'accaparer des morceaux de vie privée des gens. C'est le résultat d'une dynamique. Si le capitalisme de surveillance s'est généralisé à ce point c'est que son existence est en réalité collective, sciemment voulue par nous tous. Il s'est développé à différents rythmes et ce n'est que maintenant que nous pouvons le nommer «&nbsp;capitalisme de surveillance&nbsp;». C'est un peu comme nos économies polluantes dont nous avons théorisé les impacts climatiques qu'assez récemment, même si en avions conscience depuis longtemps &nbsp;: qui prétend jeter la première pierre et à qui exactement&nbsp;? Est-ce que d'ailleurs cela résoudrait quelque chose&nbsp;? 

-- Oui, mais à lire Zuboff, on a atteint un point culminant avec l'Intelligence artificielle, les monopoles comme Google  / Alphabet et le courtage de données. Tout cela nous catégorise et nous rend économiquement malléables. C'est vraiment dangereux depuis peu de temps, finalement.

-- À partir du moment où nous avons inventé le marketing, nous avons inventé le tri social dans le capitalisme consumériste. À la question «&nbsp;pourquoi existe-t-il un capitalisme de surveillance&nbsp;?&nbsp;», la réponse est&nbsp;: «&nbsp;parce qu'il s'agit d'un moyen efficace de produire du capital à partir de l'information et du profilage des agents économiques afin d'assurer une hégémonie (des GAFAM, des États-Unis, ou autre) qui assurera une croissance toujours plus forte de ce capital&nbsp;». 

-- Mais tu es bien d'accord avec l'idée que ce capitalisme est une menace pour la démocratie, ou du moins que le capitalisme de surveillance est une menace pour les libertés. Mais enfin, si tu prétends que cette histoire a plus de 50 ans, ne penses-tu pas que les régimes démocratiques auraient pu y mettre fin&nbsp;?

-- Dans les démocraties occidentales, les politiques libérales ont toujours encouragé les pratiques capitalistes depuis près de trois siècles, à des degrés divers. Elles se sont simplement radicalisées avec des formes plus agressives de libéralisme qui consistaient à augmenter les profits grâce aux reculs progressifs des régulations des institutions démocratiques et des protections sociales. Si d'un côté des lois ont été créés pour protéger la vie privée, de l'autre côté on a libéré les marchés des télécommunications. Tout est à l'avenant. Encore aujourd'hui par exemple, alors qu'on plaide pour rendre obligatoire l'interopérabilité des réseaux sociaux (pour que les utilisateurs soient moins captifs des monopoles), les politiques rétorquent qu'un tel principe serait «&nbsp;[excessivement agressif pour le modèle économique des grandes plateformes](https://www.nextinpact.com/news/107951-imposer-linteroperabilite-aux-plateformes-les-doutes-et-prudence-cedric-o.htm)&nbsp;». 

-- Des luttes s'engagent alors&nbsp;?

-- Oui mais attention à ne pas noyer le poisson. Focaliser sur la lutte pour sauvegarder les libertés individuelles face aux GAFAM, c'est bien souvent focaliser sur la sauvegarde d'un «&nbsp;bon&nbsp;» libéralisme ou d'un «&nbsp;capitalisme distributif&nbsp;» plus ou moins imaginaire. C'est-à-dire vouloir conserver le modèle politique libéral qui justement a permis l'émergence du capitalisme de surveillance. Comme le montre [ce billet](https://golb.statium.link/post/20190331structuresconn/), c'est exactement la brèche dans laquelle s'engouffrent certains politiques qui ont le beau jeu de prétendre défendre les libertés et donc la démocratie.

-- C'est déjà pas mal, non&nbsp;?

-- Non. Parce que lutter contre le capitalisme de surveillance uniquement en prétendant défendre les libertés individuelles, c'est faire bien peu de cas des conditions sociales dans lesquelles s'exercent ces libertés. On ne peut pas mettre de côté les inégalités évidentes que créent ces monopoles et qui ne concernent pas uniquement le tri social du capitalisme de surveillance. Que dire de l'exploitation de la main d'œuvre de la part de ces multinationales ([et pas que dans les pays les plus pauvres](https://cfeditions.com/visages/)), sans compter les impacts environnementaux (terres rares, déchets, énergies). Si on veut lutter contre le capitalisme de surveillance, ce n'est pas en plaidant pour un capitalisme acceptable (aux yeux de qui&nbsp;?) mais en proposant des alternatives crédibles, en créant des solutions qui reposent sur l'entraide et la coopération davantage que sur la concurrence et la croissance-accumulation sans fin de capital. Il ne faut pas lutter contre le capitalisme de surveillance, il faut le remplacer par des modèles économiques et des technologies qui permettent aux libertés de s'épanouir au lieu de s'exercer dans les seules limites de l'exploitation capitaliste. [Fuck le néolibéralisme](https://acme-journal.org/index.php/acme/article/view/1342)&nbsp;! Fuck la surveillance&nbsp;!

-- Bon, tu reprends une bière&nbsp;?



