---
title: "Qu'est-ce que le radicalisme ?"
date: 2022-06-11
author: "Christophe Masutti"
image: "/images/postimages/cm.png"
description: "Je propose dans ce billet une traduction d'un texte de 2013 écrit par Jeff Shantz : In defense of radicalism"
tags: ["Libres propos", "Anarchie", "radicalisme", "traduction"]
categories:
- Libres propos
---

«&nbsp;Le pouvoir n'admet jamais son propre extrémisme, sa propre violence, son propre chaos, sa propre destruction, son propre désordre. Le désordre de l'inégalité, le chaos de la dépossession, la destruction des communautés et des relations traditionnelles ou indigènes &ndash;&nbsp;l'extermination de la vie, de la planète elle-même. Ce sont de véritables comportements extrémistes.&nbsp;» (Jeff Shantz)

<!--more-->

## Avant propos

Je propose dans ce billet une traduction d'un texte de 2013 écrit par [Jeff Shantz](http://jeffshantz.ca/). Les travaux de ce dernier appartiennent au courant de la criminologie anarchiste (voir [ici](https://en.wikipedia.org/wiki/Anarchist_criminology) ou [là](https://journals.openedition.org/champpenal/12317?lang=en)). Pour faire (très) court, il s'agit de s'interroger sur les causes de la criminalité et de la violence à partir d'une critique des actions de l'État et leurs effets néfastes (y compris la violence d'État).

Pour être tout à fait clair ici, je ne partage pas d'un bloc l'approche de la criminologie anarchiste, à cause des achoppements logiques auxquels elle doit se confronter. En particulier le fait qu'un mouvement de violence peut certes aller chercher ses causes dans un enchaînement d'effets du pouvoir institutionnel, il n'en demeure pas moins que, en tant qu'historien, il m'est difficile de ne jamais fouiller qu'à un seul endroit. Je crois qu'il y a toujours un faisceaux de causalités, toutes plus ou moins explicables, avec des déséquilibres qui peuvent faire attribuer l'essentiel à une partie plutôt qu'une autre. La criminologie anarchiste remet assez abruptement en cause la théorie du contrat social (surtout dans son rapport droit/crime/pouvoir), ce qui est plutôt sain. Mais si le pouvoir institutionnel est bien entendu en charge de lourdes responsabilités dans la révolte légitime (en particulier dans un contexte néolibéral autoritaire où la violence est la seule réponse faite au peuple), tous les mouvements de résistance font toujours face, à un moment ou un autre, à leurs propres contradictions, c'est humain. C'est ce qui justement les fait grandir et leur donne leur force et d'autant plus de légitimité, ou au contraire contribue à leur extinction (qu'on peut certes attribuer à l'une ou l'autre stratégie de pouvoir, bref&hellip;).

D'ailleurs, c'est justement l'un des objets de ce texte à propos du radicalisme. C'est un avertissement aux mouvements de résistance qui ne comprennent pas, d'une part, que le radicalisme est une méthode et, d'autre part, que son emploi galvaudé est une stratégie de défense (et d'offensive) du capital, en particulier via la communication et l'exercice du pouvoir médiatique. En cela il vise assez juste. En tout cas, c'est à garder en tête lorsque, sur un plateau TV, on interroge toujours&nbsp;: «&nbsp;Mais&hellip; vous condamnez ces violences de la part des ces jeunes radicalisés&nbsp;?&nbsp;»


## Plaidoyer pour le radicalisme

Par **Jeff Shantz**

&mdash; Article paru dans la revue *Radical Criminology*, num. 2, 2013, Éditorial ([source](http://www.radicalcriminology.org/index.php/rc/article/view/34/html)), Licence CC By-Nc-Nd. Traduit avec l'aimable autorisation de l'auteur.

---


Aujourd'hui, peu de termes ou d'idées sont autant galvaudés, déformés, diminués ou dénaturés que «&nbsp;radical&nbsp;»  ou «&nbsp;radicalisme&nbsp;». Cela n'est sans doute pas très surprenant, étant donné que nous vivons une période d'expansion des luttes contre l'État et le capital, l'oppression et l'exploitation, dans de nombreux contextes mondiaux. Dans de tels contextes, la question du radicalisme, des moyens efficaces pour vaincre le pouvoir (ou étouffer la résistance) devient urgente. Les enjeux sont élevés, les possibilités d'alternatives réelles sont avancées et combattues. Dans de tels contextes, les militants et les universitaires doivent non seulement comprendre le radicalisme de manière appropriée, mais aussi défendre (et faire progresser) les approches radicales du changement social et de la justice sociale.

La première utilisation connue du terme radical remonte au XIV<sup>e</sup> siècle, 1350-1400&nbsp;; le moyen anglais venant du latin tardif *rādīcālis*, avoir des racines[^1]. Il est également défini comme étant très différent de ce qui est habituel ou traditionnel. Le terme radical signifie simplement de ou allant vers les racines ou l'origine. Rigoureux. En clair, cela signifie aller à la racine d'un problème.

Le radicalisme est une perspective, une orientation dans le monde. Ce n'est pas, comme on le prétend souvent à tort, une stratégie. Être radical, c'est creuser sous la surface des certitudes, des explications trop faciles, des réponses insatisfaisantes et des panacées qui se présentent comme des solutions aux problèmes. Le radicalisme conteste et s'oppose aux définitions *du statu quo* - il refuse les justifications intéressées que fournissent l'autorité et le pouvoir.

Plutôt qu'un ensemble d'idées ou d'actions, il s'agit d'une approche cruciale de la vie. Comme l'a suggéré l'analyste existentialiste marxiste Erich Fromm dans un contexte de lutte antérieure&nbsp;:

> En premier lieu, cette approche peut être caractérisée par la devise&nbsp;: *de omnibus dubitandum*&nbsp;; tout doit être mis en doute, en particulier les concepts idéologiques qui sont quasiment partagés par tous et qui sont devenus par conséquent des axiomes incontournables du sens commun&hellip; Le doute radical est un processus&nbsp;; un processus de libération de la pensée idolâtre&nbsp;; un élargissement de la conscience, de la vision imaginative et créative de nos opportunités et possibilités. L'approche radicale ne se déploie pas dans le vide. Elle ne part pas de rien, elle part des racines. (1971, vii)

Comme c'est le cas pour la plupart des opinions et des pratiques dans la société capitaliste de classes, il y a deux approches distinctes du radicalisme, deux significations. Selon la première, celle qui consiste à aller aux racines &ndash;&nbsp;à la source des problèmes --, la nature du capital doit être comprise, abordée, affrontée &ndash;&nbsp;vaincue. Mettre fin à la violence du capital ne peut se faire qu'en mettant fin aux processus essentiels à son existence&nbsp;: l'exploitation, l'expropriation, la dépossession, le profit, l'extraction, l'appropriation des biens communs, de la nature.  Et comment y parvenir&nbsp;? Le capital et les États savent &ndash;&nbsp;ils comprennent. Voilà qui explique la reconnaissance des actes décrits plus haut &ndash;&nbsp;reconnus, précisément, comme radicaux.

Le radicalisme, vu d'en bas, est sociologique (et devrait être criminologique, bien que la criminologie soit parfois à la traîne). Il exprime cette ouverture sur le monde que C. Wright Mills appelle l'imagination sociologique (1959). Le radicalisme dans son sens premier relie l'histoire, l'économie, la politique, la géographie, la culture, en cherchant à aller au-delà des réponses faciles, rigidifiées de façon irréfléchie en tant que «&nbsp;sens commun&nbsp;» (qui n'est souvent ni commun ni sensé). Il creuse sous les conventions et le statu quo. Pour Fromm&nbsp;:

> «&nbsp;Douter&nbsp;», dans ce sens, ne traduit pas l'incapacité psychologique de parvenir à des décisions ou à des convictions, comme c'est le cas dans le doute obsessionnel, mais la propension et la capacité à remettre en question de manière critique toutes les hypothèses et institutions qui sont devenues des idoles au nom du bon sens, de la logique et de ce qui est supposé être «&nbsp;naturel&nbsp;». (1971, viii)

Plus encore, le radicalisme ne cherche ni se conforte dans le moralisme artificiel véhiculé par le pouvoir &ndash;&nbsp;par l'État et le capital. Une approche radicale n'accepte pas le faux moralisme qui définit la légitimité des actions en fonction de leur admissibilité pour les détenteurs du pouvoir ou les élites (la loi et l'ordre, les droits de l'État, les droits de propriété, et ainsi de suite). Comme l'a dit Fromm&nbsp;:

> Cette remise en question radicale n'est possible que si l'on ne considère pas comme acquis les concepts de la société dans laquelle on vit ou même de toute une période historique &ndash;&nbsp;comme la culture occidentale depuis la Renaissance &ndash;&nbsp;et si, en outre, on élargit le périmètre de sa conscience et on pénètre dans les aspects inconscients de sa pensée. Le doute radical est un acte de dévoilement et de découverte&nbsp;; c'est la prise de conscience que l'empereur est nu, et que ses splendides vêtements ne sont que le produit de son fantasme. (1971, viii)

Enfreindre la loi (des États, de la propriété) peut être tout à fait juste et raisonnable. De même que faire respecter la loi peut être (est, par définition) un acte de reconnaissance des systèmes d'injustice et de violence. Les affamés n'ont pas besoin de justifier leurs efforts pour se nourrir. Les démunis n'ont pas besoin d'expliquer leurs efforts pour se loger. Les personnes brutalisées n'ont pas besoin de demander la permission de mettre fin à la brutalité. Si leurs efforts sont radicaux &ndash;&nbsp;car ils savent que cela signifie de vraies solutions à de vrais problèmes &ndash;&nbsp;alors, qu'il en soit ainsi.

D'autre part, il y a la définition hégémonique revendiquée par le capital (et ses serviteurs étatiques). Dans cette vision, déformée par le prisme du pouvoir, le radicalisme est un mot pour dire extrémisme (chaos, désordre, violence, irrationalité). La résistance de la classe ouvrière, les mouvements sociaux, les luttes indigènes, les soulèvements paysans, les actions directes et les insurrections dans les centres urbains &ndash;&nbsp;toute opposition qui conteste (ou même remet en question) les relations de propriété, les systèmes de commandement et de contrôle, l'exploitation du travail, le vol des ressources communes par des intérêts privés &ndash;&nbsp;sont définis par l'État et le capital comme du radicalisme, par lequel ils entendent l'extrémisme, et de plus en plus, le terrorisme.

Tous les moyens de contrôle de l'autorité de l'État sont déployés pour juguler ou éradiquer ce radicalisme &ndash;&nbsp;c'est en grande partie la raison pour laquelle la police moderne, les systèmes de justice pénale et les prisons, ainsi que l'armée moderne, ont été créés, perfectionnés et renforcés. En outre, les pratiques «&nbsp;douces&nbsp;» de l'État et du capital, telles que les industries de la psy, qui ont longtemps inclus la rébellion parmi les maladies nécessitant un diagnostic et un traitement[^2], sont moins remarquées Comme le suggère Ivan Illich, théoricien de la pédagogie radicale&nbsp;: «&nbsp;Le véritable témoignage d'une profonde non-conformité suscite la plus féroce violence à son encontre&nbsp;» (1971, 16). C'est le cas dans le contexte actuel des luttes sociales, et de la répression déployée par l'État et le capital pour étouffer toute résistance significative (et effrayer les soutiens mous).

Pourtant, les opinions et les pratiques visées par cette construction du radicalisme sont tout bonnement celles qui défient et contestent les États et le capital et proposent des relations sociales alternatives. Même lorsque ces mouvements ne font pas ou peu de mal à qui que ce soit, même lorsqu'ils sont explicitement non-violents (comme dans les occupations des lieux de travail, les grèves, les revendications territoriales des indigènes), le pouvoir présente ces activités comme radicales et extrêmes (et, par association, violentes). C'est en réalité parce que de telles activités font planer le spectre de la première compréhension du radicalisme &ndash;&nbsp;celui qui vient d'en bas &ndash;&nbsp;celui qui parle des perspectives des opprimés et des exploités. Cette définition est, en fait, fidèle aux racines du mot et cohérente avec sa signification.

L'accusation de radicalisme par les détenteurs du pouvoir, la question du radicalisme elle-même, devient toujours plus importante dans les périodes de lutte accrue. C'est à ces moments que le capital étatique a quelque chose à craindre. Les efforts visant à s'attaquer aux racines ne sont plus relégués aux marges du discours social, mais ce que le pouvoir cherche à faire, c'est le ramener dans un lieu de contrôle et de réglementation. Dans les périodes de calme, la question du radicalisme est moins souvent posée. Cela en dit long sur la nature des débats au sujet du radicalisme.

Le radicalisme au sens premier n'est pas une réaction spontanée aux conditions sociales. Pour Illich, il faut apprendre à distinguer «&nbsp;entre la fureur destructrice et la revendication de formes radicalement nouvelles&nbsp;» (1971, 122). Là où il démolit, il démolit pour construire. Il faut «&nbsp;distinguer entre la foule aliénée et la protestation profonde&nbsp;» (1971, 122-123). Dans la perspective de Fromm&nbsp;:

> Le doute radical signifie questionner, il ne signifie pas nécessairement nier. Il est facile de nier en posant simplement le contraire de ce qui existe&nbsp;; le doute radical est dialectique dans la mesure où il appréhende le déroulement des oppositions et vise une nouvelle synthèse qui nie *et* affirme. (1971, viii)

Comme l'a suggéré l'anarchiste Mikhaïl Bakounine, la passion de détruire est aussi une passion créatrice.

Les problèmes d'extrémisme, introduits par les détenteurs du pouvoir pour servir leur pouvoir, sont une diversion, un faux-fuyant pour ainsi dire. Les actes supposés extrêmes ou scandaleux ne sont pas nécessairement radicaux, comme le suggèrent les médias de masse qui les traitent souvent comme des synonymes. Les actes extrêmes (et il faudrait en dire plus sur ce terme trompeur) qui ne parviennent pas à s'attaquer aux racines des relations entre l'État et le capital, comme les actes de violence malavisés contre des civils, ne sont pas radicaux. Ils ne s'attaquent pas aux racines de l'exploitation capitaliste (même si la frustration liée à l'exploitation les engendre). Les actes qui servent uniquement à renforcer les relations de répression ou à légitimer les initiatives de l'État ne sont pas radicaux.

En même temps, certains actes extrêmes sont radicaux. Ces actes doivent être jugés au regard de leur impact réel sur le pouvoir capitaliste d'État, sur les institutions d'exploitation et d'oppression.

Dans le cadre du capitalisme d'État, l'extrémisme est vidé de son sens. Dans un système fondé et subsistant sur le meurtre de masse, le génocide et l'écocide comme réalités quotidiennes de son existence, les concepts de l'extrémisme deviennent non pertinents, insensés. En particulier lorsqu'ils sont utilisés de manière triviale, désinvolte, pour décrire des actes mineurs d'opposition ou de résistance, voire de désespoir. Dans ce cadre également, la question de la violence (dans une société fondée et étayée par des actes quotidiens d'extrême violence) ou de la non-violence est une construction factice (favorable au pouvoir qui légitime sa propre violence ou fait passer pour non-violents des actes violents comme l'exploitation), un jeu truqué.

Le pouvoir n'admet jamais son propre extrémisme, sa propre violence, son propre chaos, sa propre destruction, son propre désordre. Le désordre de l'inégalité, le chaos de la dépossession, la destruction des communautés et des relations traditionnelles ou indigènes &ndash;&nbsp;l'extermination de la vie, de la planète elle-même. Ce sont de véritables comportements extrémistes. Ils sont, en fait, endémiques à l'exercice du pouvoir dans les sociétés capitalistes étatiques.

La destruction d'écosystèmes entiers pour le profit de quelques-uns est un acte férocement «&nbsp;rationnel&nbsp;» (contre l'irrationalité des approches radicales visant à mettre fin à ces ravages). L'extinction de communautés entières &ndash;&nbsp;le génocide des peuples &ndash;&nbsp;pour obtenir des terres et des ressources est une action indiciblement extrême, en termes écologiques et humains. Pourtant, le pouvoir n'identifie jamais ces actes comme étant radicaux &ndash;&nbsp;il s'agit toujours d'une simple réalité de la vie, du coût des affaires, d'un effet secondaire du progrès inévitable, d'un résultat malheureux de l'histoire (dont personne n'est responsable).

Et il ne s'agit même pas des extrêmes, ni de rares dérives du capitalisme &ndash;&nbsp;ce sont les actes fondateurs de l'être du capital --, ils sont la nature du capital. La conquête coloniale, par exemple, n'est pas un effet secondaire regrettable ou un excès du capitalisme &ndash;&nbsp;c'est sa possibilité même, son essence.

Les militants qui ne parviennent pas à aller à la racine des problèmes sociaux ou écologiques &ndash;&nbsp;qui ne comprennent pas ce que signifie le radicalisme d'en bas pour la résistance &ndash;&nbsp;peuvent être, et sont généralement, trop facilement enrôlés par le capital étatique dans le chœur dominant qui assaille et condamne, qui calomnie et dénigre le radicalisme. Nous le voyons dans le cas des mouvements de mondialisation alternative où certains activistes, revendiquant la désobéissance civile non-violente (DCNV) de manière anhistorique, sans contexte, comme s'il s'agissait d'une sorte d'objet fétiche, se joignent ensuite à la police, aux politiciens, aux firmes et aux médias de masse pour condamner l'action directe, les blocages, les occupations de rue, les barricades ou, bien sûr, les dommages infligés à la propriété, comme étant trop radicaux &ndash;&nbsp;en tant qu'actes de violence. Les voix des activistes anti-radicaux deviennent une composante de la délégitimation de la résistance elle-même, un aspect clé du maintien du pouvoir et de l'inégalité.

De tels désaveux publics à l'endroit de la résistance servent à justifier, excuser et maintenir la violence très réelle qui *est* le capital. Les approches, y compris celles des activistes, qui condamnent la résistance, y compris par exemple la résistance armée, ne font que donner les moyens d'excuser et de justifier la violence continuelle et extensible (elle s'étend toujours en l'absence d'une réelle opposition) du capital étatique.

La survie n'est pas un crime. La survie n'est *jamais* radicale. L'exploitation est *toujours* un crime (ou devrait l'être). L'exploitation n'est *jamais que* la norme des relations sociales capitalistes.

Les détenteurs du pouvoir chercheront toujours à discréditer ou à délégitimer la résistance à leurs privilèges. Le recours à des termes chargés (mal compris et mal interprétés par les détenteurs du pouvoir) comme le radicalisme sera une tactique à cet égard. On peut suivre la reconstruction du terme «&nbsp;terreur&nbsp;» pour voir un exemple de ces processus. Le terme «&nbsp;terreur&nbsp;» était initialement utilisé pour désigner la violence d'État déployée contre toute personne considérée comme une menace pour l'autorité instituée, pour l'État (Badiou 2011, 17). Ce n'est que plus tard &ndash;&nbsp;comme résultat de la lutte hégémonique &ndash;&nbsp;que la terreur en est venue (pour les détenteurs du pouvoir étatique) à désigner les actions des civils &ndash;&nbsp;même les actions *contre* l'État.

Et cela fonctionne souvent. Il est certain que cela a joué un rôle dans l'atténuation ou l'adoucissement des potentiels des mouvements alternatifs à la mondialisation, comme cela a été le cas dans les périodes de lutte précédentes. En cela, ces activistes anti-radicaux soutiennent inévitablement le pouvoir et l'autorité de l'État capitaliste et renforcent l'injustice.

Toutefois, nous devons également être optimistes. L'accusation de radicalisme venant d'en haut (affirmée de manière superficielle) est aussi un appel à l'aide de la part du pouvoir. C'est un appel du pouvoir aux secteurs non engagés, le milieu mou, pour qu'ils se démarquent des secteurs résistants et se rangent du côté du pouvoir (États et capital) pour réaffirmer le statu quo (ou étendre les relations et les pratiques qu'ils trouvent bénéfiques, un nouveau statu quo de privilèges) &ndash;&nbsp;les conditions de la conquête et de l'exploitation.

Le radicalisme (ou l'extrémisme, ou le terrorisme) est la méthode utilisée par le pouvoir pour réprimer l'agitation en attirant le public vers les intérêts dominants. En ce sens, il reflète un certain désespoir de la part des puissants &ndash;&nbsp;un désespoir dont il faut profiter, et non pas en faire le jeu ou le minimiser.

Dans les périodes de recrudescence des luttes de masse, la question du radicalisme se pose inévitablement. C'est dans ces moments qu'une orientation radicale brise les limites de la légitimation hégémonique &ndash;&nbsp;en proposant de nouvelles interrogations, de meilleures réponses et de réelles alternatives. S'opposer au radicalisme, c'est s'opposer à la pensée elle-même. S'opposer au radicalisme, c'est accepter les conditions fixées par le pouvoir, c'est se limiter à ce que le pouvoir permet.


L'anti-radicalisme est intrinsèquement élitiste et anti-démocratique. Il part du principe que tout le monde, quel que soit son statut, a accès aux canaux de prise de décision politique et économique, et peut participer de manière significative à la satisfaction des besoins personnels ou collectifs. Il ne tient pas compte des vastes segments de la population qui sont exclus des décisions qui ont le plus d'impact sur leur vie, ni de l'accès inégal aux ressources collectives qui nécessitent, qui exigent, des changements radicaux.

Les activistes, ainsi que les sociologues et les criminologues, doivent défendre le radicalisme d'en bas comme une orientation nécessaire pour lutter contre l'injustice, l'exploitation et l'oppression et pour des relations sociales alternatives. Les actions doivent être évaluées non pas en fonction d'un cadre moral légal établi et renforcé par le capital étatique (pour son propre bénéfice). Elles doivent être évaluées en fonction de leur impact réel sur la fin (ou l'accélération de la fin) de l'injustice, de l'exploitation et de l'oppression, et sur l'affaiblissement du capital étatique. Comme Martin Luther King l'a suggéré, une émeute est simplement le langage de ceux qui ne sont pas entendus.

La morale bien-pensante et la référence à l'autorité légale, en reprenant les voix du capital étatique, est pour les activistes une abdication de la responsabilité sociale. Pour les sociologues et les criminologues, c'est un renoncement à l'imagination sociologique qui, en mettant l'accent sur la recherche des racines des problèmes, a toujours été radicale (au sens non hégémonique du terme). Les penseurs et les acteurs critiques de tous bords doivent défendre ce radicalisme. Ils doivent devenir eux-mêmes radicaux.

Les débats devraient se concentrer sur l'efficacité des approches et des pratiques pour s'attaquer aux racines des problèmes sociaux, pour déraciner le pouvoir. Ils ne devraient pas être centrés sur la conformité à la loi ou à la moralité bourgeoise. Ils ne devraient pas être limités par le manque d'imagination des participants ou par le sentiment que le meilleur des mondes est celui que le pouvoir a proposé.

Encore une fois, le radicalisme n'est pas une tactique, un acte, ou un événement. Ce n'est pas une question d'extrêmes, dans un monde qui considère comme indiscutable que les extrêmes sont effroyables. C'est une orientation du monde. Les caractéristiques du radicalisme sont déterminées par, et dans, des contextes spécifiques. C'est le cas aujourd'hui pour les mobilisations de masse, voire des soulèvements populaires contre les offensives d'austérité étatiques au service du capitalisme néolibéral. Le radicalisme menace toujours de déborder les tentatives de le contenir. C'est parce qu'il fait progresser la compréhension &ndash;&nbsp;il met en évidence l'injustice sociale &ndash;&nbsp;qu'il est par nature re-productif. Il est, en termes actuels, viral.

*Jeff Shantz, Salt Spring Island, été 2013*

## Bibliographie

Badiou, Alain. 2011. *Polemics*. London: Verso

Fromm, Erich. 1971. «&nbsp;Introduction&nbsp;». *Celebration of Awareness: A Call for Institutional Revolution*. New York: Doubleday Anchor

Illich, Ivan. 1971. *Celebration of Awareness: A Call for Institutional Revolution*. New York: Doubleday Anchor

Mills, C. Wright. 1959. *The Sociological Imagination*. London: Oxford University Press.

Rimke, Heidi. 2011. «&nbsp;The Pathological Approach to Crime: Individually Based Theories&nbsp;». In *Criminology: Critical Canadian Perspectives*, ed. Kirsten Kramar. Toronto: Pearson Education Canada, 78–92.

&mdash;. 2003 «&nbsp;Constituting Transgressive Interiorities: C19th Psychiatric Readings of Morally Mad Bodies&nbsp;». In *Violence and the Body: Race, Gender and the State*, ed. A. Arturo. Indiana: Indiana University Press, 403–28.

## Notes

[^1]: NdT&nbsp;: le terme vient effectivement du latin tardif, comme mentionné par A. Blaise dans son dictionnaire du latin chrétien, dont voici la définition complète&nbsp;: «&nbsp;qui tient à la racine, premier, fondamental&nbsp;» dérivé de *radix*, *-icis* «&nbsp;racine, origine première&nbsp;».

[^2]: Pour une analyse plus approfondie de cette question, voir le travail en cours de Heidi Rimke (2011, 2003).
