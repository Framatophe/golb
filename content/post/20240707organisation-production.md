---
title: "Organisation et production de texte"
date: 2024-07-07
author: "Christophe Masutti"
description: "Les années passent et il est indéniable que les habitudes prises au fil du temps montrent leur efficacité au regard de la pratique plus que de la logique. Néanmoins, il me semble que mon organisation logicielle et mon flux de production peuvent intéresser. C'est pourquoi je vous les présente ici."
tags: ["markdown", "logiciels", "écriture", "Vim", "Zettlr"]
categories: 
- Logiciel libre
---


Les années passent et il est indéniable que les habitudes prises au fil du temps montrent leur efficacité au regard de la pratique plus que de la logique. Néanmoins, il me semble que mon organisation logicielle et mon flux de production peuvent intéresser. C'est pourquoi je vous les présente ici.

<!--more--> 

# Les objectifs

* Utiliser exclusivement des logiciels libres et des formats ouverts pour assurer la pérennité de la production dans le temps ;
* Pouvoir utiliser les logiciels que je veux (et au fil de mes découvertes) indépendamment de l'organisation de mes fichiers pour ne pas dépendre d'un logiciel en particulier ;
* Ne pas se retrouver prisonnier d'un format, fût-il libre ;
* Assurer la sauvegarde de mes sources et de mes travaux ;
* Pouvoir travailler sur plusieurs machines, à plusieurs endroits si je le souhaite.

# Les choix stratégiques

Ayant l'habitude de travailler avec une suite bureautique comme LibreOffice et les formats ouverts, il me fallait trouver un processus qui soit capable de produire de tels formats. En revanche, l'utilisation de logiciels de traitement de texte, comme LibreOffice Writer, est assez lourde : bien qu'il soit possible d'utiliser les raccourcis claviers, l'ensemble propose une pléthore de menus et autres options qu'il n'est finalement pas pertinent d'avoir à disposition lorsqu'on _produit_ du texte. Par ailleurs, la règle qui prévaut avec les suites bureautiques consiste à s'occuper de la mise en page _après_ avoir produit du texte. Dans ce cas, pourquoi ne pas séparer les deux processus ? Séparer la production de texte de sa valorisation finale. 

Il existe, d'un côté, des outils pour produire et organiser et, de l'autre côté, des outils pour finaliser et mettre en page.
Se concentrer sur la production implique cependant d'avoir des outils spécifiques : faciliter l'écriture, y compris du point de vue ergonomique, permettre d'interagir facilement avec d'autres logiciels (gestion bibliographique, synchronisation des fichiers, etc), privilégier le texte dans l'interface sans l'enfermer dans un formatage dont il sera difficile de s'extraire par la suite.

C'est là qu'un troisième terme intervient : la _conversion_. À partir d'une production essentiellement textuelle, il doit être possible de _convertir_ la production vers des formats soit directement destinés à être transmis, soit destinés à entrer dans un flux de mise en page spécifique. Par exemple, je convertis du texte vers un format `LaTeX` pour peaufiner une mise en page ou je le convertis vers un format `ODT` pour le transmettre à une autre personne qui a besoin de ce format, ou bien encore je produis du `HTML` ou du `epub` pour d'autres usages encore. Et cette conversion doit être la plus « propre » possible.

Les outils de production doivent être choisis en fonction des goûts et des pratiques. Comme il s'agit de texte, c'est donc vers les éditeurs de texte qu'il faut se tourner. Parmi ceux-ci, certains sont plus spécialisés que d'autres ou disposent d'options et d'extensions utiles à la production elle-même ou visant à améliorer la pratique de l'utilisateur.

## Le format de référence

S'agissant d'un format dont la permanence dans le temps doit être assurée, le format devrait reposer sur une base de texte (_plain text_), écrit directement dans l'éditeur, formaté de manière simple et lisible humainement. Pour cela on laissera la possibilité d'exprimer des directives de mise en page et de hiérarchisation du texte, faciles à apprendre et gênant le moins possible la lecture.  Au lieu de se contenter de texte simplement exprimé, on utilisera le [langage _markdown_](https://fr.wikipedia.org/wiki/Markdown), un langage de balisage léger, simple à l'apprentissage comme à l'écriture et qui ne gêne pas la lecture (surtout si l'éditeur dispose d'une interprétation du markdown).

## L'organisation

J'utilise une interprétation personnelle de la [méthode Zettelkasten](https://fr.wikipedia.org/wiki/Zettelkasten) pour gérer mes connaissances et mes notes. Cela suppose une gestion rigoureuse des dossiers et des fichiers.

J'utilise aussi un logiciel de gestion bibliographique (Zotero) et un logiciel de gestion de livres numériques (Calibre), et j'apprécie l'utilisation d'un correcteur grammatical (comme Grammalecte).

Je sauvegarde mes sources et mes productions finales sur un serveur distant avec Nextcloud. Je travaille mes textes en les synchronisant avec Git sur un serveur Gitlab. Tandis que Nextcloud me permet d'avoir accès à mes sources depuis n'importe quel dispositif (tablette, liseuse, téléphone portable et ordinateur), travailler avec Git me permet de synchroniser lorsque je travaille sur différentes machines et de versionner mes productions intermédiaires et mes notes. Pour la bagatelle, l'ensemble est de toute façon sauvegardé de manière quotidienne sur un autre serveur avec Borg backup/Borgmatic.


## Le choix des éditeurs

Un éditeur de texte doit comprendre à minima une coloration syntaxique permettant de visualiser les balises markdown. Mais ce n'est pas suffisant pour travailler sur des articles de recherche académique, par exemple. Il faut pour cela que l'éditeur puisse :

* proposer une interface agréable et hautement configurable pour une utilisation quotidienne,
* permettre d'écrire « au kilomètre » sans avoir besoin de saisir la souris à la moindre occasion,
* permettre si possible une interaction avec un logiciel de gestion bibliographique, par exemple pour saisir facilement les clés de références bibliographiques citées,
* proposer divers outils destinés à l'organisation de l'écriture : l'arborescence des fichiers, une navigation dans le texte, un gestionnaire de mots-clés pour les références croisées, une correction orthographique et grammaticale, etc.
* permettre de temps à autre une interaction avec le reste du système d'exploitation, par exemple avoir accès au terminal pour lancer des commandes de conversion, ou de gestion de fichiers,
* avoir accès à des petits programme de conversion « intégrés » pour un premier aperçu des résultats de production finale

Certains éditeurs de texte proposent tout cela en même temps, avec un système de _plugins_ et il est parfois facile de se noyer dans la configuration d'un éditeur de texte. Comprenez : un éditeur de texte digne de ce nom doit pourvoir s'adapter à la pratique de son utilisateur, et non l'inverse. Si bien que les besoins étant relatifs, les solutions qui se présentent dans le  monde des logiciels libres sont quasiment infinies. Il est même possible de programmer son propre plugin et refaire tout le mapage des commandes si on prend le cas des éditeurs Emacs ou Vim.

J'ai donc porté _mes_ choix vers plusieurs éditeurs qui correspondent à mes usages et me permettent de choisir l'un ou l'autre selon mes envies et ce que je prévois de faire durant une session de travail :

* [Zettlr](https://www.zettlr.com/) : parce que ce logiciel a été pensé littéralement _pour_ la production de textes scientifiques,
* [Vim](https://www.vim.org/) : parce qu'il s'utilise dans le terminal et qu'un Vim bien configuré avec les bons plugins est très efficace (plugins utilisés:  vim-pandoc, vim-pandoc syntax, Nerd Tree, Grammalecte, vim-airline...)
* [Gedit](https://gedit-technology.github.io/apps/gedit/) : un éditeur simple, prêt à l'emploi, sans fioritures.
* On peut aussi citer les éditeurs spécialisés pour markdown tel [Ghostwriter](https://ghostwriter.kde.org/fr/).


## Pourquoi ne pas...

* Utiliser Emacs ? je l'ai fait et le ferai encore. En ce moment c'est Vim. Emacs propose, du  point de vue de mes besoins, les mêmes possibilités. 
* Utiliser directement LibreOffice : certes les extensions de type Zotero et Grammalecte sont disponibles pour LibreOffice, et il est possible d'exporter depuis LibreOffice dans de nombreux formats. Par contre, LibreOffice ne propose pas une interface que je puisse conformer à mes usages et mes besoins, et tout particulièrement la possibilité d'écrire au kilomètre sans que mes mains quittent le clavier pour se saisir à tout bout de champ de la souris. Je réserve donc LibreOffice à un autre usage en fin de processus.
* Utiliser des logiciels de gestion de notes comme Obsidian ou Logseq : parce que justement leur usage spécialisé est trop éloigné de mes usages académiques.

# L'organisation

Rien ne vaut un bon vieux diagramme !

![Flux de production](/images/flux-framatophe.png)

Comme on peut le voir, quatre dossiers principaux sont en jeu : `Bibliographies`, `Templates`, `Notes`, `Travaux`. Je ne sors pas de ces dossiers et ils sont systématiquement synchronisés avec Git sur un dépôt Gitlab.

`Bibliographies` : il contient les fichiers bibliographiques que j'utilise et tout particulièrement un fichier `.json` généré avec Zotero et qui se met à jour automatiquement à chaque ajout dans Zotero. C'est sur ce fichier que pointent mes éditeurs (Zettlr et Vim) pour interagir avec la bibliographie et entrer les clés de références.

`Templates` : il contient les styles et modèles utilisés pour les productions en sortie de flux. À savoir : les fichiers `.csl` pour les styles bibliographiques utilisés, les fichiers `css` utilisés pour sortir des fichiers `HTML` ou certains `PDF`, ou encore des modèles `.odt` qui me permettent de plaquer directement des styles à une sortie `.odt` « brute ».

`Notes` : Il s'agit de mes fichiers d'écriture selon la méthode Zettelkasten. 

`Travaux` : là où sont rédigés les textes destinés à sortir du flux, par exemple des articles destinés à la publication. Notons que les sorties elles-mêmes n'ont pas vocation à être versionnées avec Git (seuls les fichiers sources le sont), et sont donc exportées dans d'autres dossiers (sauvegardés sur Nextcloud).

## La conversion et la production

Pour convertir les fichiers markdown vers toutes sortes de formats (`odt`, `html`, `pdf`, `docx` pour l'essentiel), j'utilise [Pandoc](https://pandoc.org/). Ce dernier présente de nombreux avantages :

- une version de Pandoc est directement intégrée à Zettlr
* il permet d'utiliser les templates et les moteurs qui configurent la sortie voulue, par exemple un fichier `.css` qui sera utilisé avec le moteur de rendu [wkhtmltopdf](https://wkhtmltopdf.org/) pour produire un PDF avec une mise en page satisfaisante, ou encore un fichier `.tex` qui contient déjà les en-têtes voulus pour produire un beau PDF, etc. Voir pour tout cela le manuel de Pandoc.
* surtout il permet d'utiliser le moteur Citeproc et les fichiers CSL qui formatent la bibliographie dans le fichier de sortie. 

Cependant, j'utilise LaTeX et LibreOffice souvent en phase de pré-production. Par exemple, lorsque je soumets un article à un comité de lecture, ce dernier souhaite un fichier `odt` ou `docx` et propose des corrections sur ce fichier ; c'est donc LibreOffice qui entre en scène. Il faut alors  quitter le flux de production personnel pour envisager un nouveau flux. Autre situation : je convertis le markdown en `.tex` lorsque le modèle utilisé ne suffit pas à obtenir une mise en page spéciale et qu'il faut travailler cette fois directement avec LaTeX (bien que, en utilisant Pandoc, il soit possible d'intégrer directement du code LaTeX dans le fichier markdown).

Dans tous les cas, l'essentiel du travail se fait en amont et un article finalisé (et publié) réintègre toujours le flux pour en avoir une version markdown finale.

## Un mot sur les autres logiciels

* [Zotero](https://www.zotero.org/) : ce logiciel de gestion bibliographique est fort connu. Il présente l'avantage de pouvoir être utilisé dans les autres logiciels avec ses plugins : dans Firefox pour sauvegarder une référence automatiquement, dans Vim, dans Zettlr, dans LibreOffice. Il permet aussi de stocker les articles et autres sources référencées soit sur le serveur de stockage de Zotero (payant) soit sur un Nextcloud (via le protocole `webdav`).
* [Calibre](https://calibre-ebook.com/fr) : c'est un logiciel de gestion de livres numériques. Ma collection étant assez importante, il me fallait pouvoir gérer cela autrement qu'avec Zotero, d'autant plus que j'y branche ma liseuse ou ma tablette en vue d'une lecture sur un autre support que l'ordinateur.

# Astuces

Je ne vais pas ici vous expliquer comment configurer Vim au poil. Cela demanderait un développement assez long. Mais je vous conseille pour débuter le petit livret de Vincent Jousse, [Vim pour les humains](https://vimebook.com/fr). Vous y apprendrez comment prendre en main Vim rapidement et comment installer des plugins. Le reste ira très bien.

Pour commencer avec Zettlr, c'est plus simple : dès l'installation de ce logiciel un tutoriel très bien fait et entièrement en français vous sera proposé.

Le markdown en un tutoriel pour historiens : Dennis Tenen et Grant Wythoff, [Rédaction durable avec Pandoc et Markdown](https://programminghistorian.org/fr/lecons/redaction-durable-avec-pandoc-et-markdown).

Comment faire avec Zotero pour obtenir un fichier (`.bib` ou `.json`) de bibliographie qui se mettra à jour automatiquement ? Voici :

* Installez le Plugin [Better Bibtex](https://retorque.re/zotero-better-bibtex/) pour Zotero,
*  Dans Zotero, sélectionnez la bibliothèque que vous souhaitez exporter, clic droit puis `Exporter la bibliothèque`,
* Sélectionnez le format Better BibTeX Json et cochez la case `Garder à jour`
* Sauvegardez et entrez le chemin vers ce fichier dans Zettlr (ou dans votre `.vimrc` pour configurer le plugin Vim-Pandoc)
* Désormais, lorsque la bibliothèque Zotero subira des modifications, le fichier d’export sera toujours à jour (et vous retrouverez vos nouvelles références)

