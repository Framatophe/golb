---
title: "Je suis allé faire un tour sur les GCP à la mode... et j'en suis revenu"
date: 2025-01-07
lastmod: "2025-01-10"
author: "Christophe Masutti"
image: "/images/postimages/cm.png"
description: "Victime de « test-app porn » je reviens d'un tour d'horizon des logiciels de Personal Knowledge Management (GCP en français). Voici pourquoi j'ai appris que Zettlr était encore à privilégier, couplé à du Nextcloud, loin du bling-bling au demeurant fort sympathique des applications à la mode."
tags: ["Libres propos", "IA", "intelligence", "artificielle", "livre"]
categories:
- Libres propos
---


La gestion des connaissances personnelles (_personal knowledge management_) est une activité issue des sciences de gestion et s'est peu à peu diffusée dans les sphères privées...

<!--more-->


🔔 L'idée qui sous-tend cette approche des connaissances est essentiellement productiviste. Elle a donc des limites dont il faut être assez conscient pour organiser ses connaissances en définissant les objectifs recherchés et ceux qui, de toute façon, ne sont pas à l'ordre du jour. Mais à l'intérieur de ces limites, il existe toute une économie logicielle et une offre face à laquelle il est possible de perdre pied. Cela m'est arrivé, c'est pourquoi j'écris ce billet.

On peut définir les objectifs de la GCP grosso modo ainsi :

- Intégrer ses connaissances dans un ensemble cohérent afin de les exploiter de manière efficace (c'est pourquoi la connexion entre les connaissances est importante)
- Définir stratégiquement un cadre conceptuel permettant de traiter l'information
- Permettre l'acquisition de nouvelles connaissances qui enrichissent l'ensemble (ce qui suppose que le cadre doit être assez résilient pour intégrer ces nouvelles connaissances, quelle que soit leur forme).

La première information importante, c'est qu'on a tendance à réduire la plupart des logiciels en question à de simples outils de gestion de « notes », alors qu'ils permettent bien davantage que d'écrire et classer des notes. Si on regarde attentivement leurs présentations sur les sites officiel, chacun se présente avec des spécificités bien plus larges et nous incite davantage à organiser nos pensées et nos connaissances qu'à écrire des notes. Pour comparer deux outils propriétaires, là où un Google Keep est vraiment fait pour des notes simples, Microsoft Onenote contrairement à son nom, permet une vraie gestion dans le cadre d'une organisation.

Une autre information importante concerne la difficulté qu'il y a à choisir un logiciel adapté à ses usages. Surtout lorsqu'on utilise déjà un outil et qu'on a de multiples écrits à gérer. Changer ses pratiques suppose de faire de multiples tests, souvent décevants. Ainsi, un logiciel fera exactement ce que vous recherchez... à l'exception d'une fonctionnalité dont vous avez absolument besoin.

⚠️ Aucun logiciel de GCP ne fera exactement ce que vous recherchez : préparez-vous à devoir composer avec l'existant.

## À la recherche du bon logiciel

Je vais devoir ici expliquer mon propre cas, une situation que j'ai déjà présentée [ici](https://golb.statium.link/post/20240707organisation-production/). Pour résumer : j'adopte une méthode [Zettelkasten](https://fr.wikipedia.org/wiki/Zettelkasten), j'ai des textes courts et longs, il s'agit de travaux académiques pour l'essentiel (fiches de lecture, notes de synthèse, fiches propectives, citations... etc.). Parmi ces documents, une grande part n'a pas pour objectif d'être communiquée ou publiée. Or, comme l'un de mes outils est Zettlr, et que ce dernier se présente surtout comme un outil de production de notes et de textes structurés, je me suis naturellement posé la question de savoir si mes notes n'auraient pas un intérêt à être travaillées avec un outil différent (tout en conservant Zettlr pour des travaux poussés).

Par ailleurs :

- Le markdown doit impérativement être utilisé, non seulement en raison de sa facilité d'usage, mais aussi pour sa propension à pouvoir être exporté dans de multiples formats,
- La synchronisation des notes entre plusieurs appareils est importante : ordinateur (pour écrire vraiment), smartphone (petites notes, marque-page, liste de tâches) et tablette (écrire aussi, notamment en déplacement),
- Les solutions d'export sont fondamentales, à la fois pour permettre un archivage et pour permettre une exploitation des documents dans d'autres contextes.

Le couple [Zettlr](https://www.zettlr.com/) - [Pandoc](https://pandoc.org/) m'a appris une chose très importante : éditer des fichiers markdown est une chose, les éditer en vue de les exploiter en est une autre. D'où la valeur ajoutée de Pandoc et des en-têtes Yaml qui permettent d'enrichir les fichiers et, justement, les exploiter de manière systématique.

Je suis donc parti, youkaïdi youkaïda, avec l'idée de trouver un logiciel d'exploitation de notes présentant des fonctionnalités assez conviviales pour faciliter leur accès, et aussi m'ouvrir à des solutions innovantes en la matière.

## Je n'ai pas été déçu

Non, je n'ai pas été déçu car il faut reconnaître que, une fois qu'on a compris l'approche de chaque logiciel, leurs promesses sont généralement bien tenues.

Je suis allé voir :

- Dans les logiciels pas libres du tout, [Obsidian](https://obsidian.md/) et [Workflowy](https://workflowy.com/).
- Dans les open source (et encore c'est beaucoup dire) : [Logseq](https://logseq.com/), [Anytype](https://anytype.io/).
- Dans le libre : [Joplin](https://joplinapp.org/) (et [Zettlr](https://www.zettlr.com/) que je connaissais déjà très bien).

Je ne vais pas présenter pour chacun toutes leurs fonctionnalités, ce serait trop long. Mais voici ce que j'ai trouvé.

Premier étonnement, c'est que c'est du côté open source ou privateur qu'on trouve les fonctionnalités les plus poussées de vue par graphe, et autres possibilités de requêtes customisables / automatisables, ou encore des analyse de flux (par exemple pour voir quel objet est en lien avec d'autres selon un certain contexte).

Second étonnement, concernant la gestion des mots-clé et des liens internes, points communs de tous les logiciels, il faut reconnaître que certains le font de manière plus agréable que d'autres. Ainsi on accorde beaucoup d'importance aux couleurs et aux contrastes, ce qui rend la consultation des notes assez fluide et efficace.

Bref, ça brille de mille feux. Les interfaces sont la plupart du temps bien pensées.

Anytype, le plus jeune, et qui a retenu le plus mon attention, a bénéficié pour son développement des critiques sur les limites des autres logiciels. Par exemple Obsidian qui est victime de ses trop nombreux plugins, reste finalement assez terne en matière de fonctions de base, là où Anytype propose d'emblée d'intégrer des documents, de manipuler des blocs, avec des couleurs, de créer des modèles de notes (on dit « objet » dans le vocabulaire de Anytype), des collections, etc.

## Alors, qu'est-ce qui coince ?

En tant que libriste, je me suis intéressé surtout à des logiciels open source prometteurs. *Exit* Obsidian, et concentration sur Logseq et Anytype.

Dans les deux cas, la cohérence a un prix, pour moi bien trop cher : on reste coincé dedans ! L'avantage d'écrire en markdown, comme je l'ai dit, est de pouvoir exploiter les connaissances dans d'autres systèmes, par exemple le traitement de texte lorsqu'il s'agit de produire un résultat final. Et comme il s'agit de texte, la pérennité du format est un atout non négligeable.

Or, que font ces logiciels en matière d'export ? Du PDF peu élaboré mais dont on pourrait se passer s'il était possible d'exploiter correctement une sortie markdown... mais l'export markdown, en réalité, appauvrit le document au lieu de l'enrichir. Vous avez bien lu, oui 🙂

**Exemple** – Avec Anytype, j'ai voulu créer des modèles de fiches de lecture avec des champs couvrant des métadonnées comme : l'auteur, l'URL de la source, la date de publication, le lien avec d'autres fiches, les tags, etc. Tout cela avec de jolies couleurs... À l'export markdown, toutes ces données disparaissent et ne reste plus que la fiche dans un markdown approximatif. Résultat : mon fichier n'est finalement qu'un contenant et toutes les informations de connexion ou d'identification sont perdues si je l'exporte. À moins d'entrer ces informations en simple texte, ce qui rend alors inutiles les fonctions proposées. (Une difficulté absente avec Obsidian qui laisse les fichiers dans un markdown correct et ajoute des en-têtes yaml utiles, à condition d'être rigoureux).

![Clic droit pour agrandir](/images/markdownanytypezettlr.png)

Avec Logseq comme avec Anytype, vous pouvez avoir une superbe présentation de vos notes avec mots-clés, liens internes, rangement par collection, etc... sans que cela puisse être exploitable en dehors de ces logiciels. L'export markdown reste succinct, parfois mal fichu comme Anytype : des espaces inutiles, des sauts de lignes négligés, élimination des liens internes, plus de mots clé, et surtout aucun ajout pertinent comme ce que pourrait apporter un en-tête Yaml qui reprendrait les éléments utilisés dans le logiciel pour le classement.

Vous allez me dire : ce n'est pas le but de ces logiciels. Certes, mais dans la mesure où, pour exploiter un document, je dois me retaper la syntaxe markdown pour la corriger, autant rester avec Zettlr qui possède déjà des fonctions de recherche et une gestion des tags tout en permettant d'utiliser les en-têtes Yaml qui enrichissent les documents. Ha... c'est moins joli, d'accord, mais au moins, c'est efficace.

Et c'est aussi pourquoi Joplin reste encore un modèle du genre. On reste sur du markdown pur et dur. Là où Joplin est critiquable, c'est sur le choix de l'interface : des panneaux parfois encombrants et surtout une alternance entre d'un côté un éditeur Wysiwyg et de l'autre un éditeur markdown en double panneau, très peu pratique (alors que la version Android est plutôt bien faite).

Joplin et Zettlr n'ont pas de fioritures et n'offrent pas autant de solutions de classements que les autres logiciels... mais comme on va le voir ces « solutions » ne le sont qu'en apparence. Il y a une bonne dose de technosolutionnisme dans les logiciels de GCP les plus en vogue.

## La synchronisation et le partage

Pouvoir accéder à ses notes depuis plusieurs dispositifs est, me semble-t-il, une condition de leur correcte exploitation. Sauf que... non seulement il faut un système de synchronisation qui soit aussi sécurisé, mais en plus de cela, il faut aussi se demander en qui on a confiance.

Anytype propose une synchronisation chiffrée et P2P par défaut, avec 1Go offert pour un abonnement gratuit et d'autres offres sont ou seront disponibles. Logseq propose une synchronisation pour les donateurs. Quant à Obsidian, il y a depuis longtemps plusieurs abonnements disponibles. On peut noter que tous proposent de choisir le stockage local (et gratuitement) sans synchronisation.

En fait, la question se résume surtout au chiffrement des données. Avec ces abonnements, même si Anytype propose une formule plutôt intéressante, vous restez dépendant•e d'un tiers en qui vous avez confiance... ou pas. Le principal biais dans ces opportunités, c'est que si vous pouvez stocker vos coffres de notes sur un système comme Nextcloud (à l'exception de Anytype), accéder aux fichiers via une autre application est déconseillé : indexation, relations, champs de formulaires... bricoler les fichiers par un autre moyen est source d'erreurs. Par ailleurs, sur un système Android, Anytype, Obsidian ou Logseq n'offrent pas la possibilité d'interagir avec un coffre situé dans votre espace Nextcloud.

⚠️ (màj) Dans [ce fil de discussion](https://bitbang.social/@loadhigh/113786983610802113) sur Mastodon, un utilisateur a testé différents logiciels de GCP et a notamment analysé les questions de confidentialité des données. Le moins que l'on puisse dire est que Anytype remporte une palme. Je cite @loadhigh : _« Le programme enregistre toutes vos actions et les envoie toutes les quelques minutes à Amplitude, une société d'analyse commerciale. Cela est [mentionné dans la documentation](https://doc.anytype.io/anytype-docs/data-and-security/analytics-and-tracking), mais il n'y a pas de consentement ni même de mention dans le programme lui-même ou dans la politique de confidentialité. Il communique également en permanence avec quelques instances AWS EC2, probablement les nœuds IPFS qu'il utilise pour sauvegarder votre coffre-fort (crypté) de documents. (...) Le fait qu'il n'y ait pas d'option de refus, ni de demande de consentement, ni même d'avertissement est inacceptable à mes yeux. Pour une entreprise qui aime parler de confiance, il est certain qu'elle n'a aucune idée de la manière de la gagner. »_ Je souscris totalement à cette analyse !

De fait, la posture « local first » est en vérité la meilleure qui soit. Vous savez où sont stockés vos documents, vous en maîtrisez le stockage, et c'est ensuite seulement que vous décidez de les transporter ou de les modifier à distance.

Sur ce point Joplin a la bonne attitude. En effet, Joplin intègre non seulement une synchronisation Nextcloud, y compris dans la version pour Android, mais en plus de cela, il permet de choisir une formule de chiffrement. On peut aussi stocker sur d'autres cloud du genre Dropbox ou prendre un petit abonnement « Joplin cloud ». En somme, vous savez où vous stockez vos données et vous y accédez ensuite. Si on choisi de ne pas chiffrer (parce que votre espace Nextcloud peut être déjà chiffré), il est toujours possible d'accéder aux fichiers de Joplin et les modifier via une autre application. Joplin a même, dans l'application elle-même, une option permettant d'ouvrir une application externe de son choix pour éditer les fichiers.

## Local first

Il m'a fallu du temps pour accepter ces faits. J'avais même commencé à travailler sérieusement avec Anytype... et c'est lorsque j'ai commencé à vouloir exporter que cela s'est vraiment compliqué.  Sans compter la pérennité des classements : si demain Anytype, Logseq ou même Obsidian ferment leurs portes, on aura certes toujours accès à l'export markdown (quoique dans un état peu satisfaisant) mais il faudra tout recommencer.

Que faire ? je me suis mis à penser un peu plus sérieusement à mes pratiques et comme je dispose déjà d'un espace Nextcloud, j'ai choisi de le rentabiliser. La solution peut paraître simpliste, mais elle est efficace.

Elle consiste en deux dossiers principaux (on pourrait n'en choisir qu'un, mais pour séparer les activités, j'en préfère deux) :

- un dossier `Zettel` où j'agis comme d'habitude avec Zettlr (et pour les relations, comme expliqué dans la [documentation](https://docs.zettlr.com/fr/academic/zkn-method/)) en mettant davantage l'accent sur les mots-clé et en exploitant de manière plus systématique les fonctions de recherche.
- Un dossier `Notes` destiné à la prise de notes courtes, comme on peut le faire avec un téléphone portable.

En pratique :

- Les deux dossiers sont synchronisés avec Nextcloud.
- Sur Zettlr en local, j'ouvre les deux dossiers comme deux espaces de travail et je peux agir simultanément sur tous les fichiers.
- Depuis le smartphone et la tablette, j'ai accès à ces deux dossiers pour modifier et créer des fichiers via l'application Notes de Nextcloud, tout simplement, et toujours en markdown. Je fais aussi pointer l'application Nextcloud Notes précisément sur le dossier `Notes`.
- Sachant que ces fichiers contiennent eux-mêmes les tags et qu'on peut ajouter d'autres données via un en-tête Yaml, je dispose des informations suffisantes pour chaque fichier et je peux aussi en ajouter, que j'utilise Zettlr ou toute autre application.

Les limites :

- Sur smartphone ou tablette je n'ai pas l'application Zettlr et ne peut donc pas exploiter ma base de connaissances comme je le ferais sur ordinateur. Mais... aucun de ces dispositifs n'est fait pour un travail long de consultation.
- Sur un autre ordinateur, je peux accéder à l'interface en ligne de Nextcloud et travailler dans ces dossiers, mais là aussi, c'est limité. Par contre je peux utiliser la fonction de recherche unifiée.
- Gérer les liens de connexion entre les fichiers (par lien internes ou tags) demande un peu plus de rigueur avec Zettlr, mais reste très efficace.

## Ce qui manque aux autres, je le trouve dans zettlr

Quant à Zettlr, il me permet tout simplement de faire ce que les autres applications ne permettent pas (ou alors avec des plugins plus ou moins mal fichus) :

- utiliser une base de donnée bibliographique (et Zotero),
- réaliser des exports multiformats avec de la mise en page (et avec Pandoc intégré),
- les détails, comme une gestion correcte des notes de bas de page,
- les modèles « snippets » qui simplifient les saisies répétitives,
- l'auto-correction à la carte,
- les volets de Zettlr (table des matières, fichiers connexes, biblio, fonction recherche etc.)

## Les tâches et Kanban

C'est sans doute les point les plus tendancieux.

La plupart des logiciels de GCP intègrent un système de gestion de liste de tâches. Il s'agit en fait de pousser la fonction markdown `- [ ] tâche bidule` en lui ajoutant deux types d'éléments :

- l'adjonction automatique de date et de tags (à faire, en cours, réalisé, etc...)
- le classement par requêtes permettant de gérer les tâches et tenir à jour ces listes.

Le tout est complété par l'automatisation de tableaux type Kanban, très utiles dans la réalisation de projets.

C'est ce qui fait que ces logiciels de GCP se dotent de fonctions qui, selon moi, ne sont pas de la GCP mais de la gestion de projet. Si l'on regarde de près les systèmes privateurs intégrés comme chez Microsoft on constate que le jeu consiste à utiliser plusieurs logiciels qui interopèrent entre eux (et qui rendent encore plus difficile toute migration). Mais de la même manière, selon moi, un logiciel de gestion de projet ne devrait faire que cela, éventuellement couplé à une gestion de tâches.

On peut néanmoins réaliser facilement un fichier de tâches en markdown, ainsi (selon le rendu markdown du logiciel, les cases à cocher seront interactives) :

```

- [ ] penser à relire ce texte 🗓️15/12/2025
- [x] acheter des légumes 🗓️ aujourd'hui
- [ ] etc.
```

Mais qu'en est-il de la synchronisation de ces tâches sur un smartphone, par exemple ? N'est-il pas plus sage d'utiliser un logiciel dédié ? Si par contre les tâches concernent exclusivement des opérations à effectuer dans le processus de GCP, alors le markdown devrait suffire.

## Sobriété

OK… C'est pas bling-bling et ni Zettlr ni Notes pour Nextcloud n'ont prétendu être la solution ultime pour la GCP. Par exemple, ma solution n'est sans doute pas appropriée dans un milieu professionnel. Dans ce dernier cas, cependant, il conviendra de s'interroger sérieusement sur la pérennité des données : si le logiciel que vous utilisez a une valeur ajoutée, il faudrait pouvoir la retrouver dans l'export et la sauvegarde. Aucun logiciel n'est assuré de durer éternellement.

Si, en revanche, vous êtes attiré•e par un logiciel simple permettant d'écrire des notes sans exigence académique, des compte-rendus de réunion, des notes de lectures et sans chercher à exploiter trop intensément les tags et autres liens internes, alors je dirais que Joplin est le logiciel libre idéal : il fonctionne parfaitement avec Nextcloud pour la synchronisation, le markdown est impeccable, l'application pour Android fonctionne très bien. Et il y a du chiffrement. Ne cherchez pas à utiliser les plugins proposés, car ils n'apportent que peu de chose. Quant à l'interface, elle souffre, je pense, d'un manque de choix assumés et gagnerait à n'utiliser que le markdown enrichi (à la manière de Zettlr) et sans double volet.

Pour ma part, après ce tour d'horizon – qui m'a néanmoins donné quelques idées pour l'élaboration de mes propres notes --, Zettlr reste encore mon application favorite… même si elle est exigeante.  😅 Pour les passionnés de Vim ou Emacs et de Org-mode… oui, je sais, ce sera difficile de faire mieux…
