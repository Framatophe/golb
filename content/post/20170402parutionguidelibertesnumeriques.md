---
title: "Parution de Libertés numériques"
date: 2017-04-02
author: "Christophe Masutti"
image: "/images/postimages/manueldumo.png"
description: "Libertés numériques. Guide des bonnes pratiques à l'usage des DuMo."
tags: ["Logiciel libre", "Livre", "Publication", "Libertés"]
categories:
- Libres propos
- Logiciel libre
---


Et voici le guide *Libertés numériques. Guide des bonnes pratiques à l'usage des DuMo* (coll. Framabook, version août 2017). Un inventaire des bonnes pratiques numériques à la portée de tous. Un ouvrage dans [collection Framabook](https://framabook.org/libertes-numeriques/)).


Nous utilisons nos terminaux, nos ordinateurs, nos téléphones portables comme nous l’avons appris, ou pas. Pourtant, pour bien des gens, ces machines restent des boîtes noires. C’est le cas des Dupuis-Morizeau, une famille imaginaire que nous citons souvent à Framasoft. Elle correspond, je crois, assez bien à une réalité : des personnes qui utilisent les réseaux et les outils numériques, souvent même avec une certaine efficacité, mais qui ne sont pas autonomes, dépendent des services des grands silos numériques du web, et sont démunis face à tout ce contexte anxiogène de la surveillance, des verrous numériques, des usages irrespectueux des données personnelles… C’est à eux que s’adresse cet ouvrage, dans l’intention à la fois de dresser un petit inventaire de pratiques numériques mais aussi d’expliquer les bonnes raisons de les mettre en œuvre, en particulier en utilisant des logiciels libres.

Pour une autre présentation de cet ouvrage, vous pouvez lire l'interview [paru sur le Framablog](https://framablog.org/2017/03/23/internet-pour-les-quarks/) où j'explique les tenants et aboutissants de ce projet.

Quelques liens :


- pour télécharger le guide [Libertés numériques sur Framabook](https://framabook.org/libertes-numeriques/),
- acheter [la version papier chez Lulu](http://www.lulu.com/shop/product-23097192.html),
- lire [le guide en ligne](https://docs.framasoft.org/fr/manueldumo/index.html),
- participer à son amélioration [sur son dépôt Git](https://framagit.org/Framatophe/manueldumo).


> Grands utilisateurs d’outils et de services numériques, les Dupuis-Morizeau (DuMo) sont pourtant entourés de boîtes noires. Installer, configurer, sauvegarder, envoyer, souscrire, télécharger… autant d’actions que, comme les DuMo, nous ne maîtrisons pas toujours dans un environnement dont nous sommes au mieux les acteurs passifs, au pire les prisonniers.
> 
> Avec des exemples concrets, ce manuel accompagne l’utilisateur au quotidien. Il montre comment l’utilisation de logiciels libres est l’une des clés d’une informatique domestique maîtrisée. Quels logiciels choisir et pourquoi ? quelle confiance accorder aux services en réseau ? qu’est-ce que la confidentialité dans les communications ? Autant de sujets vulgarisés permettront au lecteur de faire bon usage de ses libertés numériques.


