---
title: "Vosges opération libre"
date: 2014-05-23
author: "Christophe Masutti"
image: "/images/postimages/terminal.png"
tags: ["Logiciel libre", "Vosges", "Gérardmer"]
description: "Vosges Opération Libre : voilà, c’est fini… et c’était formidable ! En commençant ce projet, je n'aurais jamais parié sur autant d'enthousiasme de la part des participants et des visiteurs !"
categories:
- Logiciel libre
---

Vosges Opération Libre : voilà, c’est fini… et c’était formidable ! En commençant ce projet, je n'aurais jamais parié sur autant d'enthousiasme de la part des participants et des visiteurs !

Le week-end des 17 et 18 mai 2014, près de 200 visiteurs sont venus se joindre aux membres du collectif d’associations et de SSLL qui présentaient à Gérardmer un panel très complet du Libre, de ses enjeux et de ses applications dans les domaines de la culture, de la technique et des données ouvertes. Profitant au mieux du cadre ensoleillé de la Villa Monplaisir et de l’Espace Lac, les visiteurs ont pu assister à des conférences de premier ordre, adaptées au Grand Public, et consulter sur leurs stands plusieurs associations et SSLL, et même participer à des ateliers thématiques. Le partage et la collaboration furent à l’honneur, qu’il s’agisse des données cartographiques, de la connaissance, et jusque dans les domaines les plus techniques de l’impression 3D, de la scannerisation, des réseaux…

Le public aura de même apprécié le grand dynamisme contagieux qui règne dans le Libre. Le symptôme est une prédiction: attendez-vous dans les prochaines saisons à d’autres événements fortement liés à cette première session introductive! Pari gagné !

![Les participants enthousiastes (CC-By Violaine)](/images/groupe_gerardmer_vol_20140517_by_violaine_licence_cc-by_d.jpg)

