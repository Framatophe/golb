---
title: "Affaires Privées : en attendant"
date: 2020-03-17
author: "Christophe Masutti"
image: "/images/postimages/affpriv-banner.jpg"
description: "En attendant les envois. Parution de mon ouvrage. Affaires Privées. Aux sources du capitalisme de surveillance, CF éditions, mars 2020."
tags: ["Histoire", "Libres propos", "Capitalisme de surveillance", "Libertés"]
categories:
- Libres propos
---



En raison de l'épidémie qui touche notre pays actuellement, l'éditeur *C&F éditions* n'est pas encore en mesure d'effectuer les envois des exemplaires précommandés de mon ouvrage *Affaires Privées. Aux sources du capitalisme de surveillance*. En attendant, voici quelques éléments pour vous mettre en bouche...

<!--more-->

Tout d'abord, je voudrais remercier ceux qui comptent précommander l'ouvrage avant même sa sortie et toutes les critiques qui ne manqueront pas de le dézinguer (ou pas&nbsp;!). C'est important pour moi parce que cela montre dans quelle mesure le livre est attendu (histoire de me rassurer un peu), et c'est important pour l'éditeur qui joue tout de même sa réputation en publiant un livre qui ne soit pas totalement hors de propos. 

Sachez aussi qu'en raison de la situation sanitaire Covid-19, l'éditeur C&F éditions a choisi d'allonger la durée de souscription (donc avec une petite remise sur le prix final). [Allez-y, n'hésitez plus&nbsp;!](https://cfeditions.com/masutti/)

C'est presque Noël&nbsp;! l'éditeur a aussi décidé d'accélérer la fabrication de la version epub de l'ouvrage. Elle sera garantie sans DRM (on dirait une publicité pour légumes bio quand on dit des choses pareilles&nbsp;!). Donc ceux qui ont choisi la souscription, et n'auront pas la joie de découvrir l'ouvrage dans leur boîte à lettres dans un délai raisonnable, pourront toujours se réconforter avec la version électronique.


<hr />

Pour terminer, et si vous avez les crocs, voici ci-dessous quelques liens vers des billets de ce blog qui concernent *Affaires Privées*. Ils sont loin (très loin) de présenter un aperçu général de l'ouvrage (475 pages, tout de même, c'est pas si cher au kilo&hellip;  pour suivre la métaphore des légumes bio). Par ailleurs il y a eu des modifications substantielles entre le moment où j'ai écrit ces billets et la publication du livre, alors ne soyez pas sévères&hellip;

Dans un premier temps voici deux billets qui transcrivent des passages complets du livre. Le premier concerne le projet Cybersyn, à une époque où le fait qu'un gouvernement puisse posséder un pouvoir de surveillance «&nbsp;total&nbsp;» n'était pas si mal vu. Le second est un point de vue plus théorique sur notre modernité à l'ère de la surveillance et qui reprend un passage où j'expose comment je vais chercher chez Anthony Giddens quelques clés de lecture.

- [Cybersyn / techno-socialisme](https://golb.statium.link/post/20190114cybersyn/)
- [Ce que le capitalisme de surveillance dit de notre modernité](https://golb.statium.link/post/20190822capitsurvgiddens/)

Les deux billets suivants font partie de l'argumentation qui me situe par rapport au récent livre de Shoshana Zuboff, *The Age of Surveillance Capitalism* qui a fait l'objet d'une grande couverture médiatique. **Attention**&nbsp;: il ne s'agit absolument pas d'une argumentation complète, mais le format «&nbsp;billet&nbsp;» pourra peut-être aider à une meilleure compréhension lorsque vous accrocherez la dernière partie de l'ouvrage.

- [Définitions du capitalisme de surveillance](https://golb.statium.link/post/20181115definitionscapitalismesurveillance/)
- [Fuckapital](https://golb.statium.link/post/20190607fuckapital/)

Alors, je vous souhaite bonne lecture en attendant le pavé dans la mare&hellip;

{{< figure src="/images/Prem-couv-masutti-affaires-privees.jpg" title="Christophe Masutti, Affaires Privées. Aux sources du capitalisme de surveillance (couverture)" >}}

<hr />

**Commander sur le site de l'éditeur : [C&F éditions](https://cfeditions.com/masutti/)**

<hr />

