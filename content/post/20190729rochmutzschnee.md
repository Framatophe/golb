---
title: "Rocher de Mutzig-Schneeberg"
date: 2019-07-29
author: "Christophe Masutti"
image: "/images/postimages/naturel.jpg"
description: "Un parcours pour vététistes confirmés sur deux sommets alsaciens mythiques&nbsp;: le Rocher de Mutzig et le Schneeberg. Ceux qui n'aiment pas les dévers et les longues montées, passez votre chemin."
tags: ["Sport", "VTT", "Cross-country", "Entraînement", "Parcours"]
categories:
- Sport
---




Après plusieurs tournées du côté de ce massif fort connu des randonneurs ([Rocher de Mutzig](https://fr.wikipedia.org/wiki/Rocher_de_Mutzig), [Schneeberg](https://fr.wikipedia.org/wiki/Schneeberg_(Vosges))), j'ai pu élaborer ce parcours réservé toutefois aux vététises confirmés. Il fera appel à un pilotage un peu engagé en cross-country (les descendeurs trouveront cela évidemment très facile). Les passages délicats seront signalés pour vous faire une idée précise. Des points de vue exceptionnels vous attendent et beaucoup de plaisir en descente.

La première remarque&nbsp;: ceux qui ont peur des dévers et n'aiment pas les longues montées monotones, passez votre chemin. Oui, le plaisir a un prix. Cependant, la distance totale est courte. Il ne s'agit que de 30&nbsp;km avec un peu plus de 1100&nbsp;m de dénivelé positif, du moins pour ce qui concerne la version «&nbsp;rapide&nbsp;» du parcours.

Une seconde remarque&nbsp;: sur la zone en question, notamment du côté de la commune de Wangenbourg-Egenthal, il existe tout un ensemble de circuits VTT fléchés (malheureusement la plupart du temps sur des grands chemins, cadre légal oblige), et nous en croiserons certains. Alors pourquoi un tel circuit supplémentaire&nbsp;? Parce qu'il est conçu pour l'entraînement aussi&nbsp;: l'idée est de cumuler en peu de temps un maximum de dénivelé positif. Mais pour éviter les aspects pénibles d'un parcours uniquement dédié à cela, la zone offre une variété de terrain, ce qui romp la monotonie. Quant aux descentes, elles se feront de manière ludique.

Notez aussi que le [Rocher de Mutzig](https://fr.wikipedia.org/wiki/Rocher_de_Mutzig) est d'abord le royaume des randonneurs&nbsp;: soyez courtois et discret lorsque vous en croisez, en particulier sur les petits sentiers. Cela fait aussi partie des qualités de pilote que de rouler en sécurité. 

Habituellement, sur le Rocher de Mutzig, la montée s'effectue via Lutzelhouse et descente du côté Porte de Pierre et col du Wildberg. Nous ferons tout l'inverse pour la première partie du parcours.

Le départ se fait depuis Oberhaslach. Garez-vous sur le parking de l'école, en tournant dans la petite rue au niveau de la mairie du village. Notez la présence de la fontaine, en façade, qui peut s'avérer utile en cas de terrain trop gras (mais ne faites pas ce circuit en temps de pluie).

![Rejoindre le parcours](/images/rochmutzvoiture.png)

La carte du parcours ci-dessous montre le chemin le plus rapide du parcours. Je mentionne les variantes possibles.

## La montée au Rocher de Mutzig

La montée sera longue. Dirigez-vous dans un premier temps (disques rouges) vers la maison forestière Weinbaechel. Dépassez cet endroit, puis suivez le chemin forestier. Tournez à gauche à l'intersection du GR532 et suivez le chemin forestier jusqu'au col du Wildberg. Variante&nbsp;: prenez les croix jaunes jusqu'au col du Wilberg avec un bon coup de mollet (80&nbsp;m de dénivelé un peu raide) mais des récents travaux forestiers ont un peu gaché le chemin.

Au col du Wildberg, vous avez le choix&nbsp;:

1. Suivez les croix bleues sur le chemin forestier puis les croix jaunes jusqu'à la Porte de Pierre. C'est à mon avis le plus simple. Une montée raide de 50&nbsp;m vous attend une fois quittée le chemin forestier, et un seul portage juste en dessous de la Porte de Pierre.
2. Ou bien suivez les croix jaunes d'emblée pour monter le Petit Katzenberg, mais il y aura plusieurs portages (au moins 3). 

Juste après la Porte de Pierre et après avoir franchi quelques racines pénibles, vous pouvez monter sans problème jusqu'au Rocher de Mutzig. Le sentier est pierreux, il fera appel à votre sens de l'équilibre et à votre explosivité.


## Du Rocher de Mutzig au Schneeberg

La descente du Rocher de Mutzig est très technique. Si vous ne le sentez pas, aucun problème, repartez sur vos pas et empruntez le chemin forestier jusqu'au Col du Narion. La descente technique, elle, se fait tranquillement une fois le terrain reconnu une ou deux fois auparavant. Si c'est la première fois que vous l'empruntez, ne présumez pas de vos réactions.

Attention aux randonneurs surtout ceux qui montent&nbsp;: s'ils ne sont pas vététistes, ils ne peuvent pas savoir où se placer pour vous laisser passer. Alors freinez, et laissez-les monter.

Depuis le Col du Narion, rejoignez le Altmatt via le large chemin forestier. Là encore vous avez le choix pour rejoindre le Elsassblick. 

Le plus simple est de contourner le Grossmann par la Maison Forestière du Grossmann, prendre les croix bleues et le chemin forestier jusque Elsassblick.

Le plus amusant est d'emprunter la variante GR (rectangles blancs et rouges) qui est très jolie à cet endroit. Là vous aurez un festival de petits frissons. Certains passages avec un fort dévers (c'est carrément un ravin) vous laissent quelques 25&nbsp;cm de chemin. Prudence. Un arbre a d'ailleurs été fourdroyé et a littéralement cassé le chemin&nbsp;: le portage peu être un peu délicat. Heureusement, il ne s'agit pas d'une descente&nbsp;: le chemin reste à niveau donc le risque est tout de même largement amoindri.

Surtout si vous passez à plusieurs, deux conseils&nbsp;: envisagez la présence des randonneurs (je me répète, je sais) et surtout restez attentifs, ne discutez pas, concentrez-vous sur ce que vous faites, pédale haute vers l'amont. Et tout se passera très bien.

![vue depuis le rocher de Mutzig](/images/rochmutzig.jpg)

Ensuite suivez toujours les rectangles blancs et rouges&nbsp;: prenez la route forestière sur quelques centaines de mètres et tournez vers le Eichkopf. Suivez toujours le GR mais méfiez-vous il faudra quitter le chemin large pour passer sous le Urstein (on loupe facilement cette bifurcation). 

Ensuite, passez le col du Hoellenwasen, suivez toujours les rectangles blancs et rouges. Attention, un passage avec une descente très technique sur 40&nbsp;m&nbsp;: je conseille même de descendre du vélo à cette occasion. Toujours sur le GR vous allez remonter, juste après cette descente, sur un chemin assez large. Vous quitterez ce chemin par les disques bleus pour rejoindre le GR 53 et monter au Schneeberg. Montez jusqu'au pied des rochers en franchissant les escaliers.


## Descendre du Schneeberg


Une fois en haut des escaliers, il y a une petite place où vous pouvez faire une pause. Prenez ensuite un chemin non balisé qui serpente dans les myrtilles. C'est une descente un peu technique mais sans obstacle, traçée par d'autres vététistes, je suppose. Elle rejoint le GR en contrebas et le balisage des croix rouges.

Là encore vous avez le choix. 

Soit vous prenez à gauche, en descente, jusqu'au croisement avec le chemin forestier des Pandours, que vous pouvez alors descendre tranquillement jusqu'au col du même nom. C'est du chemin large...

Soit vous prenez à droite et remontez un peu pour reprendre le sentier balisé croix rouges, passer l'abri du Schneeberg et rejoindre le chemin forestier des Pandours. La descente est encore une fois assez technique, et il y aura deux portages au moment de croiser d'autres chemins forestiers. 


## Col des Pandours

C'est la dernière étape après avoir traversé la route départementale 218. En empruntant le chemin forestier, vous suivrez les rectangles jaunes et blancs jusqu'au carrefour Anlagen puis les rectangles bleu (GR 531) jusqu'à Oberhaslach. Là c'est facile&nbsp;: c'est une très belle descente sur un sentier plutôt joueur. Une manière de terminer cette balade sur une note agréable.

Variante&nbsp;: du col des Pandours, poussez via les croix rouges vers le Carrefour du Brigadier Jérôme puis allez jusqu'au rocher du Pfaffenlap (très jolie vue). Ensuite, soit vous revenez sur le Carrefour Anlagen soit vous passez sur le Breitberg et, toujours en suivant les croix rouges, vous redescendez sur Oberhaslach via un sentier un peu moins joueur que l'autre (vous arrivez alors juste au-dessus de la Maison Forestière Ringenthal). 

Dans les deux cas, reprenez les croix jaunes et rectangles bleus jusqu'au parking. Le tour est fini.


<iframe width="100%" height="300px" frameborder="0" allowfullscreen src="https://umap.openstreetmap.fr/fr/map/tour-vtt-rocher-de-muzig-schneeberg_351057?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=databrowser&captionBar=false"></iframe><p><a href="https://umap.openstreetmap.fr/fr/map/tour-vtt-rocher-de-muzig-schneeberg_351057">Voir en plein écran</a></p>





