
---
title: "Oui au chiffrement, non à la liberté"
date: 2017-05-12
author: "Christophe Masutti"
image: "/images/postimages/brain.png"
description: "La stratégie des macronistes en vue de limiter nos libertés tout en promouvant le chiffrement"
tags: ["Libres propos", "Politique", "Libertés", "Numérique", "Logiciel libre"]
categories:
- Libres propos
---


Dans cet article, nous allons voir comment la stratégie gouvernementale en matière de chiffrement suppose de limiter nos libertés numériques pour favoriser une économie de la norme à l'exclusion du logiciel libre. Telle est la feuille de route d'Emmanuel Macron.
>

Il y a quelque temps, j'ai écrit un article intitulé «&nbsp;Le contrat social fait 128 bits… ou plus&nbsp;». J'y interrogeais les velléités gouvernementales à sacrifier un peu plus de nos libertés afin de pouvoir mieux encadrer les pratiques de chiffrement au nom de la lutte contre le terrorisme. Le ministre Manuel Valls, à l'époque, faisait mention de pratiques de «&nbsp;cryptologie légales&nbsp;», par opposition, donc, à des pratiques illégales. Ce à quoi toute personne moyennement intelligente pouvait lui faire remarquer que ce n'est pas le chiffrement qui devrait être réputé illégal, mais les informations, chiffrées ou non. L'idée de Valls était alors de prévenir de l'intention du gouvernement à encadrer une technologie qui lui échappe et qui permet aux citoyens de protéger leur vie privée. L'ennemi, pour qui conçoit l'État sous une telle forme archaïque, ce n'est pas la vie privée, c'est de ne pas pouvoir choisir ceux qui ont droit à une vie privée et ceux qui n'y ont pas droit. De là à la lutte de classe, on n'est pas loin, mais nous ne suivrons pas cette direction dans ce billet…

Le chiffrement, c'est bien plus que cela. C'est la capacité à user du secret des correspondances. Et la question qui se pose est de savoir si un gouvernement peut organiser une intrusion systématique dans toute correspondance, au nom de la sécurité des citoyens.

## Le secret, c'est le pouvoir

C'est un choix cornélien. D'un côté le gouvernement qui, depuis Louis&nbsp;XI a fait sienne la doctrine *qui nescit dissimulare, nescit regnare* (qui ne sait dissimuler ne sait régner), principe érigé à l'état de science avec *le Prince* de Machiavel. Et de l'autre, puisque savoir  c'est pouvoir, la possibilité pour un individu d'échanger en secret des informations avec d'autres. De tout temps ce fut un jeu entre l'autorité absolue de l'État et le pouvoir relatif des individus que de savoir comment doit s'exercer le secret de la correspondance.

Après qu'on eut littéralement massacré la Commune de Paris, les anarchistes les plus vengeurs furent visés par les fameuses [Lois scélérates](https://fr.wikipedia.org/wiki/Lois_sc%C3%A9l%C3%A9rates), dont l'une d'entre elles fit dire à Jean Jaurès qu'elle se défini par «&nbsp;l’effort du législateur pour aller chercher l’anarchie presque dans les secrets de la conscience humaine&nbsp;». Après une chasse aux sorcières qui habitua peu à peu les Français à la pratique de la délation,  le mouvement des anarchistes (pacifistes ou non) ont dû se résigner à entretenir des modes d'organisation exerçant le secret, presque aussitôt contrecarrés par l'État de guerre qui s'annonçait. Depuis la fin du XIX<sup>e</sup> siècle, l'histoire des Républiques successives (mais c'est aussi valable hors la France) n'a fait que brandir le chiffon rouge de la sécurité des citoyens pour bâtir des législations afin d'assurer au pouvoir le monopole du secret.

Et heureusement. Le secret d'État, c'est aussi un outil de sa permanence. Qu'on soit pour ou contre le principe de l'État, il n'en demeure pas moins que pour exercer la sécurité des individus, le Renseignement (avec un grand R) est un organe primordial. Le fait que nous puissions discuter du secret d'État et qu'il puisse être régulé par des instances auxquelles on prête plus ou moins de pouvoir, est un signe de démocratie.

Néanmoins, depuis la Loi Renseignement de 2015, force est de constater une certaine conception «&nbsp;leviathanesque&nbsp;» du secret d'État dans la V<sup>e</sup> République. Il s'agit de la crainte qu'une solution de chiffrement puisse être étendue à tous les citoyens.

## Quand c'est le chiffrement qui devient un risque

En réalité c'est déjà le cas, ce qui valu quelques ennuis à Philip Zimmermann, le créateur de PGP au début des années 1990. Aujourd'hui, tout le monde peut utiliser PGP, y compris avec des clés de plus de 128 bits. On peut dès lors s'étonner de la tribune signée, entre autre, par le procureur de Paris dans le New York Times en août 2015, intitulée «&nbsp;[When Phone Encryption Blocks Justice](https://www.nytimes.com/2015/08/12/opinion/apple-google-when-phone-encryption-blocks-justice.html?_r=1">When Phone Encryption Blocks Justice)&nbsp;». Selon les auteurs, «&nbsp;Les nouvelles pratiques de chiffrement d’Apple et Google rendent plus difficile la protection de la population contre les crimes&nbsp;». Or, en réalité, le fond de l'affaire n'est pas là. Comment en vouloir à des vendeurs de smartphone de faciliter le chiffrement des données personnelles alors que les solutions de chiffrement sont de toute façon disponibles&nbsp;? Le travail de la justice ne serait donc facilité qu'à partir du moment où les criminels «&nbsp;oublieraient&nbsp;» simplement de chiffrer leurs données&nbsp;? Un peu de sérieux…

La véritable intention de cette tribune, elle est exprimée en ces termes : «&nbsp;(…) les régulateurs et législateurs de nos pays doivent trouver un moyen approprié d’équilibrer le gain minime lié au chiffrement entier des systèmes et la nécessité pour les forces de l’ordre de résoudre les crimes et poursuivre les criminels&nbsp;». Il faut en effet justifier par n'importe quel argument, même le moins crédible, une forme de dérégulation du Renseignement tout en accusant les pourvoyeurs de solutions de chiffrement d'augmenter les risques d'insécurité des citoyens. Le principal risque n'est plus le terrorisme ou le crime, qui devient une justification&nbsp;: le risque, c'est le chiffrement.

À partir de ces idées, les interventions d'élus et de hauts responsables se sont multipliées, [sans crainte du ridicule](http://www.numerama.com/politique/149077-lutte-contre-le-chiffrement-ciotti-surencherit-et-veut-interdire-liphone.html) faisant fi de tous les avis de la CNIL, comme  de l'Observatoire des libertés numériques ou même du Conseil national du numérique. Las, ce débat ne date pas d'hier, comme Guillaume Poupard [le rappelle en janvier 2015](http://www.lepoint.fr/chroniqueurs-du-point/guerric-poncet/cyberdefense-interdire-le-chiffrement-est-irrealiste-23-01-2015-1899054_506.php)&nbsp;: «&nbsp;Ce débat, nous l'avons eu il y a vingt ans. Nous avons conclu que chercher à interdire le chiffrement était à la fois passéiste et irréaliste.&nbsp;»

## Si on ne peut interdire, il faut encadrer : les backdoors

Voyant qu'une opposition frontale à la pratique même du chiffrement ne mène  à rien, en particulier parce que le chiffrement est un secteur économique à part entière, les discours se sont peu à peu infléchis vers une version édulcorée mais scélérate&nbsp;: imposer des *backdoors* obligatoires dans tout système de chiffrement autorisé. Ne pouvant interdire le chiffrement, il faut trouver le moyen de le contrôler. L'idée s'est insinuée petit à petit dans l'appareil d'État, jusqu'à commencer le travail d'une possible réforme.

C'est l'ANSSI (Agence nationale de la sécurité des systèmes d'information) qui réagit assez vertement, notamment dans une lettre diffusée par le journal [Libération en août 2016](http://www.liberation.fr/france/2016/08/02/controler-le-chiffrement-un-calcul-difficile-pour-le-gouvernement_1469978)[^0], et signée de G. Poupard. Pour ce dernier, imposer des *backdoors* revient à affaiblir le niveau de sécurité général. On lit dans sa lettre&nbsp;:

> «&nbsp;Il convient de noter que les technologies robustes de cryptographie, longtemps réservées à une communauté très restreinte, sont aujourd'hui largement diffusées et relativement aisées à mettre en œuvre. Le développement de logiciels non contrôlables, faciles à distribuer et offrant un niveau de sécurité très élevé est par conséquent à la portée de n'importe quelle organisation criminelle.
> 
> Imposer un affaiblissement généralisé des moyens cryptographiques serait attentatoire à la sécurité numérique et aux libertés de l'immense majorité des utilisateurs respectueux des règles tout en étant rapidement inefficace vis-à-vis de la minorité ciblée.&nbsp;»


Difficile d'exprimer combien G. Poupard a raison. Pourtant le débat resurgit en février 2017, cette fois sous l'angle de la coopération franco-allemande. C'est une lettre officielle publiée par [Politico<](http://www.politico.eu/wp-content/uploads/2017/02/2017-02-17-De%CC%81claration-FR-DE-II_Officielle.pdf)[^1], signée des Ministères de l'Intérieur Français et Allemand, qui met en exergue la lutte contre le terrorisme et annonce que cette dernière…

> …requiert de donner les moyens juridiques aux autorités européennes afin de tenir compte de la généralisation du chiffrement des communications par voie électronique lors d'enquêtes judiciaires et administratives. La Commission européenne doit veiller à ce que des travaux techniques et juridiques soient menés dès maintenant pour étudier la possibilité de définir de nouvelles obligations à la charge des prestataires de services de communication par voie électronique tout en garantissant la fiabilité de systèmes hautement sécurisés, et de proposer sur cette base une initiative législative en octobre 2017.&nbsp;»


En d'autres termes, c'est aux prestataires qu'il devrait revenir l'obligation de fournir aux utilisateurs des solutions de communication chiffrées tout en maintenant des ouvertures (forcément secrètes pour éviter qu'elles soient exploitées par des malveillants, donc des *backdoors*) capables de fournir en clair les informations aux autorités.

Il est très curieux de découvrir qu'en ce début d'année 2017, la Commission Européenne soit citée comme garante de cette coopération franco-allemande, alors même que son vice-président chargé du marché numérique unique Andrus Ansip déclarait un an plus tôt&nbsp;: «&nbsp;[I am strongly against any backdoor to encrypted systems](https://www.euractiv.com/section/digital/interview/ansip-i-am-strongly-against-any-backdoor-to-encrypted-systems/)&nbsp;»[^2], opposant à cela les récentes avancées numériques pour lesquelles l'Estonie est connue comme particulièrement avant-gardiste.

## La feuille de route du gouvernement Macron

Nous ne sommes pas au bout de nos  surprises, puisqu'on apprend qu'en bon élève, notre Président E. Macron fraîchement élu, reprendra cette coopération franco-allemande. Interrogé à propos de la cybersécurité par le mathématicien Cédric Villani, il l'affirme en effet dans une vidéo [publiée par Science et Avenir](https://www.sciencesetavenir.fr/politique/video-quand-cedric-villani-et-emmanuel-macron-parlent-de-science-pour-sciences-et-avenir_112884) ([source directe](https://www.ultimedia.com/default/index/videogeneric/id/8k0rxf))&nbsp;:

> C. Villani : «&nbsp;J'ai une question sur le domaine numérique, qui va être plus technique, c'est la question sur laquelle on est en train de plancher au Conseil Scientifique de la Commission Européenne, ça s'appelle la cybersécurité. C'est un sujet sur lequel on nous a demandé de plancher en tant que scientifiques, mais on s'est vite aperçu que c'est un sujet qui ne pouvait pas être traité juste sous l'angle scientifique parce que les questions politiques, économiques, étaient mêlées de façon, heu, indémêlable et que derrière il y avait des questions de souveraineté de l'Europe par rapport au reste du monde…
> 
> E. Macron : «&nbsp;le sujet de la cybersécurité est un des axes absolument essentiel et on est plutôt en retard aujourd'hui sur ce sujet quand on regarde… Le pays qui est le plus en pointe,  c'est  Israël qui a fait un travail absolument remarquable en construisant carrément une cité de la cybersécurité, qui a un rapport existentiel avec cet aspect… et les États-Unis… nous sommes en retard… il en dépend de notre capacité à nous protéger, de notre souveraineté numérique derrière, et notre capacité à protéger nos systèmes puisqu'on agrège tout dans ce domaine-là.
> 
> Je suis assez en ligne avec la stratégie qui avait été présentée en France par Le Drian, sur le plan militaire. Le sujet, c'est qu'il faut le désenclaver du plan militaire. C'est un sujet qui dépend du domaine militaire, mais qui doit également protéger tous les systèmes experts civils, parce que si on parle de cybersécurité et qu'on va au bout, les systèmes scientifiques, les systèmes de coopération universitaire, les systèmes de relation entre les ministères, doivent être couverts par ce sujet cybersécurité. Donc c'est beaucoup plus inclusif que la façon dont on l'aborde aujourd'hui qui est juste «&nbsp;les systèmes experts défenses&nbsp;» et qui est très sectoriel. La deuxième chose, c'est que je pense que c'est une stratégie qui est européenne, et qui est au moins franco-allemande. Pourquoi&nbsp;? parce que, on le sait, il y a des sujets de standards, des sujets de normalisation qui sont assez techniques, et qui font que si on choisit une option technologique plutôt qu'une autre, en termes de chiffrement ou de spécifications, on se retrouve après avec des non-compatibilités, et on va se retrouver —&nbsp;on est les champions pour réussir cela&nbsp;— dans une bataille de chiffonniers entre la France et l'Allemagne. Si on veut avoir une vraie stratégie en la matière sur le plan européen, il faut d'abord une vraie stratégie franco-allemande, une vraie convergence normative, une vraie stratégie commune en franco-allemand, en termes, pour moi, de rapprochement des univers de recherche, de rapprochement des intérêts militaires, de rapprochement de nos experts en la matière. Pour moi c'est 1) un vrai sujet de priorité et 2) un vrai sujet d'organisation en interne et en externe. Quand je m'engage sur le 2% en Défense, j'y inclus le sujet cybersécurité qui est absolument critique.&nbsp;»


La position d'E. Macron entre donc dans la droite ligne de la satisfaction des députés en faveur d'une limitation du chiffrement dans les usages individuels, au profit a) d'une augmentation capacitaire du chiffrement pour les autorités de l'État et b) du renforcement des transactions stratégiques (bancaires, communicationnelles, coopérationnelles etc.). Cela ouvre à la fois des perspectives de marchés et tend à concilier, malgré les critiques et l'inefficacité, cette fameuse limitation par solution de *backdoors* interposée.

Pour que de telles solutions existent pour les individus, il va donc falloir se tourner vers des prestataires, seuls capables à la fois de créer des solutions innovantes et d'être autorisés à fournir au public des services de chiffrement «&nbsp;backdorisés&nbsp;».

Pour cela, c'est vers la [French Tech](http://www.lafrenchtech.com/) qu'il va falloir se tourner. Et cela tombe plutôt bien, puisque Mounir Mahjoubi, le directeur de la campagne numérique d'Emmanuel Macron et sans doute futur député, en faisait la promotion [le 11 mai 2017 sur France Inter](https://www.franceinter.fr/emissions/l-invite-de-8h20/l-invite-de-8h20-11-mai-2017)[^3], tout en plaidant pour que les «&nbsp;français créent une culture de la sécurité en ligne&nbsp;».

Il faut donc bien comprendre que, bien que les individus aient tout intérêt à chiffrer leurs communications au moins depuis l'Affaire Snowden, l'objectif est de les encourager&nbsp;:


- pour asseoir notre autonomie numérique nationale et moins prêter le flanc aux risques sécuritaires,
- dans le cadre de solutions de chiffrement backdorisées,
- en poussant des start-up et autres entreprises de la French Tech à proposer qui des logiciels de chiffrement qui des messageries chiffrées de bout en bout, conformes aux normes mentionnées par E. Macron et dont la coopération franco-allemande dressera les premiers étalons.


Dans ce domaine, il va de soi que les solutions de chiffrement en cours aujourd'hui et basées sur des logiciels libres n'auront certainement plus aucun doit de cité. En effet, il est contradictoire de proposer un logiciel libre doté d'une *backdoor* : il serait aussitôt modifié pour supprimer cette dernière. Et, d'un autre côté, si des normes viennent à être établies, elles seront décidées à l'exclusion de toute tentative de normalisation basée sur des chiffrements libres existants comme PGP, GNUPG…

## Si nous résumons

Il suffit de rassembler les idées&nbsp;:


- tout est en place aujourd'hui pour pousser une limitation de nos libertés numériques sous le recours fallacieux d'un encouragement au chiffrement mais doté de solutions obligatoires de portes dérobées,
- l'État assurerait ainsi son pouvoir de contrôle total en privant les individus de tout droit au secret absolu (ou quasi-absolu, parce qu'un bon chiffrement leur est aujourd'hui accessible),
- les solutions libres pourraient devenir interdites, ce qui annonce alors la fin de plus de vingt années de lutte pour que des solutions comme PGP puissent exister, quitte à imposer aux entreprises d'utiliser des nouvelles «&nbsp;normes&nbsp;» décidées de manière plus ou moins unilatérales par la France et l'Allemagne,
- cette stratégie, bien que profondément inutile au regard de la lutte contre le terrorisme et la sécurité des citoyens, sert les intérêts de certaines entreprises du numérique qui trouvent dans la politique de E. Macron une ouverture que le gouvernement précédent refusait de leur servir sur un plateau en tardant à développer davantage ce secteur.


Je me suis souvent exprimé à ce propos&nbsp;: pour moi, le chiffrement des communications des individus grâce au logiciel libre est une garantie de la liberté d'expression et du secret des correspondances. Aucun État ne devrait interdire ou dégrader de telles solutions technologiques à ses citoyens. Ce qui distingue un État civilisé et démocratique d'une dictature, c'est justement cette possibilité que les capacités de renseignement de l'État puissent non seulement être régulées de manière démocratique, mais aussi que le chiffrement, en tant qu'application des lois mathématiques, puisse être accessible à tous, sans discrimination et rendre possible l'exercice du secret par les citoyens. Problème&nbsp;: aujourd'hui, le chiffrement libre est remis en cause par la stratégie gouvernementale.



[^0]: [Page archivée](https://web.archive.org/save/http://www.liberation.fr/france/2016/08/02/controler-le-chiffrement-un-calcul-difficile-pour-le-gouvernement_1469978).

[^1]: Voir aussi sur le [site du Ministère de l'Intérieur](http://www.politico.eu/wp-content/uploads/2017/02/2017-02-17-De%CC%81claration-FR-DE-II_Officielle.pdf).

[^2]: Page [archivée](https://web.archive.org/web/20170514140055/https://www.euractiv.com/section/digital/interview/ansip-i-am-strongly-against-any-backdoor-to-encrypted-systems/).

[^3]: Page [archivée](https://web.archive.org/web/20190320154416/https://www.franceinter.fr/emissions/l-invite-de-8h20/l-invite-de-8h20-11-mai-2017).
