---
title: "À propos de Mutt"
date: 2011-01-11
author: "Christophe Masutti"
image: "/images/postimages/terminal.png"
tags: ["Logiciel libre", "Informatique", "Internet", "Courriel"]
description: "Le client de courriel le plus paramétrable au monde, à condition de savoir s'en servir"
categories:
- Logiciel libre
---


Pour résumer simplement, Mutt est une sorte de navigateur de fichiers en mode texte spécialisé dans la lecture de boites courriel (comme le format mbox par exemple). Ainsi, à l'aide d'un "récupérateur" de courriel (comme fetchmail ou procmail) vous pouvez télécharger vos courriels, les stocker dans un dossier et les lire avec Mutt. Cela dit, Mutt est tellement bien qu'il peut lire des dossiers distants, en utilisant le protocole IMAP notamment. En d'autres termes si vous utiliser IMAP, Mutt peut (presque) se suffire à lui tout seul. De plus, Mutt n'a pas d'éditeur de texte intégré, et il s'utilise dans un terminal. Si vous voulez écrire un courriel, il vous faut configurer Mutt de manière à utiliser l'éditeur de votre choix. Et si vous voulez des couleurs, ce seront celles dont est capable d'afficher votre terminal. Ce billet vise à faire le point sur l'utilisation de Mutt et montre combien ce petit logiciel peut s'avérer très puissant.



## S'il est si petit, pourquoi utiliser Mutt?

Mutt est un programme dans la droite lignée des programmes Unix. C'est un petit programme, réalisé pour effectuer un seul type de tâche, **mais il le fait bien**. Ainsi pour utiliser convenablement Mutt, il faudra tout de même un peu de débrouillardise et de bon sens, surtout dans la mesure où Mutt est dit "hautement configurable". Sa puissance vient tout simplement de ce que vous même avez configuré. C'est pour cela que Mutt est sans doute le client de courriel le plus efficace.

Voici deux exemples rapides:

-- Je possède un compte courriel sur un serveur et je veux y accèder en IMAP. Le processus est assez simple. Après avoir correctement configuré Mutt pour l'accès à mon compte, Mutt lira les dossiers sur le serveur, me renverra les en-têtes, me permettra de lire les courriels et si je désire envoyer un message, Mutt ira le placer dans la boîte d'envoi. De ce point de vue, Mutt effectue un travail de rapatriement de données, le serveur IMAP fait le reste.

-- Je possède un compte courriel et j'y accède en POP. A l'aide d'un "récolteur" de courriel, comme <a href="http://www.procmail.org/">procmail</a> ou <a href="http://fetchmail.berlios.de/">fetchmail</a>, je rapatrie mes données dans un dossier local et je configure Mutt pour qu'il lise les éléments de ce dossier. Le tri, les règles de stockage et classement reposent sur la configuration du "récolteur" que j'ai décidé d'utiliser (notez que fetchmail ou procmail sont de même "hautement configurables"). Vous pouvez lire <a href="http://formation-debian.via.ecp.fr/mail-console.html">cette page du manuel de formation Debian</a> pour en apprendre davantage sur ce système

## Comment utiliser Mutt?

![Mutt capture d'écran](/images/mutt_capture_ecran.png)

Il vous faudra d'abord le configurer. Pour ce faire, le principe est simple: après avoir installé le programme via les dépôts de votre distribution, créez un fichier .muttrc dans votre /home. Ensuite il existe toute une série de commandes que vous pouvez renseigner pour faire fonctionner Mutt. Ces commandes concernent les modalités de connexion à un compte, les règles d'affichage des messages et de leurs en-tête, les règles de rédaction, les règles de tri et d'envoi, la personalisation des en-têtes des messages envoyés, les jeux de couleurs, l'éditeur de texte choisi, etc, etc. Bref, tout ce qui permet à Mutt de faire ce que vous lui demandez tout en coopérant avec d'autres programmes selon vos besoins.

Le seul bémol est qu'il vous faudra sans doute passer du temps à comprendre et implémenter ces commandes dans le fichier de configuration. En contrepartie, Mutt sera capable de faire tout ce que vous lui demandez et l'éventail des fonctionnalités est particulièrement grand et malléable. Mutt est vraiment un logiciel libre!

## Barre latérale

Certains utilisateurs préfèrent avoir sous les yeux la liste des dossiers dans lesquels ils trient leurs messages. Lorsqu'un nouveau message arrive, il est signalé présent dans l'un des dossiers et l'utilisateur peut naviguer entre ces derniers. Nativement, Mutt n'intègre pas une telle barre latérale. Il faut alors patcher Mutt dans ce sens. Pour cela, certaines distributions, outre le fait de proposer Mutt dans les dépôts, proposent aussi le paquetage *mutt-patched*. Si ce n'est pas le cas, il faut alors se rendre sur <a href="http://www.lunar-linux.org/index.php?option=com_content&amp;task=view&amp;id=44">le site officiel de ce patch</a>.

## Imprimer avec Mutt?

Imprimer avec Mutt, c'est déjà beaucoup dire. En fait, lorsque l'on configure Mutt en éditant .muttrc, il suffit d'ajouter cette commande

<pre>set print_cmd="lpr -P nom_de_l_imprimante"</pre>

pour pouvoir imprimer (touche "p") l'entrée courante. Mutt envoie alors le texte... tout le texte visible.

De même, selon l'éditeur de texte que vous utilisez avec Mutt pour écrire vos courriels, il possède sans aucun doute une fonction d'impression. Mais il ne s'agit que d'imprimer le texte que vous entrez.

Vous aimeriez peut-être pouvoir formater l'impression et gérer la mise en page des courriels que vous imprimez. Pour cela <a href="http://muttprint.sourceforge.net/">Muttprint</a> est un petit utilitaire configurable (lui aussi!) qui vous rendra de bien grands services, ne serait-ce que pour limiter l'impression aux en-têtes les plus utiles comme *date:*, *à:*, et *de:*. Certaines distributions proposent le paquetage *muttprint*, il vous suffit alors de l'installer en quelques clics depuis les dépôts.

Configurer Muttprint se fait de manière similaire à Mutt. Vous avez cependant le choix entre soit éditer directement le fichier /etc/Muttprint (donc en mode root), soit créer et éditer un fichier .muttprintrc dans votre /home, à coté de .muttrc.

Dans .muttrc, au lieu de spécifier l'imprimante, vous devez alors dire à Mutt d'appeler Muttprint à la rescousse:

<pre>set print_command="muttprint"</pre>

Et dans muttprintrc, il vous suffit de renseigner les commandes, en particulier celle-ci:

<pre>PRINT_COMMAND="lpr -P nom_de_l_imprimante"</pre>

Le fichier /etc/Muttrc qui se crée lors de l'installation de Muttprint servira de modèle (à défaut d'être directement modifié lui-même). Il a l'avantage de voir chaque commande explicitée, ce qui le rend très facile à configurer.

**Petite astuce:** si dans l'en-tête que vous imprimez, vous désirez voir figurer un petit manchot linuxien, il vous suffit d'installer le paquetage *ospics* qui ira placer une série de petit dessins dans /usr/share/ospics/. Il reste à faire appel à l'un d'entre eux (format .eps) pour égailler un peu vos impressions (mais vous pouvez très bien utiliser un autre fichier .eps de votre choix, comme le logo de votre labo, une photo de votre chien, etc.).

## Gestion des profils

<a href="https://statium.link/blog/wp-content/uploads/2016/10/mutt_capture_ecran_reponse.png"><img src="https://statium.link/blog/wp-content/uploads/2016/10/mutt_capture_ecran_reponse-300x219.png" alt="mutt_capture_ecran_reponse" width="300" height="219" class="aligncenter size-medium wp-image-287" /></a>

Vous aimeriez peut-être utiliser plusieurs adresses courriel et signatures, selon vos destinataires ou les listes auxquelles vous êtes abonné. Pour cela il y a au moins deux possibilités.

La première est de partir de l'idée que vous rapatriez plusieurs boites courriel, par exemple avec fetchmail. Et que selon les boites que vous consultez et répondez aux correspondants, vous devez passer par un serveur smtp différent, avec une connexion (mot de passe) différente, une clé GPG différente, etc. Le petit utilitaire <a href="http://www.acoustics.hut.fi/%7Emara/mutt/muttprofile.html">Muttprofile</a> est là pour gérer ce type de situation. Depuis Mutt, on peut alors passer d'un profil à l'autre via une série de macro adéquates. Certaines distributions proposent le paquetage *muttprofile* dans leurs dépôts.

Cela dit, le système précédent est un peu lourd à gérer (opinion personnelle de l'auteur de ces lignes). Il présente néanmoins le grand avantage de se prêter parfaitement à l'utilisateur qui héberge son propre serveur de courriel, où à celui qui tient vraiment à avoir une configuration relative à ses profils. Pour l'utilisateur qui souhaite fonctionner plus simplement, le mieux est encore de rapatrier toutes les boites courriel dans une seule depuis les serveurs (généralement, vous pouvez configurer cela depuis l'interface webmail de votre boite) et finalement ne consulter avec Mutt qu'une seule boîte. Il s'agit donc de ne passer que par un seul serveur au lieu de plusieurs. Mais dans ce cas, comment gérer ses profils?

## Hooks

Une fonctionnalité formidable de Mutt, ce sont les "hook". Il s'agit de fonctions de type "si ... alors" qui sont très précieuses.

Par exemple, si j'ai besoin de spécifier une adresse en fonction d'une adresse d'un expéditeur ou d'une liste à laquelle je suis abonné, je peux écrire dans mon .muttrc la séquence suivante :

<pre>send-hook olivier.durand@machin.com my_hdr From: Patrick Dupont</pre>

Cette séquence permet de répondre à Olivier Durant en utilisant l'adresse Patrick Dupont (lorsque j'écris un message, Mutt repère la chaîne "olivier.durand" dans le champ *To:* et place ce que je lui ai demandé dans le champ *From:*).

Autre exemple: si je souhaite utiliser une adresse courriel précise lorsque je me situe dans un dossier (par exemple le dossier dans lequel je rapatrie les messages provenant de mon compte secondaire), alors je peux utiliser ce type de commande:

<pre>folder-hook laposte my_hdr From: Jean Dugenou</pre>

qui permet d'utiliser l'adresse Jean Dugenou lorsque je me situe dans le dossier /laposte (cela marche aussi bien en IMAP).

Il y a beaucoup d'autres possibilités ouvertes par le système des "hook", et, au fil du temps, vous finirez par constituer une suite de règles variées correspondant exactement à vos besoins. C'est aussi la raison pour laquelle vous avez tout intérêt à sauvegarder en lieu sûr une copie de votre .muttrc, histoire de ne pas avoir à tout refaire si vous perdez la première...

## Commandes et variables

Une autre particularité de Mutt est que l'apprentissage consiste essentiellement à comprendre son .muttrc et comparer avec celui des autres utilisateurs. Pour cela, il est possible de trouver sur internet des exemples bien faits de .muttrc, en particulier sur le site officiel (qui propose aussi un wiki). La liste des commandes et variables pour la configuration de Mutt se trouve sur <a href="http://www.mutt.org/doc/manual/manual-6.html#ss6.1">cette page du manuel en ligne</a> et sa traduction en français sur <a href="=%22http://cedricduval.free.fr/mutt/fr/download/Muttrc%22">le site de Cedric Duval</a>.

## L'éditeur de texte

Comme nous l'avons précisé plus haut, Mutt ne fait que naviguer dans votre courriel. Il ne permet pas d'écrire. Pour cela, il lui faut faire appel à un éditeur de texte. Le principe est, là encore, assez simple: lorsque vous écrivez un message, vous entrez le texte sous les en-têtes définies par Mutt (et que vous pouvez modifier "à la main"), vous quittez l'éditeur et Mutt reprend la main pour envoyer le message.

Avec Mutt, vous pouvez utiliser n'importe quel éditeur de texte de votre choix. Après l'installation, la configuration par défaut utilise l'éditeur par défaut de votre système. Si vous souhaitez en changer, il suffit de le déclarer dans .muttrc. En pratique, <a href="http://www.vim.org/">Vim</a> ou <a href="http://www.gnu.org/software/emacs/">Emacs</a> sont sans doutes les éditeurs les plus appropriés, Mutt ayant une apparence par défaut se rapprochant plutôt de Vim. Nano, un gentil petit éditeur de texte qui a aussi sa cohorte d'admirateurs, pourra de même combler vos attentes.

Pour ceux qui désirent utiliser Vim, là encore, de manière optionnelle, vous pouvez créer un fichier .vimrc dans votre /home, de manière à configurer Vim pour une utilisation avec Mutt, entrer vos propres commandes par exemple. L'une d'entre elles (<code>syntax on</code>) vous permettra d'utiliser un jeu de coloration syntaxique se rapprochant de Mutt (quoi que cela dépende des couleurs que vous avez configuré dans .muttrc).

[Attention, utilisateurs de Ubuntu, la version de Vim installée par défaut est Vim-Tiny. Il vaut mieux installer Vim dans sa version complète pour pouvoir faire ce que vous voulez avec .vimrc].

## Lire les messages en HTML

Pour des raisons diverses (et pas toujours justifiées) certaines personnes aiment envoyer des courriel au format HTML, parfois même accompagnés d'images à l'esthétique douteuse censées "embellir" le message. Avec Mutt, il faut donc faire appel à un logiciel capable de lire le HTML. Il existe au moins plusieurs navigateurs en mode texte: Lynx, W3m, Elinks, Links, Links2...

Il s'agit en fait d'utiliser les entrées mailcap pour reconnaitre le type d'information à traiter et faire appel au bon logiciel pour les afficher. Dans le fichier .muttrc, il faudra donc entrer le code permettant de faire appel au fichier .mailcap que vous devez créer dans votre /home:

<pre>set implicit_autoview
auto_view text/html  application/x-pgp-message
set mailcap_path="~/.mailcap"
set mailcap_sanitize=yes</pre>

Puis, dans .mailcap, au choix :

<ul>
<li>Si vous désirez utiliser Lynx*:

<pre>text/html; lynx -dump -force-html -assume_charset %{charset} -localhost %s; copiousoutput</pre></li>
<li>Si vous désirez utiliser W3m:

<pre>text/html; w3m -dump %s; copiousoutput; nametemplate=%s.html</pre></li>
<li>Si vous désirez utiliser Links ou Links2:

<pre>text/html; links2 -dump %s; nametemplate=%s.html; copiousoutput</pre></li>
<li>Si vous désirez utiliser Elinks:

<pre>text/html; elinks -default-mime-type text/html %s; needsterminal;</pre></li>
<li>Dans le cas Lynx, vous noterez la séquence <code>-assume_charset %{charset}</code>. Elle vise à tirer avantage de la configuration de Lynx utilisant le paramètre assume_charset afin de lire correctement la majorité des messages et leur encodage.</p></li>
</ul>

<p>L'option -dump permet l'affichage dans le même processus que Mutt, c'est à dire dans la même fenêtre. Si vous désirez utiliser les navigateurs de manière autonome, puis, en les quittant, revenir automatiquement à Mutt:

<ul>
<li><code>text/html; Lynx %s; nametemplate=%s.html</code></li>
<li><code>text/html; w3m %s; nametemplate=%s.html</code></li>
<li><code>text/html; links2 %s; nametemplate=%s.html</code></li>
</ul>

Attention: si votre système est configuré en UTF8 (Locales), c'est cet encodage qui sera utilisé par le navigateur que vous aurez choisi. Un avantage de Links(2) est que si le "content-type" du courriel envoyé n'est pas ou mal renseigné, ou si l'encodage n'est pas le même que le vôtre, alors ce navigateur gèrera les défauts d'affichage (par exemple les lettres accentuées) en trouvant des solutions permettant une lecture agréable du courriel.

## Le Carnet d'adresses

<h3>Les alias, mode classique</h3>

Mutt intègre un système de répertoire d'adresses très simple. Un fichier d'adresse est créé dans lequel vous enregistrez vos contacts.

Ainsi, dans votre .muttrc, vous pouvez indiquer ceci :

<pre>#CARNET ADRESSES
set alias_file = ~/.mutt/adresses
source ~/.mutt/adresses</pre>

Cela aura pour effet d'enregistrer vos alias dans <code>/.mutt/adresses</code> et de rechercher ces alias dans ce même fichier (source).

<h3>Utilisation de Abook</h3>

Abook est un petit programme de gestion de contacts fait pour fonctionner avec Mutt. Plus élaboré que le systèmes des alias (cf. ci-dessus), il permet notamment l'import et l'export de carnets d'adresses dans différents formats et surtout il présente une interface graphique et stocke davantage d'informations sur vos contacts.

Abook doit certainement être disponible dans les paquets de votre distribution. Le site du projet se trouve à <a href="http://abook.sourceforge.net/">cette adresse</a>.

Pour utiliser Abook avec Mutt, il suffit de configurer votre .muttrc ainsi:

<pre># Abook
set query_command= "abook --mutt-query '%s'"
macro index,pager A "abook --add-email-quiet" "Ajouter l'expediteur dans abook"</pre>

Cela aura pour effet de permettre à Mutt de faire appel à Abook, deux deux manières : en tapant **A** (au lieu de **a**, utilisé pour les alias), l'expéditeur sera ajouté au carnet de Abook. Et pour utiliser Abook "à l'intérieur de Mutt", c'est à dire dans le même terminal, il suffira de faire **CTRL+t** lors de l'entrée du destinataire. La liste des contacts de Abook apparaîtra alors.

<hr />

<blockquote>
  Le logo de Mutt affiché en haut à gauche n'est pas le logo officiel (mais il est très joli). Il a été créé par Malcolm Locke. Vous pouvez vous le procurer <a href="http://wholemeal.co.nz/%7Emalc/mutt-logo/">ici</a>, ainsi que sa licence (creative common).
</blockquote>

<ul>
<li><a href="http://www.mutt.org/">Site officiel</a></li>
<li><a href="http://wiki.mutt.org/">Le wiki de Mutt</a></li>
<li><a href="http://cedricduval.free.fr/mutt/fr/">Documentation en francais</a></li>
<li><a href="http://cedricduval.free.fr/mutt/fr/download/Muttrc">Toutes les variables de Mutt dans ce muttrc</a></li>
<li><a href="http://www.vinc17.org/mutt/index.fr.html">Une présentation de Mutt</a></li>
<li><a href="http://chl.be/glmf/www.linuxmag-france.org/old/lm2/mutrc.html">Un autre fichier muttrc commenté</a></li>
<li><a href="http://www.ucolick.org/%7Elharden/learnmutt.html">Se servir de Mutt</a></li>
</ul>
