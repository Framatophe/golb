---
title: "Bibliographie : aidez vos étudiants !"
date: 2024-03-17
author: "Christophe Masutti"
image: "/images/postimages/cm.png"
description: "Dans ce court billet, je questionne le rapport à la gestion bibliographique dans l'enseignement supérieur. Souvent, les exigences de rendus (devoirs et rapports) précisent combien la présentation de la bibliographie est importante, alors qu'on néglige son apprentissage. Ne serait-ce pas souhaitable d'enseigner l'automatisation de la gestion bibliographique plutôt que de se contenter de distribuer des documents exhortant à reproduire des modèles plus ou moins fiables ?"
tags: ["Libres propos", "bibliographie", "enseignement", "université"]
categories:
- Libres propos
---


Dans ce court billet, je questionne le rapport à la gestion bibliographique dans l'enseignement supérieur. Souvent, les exigences de rendus (devoirs et rapports) précisent combien la présentation de la bibliographie est importante, alors qu'on néglige son apprentissage. Ne serait-ce pas souhaitable d'enseigner l'automatisation de la gestion bibliographique plutôt que de se contenter de distribuer des documents exhortant à reproduire des modèles plus ou moins fiables ?

<!--more-->

Chacun sait combien il est important, lors de toute activité scientifique, d'être en mesure de citer des références bibliographiques. Les raisons sont multiples : la première consiste à prouver de cette manière dans quel domaine, dans quelle école de pensée, ou dans quelle discipline on souhaite inscrire ce qu'on est en train d'écrire. Une seconde raison consiste à démontrer la maîtrise du sujet qu'on prétend aborder, c'est-à-dire démontrer qu'une recherche préalable a été effectuée et permettre aux lecteur·ices de considérer la pertinence de l'argumentation. Une troisième raison concerne les sources qu'on utilise pour les besoins de l'argumentation. Ces sources doivent être convenablement citées de manière à ce que les lecteur·ices puissent s'y rapporter et juger ainsi de la fiabilité autant que de la scientificité de la démonstration.

C'est pourquoi l'apprentissage de la gestion bibliographique doit commencer très tôt dans la formation des étudiants. Elle doit faire partie intégrante des enseignements, et non se présenter comme un vague module optionnel offert par un service interne dont la plupart des étudiants ignorent l'existence. D'autant plus qu'une initiation ne demande pas énormément d'heures d'enseignement et reste d'autant plus efficace qu'elle est adaptée à la discipline concernée.

Elle est même nécessaire si on compare l'état de l'enseignement supérieur d'une vingtaine d'années plus tôt. Les étudiants ont aujourd'hui à rendre des travaux écrits qui demandent davantage de travail rédactionnel sous format numérique, en particulier les différents rapports d'activité ou de stage. Avec la progression des outils numériques, on attend des étudiants dès la première année une certaine maîtrise des logiciels bureautiques dans la préparation de documents longs. Or, bien que les formations du secondaire aient quelque peu progressé, très rares sont les étudiants qui, dès la première année, sont capables d'utiliser correctement un logiciel de traitement de texte comme LibreOffice dans cet objectif, car ils n'ont jamais eu à écrire de longs textes rigoureux. Les éléments à maîtriser sont, en vrac : la gestion des styles, la gestion des index, les règles de mise en page, la correction ortho-typographique, et&#x2026; la bibliographie. Autant d'éléments dont on ne se souciait guère lorsqu'il s'agissait de rendre un devoir de 4 copies doubles manuscrites (dans le cas des études en humanités).

De même, très peu d'étudiants ont déjà mis en place, avec leurs propres compétences numériques, une sorte de chaîne éditoriale qui leur permettrait d'écrire, d'organiser leurs fichiers correctement et de gérer leur documentation. L'exemple de la gestion de prise de notes avec une méthodologie de type Zettelkasten, un format malléable comme le markdown (y compris avec des logiciels spécialisés permettant de saisir des mathématiques), des sorties d'exportation vers du PDF ou des formats `.odt`, `docx`, ou LaTeX&#x2026; tout cela ne s'acquiert finalement qu'assez tard dans les études supérieures.

Pourtant, beaucoup de formations ont tendance à considérer ces questions comme étant annexes. Pire, elles incitent les étudiants à utiliser des outils tout intégrés et privateurs chez Microsoft et Google parce que les universités passent des marchés avec des prestataires, et non pour aider vraiment les étudiants à maîtriser les outils numériques. Dans ce contexte, la question de la gestion bibliographique devient vite un bazar :

-   les enseignants qui s'y collent ont souvent leurs propres (mauvaises) habitudes,
-   on s'intéresse à la *présentation* de la bibliographie uniquement, sans chercher à former l'étudiant à la gestion bibliographique,
-   les modèles proposés sont souvent complètement inventés, tirés de revues diverses, alors qu'une seule norme devrait servir de repère : [ISO 690](https://fr.wikipedia.org/wiki/ISO_690).

On constate que presque aucune université ou école française ne propose de style bibliographique à utiliser dans un format de type CSL (et ne parlons même pas de BibTeX). À la place on trouve tout un tas de documents téléchargeables à destination des étudiants. Ces documents ne se ressemblent jamais, proposent des styles très différents et, lorsque les rédacteurs y pensent, renvoient les étudiants aux logiciels de gestion bibliographique comme Zotero sans vraiment en offrir un aperçu, même succinct.

Prenons deux exemples : [ce document](https://ufr-culture-communication.univ-paris8.fr/IMG/pdf/m1-m2_vademecum_biblio.pdf) de l'Université Paris 8 et [ce document](https://bu.univ-lorraine.fr/sites/default/files/users/user779/Guide%20biblio%20APA%202022-2023.pdf) de l'Université de Lorraine. Seul le second mentionne « en passant » la question de l'automatisation de la gestion bibliographique et propose d'utiliser le style APA de l'Université de Montréal (en CSL). Mais les deux documents proposent des suites d'exemples qu'on peut s'amuser à comparer et deviner le désarroi des étudiants : les deux proposent un style auteur-date, mais ne sont pas harmonisés quand à la présentation. Il s'agit pourtant de deux universités françaises. Il y a pléthore de documents de cet acabit. Tous ne visent qu'à démontrer une chose : pourvu que la bibliographie soit présentée de manière normalisée, il est inutile de dresser des listes interminables d'exemples, mieux vaut passer du temps à apprendre comment automatiser et utiliser un style CSL une fois pour toute.

Ce qui se produit *in fine*, c'est une perte de temps monstrueuse pour chaque étudiant à essayer de faire ressembler sa liste bibliographique au modèle qui lui est présenté&#x2026; et sur lequel il peut même être noté ! Et tout cela lorsqu'on ne lui présente pas plusieurs modèles différents au cours d'une même formation.

Je vais l'affirmer franchement : devrait se taire sur le sujet de la bibliographie qui n'est pas capable d'éditer un fichier CSL (en XML) correctement (en suivant la norme ISO 690), le donner à ses étudiants pour qu'ils s'en servent avec un logiciel comme Zotero et un plugin pour traitement de texte. À la limite, on peut accepter que savoir se servir d'un fichier CSL et montrer aux étudiants comment procéder pour l'utiliser peut constituer un pis-aller, l'essentiel étant de faciliter à la fois la gestion bibliographique et la présentation.

Oui, c'est élitiste ! mais à quoi servent les documents visant à montrer comment présenter une bibliographie s'ils ne sont pas eux-mêmes assortis du fichier CSL qui correspond à la présentation demandée ?

Ce serait trop beau de penser que les rédacteurs de tels documents proposent rigoureusement les mêmes exigences de présentation. Il y a toujours des variations, et souvent, pour une même ressource bibliographique, des présentations très différentes, voire pas du tout normalisées. S'il existe un tel flou, c'est pour ces raisons :

1.  la norme ISO (lorsqu'elle est respectée) permet toujours de petites variations selon la présentation voulue, et ces petites variations sont parfois interprétées comme autant de libertés que s'octroie arbitrairement le corps enseignant jusqu'à croire détenir le modèle ultime qui devient alors la « norme » à utiliser,
2.  toutes les bibliographies ne sont pas censées utiliser la méthode auteur-date (certains s'évertuent toujours à vouloir citer les références en notes de bas de page&#x2026;), et le style APA fait tout de même dans les 400 pages,
3.  les abréviations ne sont pas toujours toutes autorisées,
4.  des questions ne sont pas toujours résolues de la même manière, par exemple : lorsqu'un document possède une URL, faut-il écrire : « [en ligne] », « URL », « DOI », « [consulté le xxx] », « [date de dernière consultation : xxx] »&#x2026; etc.

Pour toutes ces raisons, on ne peut pas se contenter de donner aux étudiants un document illustratif, ****il faut les aider à automatiser la bibliographie et sa présentation**** de manière à ce que le document final soit harmonisé de manière efficace tout en permettant à l'étudiant de stocker ses références pour une utilisation ultérieure.

Pour finir, quelques liens :

-   La [documentation du Citation Style Langage](https://docs.citationstyles.org/en/stable/index.html),
-   Le [logiciel Zotero](https://www.zotero.org/) (et ne pas oublier ses plugins),
-   Le [dépôt Zotero des styles CSL](https://www.zotero.org/styles) (on y trouvera par exemple le style auteur-date iso 690 Fr),
-   L'article d'Arthur Perret (Univ. Jean Moulin Lyon 3), intitulé [« Bibliographie »](https://www.arthurperret.fr/cours/bibliographie.html), précis et explicite et dont je conseille la lecture.

