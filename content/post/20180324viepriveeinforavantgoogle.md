---
title: "Vie privée, informatique et marketing dans le monde d'avant Google"
date: 2018-03-24
author: "Christophe Masutti"
image: "/images/postimages/mainframe.png"
description: "Une extension à la série Leviathan, plus historique et qui préfigure un travail de fond"
tags: ["Libres propos", "Histoire", "Informatique", "Libertés"]
categories:
- Libres propos
---


En 1969, Paul Baran affirmait: « Quelle belle opportunité pour l'ingénieur informaticien d'exercer une nouvelle forme de responsabilité sociale ». En effet, il y a presque 70 ans, les interrogations sociales au sujet du traitement informatique des données personnelles étaient déjà sur le devant de la scène.

Voici le résumé d'un assez long texte, écrit début mars 2018, que vous pouvez lire en version [HTML](https://statium.link/bazaar/leviathans/lev5/lev.html) ou récupérer [en PDF sur HAL-SHS](https://halshs.archives-ouvertes.fr/halshs-01761828).


> Il est notable que les monopoles de l'économie numérique d'aujourd'hui exploitent à grande échelle nos données personnelles et explorent nos vies privées. Cet article propose une mise au point historique sur la manière dont se sont constitués ces modèles : dès les années 1960, par la convergence entre l'industrie informatique, les méthodes de marketing (en particulier le marketing direct) et les applications en bases de données. Les pratiques de captation et d'exploitation des données personnelles ont en réalité toujours été sources de débats, de limitation et de mises en garde. Malgré cela, le contrôle social exercé par la segmentation sociale, elle-même imposée par le marketing, semble être une condition de l'avènement d'une forme d'économie de la consommation que de nombreux auteurs ont dénoncé. Peut-on penser autrement ce capitalisme de surveillance ?

