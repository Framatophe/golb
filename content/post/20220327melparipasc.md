---
title: "Néolibéralisme et élections : le pari pascalien ?"
date: 2022-03-27
author: "Christophe Masutti"
image: "/images/postimages/cm.png"
description: "Un billet globiboulga où je regroupe tout : l'accord de principe USA/Europe au sujet des données personnelles, le néolibéralisme à la Macron et la question du vote aux présidentielles."
tags: ["Libres propos", "Politique", "surveillance"]
categories:
- Libres propos
---



Guerre en Ukraine... À l'heure où les gouvernements européens sont gentiment
pressés de choisir l'impérialisme qu'ils préfèrent (disons le moins pire), le
président des États-Unis Joe Biden est venu nous rendre une petite visite. Que
de beaux discours. Ils firent passer le nouvel accord de principe
américano-européen sur le transfert des données personnelles pour une simple
discussion entre gens de bonne compagnie autour de la machine à café. Même si
cet accord de principe a [reçu un accueil
mitigé](https://www.lesechos.fr/tech-medias/hightech/accord-surprise-sur-le-transfert-des-donnees-personnelles-entre-leurope-et-les-etats-unis-1396225).

Le gouvernement américain aurait en effet bien tort de s'en passer. Maintenant
que l'Europe est en proie à la menace avérée d'une guerre nucléaire et risque
d'être en déficit énergétique, Joe Biden ne fait qu'appliquer ce qui a toujours
réussi aux entreprises multinationales américaines, c'est-à-dire la bonne
vieille recette de l'hégémonie sur les marchés extérieurs appuyée par l'effort
de guerre (voir [cet
article](https://lvsl.fr/marketing-financiarisation-et-complexe-militaro-industriel-les-trois-sources-de-la-data-economie/)). 

Après l'invalidation du Privacy Shield, les GAFAM (et autres sociétés
assimilées, comme les courtiers de données tels Acxiom) n'auront plus cette épée
de Damoclès au-dessus de leurs têtes pour contrecarrer les pratiques
d'extraction des données qu'elles opèrent depuis de longues années ou pour
coopérer avec les agences de renseignement, de manière active, dans la
surveillance de masse.

Il est intéressant, par ailleurs, de comparer le [communiqué de presse de la
Maison
Blanche](https://www.whitehouse.gov/briefing-room/statements-releases/2022/03/25/fact-sheet-united-states-and-european-commission-announce-trans-atlantic-data-privacy-framework/)
et le [Discours de Ursula von der
Leyen](https://ec.europa.eu/commission/presscorner/detail/fr/STATEMENT_22_2043). 

Du point de vue Américain, c'est surtout une affaire de pognon et de
concurrence&nbsp;:

> (...) l'accord permettra la fluidité du trafic de données qui représente plus
> de 1 000 milliards de dollars de commerce transfrontalier chaque année, et
> permettra aux entreprises de toutes tailles de se concurrencer sur leurs
> marchés respectifs.

et l'enjeu consiste à laisser les commandes aux agences de renseignement
américaines&nbsp;:

> Ces nouvelles politiques seront mises en œuvre par la communauté du
> Renseignement des États-Unis de manière à protéger efficacement ses citoyens,
> ainsi que ceux de ses alliés et partenaires, conformément aux protections de
> haut niveau offertes par ce Cadre.

Du point de vue Européen, on mélange tout, la guerre, l'énergie et le numérique.
Et pour cause, la peur domine&nbsp;:

> Et nous continuons à renforcer notre coopération dans de nombreux domaines
> stratégiques: en apportant de l'aide à l'Ukraine en matière humanitaire et
> sécuritaire; dans le domaine de l'énergie; en luttant contre tout ce qui
> menace nos démocraties; en résolvant les points en suspens dans la coopération
> entre les États-Unis et l'Union européenne, y compris en ce qui concerne la
> protection des données et de la vie privée.

D'aucuns diraient que conclure des accords de principes en telle situation
d'infériorité est à la fois prématuré et imprudent. Et c'est là que tout
argument stratégique et rationnel se confronte au fait que c'est toute une
doctrine néolibérale qui est à l'œuvre et affaibli toujours plus les plus
faibles. Cette doctrine est révélée dans cette simple phrase d'U. von der
Leyen&nbsp;:

> Et nous devons également continuer à adapter nos démocraties à un monde en
> constante évolution. Ce constat vaut en particulier pour la numérisation
> (&hellip;)

Encore une fois on constate combien nos démocraties sont en effet face à un
double danger.

Le premier, frontal et brutal, c'est celui de la guerre et la menace du
totalitarisme et du fascisme. Brandie jusqu'à la nausée, qu'elle soit avérée
(comme aujourd'hui) ou simplement supposée pour justifier des lois scélérates,
elle est toujours employée pour dérouler la logique de consentement censée
valider les modifications substantielles du Droit qui définissent le cadre
toujours plus réduit de nos libertés.

Le second n'est pas plus subtil. Ce sont les versions plus ou moins édulcorées du
TINA de Margareth Thatcher (vous vous souvenez, [la copine à
Pinochet](https://savoirs.rfi.fr/fr/comprendre-enrichir/histoire/1999-margaret-thatcher-soutient-pinochet)).
*There is no alternative* (TINA). Il n'y a pas d'alternative. Le monde change,
il faut adapter la démocratie au monde et non pas adapter le monde à la
démocratie. Les idéaux, les rêves, les mouvements collectifs, les revendications
sociales, la soif de justice... tout cela n'est acceptable que dans la mesure où
ils se conforment au monde qui change.

Mais qu'est-ce qui fait changer le monde&nbsp;? c'est simplement que la seconde
moitié du XX<sup>e</sup> siècle a fait entrer le capitalisme dans une phase où,
considérant que le laissez-faire est une vaste fumisterie qui a conduit à la
crise de 1929 et la Seconde Guerre, il faut que l'État puisse jouer le jeu du
capitalisme en lui donnant son cadre d'épanouissement, c'est-à-dire partout où
le capitalisme peut extraire du profit, de la force de travail humaine à nos
intimités numériques, de nos santés à notre climat, c'est à l'État de
transformer le Droit, de casser les acquis sociaux et nos libertés, pour assurer
ce profit dans un vaste jeu mondialisé de la concurrence organisée entre les
peuples.  Quitte à faire en sorte que les peuples entrent en guerre, on organise
la course des plus dociles au marché.

Le néolibéralisme est une doctrine qui imprègne jusqu'au moindre vêtement les
dirigeants qui s'y conforment. Macron n'est pas en reste. On a beaucoup commenté
son mépris de classe. L'erreur d'interprétation consiste à penser que son mépris
est fondé sur la rationalité supposée du peuple. Dès lors, comment  avoir du
respect pour un peuple à ce point assujetti au capitalisme, plus avide du
dernier smartphone à mode que des enjeux climatiques&nbsp;? Mais que nenni.
Premièrement parce que ce peuple a une soif évidente de justice sociale, mais
surtout parce le mépris macroniste relève d'une logique bien plus rude&nbsp;: 
pour mettre en application la logique néolibérale face à un peuple rétif, il
faut le considérer comme radicalement autre, détaché de sa représentation de
soi, en dehors de toute considération morale. 

Si nous partons du principe que dans la logique communicationnelle de Macron
toute affirmation signifie son exact contraire[^1] on peut remonter au tout
début dans sa campagne de 2017 où il se réclamait plus ou moins du philosophe
Paul Ricoeur. C'est faire offense à la mémoire de cet éminent philosophe. Pour
dire vite, selon Ricoeur, on construit le sens de notre être à partir de
l'altérité&nbsp;: l'éthique, la sollicitude, la justice. Bref, tout l'exact
opposé de Macron, ou plutôt de son discours (je me réserve le jugement sur sa
personne, mais croyez bien que le vocabulaire que je mobilise dans cette optique
n'aurait pas sa place ici).

Il n'y a donc aucune surprise à voir que pour le capitalisme, tout est bon dans
le Macron. Lui-même, [dans
Forbes](https://www.franceculture.fr/emissions/le-billet-politique/le-billet-politique-du-jeudi-03-mai-2018),
répétait à deux reprises&nbsp;: «&nbsp;There's no other choice&nbsp;», prenant
modèle sur Thatcher. Il n'y a pas de politique, il n'y a pas d'idéologie, le
parti sans partisan, la nation start-up en dehors du peuple. Il y a une
doctrine et son application. Un appareillage économique et technocratique  sans
âme, tout entier voué à la logique extractiviste pour le profit capitaliste.
Bien sûr on sauve les apparences. On se drape d'irréprochabilité lorsque le
scandale est trop évident, comme de le cas du scandale des maisons de retraite
Orpea. Mais au fond, on ne fait qu'appliquer la doctrine, on adapte la
démocratie (ou ce qu'il en reste) au monde qui change&hellip; c'est-à-dire qu'on
applique en bon élève les canons néolibéraux, jusqu'à demander à des cabinets
d'audit comment faire exactement, de la coupe programmée du système éducatif à
nos retraites, en passant par les aides sociales.

Comment comprendre, dans ces conditions pourtant claires, que Macron soit à ce
point si bien positionné dans les sondages en vue des prochaines élections
présidentielles&nbsp;? C'est tout le paradoxe du vote dans lequel s'engouffre
justement le néolibéralisme. Ce paradoxe est simple à comprendre mais ses
implications sont très complexes. Si je vais voter, ce n'est pas parce que mon
seul vote va changer quelque chose... mais si je n'en attends rien
individuellement en retour, pourquoi vais-je voter&nbsp;? Les idéalistes partent
alors du principe de la rationalité du votant&nbsp;: si Untel va voter, c'est
parce qu'il souhaite défendre un intérêt collectif auquel il adhère. Mais on
peut alors retourner le problème&nbsp;: cela supposerait une complétude de
l'information, un contexte informationnel suffisant pour que le vote au nom de
cet intérêt collectif ne soit pas biaisé. Or, le principe d'un vote électoral
est justement de diffuser de l'information imparfaite (la campagne de
communication politique). En gros&nbsp;: bien malin celui qui est capable de
dénicher toutes les failles d'un discours politique. De surcroît les techniques
de communications modernes se passent bien de toute morale lorsqu'elles
réussissent à faire infléchir le cours des votes grâce au profilage et à
l'analyse psychographique (le scandale Cambridge Analytica a soulevé légèrement
le voile de ces techniques largement répandues). Donc la raison du vote est
presque toujours irrationnelle&nbsp;: on vote par conformité sociale, par
pression familiale, par affinité émotionnelle, par influence inconsciente des
médias, et cela même si une part rationnelle entre toujours en jeu. 

Par exemple, comment comprendre que l'abstention soit toujours à ce point
considérée comme un problème de réputation sociale et non comme un choix
assumé&nbsp;? Ne pas voter serait un acte qui nuit à la représentation de
l'intérêt collectif que se font les votants. Rien n'est moins évident&nbsp;: on
peut s'abstenir au nom de l'intérêt collectif, justement&nbsp;: ne pas entrer
dans un jeu électoral qui nuit à la démocratie, donc à l'intérêt collectif. Il y
a plein d'autres raisons pour lesquelles s'abstenir est une démarche très
rationnelle (voir F. Dupuis-Déri, *[Nous n'irons plus aux
urnes](https://luxediteur.com/catalogue/nous-nirons-plus-aux-urnes/)*).

L'autre raison de l'abstention, beaucoup évidente, c'est la démonstration que le
choix est biaisé. Une course électorale à la française qui se termine par un
second tour opposant deux partisans de la même doctrine néolibérale. On a déjà
vu cela plus d'une fois. Le scénario consiste à opposer le couple thatchérisme
et mépris de classe au couple Pinochisme et racisme. Les deux contribuent à
créer un contexte qui est de toute façon néo-fasciste. Soit un durcissement de
la logique néolibérale au détriment des libertés et de la justice sociale (car
il faudra bien satisfaire les électeurs du Front National), soit un durcissement
de la logique néolibérale au détriment des libertés et de la justice sociale
(parce qu'il faudra bien satisfaire les électeurs de Macron). Vous voyez la
différence&nbsp;? sans blague&nbsp;? Bon, je veux bien admettre que dans un cas,
on pourra plus clairement identifier les connards de fachos complotistes.

Bon, alors que faire&nbsp;? Aller voter ou pas&nbsp;?

Je me suis fait un peu bousculer dernièrement parce que j'affichais mon
intention de m'abstenir. Il faut reconnaître qu'il y a au moins un argument qui
fait un peu pencher la balance&nbsp;: la présence de la France Insoumise comme le
seul mouvement politique qui propose une alternative au moment où l'effet TINA
est le plus fort.

Il y aurait donc une utilité rationnelle au vote&nbsp;: en l'absence d'un
contexte informationnel correct, au moins un élément rationnel et objectif entre
en jeu&nbsp;: préserver le débat démocratique là où il a tendance à disparaître
(au profit du racisme ou de l'anesthésie générale du néolibéralisme).

Comment un anarchiste peut-il aller tout de même voter&nbsp;? ne rigolez pas,
j'en connais qui ont voté Macron au second tour il y a 5 ans pour tenter le
barrage aux fachos. C'est un vrai cas de conscience. En plus, il y a rapport avec
Dieu&nbsp;! si&nbsp;! C'est le fameux pari de Pascal&nbsp;: je ne crois pas en
Dieu, mais qu'il existe ou non, j'ai tout à gagner à y croire&nbsp;: s'il
n'existe pas, je suis conforté dans mon choix, et s'il existe, c'est qu'il y a
un paradis réservé au croyants et un enfer pour les non-croyants et dans lequel
je risque d'être envoyé. Donc voter Mélenchon serait un acte rationnel fondé sur
l'espérance d'un gain individuel... Zut alors.

On s'en sort quand même. L'acte rationnel repose sur une conviction et non une
croyance&nbsp;: voter Mélenchon au premier tour consiste à un vote utile
contribuant à l'émergence d'un débat démocratique qui opposerait deux visions du
monde clairement opposées. Que Mélenchon soit finalement élu ou pas au second
tour permettrait d'apporter un peu de clarté.

L'autorité et le pouvoir ne sont décidément pas ma tasse de thé. Je me méfie de
beaucoup de promesses électorales de Mélenchon, à commencer par sa conception
d'une VI<sup>e</sup> République qui n'entre pas vraiment dans mes critères d'une
démocratie directe, ou encore sa tendance à l'autoritarisme (et j'ai du mal à
voir comment il peut concilier l'un avec l'autre).

Que ferai-je au premier tour&nbsp;? Joker&nbsp;!



# notes

[^1]: On peut prendre un exemple très récent dans sa campagne électorale&nbsp;: 
  conditionner le RSA à un travail qui n'en n'est pas un, plutôt un
  accompagnement ou du travail d'intérêt général, bref tout ce qui peut produire
  sans être qualifié par un contrat de travail.

