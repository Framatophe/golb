---
title: "Moi aussi je peux écrire un livre sur l'IA"
date: 2024-11-18
author: "Christophe Masutti"
image: "/images/postimages/cm.png"
description: "Chez Framasoft, on se torture les méninges. Alors quand on parle d'Intelligence Artificielle, on préfère essayer de gratter un peu sous la surface pour comprendre ce qui se trame. Comme le marronnier éditorial du moment est l'IA dans tous ses états, je me suis dis que finalement, moi aussi..."
tags: ["Libres propos", "IA", "intelligence", "artificielle", "livre"]
categories:
- Libres propos
---

Chez Framasoft, on se torture les méninges. Alors quand on parle d'Intelligence Artificielle, on préfère essayer de gratter un peu sous la surface pour comprendre ce qui se trame. Comme le marronnier éditorial du moment est l'IA dans tous ses états, je me suis dis que finalement, moi aussi...

<!--more-->

<hr />

C'est un petit projet, comme ça en passant. Il ne prétend par faire un tour exhaustif de ce qu'on entend exactement par « apprentissage automatique », mais au moins il m'a donné l'opportunité de réviser mes cours sur les dérivées… ha, ha !

Majpeulsia : _Moi aussi je peux écrire un livre sur l'IA_ !

Ma manière à moi de comprendre des concepts, c'est d'en écrire des pages. J'ai pensé que cela pouvait éventuellement profiter à tout le monde. En premier lieu les copaing•nes de Framasoft mais pas que…

L'objectif consiste à développer les concepts techniques de l'apprentisage automatique et les enjeux du moment autour de cela. Pourquoi faire ? par exemple, lorsque l'Open Source Initiative a sorti a sorti sa définition d'une IA open source (voir mon billet précédent sur ce blog), il a été aussitôt question du statut des données d'entraînement. Mais… c'est quoi des données d'entraînement ? et surtout comment entraîne-t-on une IA ?

Vous allez me dire : ok, quand je conduis une voiture, je n'ai pas besoin de connaître la théorie du moteur à explosion. Oui, certes, mais connaître un peu de mécanique, c'est aussi assurer un minimum de sécurité. Alors, voilà, c'est ce minimum que je propose.

Cela se présente sous la forme d'un MKDocs à [cette adresse](https://majpeulsia-884208.frama.io/) (j'ai pas pris la peine d'un nom de domaine), et les sources sont [ici](https://framagit.org/Framatophe/majpeulsia). Le travail est lancé **et il sera toujours en cours** :)

Bonne lecture !

