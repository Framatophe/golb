---
title: "Entraînement trail, 6 châteaux"
date: 2015-05-30
author: "Christophe Masutti"
image: "/images/postimages/hikebike.png"
tags: ["Sport", "Trail", "Entraînement", "Parcours"]
description: "Une sortie qui permet de visiter 6 châteaux sur les hauteurs de Barr et du Mont Sainte-Odile"
categories:
- Sport
---

La sortie trail du jour m'a embarqué autour du Mont Sainte Odile, un des hauts lieux de l'identité alsacienne. Un parcours à éviter absolument le dimanche si vous ne voulez pas gêner les randonneurs. Côté repérage, la multitude de chemins balisés du Club Vogien a ceci de paradoxal qu'à la vitesse de course à pied, on en perd un peu le fil, donc carte obligatoire. Ce parcours me semble idéal si vous êtes de passage dans la région et si vous voulez visiter un maximum d'endroit en peu de temps. C'est parti, au rythme d'un car de touristes japonais frénétiques !



## Description

L'objectif du jour était simple : cumuler un dénivelé positif d'au moins 1000 m tout en prenant plaisir à rallier différents points remarquables autour du Mont Sainte Odile. L'intérêt d'aligner sur un même parcours des monuments ou des rochers particuliers, c'est de rompre la monotonie. Ce fut plutôt réussi. Sans vouloir crâner, je pense que s'il y a un trail du côté de Barr, c'est ce parcours-là qu'il faudrait faire pour un trail court.

C'est donc à Barr qu'on est invité à laisser sa voiture, le cas échéant. Un emplacement intéressant est sur le parking de l'église S<sup>t</sup> Martin, ce qui permet d'amorcer directement sur le GR 5 à la droite du cimetière. Pour lire la suite de ce billet, prenez une carte IGN, surtout si vous n'êtes pas un(e) habitué(e) des lieux.

Données du parcours : 28 km, 1285 m D+.

Le parcours en général est assez régulier et rapide. Les difficultés sont étalées tout au long du chemin. La première montée vers le Mont S<sup>te</sup> Odile, est assez surprenante. On emprunte le GR pour rejoindre le château du Landsberg puis on est finalement assez vite rendu sur la Bloss puis le Maennelstein. Ensuite entre les replats et les descentes, on couvre très rapidement la distance jusqu'au Monastère, puis les ruines du Hagelschloss et le site du Dreistein (en réalité, à cet endroit, il y a trois châteaux, mais je n'en compte qu'un pour cette balade :) ). Jusqu'au Dreistein, honnêtement, je n'ai pas vu le temps passer. Par contre, la descente du Dreistein et la montée qui suit juste après pour rejoindre la maison forestière du Willerhof nécessite une petite préparation psychologique : le Saegmuehlrain marque la division du parcours en deux grandes parties : la première plutôt régulière et la seconde qui nécessite de bien jauger l'énergie disponible selon la difficulté.

Cette seconde partie est inaugurée par le château du Birkenfels puis, en passant par la Breitmatt, un très joli point de vue sur le rocher du Kienberg. Pour la faire courte, l'ascension du Kienberg (par la face sud) m'a fait apprécier mes bâtons ! Ensuite vient la grande descente, sur plusieurs kilomètres, jusque la Holtzplatz (fond de vallée). Méfiez-vous : repérez bien la nomenclature du Club Vosgien sinon il est très facile de se tromper et se retrouver directement à Barr. Ce serait dommage, puisque les deux derniers châteaux n'attendent que vous à condition d'accomplir la dernière montée NTM (Nique Tes Mollets, un groupe célèbre qui m'a trotté dans la tête tout le temps de cette montée). Cette montée débouche sur Hungerplatz, ce qui marque la fin des négociations de dénivelés, puisqu'entre le château du Spesbourg et celui d'Andlau, c'est une descente qui ne se terminera finalement qu'à Barr. La toute fin du parcours, au sortir de la forêt, surplombe les vignes côté sud.

Je récapitule rapidement les étapes du parcours :


- chateau du Landsberg,
- point de vue du Maennelstein,
- monastère de S<sup>te</sup> Odile,
- chateau du Hagelschloss,
- château(x) du Dreistein,
- château du Birkenfels (en passant par Willerhof),
- Rocher du Kienberg,
- Carrefour du Luttenbach puis Holtzplatz et Hungerplatz,
- Chateau du Spesbourg,
- chateau d'Andlau,
- Les Trois Chênes puis retour à Barr par les vignes



## Carte et profil altimétrique

La carte ci-dessous et son fichier GPX ne sont là qu'à titre indicatif. Cette précision est importante car à deux reprises, j'ai bricolé ma montre GPS et oublié de la remettre en route (!). Il y a donc deux lignes droites qui faussent un peu le parcours, mais à ces deux endroits, il suffit de suivre le sentier (suivre le sentier du Mur Païen, et le chemin forestier sur la commune de Barr).

![Profil](/images/O6Ctrail_odile6chat_profil.png)

<iframe src="https://umap.openstreetmap.fr/fr/map/odile-et-ses-6-chateaux_41758?scaleControl=false&amp;miniMap=false&amp;scrollWheelZoom=false&amp;zoomControl=true&amp;allowEdit=false&amp;moreControl=true&amp;datalayersControl=true&amp;onLoadPanel=undefined&amp;captionBar=false" frameborder="0" height="400px" width="100%"></iframe>

<a href="https://umap.openstreetmap.fr/fr/map/odile-et-ses-6-chateaux_41758">Voir la carte</a>





