---
title: "Sur l’anarchie aujourd’hui"
date: 2023-03-19
author: "Christophe Masutti"
image: "/images/postimages/cm.png"
description: "Dans ce petit texte, G. Agamben nous rappelle cet interstice où le pouvoir s'exerce, entre l'État et l'administration dans ce qu'il est convenu d'appeller la gouvernance. En tant que système, elle organise le repli des pouvoirs séparés de la justice, de l'exécutif et du législatif dans un grand flou. Pour G. Agamben, la lutte anarchiste consiste justement à se situer dans cet interstice."
tags: ["Libres propos", "anarchie", "pouvoir", "Agamben"]
categories:
- Libres propos
---


Dans ce petit texte, G. Agamben nous rappelle cet interstice où le pouvoir s'exerce, entre l'État et l'administration dans ce qu'il est convenu d'appeller la gouvernance. En tant que système, elle organise le repli des pouvoirs séparés de la justice, de l'exécutif et du législatif dans un grand flou : de la norme ou de la standardisation, des agences au lieu des institutions traditionnelles, le calcul du marché (libéral) comme grand principe de gestion, etc. Pour G. Agamben, la lutte anarchiste consiste justement à se situer entre l'État et l'administration, contre cette gouvernance qui, comme le disait D. Graeber (dans <cite>Bullshit Jobs</cite>), structure l'extraction capitaliste. On pourra aussi penser aux travaux d'Alain Supiot (<cite>La Gouvernance par les nombres</cite>) et ceux aussi d'Eve Chiapello sur la sociologie des outils de gestion.

<!--more-->

---

## Sur l’anarchie aujourd’hui

Par Giorgio Agamben (février 2023).


Traduction reprise d'[Entêtement](https://entetement.com/sur-lanarchie-aujourdhui/) avec de très légères corrections.

Texte original sur [Quodlibet](https://www.quodlibet.it/giorgio-agamben-anarchia-oggi).

Si pour ceux qui entendent penser la politique, dont elle constitue en quelque sorte le foyer extrême ou le point de fuite, l’anarchie n’a jamais cessé d’être d’actualité, elle l’est aujourd’hui aussi en raison de la persécution injuste et féroce à laquelle un anarchiste est soumis dans les prisons italiennes. Mais parler de l’anarchie, comme on a dû le faire, sur le plan du droit, implique nécessairement un paradoxe, car il est pour le moins contradictoire d’exiger que l’État reconnaisse le droit de nier l’État, tout comme, si l’on entend mener le droit de résistance jusqu’à ses ultimes conséquences, on ne peut raisonnablement exiger que la possibilité de la guerre civile soit légalement protégée.

Pour penser l’anarchisme aujourd’hui, il convient donc de se placer dans une tout autre perspective et de s’interroger plutôt sur la manière dont Engels le concevait, lorsqu’il reprochait aux anarchistes de vouloir substituer l’administration à l’État. Dans cette accusation réside en fait un problème politique décisif, que ni les marxistes ni peut-être les anarchistes eux-mêmes n’ont correctement posé. Un problème d’autant plus urgent que nous assistons aujourd’hui à une tentative de réaliser de manière quelque peu parodique ce qui était pour Engels le but déclaré de l’anarchie – à savoir, non pas tant la simple substitution de l’administration à l’État, mais plutôt l’identification de l’État et de l’administration dans une sorte de Léviathan, qui prend le masque bienveillant de l’administrateur. C’est ce que théorisent Sunstein et Vermeule dans un ouvrage (*Law and Leviathan, Redeeming the Administrative State*) dans lequel la *gouvernance*, l’exercice du gouvernement, dépasse et contamine les pouvoirs traditionnels (législatif, exécutif, judiciaire), exerçant au nom de l’administration et de manière discrétionnaire les fonctions et les pouvoirs qui étaient les leurs.

Qu’est-ce que l’administration ? *Minister*, dont le terme est dérivé, est le serviteur ou l’assistant par opposition à *magister*, le maître, le détenteur du pouvoir. Le mot est dérivé de la racine **men*, qui signifie diminution et petitesse. Le *minister* s’oppose au magister comme *minus* s’oppose à *magis*, le moins au plus, le petit au grand, ce qui diminue à ce qui augmente. L’idée d’anarchie consisterait, du moins selon Engels, à essayer de penser un ministre sans magister, un serviteur sans maître. Tentative certainement intéressante, puisqu’il peut être tactiquement avantageux de jouer le serviteur contre le maître, le petit contre le grand, et de penser une société dans laquelle tous sont ministres et aucun *magister* ou chef. C’est en quelque sorte ce qu’a fait Hegel, en montrant dans sa fameuse dialectique que le serviteur finit par dominer le maître. Il est néanmoins indéniable que les deux figures clés de la politique occidentale restent ainsi liées l’une à l’autre dans une relation inlassable, qu'il est impossible de solutionner une fois pour toutes.

Une idée radicale de l’anarchie ne peut alors que se dissoudre dans l’incessante dialectique du serviteur et de l’esclave, du *minister* et du *magister*, pour se situer résolument dans l’écart qui les sépare. Le *tertium* qui apparaît dans cet écart ne sera plus ni administration ni État, ni *minus* ni *magis* : il sera plutôt entre les deux comme un reste, exprimant leur impossibilité de coïncider. En d’autres termes, l’anarchie est d’abord et avant tout le désaveu radical non pas tant de l’État ni simplement de l’administration, mais plutôt de la prétention du pouvoir à faire coïncider État et administration dans le gouvernement des hommes. C’est contre cette prétention que l’anarchiste se bat, au nom finalement de l’ingouvernable, qui est le point de fuite de toute communauté entre les êtres humains.

*26 février 2023*

*Giorgio Agamben*
