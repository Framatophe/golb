
---
title: "Parcours VTT. Le Schneeberg"
date: 2016-05-12
author: "Christophe Masutti"
image: "/images/postimages/hikebike.png"
description: "Histoire de se mettre en jambe, le temps d'un parcours facile."
tags: ["Sport", "VTT", "Entraînement", "Parcours"]
categories:
- Sport
---


Et voilà que j'inaugure par ce billet une nouvelle section VTT à ce blog. Pour cette fois, c'est au Schneeberg que je vous propose une petite virée sympathique pour 30 km (900m D+) avec une variété de chemins intéressante.


Le départ se fait sur le parking de l'école d'Oberhaslach, derrière la mairie, où l'on peut garer son véhicule. D'emblée, le GR 531 vous tend les bras : empruntez-le jusqu'au Carrefour Anlagen. D'une part cela permet d'éviter d'emprunter un chemin forestier long et laborieux et d'autre part c'est sur un petit chemin façon *single* que se  fera l'essentiel de cette première étape, ni trop technique ni trop fatigante, bref de quoi se mettre en jambe.

C'est au Carrefour Anlagen, que commence vraiment l'ascension de l'essentiel des dénivelés. On rejoint le Col des Pandours pour enchaîner sur le GR 532 jusqu'à rejoindre les chemins forestiers Schlangenweg puis Kohlweg jusqu'au col de l'Eichkopf. Cette partie sur chemins larges est très roulante et mettra votre endurance à l'épreuve : plat, montées, descentes. Une petite remarque : à chaque patte d'oie, prenez toujours le chemin de droite (tout fini par se ressembler au bout d'un moment).

C'est sur l'Eichkopf que commence une belle partie technique jusqu'au Schneeberg. En montant le long de l'Urstein jusqu'au Col du Hoellenwasen on peut s'amuser à essayer de ne jamais poser pied à terre : la montée n'est pas dure, c'est le terrain qui est amusant... par temps sec. En cas d'humidité... il vaudrait mieux prendre un autre chemin. Au Col du Hoellenwasen, on pénètre dans une réserve biologique. Une descente très technique s'amorce dans laquelle il faut faire preuve d'une grande prudence : les racines créent des marches hautes qu'il vaut mieux passer en douceur et en retenant toute l'attention possible. On commence alors à remonter vers le Schneeberg en rejoignant la crête et le GR 53.

Pour la descente du Schneeberg, mon conseil est de ne pas suivre tout à fait les "croix rouges" jusqu'au refuge du Schneeberg. En effet, à deux endroits un portage de vélo sera nécessaire pour franchir les talus entre sentier et chemin forestiers. Je préconise de rester un petit peu sur le GR 53 puis embrayer sur le chemin forestier des Pandours jusqu'au Col du même nom. On rejoint ensuite le Carrefour Anlagen puis je propose d'aller visiter les ruines du château du Hohenstein avant de rejoindre la D 218 et le village d'Oberhaslach (ou bien poursuivre sur le chemin et rejoindre les croix jaunes pour arriver au dessus du parking de l'école).

Caractéristiques du parcours&nbsp;: 30&nbsp;Km, 945&nbsp;m&nbsp;D+.

<iframe width="100%" height="300px" frameBorder="0" src="https://framacarte.org/fr/map/oberhaslach-schneeberg-vtt_2896?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&datalayersControl=true&onLoadPanel=caption&captionBar=false"></iframe>

<a href="https://framacarte.org/fr/map/oberhaslach-schneeberg-vtt_2896">Voir en plein écran</a>
