---
title: "Parution : Affaires Privées"
date: 2020-03-02
author: "Christophe Masutti"
image: "/images/postimages/affpriv-banner.jpg"
description: "Parution de mon ouvrage. Affaires Privées. Aux sources du capitalisme de surveillance, CF éditions, mars 2020."
tags: ["Histoire", "Libres propos", "Capitalisme de surveillance", "Libertés"]
categories:
- Libres propos
---

Au quotidien, nos échanges numériques et nos comportements de consommateurs sont enregistrés, mesurés, calculés afin de construire des profils qui s'achètent et se vendent. Des débuts de la cybernétique aux big data, la surveillance a constitué un levier économique autant qu'idéologique.

<!--more-->

{{< toctoc >}}

## Descriptif

*Titre* : AFFAIRES PRIVÉES. Aux sources du capitalisme de surveillance

*Auteur* : Christophe Masutti

*Préface* de Francesca Musiani

*Édition* : [C&F éditions](https://cfeditions.com/masutti/), Collection Société numérique, 5

*Date de parution* : mars 2020

*Nombre de pages* : 475

*ISBN* :  978-2-37662-004-4

*Dimensions* : 15 x 21 cm

**Commander sur le site de l'éditeur : [C&F éditions](https://cfeditions.com/masutti/)**

**[Télécharger un descriptif de l'ouvrage (PDF)](https://statium.link/bazaar/Livre-affpriv/AFFAIRES-PRIVEES-CFeditions-Masutti-2020.pdf)**

## Couverture

{{< figure src="/images/Prem-couv-masutti-affaires-privees.jpg" title="Christophe Masutti, Affaires Privées. Aux sources du capitalisme de surveillance (couverture)" >}}

## 4e couverture

Au quotidien, nos échanges numériques et nos comportements de consommateurs sont enregistrés, mesurés, calculés afin de construire des profils qui s'achètent et se vendent. Des débuts de la cybernétique aux big data, la surveillance a constitué un levier économique autant qu'idéologique.

Dans Affaires privées, Christophe Masutti retrace l'histoire technique et culturelle de soixante années de controverses, de discours, de réalisations ou d'échecs. Ce retour aux sources offre un éclairage passionnant sur le capitalisme de surveillance et sur le rôle joué par le marketing dans l'informatisation de la société. Il décrit les transformations sociales et politiques provoquées par les révolutions informatiques et le marché des données.

La surveillance est utilisée par les administrations à des fins de contrôle, et par les entreprises pour renforcer leurs capacités commerciales. Si les pratiques de renseignement des États ont souvent été dénoncées, la surveillance venue du monde des affaires n'a longtemps suscité qu'indifférence. Le business des données en a profité pour bousculer les cadres juridiques et réglementaires de la vie privée.

Comment développer une économie numérique qui respecterait la vie privée des individus&nbsp;? Comment permettre à la vie privée d'échapper au pouvoir des affaires&nbsp;? Christophe Masutti propose une réflexion historique et politique sur les conditions d'émancipation face à l'économie de la surveillance.

## Une citation

Cité dans l’ouvrage&nbsp;:

> Ces grandes banques de données permettront au citoyen, lorsqu’il est confronté à un nouvel environnement, d’établir « qui il est » et d’acquérir rapidement les avantages qui découlent d’une évaluation de solvabilité (bancaire) fiable et d’un caractère social acceptable aux yeux de sa nouvelle communauté. En même temps, les établissements commerciaux ou gouvernementaux en sauront beaucoup plus sur la personne avec laquelle ils traitent. Nous pouvons nous attendre à ce qu’un grand nombre de renseignements sur les caractéristiques sociales, personnelles et économiques soient fournis volontairement – souvent avec empressement – afin de profiter des bienfaits de l’économie et du gouvernement.

Lance J. Hoffman, « Computer and Privacy : a survey » (1969).

## Argumentaire

« Envahisseurs de la vie privée », « homme informatisé », « société du dossier », « surveillance publique », « contrôle social »... toutes ces expressions sont nées à la fin des années 1960. Le processus d'informatisation de la société occidentale s'est fait sur le fond d'une crainte de l'évènement de la dystopie de 1984. Et pourtant, lorsqu'on raconte l'histoire de la révolution informatique, on retient surtout l'épopée des innovations technologiques et le storytelling californien.

Les pratiques de surveillance sont les procédés les plus automatisés possible qui consistent à récolter et stocker, à partir des individus, de leurs comportements et de leurs environnements, des données individuelles ou collectives à des fins d’analyse, d’inférence, de quantification, de prévision et d’influence. Des années 1960 à nos jours, elles se révèlent être les outils précieux des administrations publiques et répondent aux besoins concurrentiels et organisationnels des entreprises. On les appréhende d'autant mieux que l'on comprend, grâce à de nombreux exemples, qu'elles dépendent à la fois des infrastructures informatiques et de leur acceptation sociale. Ainsi, le marché de la donnée s'est progressivement étendu, économique, idéologique, culturel. Fruit de rapports sociaux, le capitalisme de la surveillance n'est pas un nouveau système économique, il est le résultat de l'adjonction aux affaires économiques de la surveillance électronique des individus et de la société.

Faire l'histoire (critique) du capitalisme de surveillance, c'est analyser les transformations sociales que l’informatisation a rendu possibles depuis les années 1960 en créant des modèles économiques basés sur le traitement et la valorisation des données personnelles. La prégnance de ces modèles ajoutée à une croissance de l’industrie et des services informatiques a fini par créer un marché hégémonique de la surveillance. Cette hégémonie associée à une culture consumériste se traduit dans plusieurs contextes : l’influence des consommateurs par le marketing, l’automatisation de la décision, la réduction de la vie privée, la segmentation sociale, un affaiblissement des politiques, un assujettissement du droit, une tendance idéologique au solutionnisme technologique. Il fait finalement entrer en crise les institutions et le contrat social. 

Du point de vue de ses mécanismes, le capitalisme de surveillance mobilise les pratiques d’appropriation et de capitalisation des informations pour mettre en œuvre le régime disciplinaire de son développement industriel et l’ordonnancement de la consommation. Des bases de données d’hier aux big data d’aujourd’hui, il permet la concentration de l’information, des capitaux financiers et des technologies par un petit nombre d’acteurs tout en procédant à l’expropriation mercantile de la vie privée du plus grand nombre d’individus et de leurs savoirs. Et parce que nous sommes capables aujourd'hui d'énoncer cette surveillance aux multiples conséquences, il importe de proposer quelques solutions collectives d'émancipation.
