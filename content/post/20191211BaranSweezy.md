---
title: "Monopoly Capital"
date: 2019-12-11
author: "Christophe Masutti"
image: "/images/postimages/fatcaptainamerican.jpg"
description: "En 1966, paraît un pavé dans les sciences économiques aux États-Unis : Monopoly Capital: An Essay on the American Economic and Social Order (trad. fr. : Le capitalisme monopoliste - Un essai sur la société industrielle américaine). Il est écrit par deux économistes d'inspiration marxiste, Paul Sweezy (fondateur de la Monthly Review) et Paul A. Baran (professeur d'économie à Standford)."
tags: ["capitalisme", "Libres propos", "oligarchie", "Politique", "marketing"]
categories:
- Libres propos
---

«&nbsp;Aux États-Unis comme dans tous les autres pays capitalistes, les masses dépossédées n'ont jamais été en mesure de déterminer leurs conditions de vie ou les politiques suivies par les différents gouvernements. Néanmoins, tant que démocratie signifiait le renversement du despotisme monarchique et l'arrivée au pouvoir d'une bourgeoisie relativement nombreuse, le terme mettait l'accent sur un changement majeur de la vie sociale. Mais qu'est-il resté de ce contenu de vérité dans une société où une oligarchie minuscule fondée sur un vaste pouvoir économique et contrôlant pleinement l'appareil politique et culturel prend toutes les décisions politiques importantes&nbsp;? Il est clair que l'affirmation selon laquelle une telle société est démocratique ne fait que dissimuler la vérité au lieu de la mettre à jour.&nbsp;» (P. A. Baran, P. Sweezy, *Monopoly Capital*, 1966).

<!--more-->

Il y a des citations dont le commentaire ne ferait que retranscrire les évidences auxquelles nous faisons face actuellement (les fonctions de l'État bourgeois). Remplacez ici le pouvoir monarchique par la peur du fascisme, et vous retrouvez la soupe continuellement touillée par une certaine oligarchie française (pour ne citer qu'elle). Lorsqu'il se plonge dans les vieux grimoires, le sorcier en sort toujours des grenouilles.

En 1966, paraît un pavé dans les sciences économiques aux États-Unis&nbsp;: *Monopoly Capital: An Essay on the American Economic and Social Order* (trad. fr.&nbsp;: *Le capitalisme monopoliste - Un essai sur la société industrielle américaine*). Il est écrit par deux économistes d'inspiration marxiste, Paul Sweezy (fondateur de la *Monthly Review*) et Paul A. Baran (professeur d'économie à Standford). Le livre sera pour ce dernier publié à titre posthume.

{{< figure src="/images/Monopoly_Capital_couverture.jpg" title="P. A. Baran, P. Sweezy, Monopoly Capital: An Essay on the American Economic and Social Order" >}}


En quoi cet ouvrage est-il intéressant aujourd'hui&nbsp;? Nous évoluons dans un modèle d'économie capitaliste à forte tendance monopoliste et cet ouvrage en développe une description particulièrement pertinente. Néanmoins, depuis les années 1960 beaucoup de choses ont changé et lire cet ouvrage aujourd'hui implique de le resituer dans son contexte&nbsp;: une économie capitaliste-monopoliste *naissante*. Et c'est là que l'ouvrage prend toute sa dimension puisqu'il explore les mécanismes du modèle que nous subissons depuis des années, en particulier la manière dont les schémas concurrentiels influent sur l'ensemble de l'économie et, par conséquent, la politique.

Qu'il s'agisse d'un capitalisme dirigiste ou de l'émergence de monopoles privés sur certains secteurs économiques (grâce aux «&nbsp;bienfaits&nbsp;» du libéralisme), le rôle de la décision publique dans le cours économique est fondamental à tous points de vue. C'est l'évidence pour la Chine par exemple. Mais en Occident, le libéralisme a tendance parfois à masquer cette évidence au profit d'une croyance populaire en des lois quasi-naturelles d'équilibre de marché et d'auto-régulation. Superstitions.

Pour ce qui concerne l'histoire contemporaine de l'économie américaine et, par extension celle des économies occidentales, l'apparition des monopoles est le résultat de stratégies politiques. Pour les deux auteurs, l'accumulation du capital par des acteurs privés atteint des sommets vertigineux à partir du moment où ces stratégies  (notamment en politique extérieure) se déploient selon deux grands principes&nbsp;: l'impérialisme économique et la domination militaire. À leur suite, et surtout après la crise du début des années 1970, d'autres économistes apportèrent encore d'autres éléments&nbsp;: le rôle de la politique dans la régulation du marché intérieur comme arme stratégique protectionniste, la division sectorielle de l'impérialisme (financier, informationnel et culturel), le lobbying, etc.

La traduction littérale du titre originel de l'ouvrage est sans doute plus évocatrice. Il s'agit bien de la création d'un nouvel ordre social et économique. P. A. Baran et P. Sweezy ont su prendre la mesure du nouvel agencement socio-économique de la seconde moitié du <span style="font-variant: small-caps;">xx</span><sup>e</sup> siècle, au-delà de la seule question de la consommation de masse. Le refrain n'a guère changé aujourd'hui&nbsp;: en faveur de la dynamique accumulatrice du capitalisme dans une économie monopoliste, la politique doit jouer son rôle de transfert des ressources vers le privé. Aucun échange non-marchand n'a d'avenir dans une telle économie. Sa nature procède par phases successives de centralisations des ressources (matérielles, informationnelles et financières) et d'éclatements de bulles erratiques.

Pour ce qui concerne le nouvel ordre social, les auteurs ont identifié plusieurs dynamiques en cours aux différents niveaux de l'économie quotidienne&nbsp;: les fluctuations de l'immobilier, les banques, les assurances et, surtout, le marketing. Cette dernière activité (avec l'impérialisme et le militarisme) a pour objectif la bonne allocation du *surplus économique*, c'est-à-dire le rapport excédentaire entre ce qu'une société produit et le coût (théorique) de la production. Avec le marketing, l'absorption du surplus est assurée selon deux moteurs&nbsp;: l'investissement et la consommation.

Il me semblait amusant de recenser quelques citations relevées par P. A. Baran et P. Sweezy et transcrites dans leur ouvrage au chapitre concernant le marketing. Je les reproduis ci-dessous. En effet, dans ce domaine on cite régulièrement l'inventeur du marketing (plus exactement des «&nbsp;relations publiques&nbsp;») Edward L. Bernays et son ouvrage *[Propaganda](https://www.editionsladecouverte.fr/catalogue/index-Propaganda-9782355220012.html)*. Mais on oublie bien souvent qu'il a fait école et que les piliers du marketing, la mesure et l'influence des comportements des individus et des masses, sont à la base de la propagande nécessaire à l'assentiment politique des foules. Jusqu'à atteindre des raffinements inégalés avec les GAFAM (pensons à l'affaire Cambridge Analytica, pour ne citer qu'elle). À cet égard, nous avons bel et bien vécu plus de 50 années bercés par une *persuasion clandestine* (pour reprendre le titre français du [best-seller de V. Packard](https://www.persee.fr/doc/pop_0032-4663_1959_num_14_1_6239)). De cette lancinante berceuse, il est temps de s'extraire (avec pertes et fracas).


## Quelques citations

Pourtant, il suffisait de lire... Les citations ci-dessous n'ont rien d'étonnant si on les considère avec nos yeux du début du <span style="font-variant: small-caps;">xxi</span><sup>e</sup> siècle, mais c'est justement ce qu'elles préfiguraient...


> La publicité affecte la demande... en transformant les besoins eux-mêmes. La distinction entre ceci et la modification des moyens de satisfaire des besoins existants est souvent cachée dans la pratique par le chevauchement des deux phénomènes ; analytiquement, elle est parfaitement claire. Une publicité qui ne fait qu'afficher le nom d'une marque de fabrique particulière ne donnera aucune information sur le produit lui-même ; cependant, si ce nom devient ainsi plus familier pour les acheteurs ils auront tendance à le demander de préférence à d'autres marques peu connues et sans publicité. De même, les méthodes de vente qui jouent sur la susceptibilité de l'acheteur, qui se servent de lois psychologiques qu'il ignore et contre lesquelles il ne peut se défendre, qui l'effrayent, le flattent ou le désarment, n'ont rien à voir avec l'information du client&nbsp;: elles tendent à manipuler et non à informer. Elles créent un nouvel ensemble de besoins en remodelant ses motivations.
>
> -- <cite>Edward H. Chamberlin, *The Theory Of Monopolistic Competition*, Cambridge, Mass., 1931, p. 119.</cite>



> «&nbsp;Les études menées au cours des douze dernières années montrent de façon évidente que les individus sont influencés par la publicité sans en être conscients. L'individu qui achète est motivé par une annonce publicitaire mais il ignore souvent l'origine de cette motivation.&nbsp;»
>
> -- <cite>Louis Cheskin, *Why People Buy*, New York, 1959, p. 61.</cite>


> Les défenseurs de la publicité affirment qu'elle comprend de nombreux avantages économiques. D'utiles informations sont transmises au public ; des marchés s'ouvrent à la production de masse ; et en guise de sous-produits nous obtenons une presse indépendante, le choix entre de nombreux programmes de radio et de télévision, et des revues épaisses. Et ainsi de suite. D'autre part on prétend que l'excès de publicité tend à en annuler les effets et procure peu d'informations valables au consommateur ; que pour chaque minute de musique symphonique il y a une demi-heure de mélo. Le problème serait plus susceptible d'être discuté s'il n'existait le fait troublant, révélé par le Sondage Gallup, que de nombreuses personnes semblent aimer la publicité. Ils ne croient pas tout ce qu'on leur raconte mais ils ne peuvent s'empêcher de s'en souvenir.
>
> -- <cite>Paul A . Samuelson, *Economics*, New York, 1961, p. 138.</cite>


> Claude Hopkins, qui est l'un des «&nbsp;immortels&nbsp;» de la publicité raconte l'histoire de l'une de ses campagnes publicitaires pour une marque de bière. Au cours d'une visite à la brasserie il écouta poliment l'exposé des qualités du malt et du houblon employés, mais ne s'intéressa qu'à la stérilisation par la vapeur des bouteilles vides. Son client lui fit remarquer que toutes les brasseries procédaient de la sorte. Hopkins lui expliqua patiemment que ce n'était pas ce que les brasseries faisaient qui importait, mais ce qu'elles affirmaient faire par leur publicité. Il fonda sa campagne sur le slogan&nbsp;: «&nbsp;Nos bouteilles sont lavées à la vapeur&nbsp;!&nbsp;». George Washington Hill, le grand fabricant de tabac, fonda une campagne publicitaire sur le slogan «&nbsp;Nous grillons notre tabac&nbsp;!&nbsp;» En fait, tous les tabacs le sont, mais aucun autre fabricant n'avait pensé à l'énorme potentiel publicitaire du slogan. Hopkins, remporta une autre grande victoire publicitaire en proclamant à propos d'une marque de dentifrice&nbsp;: «&nbsp;Enlève la pellicule qui se forme sur vos dents&nbsp;!&nbsp;» En vérité tous les dentifrices en font autant.
>
> -- <cite>Rosser Reeves, *Reality in Advertising*, New York, 1961, p. 55-56.</cite>


> Quand la forme du produit est reliée à la vente plutôt qu'à la fonction productive, comme cela est le cas de plus en plus souvent, et quand la stratégie de vente est fondée sur de fréquents changements de style, certains résultats sont presque inévitables&nbsp;: tendance à l'emploi de matières de qualité inférieure ; «&nbsp;raccourcis&nbsp;» adoptés pour limiter le temps indispensable à une bonne mise au point des produits ; négligence sur l'indispensable contrôle de qualité. Une telle obsolescence provoquée amène une augmentation de prix pour le consommateur sous la forme d'une réduction de la durée des biens et d'un accroissement des frais de réparation.
>
> -- <cite>Dexter Master, ancien dirigeant de l'Association des Consommateurs, cité par Vance Packard, *The  Waste Makers*, 1960, p.14.</cite>
