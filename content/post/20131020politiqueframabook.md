---
title: "La politique Framabook"
date: 2013-10-20
author: "Christophe Masutti et Benjamin Jean"
image: "/images/postimages/say.png"
tags: ["Logiciel libre", "Framasoft", "édition"]
description: "Quelle est la politique éditoriale du projet Framabook ?"
categories:
- Libres propos
---



Voici un texte [publié sur Framablog](http://www.framablog.org/index.php/post/2013/10/16/framabook-et-licences-libres) avec Benjamin Jean, à propos des licences libres et de la question de
leur choix dans la collection Framabook. Ce texte est aussi le prétexte
pour une mise au point démontrant les limites du choix des licences
interdisant les usages commerciaux ou les modifications d'une œuvre.
Nous avons voulu montrer que le modèle éditorial de la collection
Framabook est non seulement viable, éthique et pertinent, mais aussi
qu'il est transposable pour toute forme de diffusion d'œuvre dans une
logique de partage. Ainsi les publications scientifiques, tout
particulièrement, devraient adopter ce modèle.

La «&nbsp;politique&nbsp;» Framabook et les licences libres
-------------------------------------------------

Par Christophe Masutti et Benjamin Jean

Version 1.1 – 16 octobre 2013

Texte placé sous LAL 1.3&nbsp;; GNU FDL 1.3&nbsp;; CC-By-SA 3.0

*Christophe Masutti est docteur en histoire des sciences et des
techniques, chercheur associé au SAGE (Société, Acteurs, Gouvernements
en Europe, Université de Strasbourg), responsable des affaires
européennes à la Direction Générale des Hôpitaux Universitaires de
Strasbourg. Président de l'association Framasoft depuis janvier 2012.*

*Benjamin Jean est membre de l'équipe du CUERPI (Centre Universitaire
d'Enseignement et de Recherches en Propriété Intellectuelle)
co-fondateur de la société Inno<sup>3</sup>, consultant au Cabinet Gilles Vercken
et maître de conférence à Science Po. Co-fondateur de l'association
Veni, Vidi, Libri et du cycle de conférences European Open Source & Free
Software Law Event (EOLE).*

---

Initié en mai 2006, Framabook est le nom donné au projet de collection
de livres libres édités par Framasoft[^1]. Basée sur
une méthode de travail collaborative entre l'auteur et des bénévoles de
l'association, la collection dispose d'un comité de lecture et d'un
comité éditorial. Elle propose des manuels, des essais et même des
bandes dessinées et des romans en lien avec le logiciel libre ou la
culture libre en général. Le choix des licences qui les accompagnent les
inscrit dans la [culture libre](http://fr.wikipedia.org/wiki/Culture_libre) et la participation
aux [biens communs](http://fr.wikipedia.org/wiki/Biens_communs).

Depuis
que Framasoft a choisi de devenir éditeur de sa collection, nous avons
tant bien que mal travaillé à la construction d'un modèle alternatif et
collaboratif d'édition. Dans nos discussions avec les auteurs, la
question des licences acceptées pour la diffusion des projets est
récurrente (pour ne pas dire systématique). Ce sujet relativement
technique a mobilisé le débat de nos assemblées générales, se
poursuivant parfois tard en soirée et sur nos listes de discussion, pour
des réponses finalement toujours similaires («&nbsp;des licences libres et
seulement des licences libres&nbsp;»).

Nous nous sommes aperçus que cette
recherche répétée de consensus résultait surtout du manque d'exposition
claire des principes auxquels adhère la collection Framabook. C'est pour
y remédier que cet article a été écrit. Il cherche à exposer les
principes de la politique éditoriale du projet Framabook tout en
rassemblant les différents éléments de discussion issus des archives de
nos listes et qui concernent précisément les licences libres. D'une
certaine manière, il témoigne aussi d'une réflexion devenue mature et
qui nous semble valider la pertinence d'un modèle d'édition ouverte.
Nous destinons aussi ces quelques lignes aux auteurs et éditeurs, afin
de les aider à cerner les enjeux du choix d'une licence pour une œuvre
destinée à être publiée dans la collection Framabook ou ailleurs.

Nous
avons conscience que ce choix n'est pas anodin et peut même être
difficile, tant d'un point de vue culturel après presque trois siècles
d'histoire [du droit d'auteur](http://fr.wikipedia.org/wiki/Droit_d'auteur), que d'un point
de vue économique et éthique, dans le contexte d'une économie de la
culture qui multiplie les abus en tout genre. Nous ne cherchons pas non
plus à prétendre que ce modèle devrait remplacer les modèles existants,
nous souhaitons seulement démontrer qu'il peut être viable et, surtout,
qu'il ne génère pas tous les risques et déviances qu'on lui rattache
fréquemment. Bien que l'un de nos ouvrages compte désormais comme une
référence en la matière (Benjamin Jean, [*Option Libre. Du bon usage des licences libres*](http://framabook.org/option-libre-du-bon-usage-des-licences-libres))[^2], certaines subtilités nécessitent à la fois une connaissance du droit
d'auteur, une connaissance du domaine de l'édition et une connaissance
des licences libres. L'ensemble est néanmoins à la portée de tous et
n'excède pas les quelques minutes de la lecture à laquelle nous vous
invitons, sous la forme de questions fréquemment posées (QFP)&hellip;

Sous quelle licence dois-je placer mon œuvre dans la collection Framabook&nbsp;?
------------------------------------------------------------------------------

Le premier postulat de notre collection est que les auteurs sont
absolument libres d'utiliser les licences qu'ils souhaitent pourvu
qu'elles soient «&nbsp;libres&nbsp;», c'est-à-dire qu'elles assurent à
l'utilisateur une libre utilisation, copie, modification ou
redistribution de l'ouvrage ou de ses dérivés (ce qui exclut donc toutes
les licences Creatives Commons limitant l'usage commercial «&nbsp;NC&nbsp;» ou la
modification «&nbsp;ND&nbsp;», ainsi que nous allons le développer plus loin).

Dans l'esprit du Libre auquel nous adhérons, cette définition n'exclut
pas les licences dites [copyleft](http://fr.wikipedia.org/wiki/Copyleft)
qui imposent la pérennité des libertés assurées à l'utilisateur
(garantissant à l'auteur que le livre ne pourra être exploité que
librement). Ce choix n'est pas neutre puisque ce type de licences permet
d'alimenter un «&nbsp;pot commun&nbsp;» auquel tout le monde peut puiser à
condition d'y reverser à son tour ses propres contributions. En d'autres
termes, un Framabook pourra être aussi bien sous [Licence Art Libre 1.3](http://artlibre.org/licence/lal), sous licence [CC-By-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/fr/), que sous licence [CC-By 3.0](http://creativecommons.org/licenses/by/3.0/fr/) («&nbsp;tout court&nbsp;») voire sous
[CC-0](http://creativecommons.org/choose/zero/?lang=fr)[^3].
Vous serez toujours libre de réutiliser ces ouvrages (même
commercialement), mais à votre charge de respecter les obligations que
ces licences contiennent.

---

**Par exemple –** Si quelqu'un rédige un texte incluant un passage
substantiel (en termes qualitatifs ou quantitatifs) tiré d'un Framabook,
et même si cet usage dépasse le cadre délimité du droit de citation, la
licence libre associée par l'auteur lui accordera les droits nécessaires
(à condition que soient parallèlement respectées les contraintes qu'elle
impose). Au titre de ces contraintes, certaines licences copyleft
imposeront que toutes modifications apportées à ce texte soient
diffusées selon la même licence, voire que l'intégralité de l'œuvre
utilisatrice soit distribuée selon la même licence (ce qui,
contrairement à la première hypothèse, limite grandement le type
d'exploitation possible). Ainsi qu'indiqué précédemment, cette
obligation permet d'assurer une relative pérennité au projet initial et
s'ajoute aux obligations classiques telle que l'obligation d'attribuer
la paternité de l'œuvre initiale à nos auteurs (et ceux-ci seulement&nbsp;;
vous serez pour votre part auteur de votre propre version dérivée).

---

Pourquoi utiliser des licences libres&nbsp;?
------------------------------------------

Avant toute autre considération, le Libre procède d'une volonté de
**partage**. Si vous placez une œuvre sous licence libre, c'est que vous
désirez la partager avec le plus grand nombre d'«&nbsp;utilisateurs&nbsp;» ou de
contributeurs possible. Importante dans le monde physique, cette notion
de partage se révèle encore plus évidente dans le monde immatériel
(celui de la propriété intellectuelle) où l'acquisition par un individu
n'implique pas l'aliénation ou la perte par l'autre (bien au
contraire)[^4]. Ainsi, «&nbsp;Libre&nbsp;» ne signifie donc pas
«&nbsp;libre de droits&nbsp;» (notion qui n'a aucune valeur juridique) et les
licences libres sont là pour traduire et sécuriser juridiquement la
relation souhaitée[^5]. Le projet Framabook repose
donc sur&nbsp;:

-   *l'usage de licences libres par lesquelles les auteurs exploitent leurs droits*. C'est grâce à ce contrat que toutes les autorisations indispensables à l'évolution et à la diffusion de l'œuvre sont données (en l'absence de licence, rien ne serait permis).
-   *le respect du droit d'auteur dans sa globalité, et notamment des prérogatives morales* (droit de divulgation, droit de paternité, droit au respect de l'intégrité de l'œuvre, etc.) qui protègent l'auteur en raison des liens étroits qu'il entretient avec son œuvre. Ajoutons qu'il n'y a pas de remise en cause de ces prérogatives morales par les licences libres&nbsp;; bien au contraire, celles-ci les rappellent (et parfois renforcent) systématiquement.
-   *sur le respect des conditions relatives au prix de vente des livres*. La loi Lang ([81-766 du 10 août 1981 modifiée 2008](http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000517179)) sur le [prix unique du livre](http://vosdroits.service-public.fr/professionnels-entreprises/F22713.xhtml) doit toujours être respectée quelle que soit la politique éditoriale choisie (ces règles s'appliquent aussi sur la vente des versions numériques – même si un prix différent du prix papier peut alors être décidé).

Ainsi, l'utilisation d'une licence libre est indispensable pour assurer
aux utilisateurs les libertés proclamées par l'auteur et par la
collection.

N'est-ce pas contradictoire avec la commercialisation des livres ?
---------------------------------------------------------------------

L'adage «&nbsp;libre ne signifie pas gratuit&nbsp;» s'applique parfaitement pour
ce qui concerne la collection Framabook.

La politique de la collection
consiste à proposer un modèle économique du livre basé à la fois sur la
primauté de la diffusion et la juste rémunération des auteurs. Puisque
nous vendons les livres «&nbsp;papier&nbsp;» et encourageons d'éventuelles
rééditions, nous ne voulons pas utiliser de clause de licence
interdisant à priori la vente (pas de -NC dans le cas des licences
Creative Commons).

Bien que la clause NC [puisse être légitimée](http://owni.fr/2012/10/18/le-non-commercial-avenir-de-la-culture-libre/),
il y a un contexte propre à la collection Framabook. Framasoft est un
réseau d'éducation populaire dédié au Libre qui s'appuie sur une
association d'intérêt général à but non lucratif. De cette orientation
découle toute l'importance de la diffusion au plus grand nombre.

C'est
pour cela que nous avons fait le choix de distribuer gratuitement les
versions numériques des ouvrages. Mais elles auraient pu aussi bien être
vendues au même titre que les livres «&nbsp;papier&nbsp;». Quoi qu'il en soit, les
sources (les fichiers originaux servant à la composition de l'œuvre)
étant elles aussi disponibles, tout le monde peut les utiliser afin de
diffuser à son tour gratuitement ou non. Par essence, la clause de type
-NC contrevient au principe de libre diffusion et de partage, à moins de
lever à chaque fois cette clause pour chaque cas particulier (et, même
dans cette situation, nous nous placerions dans une situation
privilégiée qui serait contre-productive compte tenu du partage qui nous
motive).

Certaines maisons d'édition effectuent ainsi une sorte de «&nbsp;Libre-washing&nbsp;» en profitant de leur position de monopole sur l'œuvre pour lever cette clause temporairement moyennant une rémunération que
l'auteur ne touche pas obligatoirement. L'idée est de prétendre une
œuvre libre mais en conservant le monopole et en exerçant des
contraintes indues. Nous pensons que dans la mesure où une maison
d'édition désire rééditer un Framabook, moyennant les conditions
exposées à la question numéro 4, elle devrait pouvoir le faire
indépendamment de Framasoft, en directe relation avec l'auteur. Nous
n'avons donc pas à fixer un cadre non-commercial et encore moins fixer
un prix pour ces rééditions.

L'exemple typique est le besoin d'une
réédition locale hors de France afin d'économiser des frais de port
parfois exorbitants&nbsp;: soit il s'agit d'une réédition, soit il s'agit
d'une simple ré-impression, mais dans les deux cas, nous n'avons aucun
profit à tirer puisqu'il s'agit de toute façon d'un territoire ou d'un
secteur dans lequel nous ne sommes pas présent ou actif. Au contraire,
une telle diffusion par un tiers (partenaire ou non) est créateur de
valeur pour ce tiers, pour la collection ainsi que pour l'auteur (qui,
selon nous, mérite un intéressement bien que les négociations ne nous
regardent pas).

Par ailleurs, ne bénéficiant que d'une simple cession de
droits non exclusive de la part de nos auteurs, nous assumons pleinement
le risque d'être concurrencés dans notre rôle d'éditeur si jamais nous
ne remplissions pas nos engagements (éthiques ou économiques).

Dans le
cas d'une traduction, l'usage d'une licence contenant une clause -NC
interdirait à priori la vente de l'œuvre traduite (et donc modifiée). En
faisant une nouvelle voie d'exploitation, des maisons d'édition
proposent parfois de lever la clause pour cette traduction moyennant une
somme forfaitaire sur laquelle l'auteur peut le plus souvent ne rien
toucher puisque son contrat ne le lie qu'à la première maison d'édition.
Par ailleurs, comme le contrat de cet auteur est généralement exclusif,
il ne peut contracter librement avec la maison d'édition qui édite la
traduction, sauf accord préalable avec la première.

Nous pensons au
contraire que non seulement les contrats d'édition ne doivent pas «&nbsp;lier
» (au sens premier) un auteur avec sa maison d'édition mais aussi que
celle-ci doit prendre la mesure de sa responsabilité éditoriale sans
exercer de monopole et signer avec l'auteur des contrats non exclusifs
qui lui permettent d'être contacté par une maison d'édition cherchant à
traduire son œuvre à lui, sans pour autant passer par un intermédiaire
rendu obligatoire uniquement pour des raisons mercantiles (il peut y
avoir des raisons tout à fait acceptables, cependant)[^6].

Concernant les livres «&nbsp;papier&nbsp;», nous
avons fait le choix de ne pas (tous) les vendre à prix coûtant. Il y a
deux raisons à cela:

1.  Depuis 2011, Framasoft a choisi de devenir son propre éditeur. À ce
    titre nous passons directement des contrats d'édition avec les
    auteurs, comprenant une rémunération à hauteur de 15% du prix de
    vente de chaque exemplaire. Ce pourcentage est toujours négociable,
    mais nous essayons d'en faire une règle, sans quoi il influe trop
    sur le prix de vente. Nous avons bien conscience que ce pourcentage
    est nettement plus élevé que ce qui se pratique habituellement dans
    le monde de l'édition. Il nous semble en effet important que nos
    auteurs qui ont fait le choix et le pari de la licence libre avec
    nous s'y retrouvent financièrement et bénéficient ainsi du modèle
    contributif dans lequel s'inscrit la collection[^7].
2.  Framasoft est composé de bénévoles mais repose sur une association
    qui compte aujourd'hui plusieurs permanents[^8]. À
    ce titre, le budget de tout projet doit être le plus équilibré
    possible. Or, éditer un livre suppose de nombreux coûts&nbsp;: le prix de
    vente est basé sur la fabrication, les frais de port et les frais
    annexes (administration, BAT, pertes, dons de livres, commission de
    l'association EnVentelibre.org qui se charge de la vente, des
    livraisons, de la charge TVA, etc.). Dans les frais annexes, nous
    pouvons aussi inclure les livres qui sont vendus à prix coûtant
    (afin de maintenir un prix «&nbsp;acceptable&nbsp;»[^9]).
    Ainsi, en faisant en sorte de rester en deçà des prix habituellement
    pratiqués et gardant comme objectif de favoriser la diffusion des
    connaissances dont elle est responsable, l'association Framasoft
    perçoit une somme forfaitaire à chaque vente qui lui permet de
    contribuer à faire vivre les projets éditoriaux de
    l'association[^10].

Ainsi, l'usage d'une licence qui autorise les usages commerciaux est à
la fois conforme à nos objectifs internes (et à la mission d'intérêt
général que revêt Framasoft) et constitutive d'un modèle d'édition
ouvert qui tire plein profit des opportunités de notre société numérique
et internationale.

Puis-je rééditer un Framabook&nbsp;?
----------------------------------

Oui, c'est même encouragé, sans quoi le modèle économique que nous
défendons n'aurait pas de sens. Cependant, n'oubliez pas que les
licences libres imposent certaines contraintes ! En plus de celles-ci,
pour ce qui concerne les Framabooks, il y a d'autres éléments à prendre
en compte.

-   Toute réédition doit bien sûr respecter la licence de l'ouvrage à la
    lettre, à défaut de quoi elle serait non autorisée et donc
    contrefaisante (cela couvre les obligations en matière de mentions
    légales – respect de la paternité, indication du Framabook
    d'origine, de la licence, etc. –, mais plus largement toutes les
    autres obligations de la licence – et donc notamment lorsqu'elle se
    présente la clause *share alike/copyleft* à laquelle est parfois
    associée l'obligation de livrer la version source du fichier
    publié).
-   Les auteurs des Framabook ont signé des contrats d'édition. Soumis
    par le Code de la propriété intellectuelle à un régime dédié, les
    contrats d'édition sont particulièrement protecteurs des intérêts
    des auteurs (et un éditeur ne peut y déroger)[^11]. Les contrats conclus par Framasoft
    avec les auteurs ne couvrent que notre propre collection et sont
    dits «&nbsp;non exclusifs&nbsp;» (n'empêchant donc pas un auteur de publier
    une réédition ailleurs). Toute nouvelle édition de l'ouvrage devra
    donc donner lieu à un nouveau contrat d'édition signé par le ou les
    auteurs (avec ou sans un intéressement à la vente, selon les
    négociations).
-   Toute réédition d'un Framabook consiste à utiliser le contenu d'un
    livre (en le modifiant ou non) pour le diffuser par une autre maison
    d'édition, avec un nouvel ISBN. Il ne s'agit donc pas seulement de
    revendre un Framabook déjà édité par Framasoft. Dans ce cadre, hors
    accord spécifique avec l'association Framasoft, toute réédition ne
    doit pas réutiliser l'identité de Framasoft (ou son dérivée
    Framabook) qui est une marque déposée. Naturellement, Framasoft doit
    être mentionné dans les crédits («&nbsp;Première édition&nbsp;: Framasoft
    (année)&nbsp;»).

Alors, tout le monde pourrait modifier mon œuvre et je n'aurais rien à dire&nbsp;? Ne devrais-je pas plutôt utiliser une licence comme CC-BY-ND (sans modification)&nbsp;?
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

La réponse mérite un développement. Certaines personnes, et c'est en
particulier le cas de [Richard M. Stallman](http://www.framablog.org/index.php/post/2013/01/31/stallman-creative-commons-non-commercial), affirment que dans le cas d'œuvres dites «&nbsp;d'opinion&nbsp;», la pensée de
l'auteur ne devrait pas pouvoir être déformée[^12]. Ces œuvres constitueraient donc autant de cas où une licence doit
pouvoir empêcher toute modification de l'œuvre[^13].

En réalité, le droit d'auteur[^14] est bien plus
subtil que ne laisse paraître ce genre de posture. Premièrement, Richard
M. Stallman confond le fond et la forme&nbsp;: le droit d'auteur protège la
forme que donne l'auteur à certaines idées, en aucun cas il ne protège
les idées ou l'opinion d'un auteur (celles-ci n'étant, en tant que
telles, génératrices d'aucun droit). À partir de là, apposer sur la
forme une licence qui limite la réutilisation qui peut en être faite
apparaît comme une limitation qui empêche *in fine* (pour un auteur)
d'utiliser une certaine matière (les écrits, tournures, etc.) sans pour
autant apporter de garantie quant à la réutilisation (ou non) des idées
ou opinions qu'elle contient. Cela est d'autant plus dommage que la
société actuelle donne une place de plus en plus grande au «&nbsp;mashup&nbsp;»,
ainsi qu'à tous ces processus de créations utilisant des œuvres
premières comme matière, et qu'une licence qui interdit les dérivations
s'oppose frontalement à cet usage. Aussi, jamais une licence libre (qui
ne porte donc que sur le droit d'auteur – l'expression, la forme)
n'autorisera de modifier une œuvre de telle sorte que cette modification
porte atteinte à l'intégrité de l'œuvre.

Dans le cadre d'une œuvre
conçue par son auteur comme ouverte et collaborative, la modification
par un contributeur est par principe entièrement respectueuse de
l'intégrité de l'œuvre. Néanmoins, s'il était porté sur l'œuvre une
modification manifestement non conforme à la représentation qu'en avait
son auteur, il serait tout à fait valable qu'un auteur agisse sur le
fondement de ses droits moraux pour faire cesser cette atteinte (de la
même façon qu'il pourrait le faire en l'absence de licence libre), en
particulier si l'œuvre était utilisée pour véhiculer des messages
manifestement contraires à l'intention de l'auteur.

Au-delà du champ du
droit d'auteur, ajoutons qu'il reste bien entendu interdit de publier
toute version dérivée qui serait présentée de telle sorte qu'elle
véhiculerait une idée fausse&nbsp;: soit que l'auteur initial de l'œuvre en
serait aussi l'auteur, soit qu'il ait écrit certaines choses de
certaines façons, etc. Ce type de comportement serait tout à fait
sanctionnable d'un point de vue civil comme pénal. Il n'est bien sûr pas
inutile de le rappeler, mais en revanche nul besoin d'utiliser une «&nbsp;licence verbatim&nbsp;» (interdisant toute modification) à cette seule fin.


Dans le cas des Framabooks, une clause de type -ND (ou toute autre
clause de la même famille) est donc superflue. La suite va nous montrer
qu'elle peut même être gênante.

Le second argument concerne la
réédition. En effet, une modification de l'œuvre n'a d'intérêt que pour
la diffuser. Il s'agit dans ce cas d'une réédition. Comme il est
expliqué dans la question numéro 4, toute réédition d'un Framabook est
soumise à certaines conditions. Parmi celles-ci, le contrat d'édition
signé par l'auteur&nbsp;: puisque le contrat est «&nbsp;nommé&nbsp;», il lie l'auteur à
son œuvre de manière formelle. Ainsi, il resterait toujours possible
pour un imprimeur de réaliser des copies papiers «&nbsp;à la demande&nbsp;» dès
lors qu'il ne rentrerait pas dans une démarche similaire à celle d'un
éditeur et toute nouvelle édition serait nécessairement rattachable à un
auteur (soit l'auteur initial de l'œuvre s'il choisit de souscrire à un
nouveau contrat et dès lors que ce nouveau contrat ne souffre pas de la
non exclusivité accordée à Framasoft&nbsp;; soit l'auteur d'une version
dérivée dès lors que les apports de chacun des auteurs sont clairement
identifiés).

Le troisième argument, «&nbsp;l'absence de risque&nbsp;», est sans
doute le plus important. Une licence sans clause -ND (ou autre clause du
même genre) aura *seulement* pour conséquence&nbsp;:

-   de permettre des créations nouvelles empruntant pour partie à
    l'œuvre initiale, mais&nbsp;: a) en attribuant l'œuvre initiale et b) en
    se dissociant de façon non équivoque. C'est le cas par exemple de
    traductions ou des «&nbsp;mises à jour&nbsp;» de l'œuvre&nbsp;;
-   de permettre des «&nbsp;grandes citations&nbsp;» (ou toute autre réutilisation
    qui dépasserait le seul cadre des exceptions prévues par la Loi) au
    sein d'une autre œuvre.

Ainsi, dans la mesure où notre objectif premier est celui de la
diffusion, une clause interdisant toute modification fait obstacle à
l'apparition de nouvelles créations susceptibles de devenir le support
second de cette propagation.

En guise d'illustration, nous pouvons citer
deux extraits du préambule de la [Licence Art Libre](http://artlibre.org/licence/lal), mise à disposition pour des
œuvres artistiques&nbsp;: 

> Avec la Licence Art Libre, l'autorisation est donnée de copier, de diffuser et de transformer librement les œuvres **dans le respect des droits de l'auteur** (&hellip;) L'intention est d'autoriser l'utilisation des ressources d'une œuvre&nbsp;; créer de nouvelles conditions de création pour amplifier les possibilités de création. La Licence Art Libre permet d'avoir jouissance des œuvres tout en reconnaissant les droits et les responsabilités de chacun.


Cet esprit est d'autant plus présent dans la LAL que le texte distingue
l'original de la copie&nbsp;: les droits portant sur les copies de l'original
(qui pour sa part ne peut être modifié sans autorisation de son auteur
et qui doit être mentionné comme tel).

Pour revenir au contexte
d'édition dans lequel nous nous situons, le choix d'une licence
entièrement libre est aussi une assurance pour le projet et ses
contributeurs&nbsp;: même si l'auteur se désengage et ne souhaite ou ne peut
assurer de nouvelle version, d'autres pourront prendre le relais (comme
ce fut le cas pour le premier Framabook [*Utilisez Thunderbird 2.0!*](http://framabook.org/utilisez-thunderbird-2-0/)).

Et si je décide de ne pas m'encombrer les neurones&nbsp;?
-------------------------------------------------------

Les raisons esthétiques ci-dessus ne s'appliquent que peu aux ouvrages
de la collection Framabook, mais restent néanmoins discutables dans le
cadre d'une démarche de partage libre. À contrario, nous pouvons
signaler que certains ouvrages de la collection sont, eux, sous licence
[CC-Zéro](http://creativecommons.org/choose/zero/?lang=fr). C'est-à-dire
qu'il s'agit de ce que l'on pourrait appeler le «&nbsp;domaine public
volontaire&nbsp;».

Certes, nous avons dit plus haut qu'il était impossible
pour un auteur, du point de vue légal et dans beaucoup de juridictions,
de renoncer à tous ses droits d'auteurs (en particulier les droits
moraux). Cela dit, les choses peuvent aussi s'envisager d'un point de
vue beaucoup plus pratique&nbsp;: le fait de déclarer que non seulement
l'œuvre est libre mais aussi qu'elle a pour vocation de suivre son cours
en pleine autonomie, un cours que l'auteur s'engage à ne pas influencer
(à ne pas exercer son droit d'auteur qui pourtant lui colle à la peau).

La licence CC-0 cherche à traduire ces effets au sein d'un contrat qui
propose alternativement et successivement&nbsp;: une renonciation aux droits,
une cession de tous les droits patrimoniaux et moraux ou une cession des
seuls droits patrimoniaux. En d'autres termes, les droits de Propriété
Intellectuelle (et régimes associés) étant territoriaux, la licence CC-0
fonctionne différemment selon que l'auteur peut renoncer à ses droits,
céder ses droits moraux ou non. Dans les faits, la licence confère ainsi
à l'œuvre le statut juridique s'approchant le plus de la volonté de
l'auteur (en France, un statut très proche de la CC By&nbsp;: une cession
très large des droits patrimoniaux avec une obligation de citer l'auteur
– sauf si ce dernier souhaite rester anonyme). C'est notamment le cas du
roman [*Le Cycle des NoéNautes*](http://framabook.org/monorchide-le-cycle-des-noenautes-tome-ii),
par Pouhiou[^15]. Nous pouvons le citer&nbsp;:

> Dès aujourd'hui, je fais passer Les Noénautes dans le domaine public
> volontaire. Cela veut dire que tu as le droit d'en faire ce que tu
> veux. Tu n'as aucun compte à me rendre. Tu peux éditer et vendre cette
> œuvre pour ton propre compte, tu peux la réécrire, l'adapter, la
> recopier, en faire de la pub ou des navets&hellip; Tu es libre. Parce que
> légalement, cette œuvre est libre. La loi Française imposerait que tu
> fasses mention de l'auteur malgré tout&nbsp;: OSEF, j'irai pas t'attaquer !
> J'avoue que si tu fais quelque chose de tout cela, ça m'amuserait que
> tu me tiennes au jus. Mais tu n'as plus d'autres obligations que
> celles que tu te crées.&nbsp;»[^16]
> 
> — <cite>Pouhiou</cite>

Conclusion
----------

Elle s'exprime en une phrase&nbsp;: la collection Framabook édite des livres
sous licence libre, sans clause non commerciale ou empêchant toute
modification de l'œuvre. Voici des exemples de licence qui peuvent être
utilisés&nbsp;:

-   [GNU FDL](http://www.gnu.org/licenses/licenses.fr.html) – Issue du projet GNU, elle est au départ adaptée aux manuels de logiciels. C'est une licence très permissive&nbsp;;
-   [CC-By](http://creativecommons.org/licenses/by/3.0/fr/) – Creative commons – paternité (obligation de nommer l'auteur pour toute redistribution avec ou sans modification)&nbsp;;
-   [CC-By-SA](http://creativecommons.org/licenses/by-sa/3.0/fr/) – Creative commons – Paternité – Partage à l'identique (*share alike*)&nbsp;: toute redistribution doit être partagée sous les mêmes termes de licence&nbsp;;
-   [LAL](http://artlibre.org/licence/lal) – Licence Art Libre, conçue comme une adaptation de la GNU GPL au domaine de l'art&nbsp;;
-   [CC-Zéro](http://creativecommons.org/choose/zero/?lang=fr) – il s'agit du versement volontaire de l'œuvre dans le domaine public.

Cette liste n'est pas limitative et nous nous ferons un plaisir de vous
accompagner si vous souhaitez discuter de la pertinence de toute autre
licence. Le choix de la licence de la part de l'auteur doit être un
choix éclairé et mûrement réfléchi. Il entre dans une démarche de
partage et en même temps dans un circuit éditorial. Il n'échappe
néanmoins pas à la juridiction du droit d'auteur.


– Framasoft, le 15 octobre 2013



[^1]: La collection est coordonnée par Christophe Masutti.

[^2]: Nous avons également publié un essai qui propose, lui, de se passer complètement du droit d'auteur&nbsp;: J. Smiers, et M. van Schijndel, [*Un monde sans copyright… et sans monopole*](http://framabook.org/10-un-monde-sans-copyright-et-sans-monopole/).

[^3]: De manière plus complexe, certains de nos ouvrages sont soumis à plusieurs licences libres&nbsp;: tel l'ouvrage précité «&nbsp;Option Libre&nbsp;» qui est diffusé sous triple licence CC-By-SA 3.0, Licence Art Libre 1.3, GNU FDL 1.3.

[^4]: Lorsque je souhaite *donner* un fichier, je fais une copie, ce qui devient du *partage*&nbsp;: ce principe est évidemment contrarié par la pléthore de dispositifs de surveillance et de protection de la part des ayants droits (type DRM, ou lobbying législatif) qui visent à empêcher le partage pour des raisons plus ou moins défendables.

[^5]: Il est en effet admis, au moins en Europe, qu'un auteur ne peut décider d'élever de lui-même une œuvre dans le domaine public (un tel acte serait certainement sans valeur juridique et l'auteur ou ses ayants droit pourraient valablement revenir dessus plusieurs années plus tard).

[^6]: Nous avons récemment rencontré le cas avec la traduction d'un chapitre de l'ouvrage de C. Kelty, tiré de *Two Bits. The Cultural Significance of Free Software* ([http://twobits.net](http://twobits.net/)), que nous souhaitions intégrer dans le Framabook [*Histoires et cultures du Libre*](http://framabook.org/histoires-et-cultures-du-libre). Bien qu'ayant l'accord de l'auteur, son livre étant sous licence CC-BY-NC-SA, c'est l'éditeur seul qui pouvait lever temporairement la clause NC, moyennant une rétribution (certes faible, de l'ordre d'une centaine de dollars), afin que nous puissions inclure ce chapitre dans l'ouvrage destiné à la vente. La clause -SA posait aussi un grave problème pour l'ensemble de l'ouvrage. Nous l'avons donc inclus uniquement dans la version numérique gratuite.

[^7]: Pour les ouvrages où il n'y a pas de contrat d'auteur, les bénéfices sont reversés à Framasoft et entrent dans le cadre de l'équilibre budgétaire (en principe, lorsque celui-ci peut être atteint).

[^8]: Framasoft compte trois permanents à ce jour, affectés à la gestion des multiples projets de l'association ainsi qu'à son administration.

[^9]: C'est par exemple le cas des bandes dessinées *GKND* pour lesquelles nous avons fixé un objectif de prix (pas au-delà de 12 euros la version imprimée). Ce prix permet à l'auteur de toucher un intéressement, mais ne couvre pas les frais annexes (stockages, frais de port pour les approvisionnements, etc.). Cela peut bien entendu changer si nous empruntons une autre voie plus économique pour la production.

[^10]: L'essentiel des revenus de l'association étant composé des dons faits à l'association. Les revenus provenant de la vente des ouvrages permet d'avoir à disposition un fonds de roulement permettant d'acheter des stocks d'imprimés.

[^11]: Voir article L132-1 du CPI&nbsp;: «&nbsp;Le contrat d'édition est le contrat par lequel l'auteur d'une œuvre de l'esprit ou ses ayants droit cèdent à des conditions déterminées à une personne appelée éditeur le droit de fabriquer ou de faire fabriquer en nombre des exemplaires de l'œuvre, à charge pour elle d'en assurer la publication et la diffusion&nbsp;». Constitue une faute de la part de l'éditeur le fait de n'avoir pas passé un contrat d'édition avec une personne à laquelle il reconnaissait la qualité d'auteur (Paris, 4^e^ chambre, 22 novembre 1990).

[^12]:  R.M. Stallman affirme en effet&nbsp;: «&nbsp;Selon moi, les licences non libres qui permettent le partage sont légitimes pour des œuvres artistiques ou de divertissement. Elles le sont également pour des œuvres qui expriment un point de vue (comme cet article lui-même). Ces œuvres ne sont pas dédiées à une utilisation pratique, donc l'argument concernant le contrôle par l'utilisateur ne s'y applique pas. Ainsi, je ne vois pas d'objection à ce qu'elles soient publiées sous licence CC BY-NC-ND, qui ne permet que la redistribution non commerciale de copies identiques à l'original.&nbsp;»

[^13]: Dans le même registre, et pour des motifs tout à fait recevables selon l'usage, certaines licences libres – une principalement&nbsp;: la GNU Free Documentation License – permettent d'identifier des passages spécifiques d'une œuvre comme *invariants* (cela notamment afin d'assurer une plus grande diffusion des textes philosophiques et/ou politiques annexer à une documentation).

[^14]: Le droit d'auteur se décompose entre droit moral et droit patrimonial&nbsp;: en vertu du droit patrimonial, l'auteur a la possibilité d'exploitation son œuvre (par des contrats de cession telle qu'une licence libre)&nbsp;; en vertu du droit moral, l'auteur peut limiter certains usages préjudiciables pour son œuvre ou le lien qu'il entretient avec cette dernière.

[^15]: Ainsi que Joost Smiers et Marieke van Schijndel, *op. cit*.

[^16]: Voir [noenautes.fr](http://noenaute.fr/bonus-13-inspirations-et-digestion/2).

