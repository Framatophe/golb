---
title: "Les voies de la préfiguration"
date: 2019-11-11
author: "Christophe Masutti"
image: "/images/postimages/manifprefiguration.jpg"
description: "Pour de récentes approches des mouvements altermondialistes ou apparentés, il semble que le concept de préfiguration devienne une clé de lecture prometteuse. Doit-on uniquement réfléchir en termes de stratégies politiques, ou justifier les initiatives en cherchant une légitimité dans des expériences fictives, comme celle d'Orwell ? Et si oui, comment obtenir l'adhésion du public ? Il importe de travailler un peu cette idée de la préfiguration et c'est ce que je propose dans cet article."
tags: ["anticapitalisme", "Libres propos", "action directe", "Politique"]
categories:
- Libres propos
---

Pour de récentes approches des mouvements altermondialistes ou apparentés, il semble que le concept de préfiguration devienne une clé de lecture prometteuse. Doit-on uniquement réfléchir en termes de stratégies politiques, ou justifier les initiatives en cherchant une légitimité dans des expériences fictives, comme celle d'Orwell ? Et si oui, comment obtenir l'adhésion du public ? Il importe de travailler un peu cette idée de la préfiguration et c'est ce que je propose dans cet article.

<!--more-->

# Titre

Qu'est-ce que préfigurer&nbsp;? La préfiguration est d'abord un concept 
travaillé dans les études littéraires, artistiques ou philosophiques. Pour un 
événement ou des idées, il s'agit de déceler des éléments antérieurs, culturels, 
issus de l'expérience, ou d'autres idées et concepts qui peuvent être liés d'une 
manière ou d'une autre au présent qu'on analyse.

L'ordre temporel n'est pas toujours aussi net. En littérature, les commentateurs 
construisent des généalogies entre les auteurs, certes, mais avec le dialogue 
qu'ils construisent avec leurs lecteurs (à titre posthume ou pas), les auteurs 
aussi construisent à rebours leurs généalogies, déterminent les «&nbsp;plagiats 
par anticipation&nbsp;». C'est-à-dire qu'il existe entre les pensées et les 
textes des fictions de généalogies. Le lecteur n'est pas en reste puisqu'à 
reconnaître chez un auteur des formes littéraires, des pensées ou des concepts 
déjà présents chez d'autres auteurs précédents, le lecteur change lui-même, de 
manière rétroactive, la réception du discours, et les textes plus anciens 
deviennent des préfigurations historiques.

Précurseurs&nbsp;? les sciences n'en sont pas exemptes&nbsp;: on voit dans 
l'histoire des sciences à quel point les auteurs sont toujours soucieux de se 
situer dans des «&nbsp;Écoles de pensées&nbsp;», de citer les expériences et les 
théories non seulement lorsqu'il s'agit de les remettre en cause ou les 
questionner, mais aussi établir une filiation qui permet de légitimer leurs 
propres recherches. En sciences, la recherche de la préfiguration est une 
recherche de légitimation, y compris lorsque cette recherche n'est pas effectuée 
par le chercheur mais aussi par le lecteur qui établit des liens, élabore des 
clés de lecture jamais pensées auparavant. On peut citer par exemple la 
re-découverte des travaux de G. Mendel au début du vingtième siècle, et les 
controverses que cela suscita[^gayon] au sujet de la construction d'une nouvelle 
discipline (la génétique), des controverses qui furent justement les produits de 
lectures différentes et donc de généalogies différentes.

En histoire, il en va de même, notamment parce qu'il s'agit d'en faire 
l'épistémologie. Les événements ont-ils toujours une histoire causale, linéaire, 
dans laquelle on peut facilement trouver les éléments qui en préfigurent 
d'autres, ou au contraire faut-il réfléchir en faisceaux d'évènements, de 
documents qui permettent de *construire* un sens dont on peut dire que ces 
indices préfigurent quelque chose, mais jamais entièrement&nbsp;?

[^gayon]: On peut se reporter aux travaux de l'historien des sciences Jean Gayon à ce sujet.

Dans les fictions politiques et leur réception dans les représentations 
dominantes à travers les époques, les mécanismes à l'œuvre ne sont pas 
tant ceux d'une légitimation de l'action (les auteurs ne sont pas des 
prophètes dont on suivrait les préceptes) qu'une reconnaissance a 
postériori d'une expérience politique qui devient alors un instrument de 
compréhension du présent. À travers l'histoire de l'informatisation de la 
société, dès les années 1950 et 1960, de nombreux textes font référence à 
la dystopie de G. Orwell et *1984* pour décrire les dangers des grandes 
bases de données. Et aujourd'hui encore cette clé de compréhension qu'est 
le monde politique de *1984* est énoncée, le plus souvent sans vraiment en 
faire la critique, comme si cette unique référence 
pouvait réellement fonctionner depuis plus de 60 ans.

Comme le dit P. Boucheron[^Boucheron]&nbsp;:

> La littérature ne prédit pas plus l'avenir qu'elle n'en 
> prévient les dangers. Et voilà pourquoi la critique littéraire 
> de l'anticipation ne peut être qu'une critique de 
> l'après-coup. Si on pense aujourd'hui que les fictions 
> politiques de Kafka ou d'Orwell *préfigurent* une politique à 
> venir, c'est parce que nous vivons aujourd'hui une situation 
> politique en tant que nous sommes préparés à les reconaître 
> comme ayant déjà été expérimentées par avance dans les 
> fictions politiques.


[^Boucheron]: Patrick Boucheron, *Qu'est-ce que préfigurer ? Des généalogies à rebours*, Cours au Collège de France, 27 mars 2017, [Lien](https://www.college-de-france.fr/site/patrick-boucheron/course-2017-03-21-11h00.htm).

L'ennui c'est que nous avons tendance à penser l'action politique en 
fonction de cette manière de concevoir la préfiguration, c'est-à-dire en 
s'efforçant de chercher les concepts dans des expériences fictives ou 
analytiques qui précèdent l'action et de manière à justifier l'action. 
Dans l'action politique, toute idée ou pratique qui n'appartiendrait pas à 
une généaologie donnée serait par définition non recevable, indépendamment 
de son caractère novateur ou non.

Ceci est particulièrement problématique car le caractère novateur des 
idées ne peut alors se prouver qu'en fonction d'efforts théoriques 
soutenus, généalogie et démonstration, ce qui ne coïncide que très 
rarement avec l'urgence politique. Penser qu'un acte, une décision ou une 
idée préfigurent toujours potentiellement quelque chose, c'est penser que 
l'action politique obéit toujours à une stratégie éprouvée et prétendre 
que «&nbsp;gouverner, c'est prévoir&nbsp;». Mais jusqu'à quel point&nbsp;?


Par exemple, l'activisme lié aux questions environnementales montre qu'on 
ne peut pas circonscrire l'action (comme une zone à défendre contre des 
projets capitalistes nuisibles aux biens communs) à la seule revendication 
pro-écologique. D'autres aspects tout aussi importants se greffent et que 
l'on pourrait rattacher aux nombreux concepts qui permettent d'opposer au 
modèle dominant des modèles sociaux et économiques différents. Pour rester 
sur le thème environnementaliste, c'est bien le cas de l'activisme dans ce 
domaine qui, dans les représentations communes chez les décideurs 
politiques comme dans la population en général, est resté longtemps 
circonscrit à une somme de revendications plus ou moins justifiées 
scientifiquement mais toujours à la merci de la décision publique, 
elle-même sacralisée sur l'autel «&nbsp;démocratique&nbsp;» du vote et de la 
représentativité.

L'apparition des ZAD et leur sociologie ont montré 
combien l'activisme ne correspondait plus à ce «&nbsp;revendicalisme&nbsp;», et 
c'est ce choc culturel qui permet de comprendre pourquoi il ne passe plus 
par les mécanismes habituels des démocraties libérales mais propose 
d'autres mécanismes, basés sur des modes de démocratie directe et 
l'exercice de la justice, mais inacceptables pour les réactionnaires. Ces 
derniers ont alors beau jeu de traduire ces nouvelles propositions 
politiques en termes de radicalismes ou d'extrémismes car elles sont en 
fait la traduction sociale de la crise des démocraties capitalistes.

Est-ce de l'action directe&nbsp;? L'action directe (qui n'est pas 
nécessairement violente, loin s'en faut) est le fait d'imposer un rapport 
de force dans la décision sans passer par l'intermédiaire de la délégation 
de pouvoir (la représentativité) et même parfois, dans certains cas, sans 
passer par les institutions judiciaires (ce qui peut parfois aider à 
justifier des actions illégales, bien qu'étant légitimées du point de vue 
moral, ou éthique, tels les actes de *désobéissance civile*).

Pour ce qui me concerne, l'ennui avec l'action directe est qu'elle relève 
d'une conception anarchiste qui me semble aujourd'hui dépassée. C'est 
Voltairine de Cleyre qui a théorisé l'action directe en 1912. Dans les 
commentaires et les reprises qui ont suivis, l'action directe est pensée 
en mettant sur le même plan la revendication (syndicale, par exemple), le 
devoir d'un ouvrier dans la défense des intérêts collectifs de sa classe 
devant le patronat, la négociation directe entre un collectif et des 
autorités, etc. C'est-à-dire que l'action directe n'a pas pour objet la 
prise de pouvoir (elle exclu donc la conquête du pouvoir par la violence) 
mais elle est d'abord un acte d'organisation. Hier, on pensait cette 
organisation généralement en termes de classes sociales, aujourd'hui 
d'autres auteurs parlent plutôt de groupes d'affinités (ce qui n'efface 
pas pour autant les classes). Et comme il s'agit d'abord d'organiser des 
actions collectives en fonction de projets prééxistant, c'est-à-dire en 
fonction d'une expérience relevant de l'imaginaire politique, c'est 
seulement après-coup que l'on peut penser la puissance préfigurative de 
l'action directe&nbsp;: «&nbsp;ils avaient bien raison&nbsp;», «&nbsp;c'est grâce à ces 
luttes sociales que nous pouvons aujourd'hui…&nbsp;», etc.

Or que constatons-nous aujourd'hui ? Tout comme n'importe quel historien le constate en 
étudiant objectivement l'histoire des luttes sociales. Très rares sont les projets 
théoriques suscitant l'adhésion d'un collectif et suffisamment compris par chacun pour 
susciter de l'action directe. On le trouve peut-être dans l'action syndicale, mais si on 
regarde les processus révolutionnaires, on voit bien que la théorie n'a pas nécessairement 
précédé l'action, elle s'est construite «&nbsp;en faisant&nbsp;», «&nbsp;en marche&nbsp;» 
(comme dirait l'autre). On peut douter par exemple que les Révolutionnaires à la Bastille 
avaient bien tous compris (et lu) les textes de l'Assemblée Constituante. Meilleur exemple, 
la Commune de Paris née d'un mouvement d'opposition et de révolte, mais débouchant sur une 
expérience de gestion collective. Ainsi, il y a une stratification des actions et des 
pratiques dans un mouvement d'opposition et de préfiguration qui font qu'une ZAD ne 
ressemble pas à une autre ZAD, qu'un rond-point de gilets jaunes dans le Sud de la France ne 
ressemblera pas à un autre rond-point dans le Nord de la France, et ce n'est pas seulement 
parce que les contextes environnementaux et sociaux ne sont pas les mêmes, mais c'est parce 
qu'ils n'ont pas de prétention à préfigurer quoique ce soit de manière stratégique, suivant 
un plan préétabli.

Cela explique aussi pourquoi il y a de grandes transformation dans le fil 
chronologique des mouvements sociaux. Les revendications à Hong-Kong se 
sont certes construites au début sur un rejet des politiques pro-Chine, 
mais elles se sont assez vite cristallisées autour de contestations plus 
étendues et diverses rendant de plus en plus difficile une réponse 
politique (autre que la violence policière et l'étouffement). Il en va de 
même au sujet des autres mouvements à travers le monde aujourd'hui, qui 
ont tendance à transformer la revendication en une opposition de modèles 
sociaux et politique.

Certains sociologues qui se sont intéressés aux mouvements tel Occupy 
(Wall Street), les ZAD ou Nuit Debout ont montré que, en réalité, à la 
logique de l'action directe répondait aussi une logique de la *théorie 
directe*. N. Sturgeon[^sturgeon] montre combien les mouvements 
d'opposition changent aussi le cadre discursif&nbsp;: en tant qu'action directe 
ils rendent le dialogue et la négociation très difficiles avec les 
autorités (et vouent souvent à l'échec le discours politique en place) 
mais ils retravaillent aussi à l'intérieur même du mouvement les notions 
d'horizontalité, de diversité, de décision, de mécanisme démocratique, 
etc. En somme ce que les mouvements *alter-* proposent, ce sont tout 
autant des alternatives que de l'altérité.

[^sturgeon]: Noel A. Sturgeon, «&nbsp;Theorizing movements: direct action and direct theory&nbsp;», in&nbsp;: M. Darnovsky, B. Epstein,  R. Flacks (Éds), *Cultural Politics and Social Movements*, Philadelphia, PA: Temple University Press, 1995, p.&nbsp;35– 51


Si ces mouvements *alter* préfigurent quelque chose, ce n'est donc pas par 
la formalisation d'un discours mais par l'organisation elle-même, au moins 
aussi importante que la revendication. C'est ce qu'un chercheur comme 
T.&nbsp;Luchies identifie en étudiant les courants anarchistes de 
l'anti-oppression[^luchies]&nbsp;:

> Dans une organisation politique anarchiste, 
> un tel activisme cherche à identifier les 
> formes normalisées d'oppression et à éliminer
> les institutions qui reproduisent la 
> suprématie blanche et masculine, le handicap, 
> l'homophobie et la transphobie, le 
> racisme et le capitalisme. La lutte contre 
> l'oppression est une action continue et, 
> grâce à sa pratique collaborative et cumulative,
> nous pouvons entrevoir des formes 
> radicalement inclusives et autonomisantes de 
> communauté politique. Son fonctionnement 
> quotidien préfigure des modes alternatifs 
> d'organisation et de résistance ensemble.

[^luchies]: Timothy Luchies, «&nbsp;Anti-oppression as pedagogy; prefiguration as praxis&nbsp;», *Interface : a journal for and about social movements*, 6(1), 2014, p.&nbsp;99-129.

Sans forcément mobiliser un collectif à part entière, ces expériences 
peuvent être éprouvées dans n'importe quelle organisation qui met 
l'autonomie et la démocratie au premier plan de ses règles de fonctionnement.

Pour prendre un exemple que je connais plutôt bien, l'association 
Framasoft fonctionne sur ce modèle. Sans avoir formalisé notre mode de 
fonctionnement dans une charte ou un règlement intérieur particulièrement 
élaboré, l'idée est justement de laisser assez de latitude pour apprendre 
en permanence à moduler le système collectif de prise de décision. Dans 
les objectifs de réalisation, l'idée est bien de prétendre à l'action 
directe&nbsp;: mettre en oeuvre des projets concrets, des expériences de 
logiciels libres et de culture libre, et faire autant de démonstrations 
possibles d'un monde numérique basé sur une éthique du partage et de la 
solidarité. Ainsi, nous ne revendiquons rien, nous mettons au pied du mur 
la décision publique et les choix politiques en proposant d'autres 
expériences sociales en dehors du pouvoir et de la représentativité 
politique.

Dans cet ordre d'idée, un dernier auteur que nous pouvons citer est la 
chercheuse M.&nbsp;Maeckelbergh qui montre bien que la préfiguration n'est ni 
une doctrine, ni une stratégie politique. Elle échappe à une lecture du 
pouvoir politique parce qu'elle est presque exclivement en acte. Et c'est 
aussi la raison pour laquelle, du point de vue de la méthodologie 
sociologique, elle ne peut s'appréhender qu'en immersion. Elle affirme 
dans son étude sur les mouvements altermondialistes[^maeckelbergh]&nbsp;:

> Ce qui différencie le mouvement altermondialiste des 
> mouvements précédents, c'est que le «&nbsp;monde&nbsp;» alternatif 
> n'est pas prédéterminé ; il est développé par la 
> pratique et il est différent partout.

[^maeckelbergh]: Marianne Maeckelbergh «&nbsp;Doing is Believing: Prefiguration as Strategic Practice in the Alterglobalization Movement&nbsp;», *Social Movement Studies*, 10(1), p. 1-20, 2011.

Est-ce pour autant du relativisme selon lequel tous les mouvements 
seraient équivalents, quelles que soient leurs orientations morales et 
politiques&nbsp;? Heureusement non, car ce qui défini l'action directe dans 
cette dynamique de préfiguration, c'est aussi l'expérimentation de 
multiples formes de démocraties directes. Si, de manière générale, elles 
ont tendance à appeler à un changement de paradigme (celui de la 
représentativité et de la délégation de pouvoir) elles appellent aussi à 
un changement de modèle social et économique. Il peut néanmoins subsister 
des désaccords. Très prochainement, nous verrons sans doute apparaître 
quelques tensions entre ceux pour qui le capitalisme pourrait retrouver 
figure humaine (et pourquoi pas humaniste) et ceux pour qui le modèle 
capitaliste est en soi un mauvais modèle. Mais quelle que soit la 
prochaine configuration, les enjeux sociaux et environnementaux 
d'aujourd'hui présentent assez d'urgence pour ne plus perdre trop de temps à 
écrire des fadaises&nbsp;: les principes d'expansion et d'exploitation 
capitalistes des ressources (naturelles et humaines) impliquent dès 
mainenant une urgence de l'action.

