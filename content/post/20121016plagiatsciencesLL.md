---
title: "Le plagiat, les sciences et les licences libres"
date: 2012-10-16
author: "Christophe Masutti"
image: "/images/postimages/say.png"
tags: ["Libres propos", "Sciences", "Communs", "droit d'auteur"]
description: "Il faut libérer les sciences : la suite"
categories:
- Libres propos
---

Ces 5-6 dernières années, la question du plagiat dans les sciences est devenu un thème de recherche à part entière. On peut saluer le nombre croissant de chercheurs qui s'intéressent à cette question à double titre: d'abord parce que le plagiat est un phénomène social, mais aussi parce que ce phénomène (pourtant très ancien) illustre combien les sciences ne constituent pas cet édifice théorique et inébranlable de l'objectivité qu'il serait étonnant de voir un jour s'effriter.

Qu'est-ce que le plagiat&nbsp;?
--------------------------

Parmi ces chercheurs, on remarquera plus particulièrement Michelle Bergadaà, de l'Université de Genève, qui a créé en 2004 le site [responsable.unige.ch](http://responsable.unige.ch). Elle fut l'un des premiers chercheurs à recueillir une multitude d'informations sur des cas de plagiat et à dresser une typographie des plagiats scientifiques. Elle a montré aussi que la reconnaissance des cas de plagiat ne peut manifestement pas se contenter d'une réponse juridique, mais qu'il appartient à la communauté des chercheurs de réguler des pratiques qui vont à l'encontre des principes de la recherche scientifique, en particulier la nécessité de
citer ses sources et d'éviter la redondance au profit de l'avancement des sciences.

En 2012, Michelle Bergadaà a publié un article bien documenté et accessible, intitulé [«&nbsp;Science ou plagiat&nbsp;»](http://responsable.unige.ch/Documents/Sciences_ou_Plagiat.pdf), dans lequel elle montre que la conjonction entre le *peer review process*, qui fait des revues non plus des instruments de diffusion de
connaissance mais des «&nbsp;instruments de prescription et de référencement à l'échelle mondiale&nbsp;» (comprendre: le système du *publish or perish*), et le web comme outil principal de communication et de diffusion des productions des chercheurs, cause l'opposition de deux modes de rapports au savoir:

> Un malentendu s’installe entre ceux qui restent fidèles au savoir scientifique véhiculé par les revues traditionnelles et ceux qui défendent le principe des licences libres inscrites dans une logique de savoir narratif. Cette tension apparaît dans la manière dont est compris et souvent envisagé le modèle de production des Creative Commons ou licences libres, issues du monde du logiciel. Ce modèle vise d'abord à partager l'information, à l’enrichir, et non la rendre privative. Il est possible de copier et de diffuser une œuvre, à condition de respecter la licence choisie&nbsp;; de la modifier à condition de mentionner la paternité de l'œuvre. Il correspond donc précisément aux pratiques canoniques de la recherche scientifique&nbsp;: publier et diffuser les connaissances le plus largement possible permettant de s’appuyer sur les résultats et les productions des autres chercheurs pour faire avancer le savoir.

Cette analyse assimile le web à un endroit où se partagent, de manière structurée, les productions scientifiques, que ce lieu de partage est fondamentalement différent des lieux «&nbsp;traditionnels&nbsp;» de partage, et que donc les défenseurs de ces lieux traditionnels ne comprennent pas leur propre miroir numérique... Je parle de miroir car en effet, ce que M. Bergadaà ne fait que sous entendre par le terme «&nbsp;malentendu », c’est que les licences libres permettent de remettre en cause l’ensemble du système de production, diffusion et évaluation scientifique. Elles démontrent même que le sens du plagiat n’est pas le même et ne questionne pas les mêmes valeurs selon qu’on se situe dans une optique ou dans l’autre. C’est une accusation radicale du système d’évaluation scientifique qu’il faut porter une fois pour toute et la «&nbsp;lutte&nbsp;» contre le plagiat est, à mon sens, l’un des premiers pas de cette étape réflexive. Il est donc important de bien situer ce «&nbsp;malentendu&nbsp;», et la question du plagiat est un bon levier.

Qu’est-ce que le plagiat, aujourd’hui? N’est-il pas le « côté obscur&nbsp;» du sentiment légitime que les connaissances appartiennent à tous&nbsp;? Dans le domaine du logiciel libre, prendre du code et le diffuser sans porter le crédit du créateur originel, même lorsqu’on modifie une partie de ce code, cela s’appelle le resquillage. Selon la législation nationale du pays où se déroule le resquillage, cette attitude peut se condamner devant les tribunaux. De même, du point de vue du droit d’auteur, reprendre à son compte des pans entiers voire la totalité d’une oeuvre sans citer l’auteur, c’est interdit et va à l’encontre du droit (moral)
d’auteur.

Au-delà du droit d'auteur
-------------------------

Mais tant que l’on en reste là, c’est à dire à la question du droit d’auteur, on passe à côté du vrai problème de la diffusion des connaissances scientifiques: dans la mesure où un chercheur publie le résultat de recherches menées sur fonds publics (et même privés, dirai-je, mais cela compliquerait notre réflexion à ce stade) comment peut-on considérer un instant que le texte qu’il produit et les analyses qu’il émet puissent être l’objet d’un droit d’auteur exclusif&nbsp;? c’est là
qu’interviennent les licences libres: qu’un droit d’auteur doive être reconnu, en raison de la paternité de l’oeuvre (ou même de la découverte), mais que ce droit soit protégé tout en permettant la diffusion la plus large possible.

Ce qui pose donc problème, ce n’est pas le droit d’auteur, c’est l’exclusivité&nbsp;! Lorsqu’un texte scientifique est plagié, aujourd’hui, qui est réellement lésé et en quoi&nbsp;?

-   L’auteur&nbsp;? dans la mesure où il est censé publier pour la postérité et dans le but de diffuser les connaissances le plus largement possible, ce qui lui pose alors problème est sa fierté et le fait que l’oeuvre dérivée ne reconnaisse pas sa paternité. Il s’agit-là d’un point de vue personnel, et un auteur pourrait prendre cela de manière plus ou moins heureuse. Par contre, cela pose problème dans le cadre de l’évaluation scientifique&nbsp;: moins d’*impact factor* à son crédit. On touche ici un des nombreux points faibles de l’évaluation de la recherche.
-   La revue? très certainement, dans la mesure où elle a les droits exclusifs de l’exploitation d’une oeuvre. Ici, ce sont les aspects purement financiers qui sont en cause.
-   La communauté des chercheurs et, au delà, l’humanité&nbsp;? c’est sans doute le plus important et ce qui devrait passer au premier plan&nbsp;! Tout resquillage en science revient à fouler aux pieds les principes de l’avancement scientifique: la production de connaissances nouvelles.

Mais alors, d’où vient le malentendu&nbsp;? Tout le monde s’accorde pour reconnaître que les productions des chercheurs doivent être diffusées le plus largement possible (on pourra se questionner sur l’exclusivité d’exploitation des revues et les enjeux financiers). De même, tout le monde s’accorde sur le fait que la paternité d’un texte doit être reconnue à travers le droit d’auteur. Par conséquent, puisque les licences libres doublent le droit d’auteur de conditions permissives d’exploitation des oeuvres par l’humanité entière, d’où vient la réticence des défenseurs du système traditionnel de la publication revue par les pairs face aux licences libres (et peut-être au web en général)&nbsp;?

Lorsqu’on discute avec ces *traditionalistes* de la production scientifique *évaluée par les copains*, les poncifs suivants ont tendance à ressortir:

-   l’absence d’évaluation dans cette jungle du web dont Wikipédia et consors représentent le pire des lieux de partage de connaissances forcément faussées puisque les articles sont rarement écrits par les «&nbsp;spécialistes&nbsp;» du domaine&nbsp;;
-   la validation du travail de chercheur passe exclusivement par l’évaluation de la part de « spécialistes&nbsp;» reconnus. Cela se fait grâce aux revues qui se chargent de diffuser les productions pour le compte des chercheurs (notamment en s’attribuant le copyright, mais là n’est pas la question)&nbsp;;
-   il faut éviter que des « non-spécialistes&nbsp;» puisse s’autoriser à porter un jugement sur mes productions scientifiques, cela serait non seulement non recevable mais présenterait aussi un danger pour la science qui ne doit être faite que par des scientifiques.

À ces poncifs, on peut toujours en rétorquer d’autres&nbsp;:

-   Comment définir un «&nbsp;spécialiste&nbsp;» puisqu’il n’y a que des *degrés* de compétences et que tout repose sur l’auto-évaluation des pairs à l’intérieur d’une seule communauté de chercheurs. Ainsi, les spécialistes en biologie du Bidule, ne sont reconnus que par les spécialistes en Biologie du Bidule. Et en cas de controverse on s’arrange toujours pour découvrir que Dupont était en réalité spécialiste en biologie du Machin.
-   Les évaluateurs des revues ([voire les revues elles mêmes](http://www.the-scientist.com/?articles.view/articleNo/27376 «&nbsp;The Scientist. L'affaire Merck&nbsp;»))
    ont bien souvent des conflits d’intérêts même s’ils déclarent le contraire. En effet, un conflit d’intérêt est souvent présenté comme un conflit qui ne concerne que les relations entre un chercheur et les complexes industriels ou le système des brevets, qui l’empêcheraient d’émettre un jugement fiable sur une production scientifique qui remettrait en cause ces intérêts. Or, que dire des évaluateurs de revues (ou d’appel à projet) qui ont eux-mêmes tout intérêt (en termes de carrière) à être les (presque) seuls reconnus spécialistes du domaine ou, d'un point de vue moins individualiste, donner toutes les chances à leur propre laboratoire&nbsp;? le monde de la recherche est un territoire de plates-bandes où il ne fait pas bon venir sans avoir d’abord été invité (demandez à tous les doctorants).
-   Enfin, relativisme extrême: pourquoi la fameuse « société civile&nbsp;» ne pourrait porter un jugement sur les productions scientifiques? Parce que, ma bonne dame, c’est pour cela qu’on l’appelle «&nbsp;société civile&nbsp;»&nbsp;: à elle Wikipédia et à nous la vraie science. Après tout c’est à cause de ce relativisme qu’on en vient à réfuter le bien fondé des essais nucléaires ou la salubrité des OGMs.

Privilégier la diffusion
------------------------

Il faut dépasser ces poncifs.... je réitère la question&nbsp;: d’où vient ce malentendu&nbsp;? Il vient du fait qu’on doit aujourd’hui considérer que les limites entre les communautés scientifiques entre elles (les «&nbsp;domaines de compétence&nbsp;») et avec la « société civile&nbsp;» deviennent de plus en plus ténues et que, par conséquent, les licences libres impliquent que les scientifiques soient prêts à assumer trois choses&nbsp;:

-   privilégier la diffusion des connaissances sur leur distribution protégée par l’exclusivité des revues, car cette exclusivité est un rempart contre les critiques extérieures à la communauté (voire extérieures aux cercles parfois très petits de chercheurs auto-cooptés), et aussi un rempart financier qui empêche l’humanité d’avoir un accès total aux connaissances, grâce au web, ce qu’elle réclame aujourd’hui à grands cris, surtout du côté des pays où la fracture numérique est importante&nbsp;;
-   que les scientifiques -- et nombreux sont ceux qui ont compris cette  évidence --, ne peuvent plus continuer à produire des connaissances à destination exclusive d’autres scientifiques mais bien à destination du Monde: c’est à dire qu’on ne produit des connaissances non pour être évalué mais pour l’humanité, et que ce principe doit prévaloir face à la gouvernance des sciences ou à l’économie de marché;
-   que le plagiat ne doit pas être condamné parce qu’il est malhonnête vis à vis de l’auteur, mais parce qu’il est tout simplement immoral, tout autant que la course à la publication et la mise en concurrence des chercheurs. Plus cette concurrence existera, plus la “société civile” la remplacera par d’autres systèmes de production scientifique: imaginez un monde où 50% des chercheurs publient leurs articles sur leurs blog personnels ou des sites collaboratifs dédiés et ouvert à tous (pourquoi pas organisés par les universités elles-mêmes) et où l’autre moitié des chercheurs accepte cette course au *peer review* pour gagner des points d’impact. Qui osera dire que la première moitié des productions ne mérite pas d‘être reconnue comme scientifique parce qu’elle n’est pas évaluée par des pairs à l’intérieur de revues de catégorie A, B, ou C&nbsp;?

Voici le postulat: *il est préférable de permettre aux pairs et au monde entier d’apprécier une oeuvre scientifique dont on donne à priori l’autorisation d’être publiée, que de ne permettre qu’au pairs d’évaluer cette production et ne donner l’autorisation de publication (exclusive) qu’à postériori.*

Les licences libres n’interviennent que de manière secondaire, pour asseoir juridiquement les conditions de la diffusion (voire des modifications).

La conséquence, du point de vue du plagiat, c’est que l’oeuvre est connue à priori et par le plus grand nombre&nbsp;: cela diminue radicalement le risque de ne pas démasquer le plagiat, en particulier grâce aux moteurs de recherche sur le web.

Par ailleurs, dans ces conditions, si l’oeuvre en question n’est pas «&nbsp;scientifiquement pertinente&nbsp;», contient des erreurs ou n’obéit pas à une méthodologie correcte, c’est toute la communauté qui pourrait alors en juger puisque tous les agents, y compris les plus spécialistes, seraient en mesure de publier leurs évaluations. Il appartient donc aux communautés scientifiques, dès maintenant, de multiplier les lieux où l’exercice de la recherche scientifique pourrait s’accomplir dans une transparence quasi-absolue et dans une indépendance totale par rapport aux revues comme aux *diktat* de l’évaluation à tout va. C'est le manque
de transparence qui, par exemple, implique l'anonymat des évaluateurs alors même que ce sont des «&nbsp;pairs&nbsp;» (j'aime l'expression «&nbsp;un article de pair inconnu&nbsp;»): ne serait-il pas juste qu'un évaluateur (comme le ferait un critique littéraire) ne se cache pas derrière le nom de la revue, afin que tout le monde puisse juger de son impartialité&nbsp;? Voilà pourquoi il faut ouvrir à priori la diffusion des articles&nbsp;: les évaluations publiques seront toujours plus recevables que les évaluations anonymes, puisqu'elles n'auront rien à cacher et seront toujours potentiellement discutées entre lecteurs. Là encore les cas de plagiat seraient très facilement identifiés et surtout discutés.

Il se pourrait même que le modèle économique du logiciel libre puisse mieux convenir à l’économie de la Recherche que les principes vieillissants du système dominant.
