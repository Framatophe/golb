---
title: "Parcours VTT. Grendelbruch-Hohwald"
date: 2021-03-06
author: "Christophe Masutti"
image: "/images/postimages/hikebike.png"
description: "Un parcours de randonnée VTT costaud, mais pas trop."
tags: ["Sport", "VTT", "Entraînement", "Parcours"]
categories:
- Sport
---


Voilà très longtemps que je n'avais pas proposé sur ce blog un bon vieux parcours VTT. Celui-ci a un objectif particulier&nbsp;: après quelques sorties de remise en jambes après l'hiver, un parcours long de dénivelé moyen permet de travailler foncièrement l'endurance. Le parcours ne présente aucune difficulté particulière du point de vue de la technique de pilotage. Quelques singles viendront briser la monotonie et il n'y a que deux passages où le portage du vélo sur 10 mètres sera nécessaire (outre les éventuelles grumes, mais de cela nous sommes habitués).

## Caractéristiques du parcours

- 50 kilomètres
- 980 à 1000 mètres de dénivelé positif.

Regardez le tracé de près avant de partir pour ne pas être surpris&nbsp;: certaines directions peuvent prêter à confusion et tous les chemins empruntés ne sont pas balisés par le Club Vosgien.

## Descriptif

Au départ de Grendelbruch (parking de l'église) nous démarrons (déjà) à 530&nbsp;m d'altitude. En guise d'échauffement, un peu de bitume pour monter en direction de la Nécropole puis le col du Bruchberg. Là je préconise encore un peu de bitume (si vous prenez les rectangles jaunes dans l'enclos à vaches, à moins d'être en plein été et au sec, vous allez galérer entres les bouses et la boue, dès le début du parcours&hellip;). Nous enchaînons la montée sur le Shelmeck puis tour du Hohbuhl jusqu'au col de Birleylaeger.

Nous rejoignons le col de Franzluhr pour prendre les croix bleues jusque la Rothlach (953&nbsp;m), sans se poser de question. Chemin large (sauf une petite variante à ne pas louper en single, regardez la trace de près). Une fois à la Rothlach, nous prenons la route forestière (GR&nbsp;531), puis bifurcation sur la Vieille Métairie. On enchaîne alors la montée vers le Champ du Feu (1098&nbsp;m).

Nous changeons alors de versant en passant par le Col de la Charbonnière afin de rejoindre (croix jaunes puis cercles rouges puis de nouveau croix jaunes) le Col du Kreuzweg (sur le chemin, joli point de vue sur Breitenbach). Puis poursuite des croix jaunes pour monter sur la Grande Bellevue. Là, une pause en profitant de la vue et du soleil s'il y en a&nbsp;! 

Il est temps alors de commencer à descendre vers le Hohwald par un sympatique sentier (variante GR balisage rouge et blanc, ou ronds jaunes). Une fois au Hohwald, on commence la montée vers Welschbruch via le GR&nbsp;5, et enchaînement (croix jaunes) sur la montée vers la Rothlach (sentier).

Il est temps d'amorcer le chemin de retour. Pour cela, depuis la Rothlach, on emprunte un chemin forestier pour rejoindre le GR&nbsp;5, et après un passage amusant on rejoint le Champ du Messin. Encore un sentier de descente vers le Col de Franzluhr. On reprend en sens inverse le chemin vers le col de Birleylaeger. Bifurcation sur le Hohbuhl puis grande descente du Shelmeck. On termine alors par le tour du Bruchberg (attention, une partie du chemin non balisé en single) et retour vers le centre de Grendelbruch.

## Avis

Comme on peut le constater, mis à part quelques singles amusants le parcours s'apparente à une promenade plutôt longue avec un faible dénivelé (compte-tenu de la longueur). Tout se joue sur l'endurance et la possibilité, surtout dans la seconde partie du parcours, de pousser un peu la machine pour rentrer plus vite. 

Pour affronter les deux montées finales vers Welschbruch et la Rothlach, aussi amusantes qu'elles soient sur des singles (qu'habituellement on descend, n'est-ce pas&nbsp;?), vous devrez vous obliger à bien doser votre effort dans la première partie. Il faut garder du coffre pour réaliser presque autant de dénivelé que la premmière partie en deux fois moins de longueur de parcours.

Une variante si vous êtes en mal de descente rapide (je ne parle pas de DH, hein?), consiste à couper directement du Champ du feu (GR&nbsp;5 via la cascade) ou de la vieille Métairie (triangles rouges) vers le Hohwald. Ces sentiers sont connus et forts sympathiques à descendre, en revanche on ampute très largement ce parcours.

Bonne rando !

P.S.: pour récupérer les données du parcours, dans la carte ci-dessous&nbsp;: cliquez sur la petite flèche du menu de gauche, puis sur «&nbsp;exporter et partager la carte&nbsp;». Puis dans al section «&nbsp;Télécharger les données&nbsp;», sélectionnez le format voulu et téléchargez.

<iframe width="100%" height="500px" frameborder="0" allowfullscreen src="https://framacarte.org/fr/map/50k-grendelbruch-hohwald_95957?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=expanded&onLoadPanel=databrowser&captionBar=false"></iframe><p><a href="https://framacarte.org/fr/map/50k-grendelbruch-hohwald_95957">Voir en plein écran</a></p>

