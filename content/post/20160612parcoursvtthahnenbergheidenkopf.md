
---
title: "Parcours VTT. Hahnenberg-Heidenkopf"
date: 2016-06-12
author: "Christophe Masutti"
image: "/images/postimages/hikebike.png"
description: "Pour durcir les mollets, rien ne vaut de bonnes montées pénibles."
tags: ["Sport", "VTT", "Entraînement", "Parcours"]
categories:
- Sport
---


Voici une belle randonnée VTT avec des passages techniques amusants, des sections boueuses et humides à souhait (même par temps sec) et de jolis point de vue. L'endurance est de mise, même si le parcours en soi ne recquiert pas de capacités de pilotage exceptionnelles. Les descentes sont très sûres, pas de mauvaises surprise : tout se joue dans les montées. Mollets en fromage blanc, s'abstenir !


Accessible en train, facilement, depuis Strasbourg, la gare de Heiligenberg possède un parking où vous pouvez aussi garer votre voiture. Le début du parcours longe la Bruche sur le fond de la vallée jusqu'à la montée vers le Hahnenberg. Le chemin est très boueux par endroits, surtout après les travaux forestiers. Au lieu-dit Hirschbaechel, la montée commence vers le Hahnenberg. Soyez attentifs au balisage : une fois sur un replat, on quitte le chemin forestier pour prendre une variante du balisage jaune : les premières difficultés surgiront, en particulier les dérapages de roue arrière sur racines, un vrai plaisir. Les derniers mètres sur le Hahnenberg sont impossibles en vélo : il faudra pousser. La descente est néanmoins très facile, sur un sentier régulier.

Une fois arrivé à Grendelbruch (après une descente façon kilomètre lancé), il faudra prendre la route pour monter rapidement au Schelmeck et rejoindre le col de Franzluhr. Il va faloir chercher à s'économiser, malgré la facilité de cette section car le reste du parcours promet quelques surprises. Une variante plus intéressante consisterait plutôt à rejoindre la D204 et passer par le Bruchberg (je ferai sans doute un billet avec un parcours mieux élaboré). 

La descente depuis le col de Franzluhr jursqu'au Rossberg se fera sur le chemin forestier balisé en croix jaune. La variante, qui longe la première partie du chemin, est faisable en VTT mais avec prudence si vous débutez. Il faudra encore être bien attentif au moment de plonger vers la Magel : le sentier est étroit avec quelques trous inattendus. Assurez-vous d'avoir de bon freins avant d'entammer cette petite section de descente, la plus complexe du parcours.

La montée du chemin du Nikishof (aujourd'hui transformé en véritable autoroute pour travaux forestiers) fera appel à votre patience et votre endurance, mais pour un court moment : on quitte le chemin forestier pour reprendre un single (croix bleues) jusqu'au col du Jaegertaennel. Cette section montante étant néanmoins très roulante, il faudra engager des trajectoires réfléchies pour ne pas perdre trop d'énergie. On redescendra enfin vers la maison forestière Ochsenlaegger tambour battant. Profitez-en pour avaler un peu de sucre...

La dernière montée mémorable du parcours s'annonce, c'est celle du Heidenkopf. Amorcez-là avec patience et calmement et tout ira bien jusqu'en haut. Les lacets ne sont pas si nombreux. Votre vitesse dépendra de l'énergie dépensée depuis le début du parcours. La longue descente vers Mollkirch se fera en suivant le balisage blanc et rouge jusqu'à la D217 (là encore une variante consiste à prendre le GR jusque Laubenheim, mais ce billet n'est pas pour les plus confirmés des vététistes). On reprend les croix jaunes pour traverser la Magel (attention, section très humide!) puis on rejoint les rectangles bleus (GR 531) à Mollkirch jusqu'à la gare.

Caractéristiques du parcours : 35 km avec 1250 m D+.

<iframe width="100%" height="300px" frameBorder="0" src="https://framacarte.org/fr/map/hahnenberg-heidenkopf_3146?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&datalayersControl=true&onLoadPanel=caption&captionBar=false"></iframe>

<a href="https://framacarte.org/fr/map/hahnenberg-heidenkopf_3146">Voir en plein écran</a>



