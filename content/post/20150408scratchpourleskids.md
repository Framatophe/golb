---
title: "Scratch pour les kids"
date: 2015-04-08
author: "Christophe Masutti"
image: "/images/postimages/terminal.png"
tags: ["Logiciel libre", "Programmation", "Informatique", "Scratch"]
description: "Un langage d'apprentissage à la programmation idéal pour les petits comme pour les grands."
categories:
- Logiciel libre
---

Vous avez certainement entendu parler de [Scratch](https://fr.wikipedia.org/wiki/Scratch_%28langage%29). Peut-être même, avec un peu de chance, un enseignant de l'école primaire ou du collège de vos enfants en a fait la promotion voire a organisé une séance d'initiation en salle informatique. Bon… ce serait dans le meilleur des mondes. Si l'Éducation Nationale se mettait vraiment à exploiter les solutions libres et efficaces d'apprentissage de l'informatique, on le saurait. Quoique cela c'est déjà produit&nbsp;! Souvenez-vous du [LOGO](https://fr.wikipedia.org/wiki/Logo_%28langage%29).


OK, ce n'était pas vraiment libre, mais cela provenait tout droit des années 1960, avant l'apparition des licences libres. J'y ai eu droit dans les années 1980, et cela a contribué énormément (fondamentalement) à ma connaissance des machines et la compréhension de la logique informatique. Depuis presque 10 ans, pourtant, un petit nouveau a fait son apparition, c'est Scratch. Et à utiliser avec les enfants, c'est un vrai bonheur&nbsp;! Voici quelques réflexions à propos de Scratch et sur la récente parution de *Scratch pour les Kids* (en français), chez Eyrolles.

- Titre&nbsp;: [Scratch pour les Kids](http://www.editions-eyrolles.com/Livre/9782212141115/scratch-pour-les-kids)
- V.O.&nbsp;: *Super Scratch Programming Adventure*, 2nd Edition
- Auteur&nbsp;: The LEAD Project
- Editeur&nbsp;: Eyrolles
- Date&nbsp;: 2015
- Nb. de pages&nbsp;: 160
- ISBN&nbsp;: 978-2-212-14111-5

Si vous voulez tester Scratch rapidement et en ligne, sans avoir besoin d'installer un programme sur votre machine, il suffit de vous rendre sur le site [scratch.mit.edu](https://scratch.mit.edu/) (j'ai une préférence personnelle pour l'alternative [Snap!](https://snap.berkeley.edu/)). Vous pourrez disposer de l'interface et faire partie ainsi des millions d'utilisateurs qui s'échangent leurs projets. Vous aurez aussi droit à la toute dernière version de Scratch, même s'il est assez facile d'installer localement le programme, en particulier si vous devez l'utiliser sans connexion Internet (je préconise personnellement l'installation en local, qui donne l'opportunité aussi d'appréhender la question de la sauvegarde et de la gestion de fichiers).

Scratch, c'est quoi&nbsp;? Il n'est pas très utile ici de développer une présentation en détail. Le site officiel est déjà très complet à ce propos. En gros, Scratch est dérivé de manière assez lointaine (mais évidente) du LOGO via un logiciel nommé [Squeak](http://fr.wikipedia.org/wiki/Squeak). Il s'agit d'interpréter graphiquement du code pour faciliter son approche, ce qui permet de manipuler de manière très visuelle des blocs de code de manière à créer des projets-programmes. Aux yeux d'un enfant, le résultat est toujours immédiat&nbsp;: il peut voir ses scripts en action, même de manière indépendante les uns des autres, et s'amuser à contruire tout un projet, le partager, l'améliorer, etc. Par exemple, au Québec, où Scratch est largement utilisé dans le monde éducatif, vous pouvez trouver le site [Squeaky MST](http://squeaki.recitmst.qc.ca/PageAccueil) qui a l'avantage de proposer des bouts de codes bien pratiques adaptés à différentes situations ainsi que des présentations et des tutoriels.

![Capture d'écran](/images/capture_scratch2.png)

L'interface, quant à elle, est attrayante. Une fenêtre montre toujours le projet en action et l'outil principal est la souris qui permet de déplacer les blocs de scripts. Pour un adulte, la prise en main n'en est que plus rapide. Au bout de quelques essais, on en vient vite à comprendre le fonctionnement général. Mais une question se pose&nbsp;: n'est-on pas vite limité par nos propres connaissances de l'informatique en général&nbsp;?

Il faut jouer carte sur table&nbsp;: soit vous laissez l'enfant s'amuser et s'approprier Scratch tout seul, soit vous l'aidez et montez des projets avec lui, et de manière pédagogique vous élevez progressivement le niveau d'exigence. Si vous préferez la première solution, c'est que peut-être vous même n'êtes pas à l'aise avec l'informatique. Ce n'est pas un reproche, mais je vous rends attentif au fait que, si pour vous il s'agit d'une question de choix (alors que c'est faux) vos enfants, eux, ne se posent pas la question d'avoir ou non le choix&nbsp;: les machines sont et seront leur quotidien.

Avant d'être assis devant un ordinateur en longueur de journée, comme c'est peut-être votre cas si vous faites un travail de bureau, par exemple, si vous aviez eu la possibilité d'apprendre l'informatique à l'école (et je parle d'informatique, pas de l'actuel B2i qui n'est qu'une forme d'utilisation superficielle de quelques logiciels), je suis certain que votre utilisation quotidienne d'une machine serait profondément différente. À l'école, je n'ai pas appris ce qu'est un protocole de communication, ni comment réaliser un site web, ni installer un système d'exploitation, etc. Rien de tout cela. J'ai juste appris à entrer quelques commandes avec le LOGO, le [BASIC](http://fr.wikipedia.org/wiki/BASIC) et, au collège, programmer des règles de calculs avec le [Pascal](http://fr.wikipedia.org/wiki/Pascal_%28langage%29). L'apprentissage de la programmation informatique (non, on ne dit pas «&nbsp;coder&nbsp;») permet de rendre autonome, d'avoir de bons réflexes, essentiellement basés sur le partage de connaissances, la recherche de solutions et, j'en suis convaincu, sert à tous les niveaux et tous les types d'apprentissages.

Bref, si vous voulez vraiment bien accompagner votre enfant dans l'apprentissage de Scratch (et plus tard vers d'autres langages), il y a quand même un minimum de pré-requis, que vous vous pouvez acquérir en un minimum de temps, simplement en vous renseignant. L'essentiel est d'être à l'aise car si vous ne l'êtes pas, vous atteindrez vite les limites de tolérance de votre enfant, en particulier si à chaque étape que vous n'aurez pas anticipée, vous êtes obligé de vous arrêter, paniquer, perdre patience, et finir par laisser tomber. Ce serait dommage, non&nbsp;?

Heureusement, Scratch ne fait pas partie de ces langages où, pour comprendre une fonction et l'utiliser, il faut en plus savoir comment elle sera interprétée par la machine. Non&nbsp;: on utilise des blocs, on les assemble, on peut y ajouter quelques variables, et on voit le résultat. C'est tout. Mais par où commencer&nbsp;? Si *a priori* Scratch semble être un joyeux cliquodrome facile d'accès, sans un guide pour vous orienter, la frustration fini par être au rendez-vous. C'est là qu'intervient un magnifique petit ouvrage parfaitement adapté à une utilisation conjointe adulte/enfant&nbsp;: *Scratch pour les Kids*.

La présentation du livre a été pensée de manière très pédagogique. Il est divisé en 10 chapitres, appelés «&nbsp;niveaux&nbsp;», introduit par une petite bande dessinée. Chaque chapitre reprend les acquis précédents pour aller toujours plus loin dans l'apprentissage de Scratch. Si l'on peut appréhender, au départ, les longues phases de saisie, tout a néanmoins été fait pour que chaque script soit expliqué, partie après partie. C'est là qu'intervient un autre avantage de Scratch&nbsp;: chaque petit script est «&nbsp;jouable&nbsp;» indépendamment du reste du programme. L'enfant ne se lasse pas et l'adulte non plus&nbsp;! Dès le niveau 2, c'est un jeu très visuel qui est créé. Il peut se lancer immédiatement mais la cerise, c'est que l'enfant a tout de suite envie d'y apporter des améliorations... comme il se trouve assez vite coinçé par manque de pratique, il va naturellement poursuivre le chapitre suivant.

Pour le mode d'emploi, l'ouvrage est déclaré «&nbsp;dès 8 ans&nbsp;». Il est vrai que c'est à partir de cet âge, et même un peu avant, qu'on peut commencer à travailler avec le LOGO. Néanmoins, le bon maniement de la souris, l'autonomie face à la gestion des fichiers, le fait de rechercher la meilleure méthode pour organiser ses scripts, etc. tout cela m'oblige à dire qu'attendre 9 ans ne me paraît pas exagéré. Dans tous les cas, avant 12 ans, la meilleure solution est de travailler à deux, l'adulte et l'enfant. L'adulte pour expliquer certains termes, et aider l'enfant à créer ses scripts. L'enfant aux commandes, lisant l'ouvrage et appliquant les méthodes des différents niveaux. De même, faire deux niveaux d'un coup serait présumer de la concentration de l'enfant&nbsp;: mieux vaut bien comprendre un niveau et le laisser improviser sur la base des connaissances nouvellement apprises que d'enchaîner aussitôt au niveau supérieur. Pour finir, il sera ensuite temps de passer à la programmation proprement dite, en commençant par [Python pour les kids](http://www.eyrolles.com/Informatique/Livre/python-pour-les-kids-9782212140880)</a>, par exemple...

Pour utiliser Scratch, vous avez plusieurs solutions&nbsp;:


- Utiliser la version en ligne sur [Scratch.mit.edu](https://scratch.mit.edu) ou bien [Snap!](https://snap.berkeley.edu/),
- Installer la version *offline* de la dernière version de Scratch (pour GNU/Linux, MacOS et Windows). Cela nécessite l'installation de Adobe AIR (dommage, ce n'est pas libre),
- Si vous êtes sous une distribution GNU/Linux, Scratch est sans doute dans vos dépôts. Attention, toutefois&nbsp;: la version de Scratch utilisée dans l'ouvrage est la version 2. Il y a de grandes chances que vos dépôts proposent une version différente.


Enfin, si vous optez pour l'installation en local, vous pouvez rapatrier les projets mentionnés dans l'ouvrage simplement en les téléchargeant depuis le site. Il suffit de se rendre sur la [page du livre](http://www.editions-eyrolles.com/Livre/9782212141115/scratch-pour-les-kids) et télécharger les *Compléments* (env. 15 Mo). Il s'agit d'un fichier compressé contenant deux dossiers&nbsp;: le premier avec les fichiers des exemples du livre et les scripts, le second avec les exemples «&nbsp;nus&nbsp;», c'est à dire les images, les lutins, etc. et sans les scripts.

