---
title: "Internet, pour un contre ordre social"
date: 2014-09-26
author: "Christophe Masutti"
image: "/images/postimages/say.png"
tags: ["Libres propos", "Informatique", "Internet", "Capitalisme de surveillance"]
description: "Degoogliser Internet par une offre de solutions éthiques et solidaires"
categories:
- Libres propos
---


Les bouleversement des modèles socio-économiques induits par Internet nécessitent aujourd'hui une mobilisation des compétences et des expertises libristes pour maintenir les libertés des utilisateurs. Il est urgent de se positionner sur une offre de solutions libres, démocratiques, éthiques et solidaires.

Voici un article paru initialement dans [Linux Pratique](http://www.unixgarden.com/) num. 85 Septembre/Octobre 2014, et repris sur le [Framablog](http://www.framablog.org/index.php/post/2014/09/05/internet-pour-un-contre-ordre-social-christophe-masutti) le 5 septembre 2014.


Téléchargements :

- [Article au format PDF (LaTeX)](/docus/cmasutti_internetpouruncontreordresocial_lal20140926.pdf)
- [Sources (.tex, .markdown, .html)](cmasutti_internetpouruncontreordresocial_lal20140926.zip)


