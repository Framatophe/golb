---
date: 2019-03-17T15:00:59-04:00
title: "Un client, un'espresso"
author: "Christophe Masutti"
description: "Vous ne vous êtes jamais demandé pourquoi, depuis quelques temps, on vous proposait un café dans certaines boutiques, chez le coiffeur, votre banquier, votre assureur ? En connaissance de cause, apprenez à refuser poliment."
image: "/images/postimages/hommemachine.jpg"
draft: false
tags: ["Libres propos", "commerce", "capitalisme", "café", "nespresso", "qualité"]
categories:
- Libres propos
---

«&nbsp;J'en veux pas de ton café. Et puis t'as bien lavé la tasse au moins ? C'est pas un bar ici.

Je viens juste essayer la jolie montre que j'ai vue en vitrine. Elle me plaît bien, cette montre, avec un bracelet métal qui semble tenir la route. Il y a un peu de monde dans le magasin, ok, on est samedi, c'est normal. Pas grave, je peux attendre.



Mais tu m'emmerdes avec ton café. Je vais être obligé de décliner. Je vais devoir aller puiser dans ces années d'apprentissage de savoir vivre pour refuser poliment. Je ne voudrais pas donner une mauvaise impression, quoi. Un peu comme si on inversait les rôles. Puisque je refuse, c'est à moi qu'il revient de faire un effort. Merde alors.



Si je ne veux pas de ton café, c'est pas qu'il n'est pas bon, ou que ta tête ne me revient pas, ou que je préfère le thé, non… juste&nbsp;: je-n'en-veux-pas.



Comment te le dire sans te froisser ? Mais en fait peut-être que tu t'en contrefout. Et puis, je le vois bien, tu n'as pas le temps de m'offrir un café. Je veux dire&nbsp;: *vraiment* m'offrir un café, pour discuter, taper une belote, parler politique et lier une amitié. Du temps, tu en as déjà pris sur le client précédent pour venir m'asséner ton prénom et ton nom (et cette fois-ci, c'est moi qui m'en fout), accompagnés d'un laïus que tu as répété si vite, si machinalement…



En fait, tu *dois* m'accueillir avec tout un discours, toute une armada de phrases toutes faites, rabâchées, brandissant ta tablette… d'ailleurs, tu la trimballes cette tablette, c'est pas encombrant ? et puis qu'est-ce que tu viens d'y écrire là ?



Pourquoi utilises-tu tous les codes des relations sociales intimes, alors qu'on ne se connaît pas ? Enfin si, maintenant, on se connaît&nbsp;: je le vois dans tes yeux, en grosses lettres fatiguées&nbsp;: surcharge discursive.



Je regarde derrière le comptoir&nbsp;: diplôme de technicienne d'horlogerie. Pas un diplôme facile. Deux ans d'études, sans compter le bac pro, des stages, un vrai métier, quoi. Qu'est-ce que tu fous à me proposer un café ?



Il y a un an, ta boutique a complètement changé de look. Une volonté des grands patrons. Des chaises design (je le sais&nbsp;: cela fait 5 minutes que je viens de m'asseoir, j'ai les fesses gelées et une sciatique), un agencement à l'avenant avec un comptoir où tu ne peux même pas exercer ton métier, ne serait-ce que pour changer une pile.



Et puis surtout des formations. À la pelle. Sur l'accueil clientèle, sur la qualité du service, l'entretien de la peur du «&nbsp;client mystère&nbsp;», les frustrations des contrôles qualité.



C'est là qu'on t'a appris un truc que tu ignorais jusqu'alors&nbsp;: 86% des clients s'estiment mieux accueillis si on leur offre un café en magasin. Ouaih! sérieux, dans une horlogerie&nbsp;! Ton patron responsable de boutique, lui aussi, l'ignorait totalement. Après 25 ans d'expérience, en voilà une surprise&nbsp;!



C'est les petits gars de la boîte Retail Service Expert qui lui ont appris. La boîte que le grand chef à Paris avait mandaté pour réaliser un audit sur la qualité de l'accueil clientèle dans la chaîne.



Ça n'a pas tardé. Moins d'un mois plus tard tous les magasins étaient équipés d'une machine à café (celle avec les capsules en alu qui polluent bien) et d'un coin «&nbsp;convivialité clientèle&nbsp;». Tu l'as organisé comme tu pouvais, au détriment des rangements disponibles, à côté de «&nbsp;l'espace enfants&nbsp;» avec les bouquins déchirés et les jouets baveux, en face du grand portique des montres Artik, celui avec le grand ours blanc de deux mètres de haut. Déjà que la boutique était petite…



Pour toi, ce café, c'est devenu l'étape obligatoire pour «&nbsp;papoter&nbsp;» avec les clients pendant que cinquante commandes t'attendent à l'arrière boutique pour être traitées en grignotant sur le temps de travail. Si ça se trouve, tu ne l'aimes pas non plus ton café.



Maintenant tu rentres plus tard le soir, tu t'occupes moins des enfants, mais après tout, tu as bien «&nbsp;papoté&nbsp;» avec les clients, c'est pas comme si tu travaillais. Enfin, bon, sur le site internet de la chaîne, c'est marqué que les clients sont censés venir «&nbsp;papoter&nbsp;» autour d'un café avec les vendeurs… Il faut dire que les contrôles qualité organisés par les grandes marque partenaires y accordent beaucoup d'importance&nbsp;: sinon tu perdrais carrément le marché&nbsp;! 



Il y en a des procédures, des discours à ressortir par cœur. Quand tu n'es pas en surcharge discursive, il te faut remplir de la paperasse, des évaluations de contrôles de procédures dans des dossiers… Staline en aurait rêvé.



Avant, tu réparais des horloges, tu vendais des belles montres et même des sur-mesure&nbsp;! attention, même des japonaises et des suisses haut de gamme. Tu étais fière de ton métier… tu possédais un savoir-faire. 



Là, tu bricoles. Tu vends ce qu'on te dit de vendre… et pourtant c'est les mêmes produits à peu de chose près. Pas de la camelote. Mais c'est un peu comme si tu n'en n'avais plus rien à foutre de ce que pense le client ou bien ce qu'il voudrait dire. Là… tu proposes un café… oui, même à la mère Chopin, 85 ans, qui est passée avant-hier pour réparer son coucou de la Forêt-Noire et qui est cardiaque. Pas son coucou, elle. Tu le sais parce que c'est son fils qui te l'a dit à la supérette, l'autre jour. Elle t'avait regardé bizarre, en rentrant, quand tu lui a annoncé ton nom en lui montrant la machine à café. Tout ça parce tu passais un audit ce jour-là. L'a rien pigé, la pauvre vieille.



Parce que si tu ne le proposes pas ce putain de café, c'est l'un des indicateurs de la qualité d'accueil qui saute. Sans ça, les managers qui ne sont pas horlogers, eux, ils ne sauraient pas dire si tu fais bien ou mal ton métier. Il faut bien te surveiller, des fois que t'oublies de remonter une info clientèle.



Alors j'ai une bonne et une mauvaise nouvelle pour toi. En attendant mon tour, j'ai un peu surfé avec mon smartphone. Je me demandais d'où pouvait bien provenir cette idée que les clients d'une horlogerie voulaient absolument du café. Ben j'ai trouvé.



Il y a beaucoup de sites internet de boîtes de conseil, et surtout des blogs, spécialisés dans la relation clientèle. Beaucoup vantent les mérites de la compétitivité de la «&nbsp;qualité d'accueil&nbsp;», celle qui transforme les vendeurs en carpettes pour clients et souffre-douleur pour managers sous pression. Tous font appel à de vagues études, ne citent presque jamais leurs sources et, quand ils le font, les études ne valent pas grand chose en définitive. Mais j'ai été étonné de constater que très peu mentionnent cette histoire de café. Il a fallu insister pour trouver ce qui, pourtant, semble connu.



Il y a [celui-ci](https://www.sensduclient.com/2017/10/comment-le-cafe-ameliore-lexperience.html), qui mentionne clairement que le café est un «&nbsp;rituel de service&nbsp;» et qui soutient que, effectivement 86% des clients s'estiment satisfait lorsqu'on leur offre un café. Et de là tout un laïus sur la «&nbsp;symétrie des attentions&nbsp;», un concept managérial construit assez récemment. Et quand on s'y penche de plus près, la «&nbsp;symétrie des attentions&nbsp;», ben ça passe par du café.



Petite explication. La «&nbsp;symétrie des attentions&nbsp;» est une  idée entretenue par l'[Académie du service](https://symetriedesattentions.com), un cabinet de conseil appartenant [anciennement au groupe hôtelier Accor](https://www.e-marketing.fr/Thematique/management-1090/Breves/Academie-Service-Accor-devient-independante-189264.htm). Grosso-modo il s'agit de soutenir que la qualité du service au client dans une entreprise est relative (symétriquement) à la qualité des relations entre les collaborateurs à l'intérieur de cette entreprise. C'est expliqué [là](https://www.journaldunet.com/management/expert/58066/la-symetrie-des-attentions---une-idee-qui-a-fait-son-chemin.shtml).



Personnellement, j'ai eu beau chercher, je n'ai pas trouvé d'éléments du côté de la littérature scientifique du management à ce propos, mais un concept semble s'y accoler parfaitement, celui de *servuction*. Là il s'agit surtout du processus de production de service, applicable pour beaucoup en hôtellerie et restauration&nbsp;: on parle d'un parcours de servuction, c'est-à-dire qu'on identifie les moment-clé la vie du client et à chaque étape proposer du service, calibrer des réponses et des rôles. Dès lors, il n'est pas délirant de se dire que si on identifie le parcours de servuction en mettant les collaborateurs et les clients ensemble dans un processus de discussion intelligente, et en faisant en sorte que les relations entre les collaborateurs soient bien huilées, sans pression inutile, on peut obtenir au bout du compte un service de premier ordre pour la majeure partie des clients. 



Quelle que soit leur valeur scientifique, ces concepts, au départ, sont donc faits pour tenter d'augmenter les chances de satisfaire la clientèle. Mais lorsqu'ils sont appliqués bêtement à n'importe quel secteur d'activité tout en conservant un modèle hiérarchique (et tayloriste) des rôles, là où la concertation est nécessaire, on arrive à des aberrations.



Ce putain de café est une aberration.



Mais d'où sort-il ? ben des ex-hotelliers de l'Académie du Service, justement… Dans une étude, intitulée «&nbsp;Le rôle sociétal du café&nbsp;» qu'on peut se procurer [sur leur site](https://www.academieduservice.com/nos-publications/), on apprend que dans le cadre d'une évaluation de la satisfaction clientèle&nbsp;: 9 clients sur 10 considèrent qu'ils sont vraiment accueillis lorsqu'on leur propose un café&nbsp;! Et cela va même jusqu'à apprécier la qualité du café, attention&nbsp;!



Alors, ma chère horlogère, tu apprendras aussi d'autres trucs&nbsp;:



- l'étude en question est une étude qui a été commandée par Nespresso&nbsp;;
- l'étude montre que non seulement il faut offrir du café au client mais aussi que le café joue aussi un rôle très important entre les collaborateurs (ben oui, parce que si le café est bon pour huiler les relations entre les collègues, il le sera forcément avec les clients)&nbsp;;
- que la Société Générale (qui témoigne avec force pour appuyer l'étude) avait déjà collaboré avec Nespresso en 2012 pour… démontrer la même chose&nbsp;;
- et puis tiens on trouve aussi une autre [étude menée par l'IFOP](https://www.ifop.com/publication/le-role-societal-du-cafe-en-entreprise/), toujours pour Nespresso, à propos du rôle sociétal du café, mais cette fois uniquement dans les relations au sein de l'entreprise (mais comme on a montré qu'il fallait une «&nbsp;symétrie des attentions&nbsp;», ce qui vaut pour les collaborateurs vaut aussi pour les clients).



Bref, t'as pas fini d'en boire du café, ma chère. Et du Nespresso, s'il te plaît.

