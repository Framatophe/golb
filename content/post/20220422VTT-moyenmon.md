---
title: "Guide de survie du vététiste débutant en moyenne montagne"
date: 2024-06-04
author: "Christophe Masutti"
image: "/images/postimages/naturel.jpg"
description: "(màj. juin 2024) Voici un long billet dont j'ai remis la rédaction depuis bien trop
longtemps. Il s'adresse aux grands débutants en VTT qui souhaitent s'y mettre.
S'agissant de réflexions tirées de mon expérience personnelle dans les Vosges,
il faut considérer que je m'adresse essentiellement aux débutants vététistes en
moyenne montagne. Quatre grands thèmes sont abordés&nbsp;: la pratique du VTT,
le choix d'un premier VTT, l'équipement et l'entretien."
tags: ["Sport", "VTT", "Cross-country", "Entraînement", "Parcours"]
categories:
- Sport
---


Voici un long billet dont j'ai remis la rédaction depuis bien trop longtemps. Il
s'adresse aux grands débutants en VTT qui souhaitent s'y mettre. S'agissant de
réflexions tirées de mon expérience personnelle dans les Vosges, il faut
considérer que je m'adresse essentiellement aux débutants vététistes en moyenne
montagne. Quatre grands thèmes sont abordés&nbsp;: la pratique du VTT, le choix
d'un premier VTT, l'équipement et l'entretien.

(màj. juin 2024)

<!--more-->

{{< toctoc >}}

Il faut parfois s'accrocher pour ne pas se perdre parmi les différentes
appellations qui désignent les pratiques du VTT. Ce n'est pas qu'elles soient
nombreuses, c'est surtout qu'elles sont parfois utilisées de manière
indifférenciée. Ainsi, certains sites ou vendeurs vous conseilleront
exclusivement un VTT tout-suspendu pour la pratique du « All Mountain », tandis que
d'autres vous diront que le semi-rigide comme le tout suspendu conviennent, car
il ne s'agit que de randonnée sportive, soit du Cross country&hellip; 

## Les pratiques du VTT

Nommer la pratique que l'on souhaite avoir, c'est aussi conditionner le choix du
matériel et donc son achat. Mon avis personnel, c'est que certains sites
internet ont tendance à pousser le choix d'un VTT tout suspendu alors que le
client débutant en est encore à se questionner sur les équipements de freins et
transmission. En contraste, je n'ai encore jamais vu un bon vendeur dans un
magasin de cycles soutenir mordicus que le client devrait obligatoirement
choisir un type de VTT plutôt qu'un autre&nbsp;: le conseil doit être technique
et comparatif dans une même gamme, mais la pratique appartient au pratiquant.

C'est pour cela qu'il faut d'abord réfléchir à la pratique que l'on souhaite. Il
faut pouvoir faire confiance au vendeur et à ses conseils, mais auparavant il
faut aussi _savoir lui communiquer vos attentes_. Il ne peut pas les deviner si
vous ne savez pas répondre à ses questions ou si vous répondez sans réellement
savoir ce que vous voulez.

Nous reviendrons plus loin sur l'éternelle question du choix entre un
tout-suspendu et un semi-rigide. 

### Mais c'est quoi «&nbsp;le VTT&nbsp;»&nbsp;?

Comme son nom l'indique en français, il s'agit de «&nbsp;vélo tout
terrain&nbsp;». En anglais, il s'agit de [vélo de
montagne](https://adventuresportsjournal.com/cruisers-clunkers-and-the-origins-of-the-off-road-bike/)
(*Mountain Bike*). Bien que la version anglophone ai longtemps eu cours, je
trouve que la version francophone est bien mieux adaptée&nbsp;: on peut utiliser
un VTT sur chemin de campagne dans les collines sous-vosgiennes comme sur les
pires aiguilles des Alpes. L'essentiel est que cela puisse rouler sur tous les
terrains (il paraît que certains en font en Bretagne, c'est tout dire).

La seule chose que vous tâchez d'éviter, c'est la route, le bitume. Premièrement
parce que vous risquez de vous y ennuyer en ayant l'impression de ne pas
avancer. Deuxièmement parce que vos pneus et la géométrie d'un VTT ne sont pas
faits pour cela. 

Il y a plusieurs manières d'aborder le VTT. La première, c'est la petite
promenade du dimanche sur des chemins de terre. C'est plus tranquille que sur la
route, pas de voiture. Si tel est votre souhait, je ne pense pas qu'il soit
utile de lire plus loin. Vous achetez un VTT assez bas prix (en dessous 500
euros, c'est tout à fait envisageable). Si vous en prenez soin, il saura vous
durer.  Mais un VTT ne sera peut-être pas votre choix le plus éclairé&nbsp;: il
existe des vélos «&nbsp;tout chemin&nbsp;» (VTC) qui seront sans doute plus
appropriés et, à l'occasion, vous permettront de faire des randonnées plus
longues et plus confortables en alternant voies communales et pistes
cyclables[^gravel].

[^gravel]: L'autre option est d'acheter un vélo dit «&nbsp;Gravel&nbsp;», assez
  à la mode en ce moment&nbsp;: pour dire vite il s'agit d'un vélo de course
  (donc sportif) avec un cadre et des roues adaptés aux chemins forestiers peu
  accidentés.

La seconde et principale manière d'envisager le VTT, c'est le _sport_. De part
sa géométrie, un VTT est d'abord _un vélo sportif_. Tout comme l'est celle du
vélo de route (de course). Et comme toute discipline sportive, cela nécessite un
entraînement régulier.

Le VTT n'a certes pas le monopole de la difficulté en sport, mais il faut
reconnaître que ce n'est pas une discipline très simple. D'abord, il y a un
aspect matériel à prendre en compte. Et surtout, il faut acquérir de l'endurance
et de l'aisance de pilotage. Débuter le VTT comme sport vous engage à envisager
quelques séances un peu décourageantes et ingrates devant les terrains
accidentés et les montées interminables. Mais dès le début vous y trouverez
beaucoup de plaisir et de satisfaction pour les distances et le dénivelé
accomplis.

Simplement, essayez de choisir des circuits avec des points de vue, donnez-vous
des objectifs, et ne présumez pas de vos forces. Soyez conscient·e de votre
niveau. Si vous êtes blême en haut des marches et des racines, ou si vous avez
le cœur en dehors de sa boîte au milieu d'une côte trop raide, il n'y aucune
honte à descendre du vélo et faire quelques pas. Ne vous inquiétez pas, de toute
façon, vous descendrez de vélo plus souvent que vous ne le croyez&nbsp;:)

Puisque nous parlons de sport, c'est l'occasion de rappeler quelques
principes&nbsp;:

- *la régularité*&nbsp;: la météo ne doit pas être une excuse pour ne pas
  rouler. C'est désagréable, certes, mais un petit coup de froid ne fait pas de
  mal. Par contre, n'oubliez pas les descentes, n'allez pas vous transformer en
  glaçon. J'ai coutume de ne pas sortir le VTT en dessous 5 degrés à moins d'un
  grand soleil. Mais vous trouverez toujours quelques givré·es pour le faire quand
  même. La grosse pluie, en revanche, il vaut mieux l'éviter&nbsp;: le vélo est
  tout terrain, mais la transmission et les suspensions prennent cher lorsqu'il
  y a trop d'humidité (et gare au nettoyage).
- *la mixité*&nbsp;: le VTT est un sport, mais il ne doit pas vous empêcher d'en
  faire d'autres. La course à pied est un excellent complément pour travailler
  l'endurance. La natation vous aidera à obtenir plus de gainage (et aussi de
  l'endurance) tout en préservant vos articulations. Et chaque matin, un peu de
  musculation (sangle abdominale et pompes) pour améliorer votre pilotage, votre
  équilibre et résister aux longues sorties.
- *mon conseil*&nbsp;: si une sortie VTT est votre seule pratique sportive de la
  semaine, vous risquez de ne pas beaucoup progresser. Essayez, lorsque viennent
  les beaux jours, de faire au moins deux sorties par semaine, une courte une
  longue, et alternez avec de l'endurance et du gainage. En hiver, si vous ne
  sortez pas le VTT du week-end à cause de la météo, faites une séance de trail
  à la place, au moins deux heures, pour garder le «&nbsp;coffre&nbsp;».
- en fonction de votre pratique (voir les sections suivantes), votre
  entraînement ne sera pas le même. Si vous vous lancez dans l'Enduro, je vous
  conseille de faire pas mal de musculation.  Pour le Cross country, privilégiez
  le cardio.


Ces considérations étant exposées, intéressons-nous désormais aux différentes
pratiques du VTT.


### All mountain (on dit : Cross country, en fait)

C'est l'expression-valise pour dire «&nbsp;randonnée sportive sur chemins
mixtes&nbsp;». Par chemins mixtes, j'entends aussi bien des sentiers qui
nécessitent un peu de pilotage technique (en descente comme en montée) et des
grands chemins forestiers, le tout avec une certaine technicité des sentiers. 

Et vous allez demander&nbsp;: n'est-ce pas justement la définition du VTT&nbsp;?
Et bien si, justement. Cette expression est selon moi purement issue du
marketing, en particulier sur les sites de vente en ligne de matériel sportif.
Si on multiplie les appellations de pratiques, on multiplie par conséquent les
choix possibles.

Si je vous disais par exemple qu'un VTT tout-suspendu est essentiellement fait
pour la descente, vous hésiteriez à juste titre car pour la plupart des gens,
descendre des sentiers en vélo est forcément dangereux. Or, il a bien fallu
multiplier les ventes de VTT tout-suspendus en les adaptant à ces personnes qui
veulent un tel VTT sans pour autant se lancer dans la discipline Enduro.
J'exagère à peine&nbsp;: le tout-suspendu a largement gagné le créneau du Cross
country, mais comme je vais l'expliquer plus loin, c'est à certaines conditions
seulement que ce choix doit être fait, et il est nullement obligatoire.

En fait, il n'y a que deux grandes pratiques de VTT, elles sont citées dans les
sections suivantes. Et le choix d'un VTT tout suspendu ou semi-rigide est
surtout un choix relatif à la manière de pratiquer et à l'investissement
financier. 

En moyenne montagne, pour un débutant, il n'y aura pas de grande différence
entre un VTT semi-rigide 29 pouces et un tout-suspendu. Pour tout dire, on peut
aimer le tout-suspendu parce que dans les grands chemins forestiers, on ressent
moins de vibrations, et dans certains sentiers en descente on peut parfois
engager un peu mieux des dénivelés&hellip; inattendus (parfois l'erreur de
pilotage peu être plus facilement corrigée). Mais on aime aussi le semi-rigide
car il est beaucoup plus nerveux en montée, il est plus léger, il est plus
précis en pilotage dans beaucoup de situation, et les sensations sont plus
authentiques. Certains diront même que le tout-suspendu, parce qu'il permet de corriger un peu trop facilement les erreurs de pilotage, fausse carrément les sensations normales d'un VTT (hors enduro). Bref, c'est en pratiquant que l'on se forge une opinion. 

Pour résumer à propos du All Mountain&nbsp;: 

- ce n'est pas une pratique précise,
- souvent employé comme un argument de vente,
- c'est plutôt du Cross country.


<figure>
<img src="/images/billetVTT/VTTmeme.jpg">
<figcaption>Sans commentaire.</figcaption>
</figure>

<!--![Sans commentaire](/images/billetVTT/VTTmeme.jpg)-->

### Enduro et DH (DownHill):

Là, c'est du sérieux. Ces pratiques concernent essentiellement la descente. Je
les mets dans le même sac car ce n'est pas vraiment l'objet de ce billet. DH et
Enduro sont des courses chronométrées. Les épreuves d'Enduro (comprendre au sens
de endurance) durent parfois une dizaine de minutes et s'étalent sur plusieurs
jours. Le VTT Enduro peut être utilisé en montée comme en descente (même si,
forcément, ce ne sera pas une bête de course en montée, hein?), il est aussi
réputé très solide car il faut tenir le choc, justement, sur la durée. La
pratique DH est encore plus spécialisée, avec du matériel hyper modifié. Bref,
si vous voulez faire comme dans les superbes vidéos Redbull et quitter le sol
pour le meilleur (ou pour le pire), cette pratique est faite pour vous.
Achetez-vous aussi une caméra, histoire de garder des souvenirs.

Dans les Vosges, plusieurs groupes de vététistes ont aménagé des circuits
dédiés, plus ou moins réglementaires. On peut citer celui du côté de Barr, qui
est en fait un ancien spot de championnat, me suis-je laissé entendre. Ce sont
eux qu'on accuse souvent de dégrader les sols, mais ce n'est pas toujours vrai.
Je connais certains spots où la roche côtoie le chemin bien solide et il n'y a
pas vraiment de problème. Mais il existe aussi des Bike Park (par exemple celui
du Lac Blanc). Si vous voulez tester, je préconise de vous y rendre. 

Autre point important&nbsp;: ne vous prenez pas pour un·e champion·ne. L'Enduro
ne s'improvise pas, et ce n'est pas parce que vous avez un VTT tout-suspendu que
vous pouvez, du jour au lendemain, prendre un élan inconsidéré. Le pilotage,
cela s'apprend&nbsp;! Avec un groupe d'ami·es on peut facilement chercher à
impressionner les copain·es. Croyez-moi, après 30 ans de pratique[^1]&nbsp;: le
seul résultat vraiment impressionnant quand on se plante, c'est celui de
l'hélitreuillage&nbsp;:)

[^1]: À vrai dire, entre les études, la vie familiale et la pratique du trail,
je n'ai pas fait que du VTT durant toutes ces années.


<figure>
<img src="/images/billetVTT/vttenduro.jpg">
<figcaption>VTT Enduro -- Nissan DH Cup in Bouillon - 53, by Nathan. F, CC BY-NC-SA 2.0.</figcaption>
</figure>



### Cross country

On nomme cette pratique aussi X-Country ou, plus court, XC. C'est la pratique la
plus répandue et présente aux jeux olympiques&nbsp;: une course de rapidité, en
somme, sur des terrains variés, quelle que soit la météo.

Au niveau du grand public, ce qui distingue les pratiquants, ce sera la vitesse,
l'allure, le rythme. On oscille entre la randonnée, la randonnée sportive et
l'entraînement marathon. En moyenne montagne, la grande variété des terrains
offre la possibilité d'élaborer des circuits adaptés à toutes les phases de
l'entraînement, y compris de longues distances. 

À la différence de l'Enduro, le Cross country ne cherche pas à jouer avec le
terrain mais optimiser le pilotage pour une trace fluide et rapide. L'endurance
du cycliste est ainsi mise à l'épreuve, mais cela n'empêche nullement de
considérer le Cross country comme un excellent moyen de se déplacer sportivement
dans la montagne et en profiter pour admirer le paysage et les sites d'intérêt.
C'est d'ailleurs l'essentiel pour la plupart des pratiquants. 

Évidemment, si vous voulez progresser, c'est l'entraînement qui fera la
différence (comme pour l'Enduro).

Cette variété au niveau de la pratique comme au niveau du pilotage conditionne
le choix de votre monture. Et ce sont sans doute les seuls critères qui
devraient présider ce choix, en plus de votre condition physique et l'analyse de
votre niveau.

<figure>
<img src="/images/billetVTT/VTT-crosscountry.jpg">
<figcaption>VTT Cross country (course) - 2012 Absa Cape Epic Prologue at Meerendal by mikkelz, CC BY-NC-SA 2.0.</figcaption>
</figure>



## Choisir votre type de VTT

J'ai bien conscience qu'en abordant ce chapitre, je risque de m'attirer les
foudres des plus spécialistes d'entre nous. Je précise donc une nouvelle fois
que je m'adresse au débutant vététiste dans les Vosges (et plus généralement en
moyenne montagne) d'après mon expérience personnelle. C'est tout.

### Quand j'étais jeune&hellip;

Au risque de passer pour un vieux briscard, voici quelques éléments de contexte.

Je vous invite tout d'abord à regarder quelques vidéos qui traînent sur les
zinternets à propos de Gary Fischer. Ce dernier, pionnier du Mountain Bike
californien était aussi concepteur et chef d'entreprise (la marque Gary Fischer,
disparue vers 2010, rachetée par Trek). Il faut voir dans [cette
vidéo](https://www.youtube.com/watch?v=OKwTKeLCs7A) ce qu'on peut faire avec un
vieux biclou 29 pouces sans suspension, en descendant des chemins en jeans,
chemise et gants de chantier&hellip; tout cela pour vous dire (vous
rappeler&nbsp;?) que ce qui fait le bon vététiste, c'est avant tout les cuissots
et le pilotage. Indépendamment de sa qualité et sa robustesse, le matériel, lui,
sera toujours secondaire.

Cette première leçon venue du fond des âges du VTT (les années 1970) vaut bien
un fromage. Pour ce qui me concerne, j'ai commencé exactement en 1989 avec un
Gary Fischer Advance. Caractéristiques&nbsp;: freins V-brake, roues 29 pouces,
tout rigide, selle rembourrée&hellip; Et avec ce VTT, j'ai roulé 5 ans dans tous
les chemins autour de la Vallée des Lacs (Hautes Vosges), sur le granit mouillé
comme sur l'herbe sèche des crêtes vosgiennes. Une excellente école pour le
pilotage. Ma conclusion est que les VTT d'aujourd'hui, équipés de freins à
disques, de jeux de transmission high-tech, de cadres à géométrie confortable,
sont certes beaucoup plus sûrs qu'à l'époque, mais présentent aussi un très
large choix en termes d'équipement, ce qui rend assez difficile le choix d'un
premier achat.

Par conséquent la pression est très forte, pour un débutant&nbsp;: comment bien
investir son argent dans un VTT s'il ignore encore sa pratique réelle, la
fréquence de ses entraînements, les conditions de ses sorties, les chemins qu'il
empruntera&nbsp;?


<figure>
<img src="/images/billetVTT/garyfisher-procaliber.jpg">
<figcaption>VTT Gary Fisher ProCaliber 1991.</figcaption>
</figure>




### Louer un VTT puis investir (ou pas)


Si vous n'avez jamais fait de VTT, bien évidemment&nbsp;: n'achetez rien pour
l'instant. Trouvez un loueur de VTT[^2] et partez faire un tour quelques heures
avec une personne déjà pratiquante (et qui essaiera de ne pas vous dégoûter en
étant pédagogique et patiente). Faites cela plusieurs fois de suite. Les
quelques euros que vous aurez alors dépensé dans la location d'un VTT seront
vite amortis parce que vous aurez une idée beaucoup plus précise de la
discipline.

[^2]: Non, je n'incite personne à emprunter le VTT d'un·e ami·e. Parce qu'il
  n'est pas conseillé de prêter son VTT chéri à un·e débutant·e, alors que la
  location, hein, on sait ce qu'on fait avec ce matériel&hellip;

Une fois que vous le sentez, commencez à vous intéresser à l'achat de votre
premier VTT. J'ai bien dit&nbsp;: le «&nbsp;premier&nbsp;» VTT. Parce qu'il y en
aura d'autres. 

Bien sûr votre capacité d'investissement dépend de la largeur de votre
portefeuille. Mais s'il s'agit de votre premier VTT, c'est aussi le VTT&nbsp;: 

- avec lequel vous allez commettre des erreurs,
- avec lequel vous allez affiner votre pratique, celle qui vous conduira à
  acheter votre second VTT,
- avec lequel vous aller apprendre comment s'entretient un VTT au quotidien et
  même changer des équipements complets.

Et comme c'est votre premier choix, et il y a des chances qu'il ne vous
corresponde pas à l'usage (quelle que soit la qualité du conseil du vendeur).

Il y a plusieurs stratégies possibles.

La première consiste à investir pour un VTT de milieu de gamme que vous roulerez
un à deux ans. Si la pratique vous plaît et que vous pensez pouvoir investir
plus sérieusement, vous pourrez alors revendre ce premier VTT et racheter un VTT
plus haut de gamme et cette fois avec de la pratique et une meilleure
connaissance du matériel. Pour revendre ce VTT, l'important est de bien en
prendre soin. C'est autant d'argent que vous pourrez sauvegarder le moment venu.

La seconde, consiste à acheter un VTT entrée de gamme pour le revendre une
bouchée de pain par la suite ou le convertir pour un autre usage en le
«&nbsp;transformant&nbsp;» en vélo de ville ou tout chemin. À condition de
planifier cela à l'avance. Vous achetez ensuite un VTT plus haut de gamme.

La troisième consiste à acheter d'emblée un VTT haut de gamme chez un
constructeur disposant de beaucoup de choix dans une même gamme.
Explications&nbsp;: au sein d'une même gamme vous trouvez les mêmes cadres mais
un équipement de transmission, freins et suspension plus ou moins haut de gamme.
Plutôt que de taper directement dans le *must* de l'équipement, vous pouvez
choisir un équipement moins bon, et le faire évoluer dans le temps, par exemple
au fur et à mesure des changements de pièces. Cela vous coûtera *in fine* plus
cher, mais la dépense restera progressive.

La quatrième, enfin, la plus simple&nbsp;: vous êtes absolument sûr·e de votre
choix. Vous avez assez d'argent et vous investissez pour un VTT censé vous durer
longtemps, donc vous mettez le paquet. Attention toutefois, on ne compte plus le
nombre de VTT de ce genre qui croupissent au fond des garages dans l'espoir
d'une hypothétique remontada de la motivation&hellip;

Mon conseil serait le suivant. Il est très difficile, lorsqu'on débute, de bien
différencier les constructeurs entre eux. Pour prendre quatre exemples&nbsp;:
Giant, Scott, Lapierre, Trek. Chacun a breveté des géométries de cadre très
différentes et qui changent selon les gammes. Rendez-vous dans les magasins et
les démonstrations.  Prenez votre temps et chaque fois que vous le pouvez,
essayez les vélos à votre taille. 

Pour un premier VTT, je pense qu'il faut éviter d'acheter en ligne sans avoir
d'abord essayé, au moins être monté sur le vélo en compagnie d'un vendeur avisé
qui saura vous dire si votre position est correcte. Il m'arrive parfois de voir
des géants sur des vélos trop petits pour eux bien qu'ils indiquent la bonne
taille dans la grille des correspondances du constructeur. La longueur de vos
bras et votre port de tête sont tout sauf standardisés. De même il peut s'avérer
parfois nécessaire de changer la potence ou le guidon&hellip; un essai de 5
minutes peut vous donner une bonne impression alors qu'une sortie de 3 heures
vous en donnera une toute différente.

Pour conclure, pour un grand débutant, je préconise un achat d'abord entrée de
gamme à revendre ou à recycler ensuite. La raison est que, à ce stade, vous
n'êtes tout simplement pas sûr·e de pratiquer assez régulièrement le VTT sur
plusieurs années.

Mais attention, lorsque j'écris «&nbsp;entrée&nbsp;» de gamme, je n'écris pas
«&nbsp;bas&nbsp;» de gamme au sens péjoratif du terme&nbsp;! Aux tarifs actuels,
il faut compter entre 800 et 1.000 euros pour un VTT entrée de gamme qui puisse
être assez qualifié pour une pratique sportive. On est loin du VTT Décat' à 400
euros. Pourquoi&nbsp;? parce que, quelle que soit votre premier choix, il y a un
niveau d'équipement en dessous duquel il ne faudra pas descendre. Mes
préconisations minimales seraient&nbsp;:

- fourche à suspension air,
- roues / jantes 29 pouces compatible tubeless,
- transmission Shimano Deore (ou équivalent Sram, cf plus loin),
- de bon pneus type Maxxis Forekaster ou Rekon Race (ou équivalent, mais vous
  pouvez les changer facilement si besoin).

Si vous avez cela sur le vélo, c'est un VTT qui devrait vous durer assez
longtemps pour un coût d'entretien modique (changement de pièces) ou plus
onéreux si vous upgradez le tout.

### Cadre aluminium ou carbone&nbsp;?

On peut dire beaucoup sur l'avantage du carbone. Le point le plus évident est la
légèreté (mais c'est aussi plus fragile).

Mais attention&nbsp;: les cadres aluminium pour un VTT aux alentours de 1.000
euros s'avèrent de très bonne qualité et assez légers surtout si on n’alourdit
pas en tout-suspendu. Dans le haut du panier en aluminium, parfois mieux vaut
acheter un bon cadre aluminium qu'un cadre carbone bas de gamme qui percera à la
première pierre projetée par votre roue avant.

La question du poids, pour un premier VTT, me semble une question hors de
propos. En effet, si vous achetez un VTT suivant les préconisations minimales
ci-dessus, la différence avec d'autres VTT plus chers se mesure à 2 kg près
(davantage si vous prenez un tout-suspendu). Or, ce qui fait la différence à
votre niveau de pratique, ce sont surtout vos jambes&nbsp;: contentez-vous déjà
de pédaler, vous optimiserez en perdant déjà du poids vous-même et en prenant de
la masse musculaire. 

L'autre point à l'avantage du carbone est sa rigidité. Cela implique une
meilleure transmission de l'énergie&hellip; mais franchement quand vous en serez
à comparer l'alu et le carbone selon ces critères là, vous n'aurez pas vraiment
besoin de conseils.

### Tout suspendu ou semi-rigide (hardtrail)&nbsp;?


C'est l'éternelle question qui hante tous les magazines et autres sites
marchands. 

En toute objectivité, les VTT tout-suspendus d'aujourd'hui offrent des
performances très intéressantes et ont réussi à perdre une grande partie du
poids qui leur était reproché il y a quelques années (ils ne sont pas poids
plume pour autant).

Il ne faut cependant pas s'accrocher aux catégories avancées par certains sites.
D'aucun soutiennent que pour le All Mountain (je rappelle que cela ne correspond
à aucune pratique précise) il vous faut du tout-suspendu sinon rien.

Il faut se poser la question&nbsp;: selon ma pratique, qu'est-ce qui va guider
mon choix&nbsp;?

Pour l'Enduro (ou le DH), je pense que la réponse va de soi&nbsp;: suspensions
avant et arrière à grands débattements. Notons tout de même qu'il existe des
fans d'Enduro en *hardtrail*. Là, je pense qu'il faut essayer avant d'en parler.

Pour le Cross country, cela se discute. Les suspensions des VTT tout-suspendus
destinés au Cross country ont des débattements bien plus faibles que pour
l'Enduro et sont vraiment destinés à cette pratique. De plus lorsqu'on regarde
les championnats de cross-country depuis ces 5 dernières années, on y voit en
majorité des VTT tout-suspendus&hellip; mais en carbone, avec le dernier cri de
la technologie, bref&hellip; chers, très chers.

Dans cette pratique, et pour la moyenne montagne, le tout-suspendu offre une
meilleure adhérence/propulsion en «&nbsp;collant&nbsp;» la roue arrière au sol,
et un meilleur confort en descente avec, peut-être, un sentiment de sécurité.
D'un autre côté, les chemins de moyenne montagne praticables en Cross country
n'offrent que très rarement l'occasion d'éprouver significativement tout le jeu
de suspension. La raison est qu'en choisissant des roues 29 pouces, l'essentiel
est déjà «&nbsp;amorti&nbsp;».

N'achetez pas un tout-suspendu si votre seule raison est de vous assurer de
passer sur tous les sentiers en moyenne montagne (dans les Alpes, c'est
différent). En fait, en moyenne montagne, les sentiers trop techniques sont peu
nombreux, représentent très peu de kilomètres comparé au reste et de toute façon
vous ne les emprunterez pas. D'après mon expérience vosgienne, si cela passe en
tout-suspendu, cela passe aussi en semi-rigide. Le tout-suspendu n'est pas
«&nbsp;plus tout-terrain&nbsp;» que le semi-rigide. Ce n'est pas le bon critère.

La combinaison 29 pouces + tout-suspendu pour du Cross country dans les Vosges
me semble revenir à surjouer l'équipement par rapport au besoin réel.

Donc le besoin se fera vraiment sentir en fonction de votre pratique du Cross
country&nbsp;: si vous voulez vraiment aller à fond en descendant les sentiers
vosgiens, _peut-être_ qu'un tout-suspendu vous conviendra. Et s'il est assez
léger pour une randonnée de 4 ou 5 heures, vous gagnerez sans doute en confort.
Par contre, cela représente un investissement conséquent. Un tout-suspendu pour
du Cross country, cela veut dire que vous allez rechercher la légèreté et comme
les suspensions ne sont jamais légères (sauf dans le très haut de gamme), vous
allez devoir vous intéresser de près aux cadres en carbone.

Mon avant-dernier VTT datait de 2017, c'était un tout-suspendu, cadre aluminium. 
Il était lourd, même si l'aluminium utilisé est allégé. Par contre, sa solidité a
fait que je n'en ai jamais eu à me plaindre. Ce que j'ai constaté
néanmoins&nbsp;: il n'est pas très utile par rapport aux chemins que j'emprunte
(et j'emprunte vraiment beaucoup de chemins différents). J'ai donc commandé un
VTT semi-rigide pour le remplacer. Ce dernier est arrivé en juin 2022. Ce retour au semi-rigide m'a parfaitemetn convaincu de ce que j'avance ci-dessus. C'est bien simple : à deux ou trois passages près, _toutes les traces_ je continue à faire toutes les traces que je faisais auparavant. J'ai aussi largement gagné en rapidité, pas seulement à cause du poid mais à cause du pilotage et de son optimisation. On ne s'ennuie vraiment pas en semi-rigide. 

Si vous choisissez un tout-suspendu pour pratiquer le Cross country, soyez aussi
conscient·e de plusieurs choses&nbsp;:

1. l'entretien se fait tous les ans ou tous les deux ans pour changer tous les
   roulements des articulations du VTT (renseignez-vous sur les prix ou apprenez
   à le faire vous-mêmes),
2. la suspension arrière nécessite une attention particulière et il est
   conseillé de faire réviser l'ensemble tous les ans (idem, renseignez-vous sur
   les prix),
3. il vaut mieux acheter un cadre carbone si vous voulez un tout-suspendu sans
   quoi, c'est un char d'assaut que vous allez devoir trimballer en montée. ce
   qui fait qu'en prenant un carbone de bonne qualité, votre VTT sera assez
   cher. Pour un premier achat, je le déconseille donc. Par contre à vous de
   voir si vous en avez le portefeuille et l'envie&nbsp;:)

Évidemment, question entretien (et nettoyage), un VTT semi-rigide… c'est les vacances.

Un dernier point à propos des VTT tout-suspendus&nbsp;:  la tige de selle
télescopique. Effectivement, c'est un outil qui peut s'avérer pratique car, en
descente très engagée, baisser la selle en un coup de pouce depuis le guidon
peut vous permettre de vous donner des airs d'Enduro. Mais n'oubliez pas, là
encore&nbsp;: cela alourdi le vélo et les selles sur tige télescopique on
toujours du jeu, source d'inconfort à la longue. Sur un VTT semi-rigide, si vous n'êtes pas confiant en haut de la pente et que vous débutez, cela peut s'avérer néanmoins un bon choix afin d'abaisser au maximum votre centre de gravité. 

<figure>
<img src="/images/billetVTT/exemplevtthardtrail.jpg">
<figcaption>Exemple de VTT semi-rigide - Mountain Bike, by Stephen Ransom Photography, CC BY-NC-SA 2.0.</figcaption>
</figure>




### VTT à assistance électrique&nbsp;?

Le débat ressemble à troll. Il y a quelques années, à l'apparition des VTT-AE, il y
eu une impression d'affrontement entre les pro et les anti-, soit entre ceux
pour qui faire du VTT est synonyme d'effort sportif et ceux pour qui le VTT-AE
reviendrait à faire du vélomoteur en forêt. 

Sauf que le débat ne se pose pas en ces termes (outre le prix d'un VAE de bonne
facture)&nbsp;:

1. Le VTT-AE peut être pratiqué sportivement&nbsp;: au lieu de partir pour une
   randonnée de 40&nbsp;km et 1200&nbsp;m de dénivelé, vous en faites le double
   et voilà tout. L'assistance vous permet de monter des «&nbsp;coups de
   cul&nbsp;» que de toute façon vous auriez fini par monter à pied ou alors
   très fatigué. On comprend surtout que le VTT-AE est plus que bienvenu en
   haute montagne. 
2. Mais pour beaucoup de personnes le VTT-AE revient effectivement à jouer les
   touristes en pensant que le pilotage est à la portée de n'importe qui. Or,
   avec un VTT-AE de 25&nbsp;kg, plus d'un amateur inexpérimenté fini par se
   viander dans un dévers.
3. En revanche, pour les personnes qui autrement ne feraient pas de VTT parce
   qu'elles ne sont pas sportives, ou encore à cause de la vieillesse ou du
   surpoids, et si elles restent prudentes sur des chemins sans difficulté, le
   VAE offre l'opportunité de rouler au grand air sans le stress des voitures
   sur la route. Et c'est plutôt une bonne chose.
4. Reste un problème tout de même, valable pour tout vélo électrique&nbsp;: les
   batteries et les filières de recyclage, la pollution que cela suppose, y
   compris dans les composants électroniques. Pour du vélo urbain (vélotaff) le
   compromis peut être atteint par l'effet de substitution à l'automobile (c'est
   un grand débat). Mais pour du loisir, c'est en raison de cet aspect du VTT AE
   que je conseillerais plutôt, lorsqu’on est en pleine possession de ses moyens
   physiques, de pédaler sur des VTT «&nbsp;musculaires&nbsp;». 

## L'équipement

En tant que débutant·e, vous risquez d'être dérouté par les contraintes
d'équipement qui s'imposent à vous. Que ce soit l'équipement du pratiquant ou
celui du VTT, il est important de faire un point.

### Pour le vététiste

Le·a vététiste doit toujours s'interroger sur ce qui le relie au vélo et son
rapport à l'environnement et le climat. Essayons de faire le tour.


#### Les vêtements

Ne soyez pas radin·e sur ce point. Revêtir un bon cycliste (long et chaud en
hiver, court en été) est votre première préoccupation&nbsp;: de bonne qualité il
vous évitera les effets désagréables des frottements après une sortie longue.
Une grande enseigne bleue fort connue en vend de très bons, il ne faut pas
hésiter à aller y faire un tour. C'est de même valable pour les vestes chaudes
d'hiver.

Cuissard ou short&nbsp;? En été, c'est une question que je qualifierais surtout
d'esthétique. Bien sûr dans les deux cas, vous vous aviserez d'avoir un chamois
en fond de culotte, c'est primordial. Ensuite, le confort d'un cuissard est
relatif à la quantité de tissu, là où le short pourrait être la cause de
frottements désagréables. En revanche, pour des sorties longues je conseille le
cuissard,  sans hésitation, en particulier pour ses qualités compressives. 

Votre garde-robe embarquée devra toujours prévoir un coupe-vent pluie&nbsp;: il vous
évitera de prendre froid dans les descentes et vous protégera des intempéries. 

Pour ce qui concerne les gants&nbsp;: les gants longs sont préconisés en VTT (ne pas
mettre de mitaines), avec coussinets sur la paume. Il en existe pour l'hiver et
pour l'été. Prenez du solide&nbsp;: il arrive souvent de poser les mains au sol, sur
de la pierre, sur des troncs d'arbre, dégager des ronces&hellip; c'est tout-terrain,
l'on vous dit&hellip;

#### Le casque

Pas de débat&nbsp;: il est **obligatoire**. Prenez du haut de gamme
systématiquement, avec visière, système de répartition d'onde de choc,
spécialisé VTT (les casques pour vélo de route ne conviennent pas). Essayez
votre casque en magasin et voyez s'il vous est confortable&nbsp;: on ne peut pas
tout acheter en ligne&hellip;


<figure>
<img src="/images/billetVTT/casquevtt.jpg">
<figcaption>Casque VTT - Mountain Bike Helmet, by Noah J Katz, CC BY-NC-SA 2.0.</figcaption>
</figure>




#### Autres protections corporelles

Lunettes&nbsp;: c'est nécessaire. Pour plusieurs raisons&nbsp;:

- protection de vos yeux (ronces, branches, projections diverses),
- teinte jaune (pas trop foncée à cause des sous-bois)&nbsp;: tous les opticiens
  vous diront que cette teinte permet d'accentuer les contrastes, donc mieux
  voir les reliefs,
- verres interchangeables&nbsp;: une paire jaune, une paire blanc (pour la
  pluie, et lorsqu'il n'y a pas assez de soleil)
- si vous avez besoin de correction de vue, allez voir votre opticien pour
  acheter une paire de lunettes pour le VTT (il y a deux ou trois grandes
  marques fort connues pour le sport&nbsp;: Demetz, Julbo, Bollé&hellip;
- les lunettes doivent être profilées&nbsp;: même si ce n'est pas à la
  mode&nbsp;:)


Protection armure&nbsp;: laissez cela aux descendeurs en Enduro/DH. Ell·eux en ont
vraiment besoin. Pour le cross country, il faut voyager léger. 

Pour votre dos, si vous avez un sac à dos avec poche à eau, vous n'en n'avez pas
vraiment besoin, mais certains sacs disposent une plaque de protection, si cela
peut vous rassurer.

En accidentologie VTT,  les études (comme
[celle-ci](https://dumas.ccsd.cnrs.fr/dumas-01502768/document)) portent le plus
souvent sur la pratique Enduro/DH, on comprend pourquoi&nbsp;: c'est celle qui
présente le plus de prise de risques. Là, ce sont les blessures sur le haut du
corps qui sont les plus courantes, particulièrement les épaules (fracture
clavicule, typiquement). Mais aussi&nbsp;: fractures poignets, coudes et
épaules&hellip; Le port du casque a largement amoindri la fréquence des dommages
à la tête, ce n'est même plus un sujet de débat.

On peut raisonnablement penser que la pratique du Cross country présente
beaucoup moins d'accidents graves, mais statistiquement, comme il s'agit de la
pratique la plus répandue, la fréquence des visites aux urgences est tout sauf
négligeable. Les fractures sur le haut du corps et membres supérieurs sont là
aussi les plus courantes. 

Pour simplifier, les accidents en Cross country sont moins dépendantes du
terrain qu'en Enduro. Cette pratique n'est pas moins dangereuse que le roller,
le vélo de route ou le skate-board. Par contre ces éléments font la
différence&nbsp;: l'erreur de pilotage, le matériel peu soigné et mal vérifié,
le degré de prise de risque. Notons aussi qu'en Cross country on trouvera plus
de blessures musculaires et tendineuses, inhérentes aux pratiques sportives
d'endurance lorsqu'on pousse le corps un peu trop loin. 


#### Sac a dos ou grosse banane&nbsp;?

Le choix se discute. 

Dans un sac à dos vous trimballez votre poche à eau, votre coupe vent,
éventuellement un surplus de vêtement, quelques outils, votre téléphone
portable, vos papiers, une ou deux barres céréale. 

Ce sac à dos doit bouger le moins possible. Regardez la marque Evoc, qui
présente d'excellentes performances (mais ce n'est pas la seule).

Mais on peut opter pour la grosse banane selon cet adage&nbsp;: c'est le vélo
qui doit porter, pas le dos. C'est un bon argument. Dès lors votre eau se
trouvera sur votre cadre dans deux gourdes, vous aurez un petit sac à outils
sous la selle, et tâchez de répartir le reste dans la banane (chez Evoc aussi,
il y en a des très bien).

#### Les outils et trousse d'urgence

Pour être autonome, en solo ou en groupe, il vous faut&nbsp;:

- chambre à air de rechange (sauf si vous êtes en tubeless — on en parle plus loin)
- une petite pompe
- une bombe «&nbsp;anti-crevaison&nbsp;»&nbsp;: oui, je sais&hellip; mais il
  m'est arrivé de crever deux fois de suite, alors bon&hellip; 
- deux démonte-pneu (attention avec ça si vous êtes en chambre à air&nbsp;:
  entraînez-vous à changer une chambre à air)
- kit de réparation tubeless avec mèches (si vous êtes en tubeless, donc),
- un multi-outils avec dérive-chaîne (utile si vous cassez la chaîne)
- deux colliers de serrage&nbsp;: pratique si vous devez attacher quelque chose
  (par exemple&nbsp;: vous cassez un rayon, vous l'attachez au rayon voisin pour
  les kilomètres qui vous restent)
- 50 cm de fil de fer (utile pour plein de choses)
- une trousse de premiers soins&nbsp;: une couverture de survie, pansements et
  compresses (dont au moins deux larges), antiseptique en fioles, bande
  adhésive, etc.


<figure>
<img src="/images/billetVTT/couplemultioutils.jpg">
<figcaption>Un couple de multi-outils très pratiques - Multitool and Hex Toolset, by dvanzuijlekom, CC BY-SA 2.0.</figcaption>
</figure>




#### La gestion de l'eau, la bouffe

L'hydratation, c'est obligatoire. Tout comme pour la course à pied, vous ne
pouvez pas faire de sortie de plus d'une heure sans emporter avec vous
suffisamment d'eau&hellip; et la boire&nbsp;!  Combien de litres par
heure&nbsp;? c'est variable&nbsp;: en fonction de votre poids, de la météo et de
l'effort, vous devrez boire plus ou moins. Par contre, une chose est valable
pour tout le monde&nbsp;: buvez avant d'avoir soif, anticipez la déshydratation.
Boire trop peu est synonyme de moins d'apport au système musculaire (notamment
de sodium) et par conséquent une baisse de performance, des risques de crampes,
en plus de risques de problèmes rénaux ou de vessie si vous perdrez trop d'eau.

Comment savoir si on s'hydrate assez&nbsp;? buvez tout le temps, beaucoup avant
la sortie, beaucoup après et beaucoup pendant. Mon repère à moi, c'est avoir
envie de pisser au moins une fois durant une sortie de trois heures, mais ce
n'est pas une règle absolue.

Cela dit, faut-il pour autant se surcharger en flotte&nbsp;? non. Surtout si
vous portez votre eau dans une poche dans le sac à dos. Il est inutile
d'emporter trois litres&nbsp;: si besoin, vous pouvez toujours remplir vos
contenants en chemin (prévoir une étape avec point d'eau). Emporter 1,5 litres
me semble un bon compromis en Cross country moyenne montagne et s'il fait très
chaud, monter à 2 litres, ce sera suffisant.

Poche style Camelback avec tuyau ou bidon sur le cadre&nbsp;? 

Personnellement, je n'ai jamais été un grand fan des porte-bidons&nbsp;: on a
tendance à se déséquilibrer pour aller chercher le bidon, le porter en bouche et
le remettre, alors qu'un tuyau bien ajusté sur la bretelle du sac à dos permet
un accès direct. Ceci sans compter le risque de perdre le bidon, justement, à
cause des obstacles du terrain. 

Le contre-argument, c'est qu'en utilisant un porte-bidon, c'est le vélo qui
porte, pas le cycliste. C'est-à-dire autant de poids en moins sur le dos.

Concernant les apports nutritionnels, là non plus je n'ai pas vraiment de
conseil à donner. Mon sentiment est que si vous mangez correctement et équilibré
(même avec quelques excès de temps en temps), votre besoin nutritionnel durant
l'effort d'une longue sortie ne devrait pas être conséquent. Il faut toujours
emporter avec soi une ou deux barres céréales ou de la pâte de fruit, ou bien
quelques fruits secs, selon ses goûts. 

Rien ne vous oblige à les manger. En tout cas, ne vous précipitez pas sur les
produits miracles vendus en pharmacie ou dans les magasins de sport, et encore
moins sur les barres céréales des rayons confiserie du supermarché&nbsp;: ces
produits contiennent généralement trop de sodium, des huiles saturées, trop de
sucres transformés, et pour certains des nutriments parfaitement inutiles si
vous avez déjà une alimentation saine au quotidien.

Si vous aimez les barres céréales, achetez des barres dont la composition est la
plus simple possible ou bien faites-les vous-mêmes avec des produits sains que
vous aurez sélectionné au préalable. Les recettes ne manquent pas sur les
z'internetz. Après quelques essais, vous obtiendrez d'excellents résultats.



#### Les pédales

Devez-vous choisir les pédales automatiques, semi-automatiques, les pédales
plates&nbsp;?

Quelque soit votre achat, votre VTT sera livré avec une paire de pédales bas de
gamme&nbsp;: tout le monde change les pédales de toute façon. Mais vous pouvez
rouler avec dans un premier temps. C'est pourquoi cette section concernant les
pédales est située dans l'équipement du vététiste&nbsp;: prolongements des
pieds, elles conditionnent le contact avec le vélo, tout comme la selle.

Pour le débutant, je préconise les pédales plates (avec les picots). Attention
aux tibias avec les retours de pédales involontaires. Comptez une cinquantaine
d'euros pour une paire solide de marque connue. Protèges-tibias&nbsp;? c'est
lourd et inconfortable, donc non&nbsp;: faites attention, c'est tout.

Pédales automatiques&nbsp;: il faut un peu d'entraînement sans quoi votre
appréhension causera votre chute. Cependant, si vous voulez faire du Cross
country et gagner en rendement tout en maîtrisant votre vélo, c'est le modèle de
pédales à utiliser.

Sur le marché il existe des pédales dites semi-automatiques. Elles se présentent
soit sous la forme d'une pédale recto attache automatique et verso pédale plate,
soit avec un recto aimant recto et verso pédale plate. Ces modèles permettent
de laisser les pieds libres dans les descentes ou les passages difficiles.
En théorie, du moins. Par contre j'ai été séduit par les pédales
automatiques à aimant (de la marque Magped) : elles ont moins de pouvoir de traction que les
pédales automatiques classiques (si on tire trop, le pied se décroche),
mais c'est ce qui fait leur intérêt : permettre un décrochage
très rapide et donc plus sécurisant. J'aurais du mal à m'en passer aujourd'hui.

En matière de pédale, il n'y a pas d'obligation. Là encore, c'est votre pratique
qui en décidera. J'ai un faible pour les pédales plates, même si j'ai moins de
rendement. En revanche, en compétition Cross country, les pédales automatiques
sont la règle.


<figure>
<img src="/images/billetVTT/pedaleplateetauto.jpg">
<figcaption>Pédale plate et pédale automatique - bike pedal, by postbear and by torisan3500, CC BY-NC-SA 2.0.</figcaption>
</figure>



#### Les chaussures

Elles dépendent de vos pédales&nbsp;! Mais sachez aussi choisir une paire qui
vous permette de marcher. Pour le Cross country, les chaussures typiques sont
assez rigides, proches de celles utilisée en vélo de route, mais pour plus de
confort, choisissez des chaussures plus appropriées à la randonnée VTT, un peu
moins rigides et plus polyvalentes.

Gardez à l'esprit qu'une paire de chaussures trop étroites ou trop justes&nbsp;:

- empêchent la circulation sanguine dans vos pieds&nbsp;: on a tendance à se
  crisper un peu en descente et à l'appui,
- empêchent de mettre des chaussettes chaudes en hiver.

Un avantage des pédales semi-automatique à aimant : à la place d'un mécanisme de fixation sous la chaussure, il suffit de visser une petite plaque en métal, très facile à nettoyer (quand on plonge toute la chaussure dans la boue !).

#### La selle

La selle est le contact quasi permanent entre votre postérieur et le VTT. À ce
titre, elle ne doit pas être négligée. Si, dans un premier temps, la selle
fournie avec votre vélo saura vous contenter, c'est à la longue que vous
constaterez que, même si vous avez correctement réglé sa hauteur, quelques
désagréments peuvent subsister, notamment les irritations.

En gros, il existe des selles arrondies et des selles plus plates. Les arrondies
limiteront les irritation en cas de sorties longues distance. Certaines selles
disposent aussi d'un canal central plus ou moins prononcé, voire parfois
complètement évidé&nbsp;: c'est pour limiter la pression sur le périnée.

Quant à la largeur de la selle, elle doit être relative à votre morphologie. Que
vous soyez homme ou femme, large ou étroit, c'est à vous de voir.

Mon conseil&nbsp;: préférez les selles arrondies et veillez à ce qu'elles ne
soient pas trop larges, sans quoi une gêne au pédalage va s'installer. Il n'y a pas vraiment de recette. Vous pouvez avoir une selle qui ne vous pose aucun problème pour une heure de pratique, tandis qu'elle vous fera souffrir au bout de trois heures. Tout ne réside pas dans la qualité de votre cuissard.


### Pour le VTT

#### La transmission

Le groupe de transmission regroupe tous les éléments qui transmettent l'énergie
musculaire (ou électrique) au vélo. Un groupe de transmission est composé de
[dérailleurs](https://fr.wikipedia.org/wiki/D%C3%A9railleur),
[cassette](https://fr.wikipedia.org/wiki/Cassette_de_pignons), pédalier
(boîtier, plateaux, manivelles, pédales), chaîne. On peut compter aussi les
manettes (ou commandes) de vitesses et les câbles. 

Pour choisir un groupe de transmission plutôt qu'un autre, il faut avoir de
l'expérience. En effet, selon votre niveau, un groupe 1x11 (mono-plateau et 11
pignons) demandera plus ou moins d'efforts pour entraîner la machine par rapport
à un 1x12. 

Aujourd'hui le mono-plateau a le vent en poupe. Et il présente un double
avantage&nbsp;: moins d'entretien et plus de légèreté. Là-dessus vous pourrez à
la longue opter pour des cassettes différentes.



<figure>
<img src="/images/billetVTT/SHIMANO-Deore-XT-M8000.jpg">
<figcaption>Image promotionnelle Shimano Deore XT - 1x11.</figcaption>
</figure>



Et non, ce n'est pas parce que vous avez un mono-plateau que vous avez moins de
vitesses&nbsp;: en double plateau vous avez la plupart du temps une cassette 10
pignons arrangés de 11 à 36 dents (on dit 10 x 11-36) mais en mono-plateau vous
avez par exemple une cassette à 11 x 10-42 ou une cassette à 12 x 10-51. En
clair&nbsp;: à l'arrière, le dernier pignon de votre cassette est juste énorme.
Donc la plage de «&nbsp;choix&nbsp;» de vitesse n'est guère moins grande mais
l'avantage est que vous n'avez plus à veiller à ne pas croiser la chaîne (il ne
faut jamais mettre un couple grand plateau — petit pignon si on a plusieurs
plateaux). 

Pour un premier achat, je préconise pas moins que le couple mono-plateau et 12
vitesses. C'est ce qui est vendu la plupart du temps sur les entrées de (bonnes) gammes.

En termes de marques, vous avez grosso-modo l'essentiel du choix entre Shimano
et Sram qui se partagent l'essentiel du marché VTT. Là encore, si vous débutez,
choisir entre les deux ne consiste pas à trouver le meilleur point de
performance, mais comprendre ce qui distingue les groupes entre eux, chez
Shimano comme chez Sram. Pour faire simple, dans les deux marques, il y a une
hiérarchie. À une ou deux exceptions près les marques sont incompatibles entre
elles. Et entre les groupes au sein d'une même marque, il y a des tableaux de
_correspondances_ et c'est normal&nbsp;: vous ne pouvez pas tout mélanger,
plateaux, pignons chaîne et manettes de vitesses.

Alors comment vous y retrouver&nbsp;?  Lorsqu'on achète un VTT, celui-ci est
fourni avec un groupe de transmission donné. Pour juger du prix de votre vélo,
au sein d'une même gamme, il faut aussi se demander avec quel niveau
d'équipement il est fourni. Notez aussi que plus vous montez en gamme, plus les
changements de pièces seront chers, pensez-y avant d'opter systématiquement pour
le plus cher.

Le tableau suivant sera peut-être démodé au moment où vous le lirez, cependant,
je pense que pour l'essentiel on s'y retrouve. Notez que ne mets pas les
shifteur électroniques ni les groupes spécialisés descente. Je me concentre sur
le Cross country.


|    Niveau    |    Shimano     |       Sram      |
|--------------|----------------|-----------------|
| 1            | XTR            | XX1             |
| 2            | XT (Deore XT)  | X01, X1         |
| 3            | SLX            | GX, X9          |
| 4            | Deore          | X7, NX          |
| 5            | Alivio         | X5              |
| 6            | Acera          | X4              |
| 7            | Altus          | X3              |
| 8            | Tourney        |                 |



#### Les freins

En matière de freinage, le choix ne doit pas être pris à la légère.  Évidemment,
les systèmes de freins dédiés à l'Enduro seront extrêmement puissants, la notion
de dosage passant plutôt au second plan (même si elle est très utile), tandis
que pour le Cross country c'est la fiabilité sur la durée du freinage qui sera
recherchée. Mais la pratique n'est pas l’essentiel&nbsp;: votre poids et le
terrain sont des données tout aussi essentielles et c'est fonction de cela que
vous allez juger si vos freins vous conviennent ou pas.

Lorsque vous achèterez votre nouveau VTT, si c'est le premier, il va falloir
faire confiance au vendeur. Un système de freinage va du levier aux plaquettes
en passant par l'étrier, les durites et les disques. Tous ces composants ont
leur importance dans l'efficacité et le ressenti. 

Par exemple, s'il y a de fortes chances que votre premier VTT soit équipé de
plaquettes de frein organiques, sachez qu'il en existe plusieurs variétés sur le
marché, avec des caractéristiques différentes, de celles qui glacent dès le
premier usage à celles qui entament méchamment votre disque. 

Si vous vous reportez au tableau des groupes de transmission ci-dessus, les noms
des groupes de frein sont les mêmes et obéissent plus ou moins à la même
hiérarchie.  Cela dit, en fonction des années et des séries, on peut trouver par
exemple chez Shimano des freins Deore qui valent très largement la gamme
supérieure. Encore une fois c'est fonction de l'expérience que vous pourrez vous
faire une idée précise. Bien sûr, le bas de gamme&hellip; reste le bas de gamme.

Attention&nbsp;: si vous vous renseignez sur des sites internet spécialisés
(c'est une bonne chose) veillez à noter la date de rédaction des comparatifs.
Shimano et Sram sortent très régulièrement des nouvelles séries et vous risquez
de vous y perdre. Mon conseil serait de voir d'abord, sur le VTT que vous voulez
acheter quels types de freins sont proposés, puis vous renseigner sur leurs
caractéristiques par la suite. Enfin, n'oubliez pas&nbsp;: des freins, cela se
change si vraiment ils ne vous conviennent pas.


#### Les pneus

Alors là&hellip; pas moyen de s'entendre sur une loi précise dans le choix des
pneus, que vous soyez en chambre à air ou en tubeless&nbsp;: c'est le terrain et
la météo qui font la différence. Mes conseils sont les suivants&nbsp;:

- analysez les terrains que vous avez l'habitude de fréquenter (si vous êtes
  dans les Hautes Vosges avec du granit mouillé, votre choix en sera pas le même
  que dans les Vosges du Nord avec du gré sableux, et bonne chance à ceux qui
  sont entre les deux)
- ne soyez pas chiche (même si le prix d'un pneu n'est pas un gage)&nbsp;: de
  bons pneus (et un gonflage correct) conditionnent  absolument votre tenue de
  route. C'est comme en voiture.
- il n'y a aucune obligation à avoir les mêmes pneus à l'avant et à
  l'arrière&nbsp;: contacts, grip, adhérence, dureté de la gomme, tout cela
  joue.

Lors de votre premier achat, le VTT sera livré avec un jeu de pneus semblables
avant et arrière. Usez-les&hellip; mais analysez votre besoin. Ensuite
rendez-vous sur les sites de vente en ligne et regardez les descriptifs, ou
allez chez votre vendeur et posez-lui la question. 

Personnellement, je suis resté depuis les 6 dernières années avec la même
référence de pneus qu'à l'achat de mon VTT, des Maxxis Forekaster. Le fabriquant
écrit&nbsp;: «&nbsp;adapté à une pratique Cross country agressive et pour le All
Mountain&nbsp;»&hellip; comme cela ne veut pas dire grand chose, je prétends
simplement que ce type de pneus typé Cross country est plutôt adapté aux
terrains vosgiens côté Alsace. Je vois souvent cette marque d'ailleurs chez les
autres vététistes. Si vous êtes plutôt côté granit, ce ne sera pas forcément
votre premier choix car je trouve qu'ils peinent un peu en traction sur du
pierreux humide. C'est du ressenti personnel, pas une loi universelle.

Par contre, le moment venu de changer vos premiers pneus, il y a tout de même une bonne recette : leur largeur. Si vous faites du Cross Country, de quoi avez-vous besoin ? un pneu avant qui soit bien stable parce que c'est sur ce pneu avant que vous donnez la direction  et un pneu arrière qui ne vous freine pas parce que c'est sur ce pneu arrière que vous envoyez la propulsion. La conclusion est simple : les deux pneus n'ont pas le même usage. Pourquoi auriez-vous les deux même pneus à l'avant et à l'arrière ?

On s'accorde à dire qu'il faut un pneu de bonne largeur à l'avant et un pneu plus étroit à l'arrière. Cela dit, il ne s'agit pas d'exagérer. Pour vous donner une idée, ma largeur de pneu arrière est de 2.25 tandis que j'ai du 2.40 à l'avant. Ferez-vous pour autant la différence si vous débutez ? il y a peu de chance. Alors vous vous poserez la question lorsque vous devrez changer vos pneus. 

##### Tubeless ou chambre à air ?

Tubeless, sans hésitation. Cependant… Je m'adresse ici aux débutants. Alors commencez par la chambre à air. D'ailleurs, il y a des chances que votre VTT vous soit vendu avec un jeu de pneus + chambre à air même si les pneus et la jantes sont _tubeless ready_. 

Pourquoi ? parce qu'il vous sera plus facile de gérer des chambres à air. Entraînez-vous chez vous à changer une chambre à air rapidement. Parce que si vous crevez en forêt quand il pleut, vous serez content de ne pas y passer trois heures. Rappelez-vous : autant pour amorcer le « déjantage » du pneu vous pouvez utiliser éventuellement un démonte-pneu (et encore, ça peut abîmer la jante), autant pour le remonter avec une chambre à air, bannissez absolument le démonte-pneu. D'ailleurs, cela s'appelle un démonte-pneu, pas un remonte-pneu, hein ? Il y a plusieurs vidéos sur Internet qui vous montreront comment procéder, c'est assez simple. 

Le Tubeless, présente des avantages par rapport à la chambre à air. Déjà, c'est plus léger parce que vous économisez le poids de deux chambres à air. Ensuite, parce que  vous n'aurez plus de crevaisons lentes (genre le pneu qui se dégonfle dans le garage et que vous vous en apercevez la veille de votre sortie), et aussi parce qu'en cas de crevaison, le liquide préventif à l'intérieur devrait faire son travail de « rebouche-trou ». Par contre :

- en cas de grosse crevaison (un gros clou, par exemple), il vous faudra placer une mèche, c'est une autre technique à apprendre, même si ce n'est pas très compliqué non plus ;
- il faut vérifier et remplacer régulièrement le liquide préventif à l'intérieur ;
- pour changer un pneu Tubeless, il vous faudra un compresseur capable d'injecter une grosse quantité d'air d'un coup dans le pneu afin de le faire claquer ;
- la valve n'est pas la même et votre fond de jante devra être parfaitement étanche.

Donc le Tubeless, ce n'est pas moins d'entretien que la chambre à air, mais c'est un peu plus de zénitude  lors de vos balades.

Mon conseil aux débutants : commencez avec des chambres à air, vous verrez plus tard pour passer au Tubeless, à l'occasion de votre premier changement de pneu par exemple.

#### Les suspensions

À quoi servent les suspensions, à l'avant comme à l'arrière&nbsp;? Non, elles ne
sont pas faites pour sauver votre popotin des dégâts d'une réception un peu
rude. Leur premier rôle consiste à optimiser le contact de votre roue avec le
sol, donc favoriser le contrôle de votre VTT. Pour ce qui concerne les fourches
(avant), le second rôle consiste à absorber les chocs pour vos bras (et aussi
votre dos) et faciliter le pilotage. 

Le réglage des suspensions, notamment en fonction de votre poids, est très
important. Il s'agit de régler correctement la détente (le retour de la
suspension à sa position initiale). Trop molle vous n'absorbez rien et vous
risquez de l'abîmer, trop raide, autant rouler en tout rigide. Par ailleurs, si
vous enfoncez trop, vous perdez en rendement de pédalage. 

C'est pourquoi la plupart des VTT disposent d'un système de blocage de
suspension, à l'avant comme à l'arrière. Je vous conseille vivement de choisir
un système de blocage situé sur le guidon, beaucoup plus pratique, surtout si
vous oubliez de débloquer votre suspension après avoir entamé votre descente.

Pour la pratique du Cross country, comme je l'ai affirmé plus haut, choisir un
semi-rigide est sans doute le meilleur choix pour un premier VTT (et cela reste
pertinent même si ce n'est pas votre premier VTT). Les suspensions pour cette
pratique sont généralement faites pour un débattement beaucoup plus faible que
pour l'Enduro. Généralement on parle de 90 à 120 mm de débattement. Avoir plus
de débattement ne me semble pas très utile (en Enduro, on peut monter à 200 mm de
débattement).

De même, on recherchera la légèreté. Donc il vaut mieux rechercher une fourche
avec un système à air (ressort pneumatique) bien que certaines fourches avec
ressort hélicoïdal peuvent s'avérer tout à fait pertinentes, bien que plus
lourdes (on ne parle pas non plus d'une différence de 20 kg, hein?).

Suspension VTT = amortisseur (hydraulique) + ressort (hélicoïdal ou à air).

Donc si nous résumons&nbsp;:

- Ressort pneumatique (air)&nbsp;: légèreté, ajustement progressif au poids du
  pilote, moins sensible aux petits chocs,
- ressort hélicoïdal&nbsp;: il faut changer le ressort s'il est trop raide, plus
  lourd, sensation de raideur sur petits chocs.

Dans les deux cas, ces fourches font très bien leur travail à moins de choisir
du trop bas de gamme, évidemment. Et n'oubliez pas&nbsp;: tout comme le reste,
une fourche peut se changer, et il en existe des centaines de modèles&hellip; à
tous les prix. Débuter avec une marque&nbsp;? allez, je vous conseille Rockshox
et Fox, vous trouverez dans ces marques des gammes assez étendues pour ajuster
votre besoin à votre portefeuille (j'aime beaucoup les Fox racing 32, très
classique, mais comme elle n'est pas trop chère, la changer pose moins de
problème).

Ha oui, pour les suspensions arrières si vous tenez au tout-suspendu, c'est le
même combat. 

#### La petite sonnette 

Hé oui, on l'oublie parfois, mais elle est très utile surtout lorsqu'on
fréquente des sentiers de randonnée où il est difficile de contourner les
marcheurs. Dans tous les cas&nbsp;: patientez le temps que les marcheurs
réalisent votre présence, soyez très courtois et remerciez.

Il peut cependant être utile de signaler votre présence par un petit coup de
sonnette. Le choix du son est important&nbsp;: trop bruyant, il sera perçu comme
une agression (nous ne sommes pas sur une piste cyclable en centre urbain). Il
existe des petites sonnettes légères en forme d'anneau à placer à peu près
n'importe où sur le guidon pour plus de facilité.
Elles produisent un tintement cristallin et assez aigu pour passer au-dessus du
vent et de la voix, sans être désagréable.

#### Garde-boue, protections de cadre

Est-ce absolument nécessaire&nbsp;? non. La première chose à éviter, dans le
choix d'un garde-boue avant ou arrière, c'est essayer de composer entre
l'encombrement et l'intérêt réel de cet équipement. Vous avez un VTT pour faire
du sport, et oui c'est une activité salissante. Ne cherchez pas à éviter la
boue, vous n'y arriverez pas.

Néanmoins, on peut essayer de limiter les projections dans le visage (et il peut
s'agir parfois de cocottes de pins et de petites pierres) ainsi que dans le bas
du dos. Pour cela, je préconise les garde-boue souples à fixer avec des colliers
de serrage, sous la selle pour l'arrière et sur la fourche pour l'avant. ils
n'alourdiront pas le VTT et limiteront les projections autant que faire se peut.
C'est surtout le garde boue avant qui est important&nbsp;: si vous roulez assez
vite sur un terrain très humide, vous pourrez comprendre plus facilement.

Enfin, ce type de garde-boue ne coûte pas cher, on le remplace facilement, on
peut aussi le découper si les dimensions ne conviennent pas. 

Allez, je vous donne un truc : achetez votre premier garde-boue et avant de le monter dessinez les contours sur une feuille de papier que vous conservez. Vous avez des vieux set de tables en plastique ? S'il ne sont pas trop moches ou kitch, ils peuvent faire l'affaire, il suffira de les découper. 

Une autre protection concerne le cadre, si on veut le protéger des projections
de pierres, notamment sur le bas du tube diagonal. Il existe des façades à fixer
sur le tube, mais c'est une protection encombrante plutôt destinée aux pratiques
de descentes, là où le risque de taper fortement le cadre est évident. Pour les
petites projections qui pourtant à la longue abîment la peinture, il existe des
films transparents qui jouent parfaitement ce rôle.

De la même manière, si votre VTT ne dispose pas déjà d'une protection, il peut
être utile d'en mettre sur la base arrière côté transmission, là où la chaîne
est parfois amenée à taper la peinture.

Toutefois certains VTT disposent maintenant de protection déjà en place, là où
parfois, à la place de la peinture, un revêtement rugueux est posé dès l'usine.
Cela n'empêche pas de rajouter un peu de film protecteur à certains endroits
stratégiques si besoin, par exemple sur les côtés de la fourche avant.

Je préconise d'éviter les adhésifs, à cause de la colle. Cette dernière peut ne
pas tenir et faire des traces inesthétiques. À la place (mais assez cher) il
existe des films transparents en polyuréthane. La pose est extrêmement
facile&nbsp;:

- bien nettoyer et dégraisser le cadre avec de l'eau savonneuse,
- découper soigneusement la bande aux dimensions voulues et appliquer avec un
  peu d'eau tout en prenant soin de ne pas faire de bulles.

Ce type de film ne garanti pas les gros chocs&nbsp;: il s'agit de protéger la
peinture en ajoutant une sur-couche solide très discrète. Ce matériau résiste
très bien à l'humidité. La marque Clearprotect est connue.


<figure>
<img src="/images/billetVTT/gardeboueavant.jpg">
<figcaption>Garde-boue souple sur fourche - image by vikapproved, CC BY-NC 2.0.</figcaption>
</figure>





## Commencez à pédaler

Je pourrais ici commencer une longue section sur l'art et la manière de débuter
le VTT en pratique. Pour simplifier, je dirais&nbsp;: pédalez d'abord. Si vous
avez pris la décision d'acheter un VTT, il y a de fortes chances que vous ayez
essayé au moins une fois que vous avez été séduit·e. 

Néanmoins voici quelques conseils pour ne pas se mettre en difficulté dès le
début et pouvoir progresser.

### Réglez votre posture

Beaucoup de vidéos proposent des tutoriels pour régler correctement votre
position sur le VTT. [Celle-ci](https://www.youtube.com/watch?v=UIlZPhrbWro)
donne d'assez bons conseils, simples à mettre en œuvre. 

1. La hauteur de selle peut se régler facilement&nbsp;: une fois en selle,
   placez la plante du pied sur la pédale position basse, votre jambe doit être
   légèrement pliée. Trop pliée, votre selle est trop basse et vous allez vous
   faire mal aux genoux, trop haute, bobo le poum. Si vous y tenez, la solution
   mathématique est la suivante&nbsp;: soit EJ la hauteur de votre entrejambe du
   talon au périnée (pensez à vos chaussures&nbsp;!), et soit HS la hauteur de
   selle (du milieu de l'axe du pédalier jusqu'au creux de la selle)&nbsp;: HS =
   EJ x 0,885 (ou 0,875, cela varie).
2. À moins d'avoir une tige de selle télescopique (dont l'utilité se
   discute en Cross country), si vous ne vous sentez pas à l'aise en cas de
   descente un peu raide, sachez vous arrêter pour abaisser votre selle et la
   re-régler ensuite. Si votre tige de selle n'a pas de traits de graduation,
   faites une marque au feutre (quitte à la refaire régulièrement), mais ne
   l'abîmez pas.
3. L'avancée de la selle&nbsp;: cela se règle au centimètre près et sur les
   longues distances, on sent bien la différence. Votre VTT a une certaine
   géométrie, mais l'avancée horizontale de la selle va favoriser ou défavoriser
   votre puissance de pédalage. Très peu, bien sûr, mais l'amplitude, une fois
   changée, va aussi conditionner votre confort. Donc, il faut essayer&hellip;
   Une méthode consiste à utiliser un fil à plomb&nbsp;: votre pied en position
   de pédalage (cf. ci-dessous), placez le fil à plomb sur pointe de votre
   genou, le fil doit passer par l'axe central de la pédale.
4. Placer son pied sur la pédale&nbsp;: l'avant du pied est sur la pédale, et
   non pas la plante du pied. Plus exactement la bosse à la base du gros orteil
   (l'articulation métatarso-phalangienne) doit se situer au niveau de l'axe de
   la pédale (ceci est valable pour tous les types de vélo, pédales automatiques
   ou pas).
4. D'autres éléments peuvent se régler et dépendent beaucoup de votre
   morphologie&nbsp;: la longueur de la potence, la largeur du cintre. Mais dans
   un premier temps, contentez-vous du matériel tel quel&nbsp;: vous en jugerez
   à l'usage.
5. Votre posture ne se détermine pas seulement matériellement&nbsp;: c'est aussi
   à vous d'adopter une bonne posture&nbsp;: épaules relâchées, ne vous crispez
   pas en serrant trop fort votre guidon (même en haut d'une descente), en forte
   montée sachez descendre vos coudes (ne les écartez pas), si vous n'avez pas
   de pédales automatiques (à régler correctement elles aussi&nbsp;!) sachez
   conserver vos pieds dans l'axe du vélo (et n'écartez pas les genoux), votre
   pédalage doit laisser l'axe de vos genoux parfaitement droit et parallèle au
   vélo. Bref, au début vous devrez penser très activement à votre posture.
6. Vos poignées doivent toujours avoir un grip correct (surveillez l'état de vos
   poignées&nbsp;: trop sales, le grip est moins bon). Surtout en descente, vous
   saisissez vos poignées en laissant deux doigts (index et majeur) sur les
   leviers de freins, de chaque côté. Donc pensez aussi à relever ou abaisser
   ces leviers en fonction de votre morphologie. Le pouce doit servir à passer
   les vitesses sur les shifteurs. Ne roulez pas avec des gants de ski, même en
   hiver&hellip; 

### Les parcours et la performance

S'il peut sembler plus commode de toujours sortir dans un coin de montagne que
l'on connaît bien, essayez de varier le plus possible. Vous ne progresserez pas
si vous arpentez toujours les mêmes chemins, c'est-à-dire ceux qui vous semblent
faciles aujourd'hui. Pour progresser, la nature de vos parcours doit être
pensée. 

Surtout en Cross country il n'est pas utile de toujours faire des sorties très
longues. Certes, on en revient fort satisfait, et on en prend plein la vue, mais
même si vous essayez de les faire toujours un peu plus vite, vous vous
retrouverez assez vite au taquet. C'est comme la course à pied&nbsp;: parfois il
faut savoir passer par des phases plus spécialisées, chronomètre en main, faire
du fractionné, etc. En VTT, le fractionné existe aussi, de même la technicité du
pilotage (en descente comme en montée), l'endurance sur des parcours pénibles
avec des montées bien longues et régulières pour travailler le
«&nbsp;foncier&nbsp;», etc.

Mon conseil&nbsp;: si vous débutez, avec deux sorties par semaine, essayez
d'alterner une longue, une courte. Pour la sortie longue, faites-vous
plaisir&nbsp;: points de vue, vieux châteaux, endroits insolites, faites du
tourisme et recherchez le fun. Pour les sorties courtes, trouvez-vous deux ou
trois terrains de jeu, même si vous passez deux fois au même endroit, et
lancez-vous des défis. L'idéal est de trouver un parcours où vous aurez deux ou
trois types de difficultés à affronter&nbsp;: à vous de voir sur quels points
vous devez progresser. 

Qu'est-ce qu'une sortie longue, qu'est-ce qu'une sortie courte&nbsp;? il s'agit
surtout du temps passé à VTT. On mesure une sortie à la fois en kilomètres et en
dénivelé (d'où l'intérêt d'utiliser un GPS, ou au moins un appli sur
smartphone). Faire 23 km avec 1000 m de dénivelé cumulé (d+), c'est déjà de la
bonne grosse sortie pour un débutant. Cela me semble même déjà trop. Dans les
Vosges il faut partir d'en bas côté Alsace, mais c'est assez faisable tout de
même. 

Donc n'ayez pas les yeux plus gros que le ventre. Essayez au début de faire des
sorties de 20 à 25&nbsp;km avec un max de 500 à  600&nbsp;m&nbsp;d+. Ensuite sur
le même dénivelé, rallongez le kilométrage, puis ensuite augmentez le dénivelé.
Ce n'est pas forcément utile de le rappeler, mais cela va mieux en le
disant&nbsp;: allez-y progressivement. 

Avec les copain·es&nbsp;: les sorties à plusieurs, c'est super. Être seul dans
la forêt, cela a un côté peu rassurant, surtout dans les descentes un peu
techniques. Par contre, si vous roulez avec des personnes d'un niveau plus
avancé, c'est bien pour progresser à la seule condition de ne pas le faire tout
le temps et de rappeler à vos collègues de savoir respecter votre rythme&nbsp;:
en effet, si vous êtes toujours en sur-régime (au seuil anaérobie), avec le cœur
en dehors de sa boîte à chaque montée, ou si la sortie est trop longue pour
vous, d'une part vous allez finir la sortie avec des jambes en coton et au bout
de votre vie en jurant que jamais plus&hellip;, et d'autre part, le plus
important, vous risquez de vous blesser (tendinite ou autre blessure musculaire)
voire de solliciter trop votre système cardiaque et subir un malaise voire plus
grave encore. 

Donc être seul a aussi des avantages, mais restez prudent.

Enfin, trop de fatigue nuit au pilotage et vous savez quoi&nbsp;? quand on roule
en montagne, le parcours se termine le plus souvent par une descente. Le faire
en état de trop grande fatigue réduit votre attention et vos réflexes, et
augmente drastiquement le risque de chute.

Inutile d'ajouter que la fatigue gagne aussi si on ne dort pas assez. Alors,
profitez de vos nuits, et cessez de vous coucher tard&nbsp;: un bon entraînement
sportif s'accomplit aussi par le repos. Concernant les repas&nbsp;: mangez
équilibré, ne vous empiffrez pas de pâtes (c'est une légende&nbsp;: les sucres
lents se trouvent aussi dans d'autres aliments), respectez aussi votre rythme
digestif. Plutôt que de chercher des régimes plus ou moins fiables, faites
surtout preuve de bon sens.

Et, pour palier les éventuels risques cardiaques, si vous avez passé vos 40
bougies, je conseille vivement de faire un bilan cardiaque avec test d'effort
(demandez-en un à votre médecin traitant), il permettra de déceler un éventuel
souci et prendre des mesures adéquates. 

Ha oui, dernier point&nbsp;: ne recherchez pas la performance. Laissez-cela aux
sportifs de profession. D'abord, faites-vous plaisir. Votre performance, ce sera
celle que vous constaterez à votre mesure&nbsp;: vous n'avez de compte à rendre
à personne et encore une fois, il n'y a aucune honte à descendre du vélo lorsque
la montée est trop dure ou si vous ne sentez pas ce petit passage de descente. 



### Les randos VTT organisées

Elsass Bike, Trace vosgienne, randonnées diverses organisées par des comités
cyclistes locaux&nbsp;: tous ces événements sont des occasions de découvrir des
circuits dans une ambiance sympathique et détendue. Généralement, ces randonnées
sont organisées en plusieurs parcours de niveau, proposent des points de
ravitaillement et se terminent avec une bonne bière au bar de l'amicale. 

N'hésitez pas à vous inscrire à de tels évènements, il y en a sans doute près de
chez vous. Vous y rencontrez d'autres cyclistes et vous pouvez y aller en
famille. Il ne s'agit pas de courses proprement dites, pas besoin de faire
partie d'un club ou d'être inscrit à la FFC. Les circuits sont balisés, les
départs sont échelonnés (il y a quand même une heure limite et comme c'est en
été, il est mieux de partir tôt) et vous faites le parcours à votre rythme. Gros
avantage&nbsp;: vous n'avez qu'à suivre le balisage.

Pour des évènements comme la Trace Vosgienne VTT, par exemple, c'est un mixte.
Il y a une partie course (un départ collectif et des gagnant·es) et une partie
randonnée sans chrono. À vous de choisir&nbsp;: vous pouvez très bien faire le
marathon tout en vous fichant du chrono, mais il va falloir tout de même dépoter
un peu. 

Vous pouvez vous fixer ces rendez-vous annuels comme des objectifs. Choisissez
la rando qui vous paraît difficile (mais pas impossible) et préparez-vous pour
la réaliser sans temps chrono, mais dans de bonnes conditions. Vous pourrez-vous
dire que l'année prochaine, vous ferez la rando du niveau supérieur.

Une autre opportunité consiste à rejoindre une association de vététistes, ou un
club cycliste qui possède une section VTT. Même certaines antennes du Club
Vosgien possèdent une section VTT. Là aussi renseignez-vous près de chez vous.
C'est la garantie de sortir à plusieurs et émuler une dynamique de motivation
qu'on n'a pas forcément en solo. Et puis, à fréquenter des plus expérimentés que
soit, on apprend toujours des choses&hellip;


## L'entretien général

Si vous voulez faire durer votre VTT, son entretien après chaque sortie est une
étape obligatoire. L'entretien concerne le nettoyage, le changement de certaines
pièces et la révision générale.

### Ne pas faire

Pour nettoyer votre VTT après une sortie boueuse, n'utilisez jamais --
entendez-vous&nbsp;? -- jamais&nbsp;! de nettoyeur haute pression. Si vraiment
vous tenez à projeter de l'eau avec un tuyau, utilisez votre tuyau d'arrosage de jardin
avec jet diffus. 

Autre chose à ne pas faire&nbsp;: huiler la transmission sans l'avoir nettoyée
auparavant. C'est le meilleur moyen pour tout encrasser et user prématurément
les toutes les pièces.

Et n'utilisez pas non plus de combinés lubrifiant / nettoyant tout-en-un pour
votre transmission. Cette dernière se soigne, se bichonne&nbsp;: elle ne mérite
pas de traitement à la va-vite (cf. plus loin).

Et vous aurez compris&nbsp;: l'entretien d'un VTT, ce n'est pas seulement le
garder à peu près propre, c'est vérifier les pièces et leur usure, prévenir la
casse, et donc rouler dans des conditions de sécurité acceptables.

### Nettoyage

Si vous revenez d'une sortie très humide, passez un coup de chiffon sec sur la
chaîne, épongez ce que vous pouvez sur la transmission, passez un coup de jet
d'eau ou un coup de brosse sur le cadre pour ôter le plus «&nbsp;épais&nbsp;» et
laissez sécher pour revenir terminer ensuite.

Une fois sec, prenez une brosse et un pinceau plat rigide&nbsp;:  la brosse pour
ôter la terre collée, le pinceau pour aller dans les recoins (du genre&nbsp;: le
dérailleur arrière). 

Le cadre d'un VTT se nettoie à l'eau. On peut éventuellement ajouter un petit
peu de liquide vaisselle dans la bassine, mais c'est tout. Cela dit, comme vous
aurez débarrassé votre VTT de la terre avec une bonne vieille brosse, une
microfibre humide fait très bien l'affaire pour la majorité des
composants&nbsp;: vous n'aurez pas à attendre le séchage avant de passer à la
transmission.


### La transmission et les lubrifiants

La durabilité et le besoin de nettoyage de votre transmission dépend énormément
du lubrifiant que vous utilisez. 

Sur le marché vous trouverez beaucoup de lubrifiants huile adaptés aux
conditions météo, mais qui attirent plus ou moins de saletés, avec du téflon ou
sans téflon, etc. Il y a même de fortes chance que le premier conseil qu'on
puisse vous donner est d'utiliser ce genre de produit. 

Lubrifier à l'huile a pour caractéristique de coller la poussière et le sable,
et d'user prématurément les dents de vos plateaux, pignons et galets. De plus il
faut savoir utiliser la bonne huile en fonction des conditions météo (conditions
sèches, conditions humides). Pour le vélo de route, par définition moins sujet à
la boue et au sable, l'huile sera sans doute le lubrifiant le plus simple à
l'usage.

Pour le VTT, rien ne vaut le lubrifiant à la cire. La cire a pour avantage
d'enrober les pièces et empêche vraiment la poussière et le sable d'y adhérer.
Les frottements des pièces entre elles est largement atténué ce qui prolonge
indéniablement la durée de vie de votre transmission.

Si on a longtemps considéré que les lubrifiants cire étaient sensibles aux
conditions humides, les nouvelles formules n'ont plus vraiment ce défaut. En
revanche, si un lubrifiant huile peut être appliqué juste avant de pédaler, il
vous faudra attendre deux ou trois heures de séchage après avoir appliqué un
lubrifiant cire. Ce lubrifiant est composé d'eau et de particules de céramique
(il est souvent de couleur blanche), et il pénètre vraiment les moindre espaces
entre les composants de la chaîne.

L'autre avantage de la cire, c'est le nettoyage. Même au retour d'une sortie
bien boueuse, vous pourrez simplement passer un coup de chiffon sur la chaîne
(prenez la chaîne à pleine main avec un chiffon et frottez) et entre les
pignons. Alors qu'une huile vous obligerait à dégraisser la transmission.

À propos de dégraissage&nbsp;: la première fois que vous appliquez un lubrifiant
cire, et si le lubrifiant précédent était à l'huile, un dégraissage en détail
est nécessaire, suivit de trois applications du lubrifiant cire espacées de deux
à trois heures.

Enfin, il n'est pas toujours utile de re-lubrifier à chaque fois&nbsp;:
l'avantage de la cire, c'est que si les conditions sont bien sèches, et si vous
n'avez pas trop sali votre transmission, on peut enchaîner deux sorties sans
appliquer le lubrifiant, ou alors un tout petit coup pour la forme (mais
toujours après vous être assuré qu'il ne subsiste pas de terre sur la chaîne,
donc coup de chiffon, toujours). Vous pouvez aussi réserver un pinceau pour
votre chaîne et les pignons, histoire de frotter doucement, y compris entre les
maillons.

### Changer les pièces de votre transmission

C'est le sujet épineux&nbsp;: quand sait-on qu'il faut changer les pièces&nbsp;?
et si c'est le cas, que doit-on changer&nbsp;?

Si votre chaîne se met à sauter, c'est soit un défaut de réglage soit une usure
qu'il faut d'urgence corriger&nbsp;:

- la chaîne qui devient trop lâche,
- des dents des pignons ou des plateaux qui deviennent pointues et ne retiennent
  plus la chaîne correctement.

Si c'est le cas, je déconseille de changer la chaîne seule, ou les plateaux ou
les pignons seuls&nbsp;: c'est un ensemble. Tout au plus on peut changer les
galets d'entraînement du dérailleur, mais généralement on les change en même
temps que le reste. Pourquoi&nbsp;? parce que votre chaîne s'est adaptée aux
autres éléments d'entraînement et inversement.

Donc&nbsp;: les dents pointues, c'est le signe d'usure à reconnaître. Ne
cherchez pas à utiliser les indicateurs d'usure de chaîne. Je lis parfois qu'il
est conseillé de changer la chaîne régulièrement&hellip; bof&nbsp;: une chaîne
de VTT est solide, et à moins de vraiment la maltraiter (changer de vitesses,
cela s'apprend) mieux vaut user l'ensemble et changer le tout lorsqu'il le faut.
Bien sûr si vous cassez la chaîne, il faut la changer&nbsp;: si elle est neuve
et qu'elle saute, c'est qu'il faut aussi changer le reste car l'usure était déjà
là.

Concernant les autres pièces de votre transmission, l'élément qu'il peut vous
arriver de changer, c'est le boîtier de pédalier. La cause est soit l'usure,
soit des chocs dans le pédalier (par exemple, la pierre qu'on n'a pas vue et qui
tape une manivelle verticalement) qui font prendre du jeu au boîtier.


Le faire soi-même&nbsp;? en théorie, si vous êtes bricoleur·se, vous pouvez
changer tout un groupe. Mais si vous débutez, contentez-vous dans un premier
temps de diagnostiquer, puis faites un tour chez votre revendeur·se&nbsp;: iel a les
bons outils, les bonnes pièces et le savoir-faire. 



### Système de frein

Oui, il faut s'assurer que vos plaquettes ne sont pas encrassées par la boue.
Par contre faites bien attention à ne pas projeter de lubrifiant ou n'importe
quel corps gras sur vos plaquettes ou le disque de frein. 

Pour prévenir les accidents, vérifiez&nbsp;:

- l'état d'usure des plaquettes&nbsp;: n'attendez pas de freiner sur l'armature
  pour faire des étincelles et amuser la galerie. Tout ce que vous risquez c'est
  abîmer votre disque et surtout vous casser le nez parce que vos freins ne sont
  plus efficaces. Les plaquettes se changent régulièrement&nbsp;: après chaque
  sortie, vérifiez-les.
- votre disque de frein&nbsp;: est-il trop usé&nbsp;? des rails se sont
  formés&nbsp;? il faut le changer. Si vous venez de changer les plaquettes et
  que vous vous apercevez qu'elles rayent profondément votre disque, c'est
  qu'elles ne conviennent pas.

Quand purger les freins hydrauliques&nbsp;? Ce n'est pas une opération à faire
régulièrement mais lorsque qu'on constate que des bulles d'air sont présentes
dans le système&nbsp;:

- à l'arrêt votre levier de frein est très lâche,
- lors d'une descente, le frein chauffe et pourtant vous devez pomper pour
  retrouver de la prise.

Remplacement des plaquettes et des disques&nbsp;: ces opérations peuvent être
faites par vous même, c'est facile et accessible. En revanche, pour une bonne purge, je conseille plutôt d'aller voir un·e spécialiste.



### Les suspensions

L'entretien complet de ces pièces se fait par un·e professionnel·le. D'ailleurs,
certains revendeur·ses les envoient dans un centre spécialisé où l'on
peut démonter les suspensions, procéder à une vidange, changer les joints
et vérifier les pièces. Certains grand·es bricoleur·ses parviennent à le faire
iels-mêmes mais honnêtement, je trouve que ce genre d'opération est très pénible.

Si l'on lit attentivement les modes d'emploi des suspensions, certains
 préconisent un entretien complet annuel voire tous les six mois.
Un tel entretien coûte entre 100 et 150 euros par suspension et très
franchement, à moins de vouloir y passer le prix d'une fourche neuve, il n'y a
pas grand intérêt à faire une entretien complet tous les ans. Si vous prenez
soin de votre matériel, une fourche rempli parfaitement son rôle des années
durant, cependant, pour en garantir l'efficacité, pensez régulièrement à sa
révision selon le rythme de vos sorties à l'année. Donc tous les ans ou tous les
deux ans&nbsp;: au-delà, c'est que vous sortez peu.

On distingue&nbsp;:

- la révision&nbsp;: tous les ans ou deux ans&nbsp;: vidange, vérification,
  nettoyage (à faire soi-même ou chez un professionnel),
- l'entretien complet&nbsp;: idem + changement des joints, tous les ans ou deux
  ans (plutôt chez un professionnel).

Conseil&nbsp;: alternez&nbsp;:) et si vous loupez une séquence, ça ira quand
même si vous prenez soin de votre matériel, rassurez-vous.

Autre solution : vous vous en fichez, vous roulez et lorsqu'il faut changer la fourche avant, vous la changez. Sur un semi-rigide, par exemple, qui ne nécessite pas d'entretien trop onéreux, et si la fourche n'est pas excessivement chère, c'est peut-être encore la solution la plus simple. Donc on se détend sur ce point.

Enfin, n'oubliez pas&nbsp;: les suspension avec ressort à air&hellip; cela se
regonfle. Si vous perdez de l'air (cherchez la raison si vous en perdez trop),
non seulement votre suspension ne sera plus réglée correctement (surtout en
fonction de votre poids) mais aussi elle va s'user davantage vu que vous mettez
à l'épreuve les matériaux. Si vous n'avez pas de pompe haute pression manuelle
(cela vaut entre 20 et 40 euros), passez chez votre revendeur·se&nbsp;: il faut 30
secondes pour rajouter un peu d'air.

## Autres dispositifs

### La caisse à outils

Même si vous ne bricolez pas jusqu'à être capable de démonter et remonter chaque
pièce de votre VTT, votre caisse à outil devra néanmoins comporter quelques
éléments essentiels. La liste suivante n'est pas exhaustive mais elle vous
permettra de survivre&nbsp;:

- les outils que vous embarquez lors de vos sorties,
- outils classiques&nbsp;: tous les outils que vous avez déjà à la maison, dont
  un jeu de clé allen (en T si possible) et au moins une petite pince coupante,
- une clé à pédales (pour changer les pédales),
- pompe à haute pression (pour suspension),
- pompe à pied avec manomètre,
- un pied d'atelier (ou si c'est trop encombrant pour vous, un simple pied
  béquille à crochets qui puisse vous permettre de soulever la roue arrière
  lorsque vous bricolez),
- un dérive chaîne,
- une pince attache rapide.

Si vous êtes en Tubeless, il faudra un compresseur. Pas besoin d'un compresseur électrique. Il s'agit d'un compresseur à gonfler jusque 8-10 bar capable de relâcher la pression très rapidement de manière à faire claquer le pneu. Cela vaut entre 50 et 80 euros.

Pour le reste, si vous voulez changer votre groupe de transmission, votre
système de frein, ou vos suspensions, il y a des outils spécifiques à se
procurer avant de faire quoi que ce soit. Et n'oubliez pas&nbsp;: il y a un
début à tout, mais quand on n'a jamais fait&hellip; on n'a jamais fait. Donc
faites vous accompagner, c'est mieux.



### Préparer ses sorties et se repérer

À moins de connaître parfaitement les moindres sentiers sur des centaines de
kilomètres carrés, vous devriez embarquer au moins une carte IGN lorsque vous
sortez rouler. 

Il y a deux manières d'envisager une sortie. La première, surtout s'il s'agit de
vos premières sorties, c'est de partir comme vous le feriez en randonnée
pédestre, avec une bonne carte, puis improviser votre parcours en vous fixant
des objectifs et en suivant le balisage. C'est une bonne manière de découvrir le
pays. En revanche, à moins d'avoir déjà pas mal de pratique, méfiez-vous de
l'improvisation qui risque de vous amener à emprunter des chemins trop
difficiles par rapport à votre niveau, voire des chemins carrément non praticables en VTT. Parfois, il peut être intéressant d'organiser une randonnée pédestre pour envisager certaines tranches du parcours. 

C'est votre niveau que, justement, vous allez pouvoir améliorer si vous
surveillez vos distances et vos dénivelés. De même, si vous préparez
correctement un parcours, vous pouvez plus facilement veiller à ne pas présumer
de vos forces. 

Donc la seconde manière d'envisager vos sorties, c'est de manipuler un peu de
matériel numérique.

**Première solution**. Préparez votre parcours en vous rendant par exemple sur
[Geoportail](https://www.geoportail.gouv.fr/), ou encore
[VTTRack](http://www.vttrack.fr/). La cartographie IGN est la plus appropriée
pour préparer correctement une sortie pleine nature. Mais si vous pensez pouvoir
vous satisfaire des cartes issues du projet OpenStreetMap (dont les couches
OpenTopoMap, présentant les courbes de dénivelé), vous pouvez vous rendre sur le
[projet Umap](http://umap.openstreetmap.fr/fr/) ou utiliser un logiciel comme
Viking ou QMapShack sous GNU/Linux.

Tracez le parcours grosso-modo avec l'outil de calcul de distance, mémorisez les
principaux points de passage et prenez une carte «&nbsp;papier&nbsp;» qui vous
servira à vous repérer sur place (astuce&nbsp;: au lieu de prendre toute une
carte, faites des photocopie couleur des lieux que vous fréquentez, et utilisez
une pochette plastique&nbsp;: sortir la carte directement depuis la poche
arrière est moins contraignant que sortir une grande carte du sac à dos, la
déplier, la replier&hellip;),


**Seconde solution**. Préparez votre parcours avec un logiciel qui permet de
réaliser un tracé que vous pourrez ensuite importer sur un dispositif GPS que
vous embarquez sur votre VTT (ou votre smartphone dans une poche facile
d'accès). C'est de loin la solution la plus courante mais elle nécessite encore
un achat, celui du GPS (c'est tellement pratique, aussi&hellip;). Notez qu'on ne
compte plus le nombre d'écrans de smartphone cassés à force de le sortir et le
ranger pour se repérer. Vous pouvez aussi avoir un support pour smartphone au
guidon&nbsp;: c'est très bien sur route, mais en VTT quand ça secoue trop fort
ou si vous tombez, votre smartphone risque de prendre cher (alors que les
dispositifs GPS sont petits et peu encombrants au guidon).

Dans les deux cas, regardez-bien les distances, les dénivelés et identifiez les
points difficiles.

Ma pratique personnelle est la suivante&nbsp;:

1. Un dispositif GPS embarqué sur le vélo (marque Garmin et son
   support guidon),
2. n'ayant jamais trouvé mieux que la cartographie IGN pour nos contrées, il est
   difficile de trouver un logiciel libre qui utilise efficacement cette
   cartographie pour tracer des parcours de manière fiable et en exporter des
   traces utilisables sur une dispositifs GPS. J'ai donc opté pour
   [IGN Rando](https://ignrando.fr/fr/) qui permet, outre l'application du
   téléphone portable, de préparer ses parcours à partir d'un simple navigateur Internet, afin de
   créer des traces et les exporter en différents formats
   (j'utilise le GPX). L'usage des cartes est payant pour quelques euros par an.
3. Une fois le GPX créé, j'uploade sur le site de Garmin (qui se gave de mes
   données personnelles, aaargh!) et je balance le tout sur mon appli smartphone
   qui, couplée à mon dispositif GPS me permettra d'y uploader la trace afin
   d'être utilisable sur le vélo lors de la sortie. 

IGN Rando est aussi une application sur Smartphone. Notez que depuis le mois de mai 20244, l'IGN a publié une autre application nommée Cartes IGN, qui est en fait un accès au Géoportail.

Pourquoi ne pas utiliser les cartes du projet OpenStreet Map ? et bien figurez-vous que c'est le cas. J'ai fait en sorte que mon dispositif GPS de vélo utilise ces cartes qui, en fait,  sont plus lisibles et à jour que les cartes présentes par défaut. Par contre, pour préparer un parcours, l'IGN a produit des cartes bien plus adaptées et dont les données peuvent aussi être croisées. 

Autre solution, si vous ne voulez pas téléverser vos cartes et vos données sur le site du fabricant :

- préparez votre trace GPX à suivre ;
- placez le fichier sur votre dispositif GPS relié à votre ordi (sous GNU Linux, c'est comme une clé USB, il suffit de choisir le bon dossier) ;
- vous pouvez aussi y aller chercher le GPX de votre parcours effectué ;
- et le lire avec un logiciel de lecture GPX libre.


### Nettoyer sur place

Et pour finir, je reviens de nouveau sur la question du nettoyage.  Si vous vous
déplacez en voiture avec votre VTT ou si vous habitez en appartement, un petit
matériel pourra vous être utile&nbsp;: un nettoyeur mobile. Il s'agit d'une
petite station sur batteries, avec une contenance de 5 à 10 litres. Le jet d'eau
n'est pas haute pression. La marque Kärcher en produit à prix raisonnable. 

Utilité&nbsp;: si vous revenez d'une sortie très boueuse, avant de charger le
VTT sur votre porte-vélo ou dans  la voiture, passez-lui un coup de flotte avec
cet outil. Idem si vous habitez en appartement, descendez sur le parking&nbsp;:
vous pourrez nettoyer votre vélo sans encrasser votre balcon. Prenez aussi
toujours une ou deux brosses avec vous pour enlever le plus gros avant de
fignoler à la maison.

##### Notes
