---
title: "Entraînement trail, Oberhaslach-Schneeberg"
date: 2015-04-26
author: "Christophe Masutti"
image: "/images/postimages/hikebike.png"
tags: ["Sport", "Trail", "Entraînement", "Parcours"]
description: "La montée du Schneeberg, à deux pas de Strasbourg"
categories:
- Sport
---



Dans ma recherche de parcours d'entraînement trail situés à une distance raisonable de Strasbourg et présentant néanmoins des dénivelés intéressants, j'ai testé ce tracé hier, par un temps quelque peu changeant mais qui n'a pas empêché le plaisir des yeux. Le tout pour 20&nbsp;km de distance et environ 900&nbsp;m&nbsp;D+.

Je vous propose donc cette petite virée du côté de Oberhaslach, une commune qui par ailleurs accueille le [Trail de la Hasel](http://traildelahasel.fr/) dont la prochaine et troisième édition est prévue le 27 juin 2015. Cela dit, le parcours n'est pas du tout celui que je compte vous présenter ici.


## Description

Depuis Strasbourg, se rendre à Oberhaslach est très simple. Il suffit de prendre l'autoroute direction Molsheim puis Schirmeck. Une fois sur la D1420, bifurquer vers Niederhaslach/Nideck par la D392 puis la D218, Dans le centre de Oberhaslach, prendre à droite juste avant la mairie et monter quelques 200 mètres en suivant l'indication du parking (voir plan ci-dessous). Il s'agit du parking de l'école du village où vous aurez l'embarras du choix pour vous garer. Notez au passage la présence de la fontaine près de la mairie, au cas où un coup de brosse devrait s'imposer pour vos chaussures au retour.

Pour le résumer simplement, le parcours consiste à longer la Hasel pour une mise en jambe agréable, puis monter au Nideck (par le Hirschfels), rejoindre le Schneeberg, puis retour par le Col des Pandours, le Petit et le Grand Ringenfels. La carte, le profil et les photos figurent en bas de ce billet.

En guise de préparation, pour une course de printemps, on pourra prévoir un coupe-vent pour contrer les effets du Schneeberg (culminant à 966&nbsp;m). La totalité du parcours est plutôt à l'abri de la forêt. Il offre plusieurs points de vue et, en tout, le Hirschfels, le Nideck, le Schneeberg, les vestiges Gallo-Romains du Petit Ringelfels et les ruines du château du Grand Ringelfels.

## Commentaires sur les sections

Le parcours peut se diviser en 5 sections&nbsp;:


- Après avoir quitté les rues bitumeuses d'Oberhaslach, on longe la Hasel avec un dénivelé très faible (quelques 50 m D+). Ce qui a pour effet de provoquer un échauffement dont on se félicitera dès la première montée.
- Au Café du Nideck, le parcours commence par une montée assez longue, pour moitié sur chemin forestier large qui se transformera ensuite en sentier ombragé. Mon conseil, sur cette section, est de vous dépenser le moins possible malgré les effets d'un sentier a priori facile par moments. La montée principale n'est pas celle-là !
- C'est la montée vers le Schneeberg, depuis la maison forestière du Nideck, qui fera sentir son dénivelé le plus important. Le sentier est fort agréable, pierreux par moments et relativement régulier.
- La descente du Schneeberg au Carrefours Anlagen (en passant par le Col des Pandours) débute sur sentier étroit mais est composé en majorité de chemin forestier large. Il ne présente aucune difficulté technique sauf un peu de longueur largement compensée par la section suivante.
- Les deux Ringelfels sont des petits sommets offrant les mêmes difficultés techniques de part et d'autre. Il s'agit d'un endroit très intéressant pour un entraînement à la descente. Ces dernières sont en effet assez raides, l'une pierreuse et l'autre moins, mais avec un rythme soutenu. La prudence est de mise ! Les deux montées n'ont rien de particulier, à ceci près qu'elles constituent d'excellents indicateurs sur votre capacité à doser vos efforts sur les sections précédentes (ha, ha, ha !).


## Le balisage

Le parcours peut se faire presque sans carte (mais je conseille de vous y référer de temps en temps). Ceci grâce au balisage du Club Vosgien.


- On rejoint les **rectangles rouges** du GR53 dès le centre d'Oberhaslach (au niveau du gymnase, à la sortie du village, prendre à gauche et monter dans le petit quartier pour rejoindre le chemin qui longe la Hasel). Jusqu'au Schneeberg, vous ne quitterez presque pas le GR53 sauf au niveau du Nideck.
- Un éboulement est signalé aux chutes du Nideck dès le début du chemin au Café du Nideck. Dès lors, le GR effectue une **déviation**, dûment balisée, en passant par le Hirschfels. Aucune déception, ce détour ne rallonge guère en distance et des roches intéressantes jalonnent le chemin.
- Au Nideck, vous serez tenté de suivre le GR53. N'en faites rien. En effet, celui-ci vous mènerait sur la route (D218) et vous seriez obligé d'arpenter le bitume jusque la Maison Forestière du Nideck. Au lieu de cela, empruntez les **cercles bleus**, jusqu'à la maison forestière, puis rattrapez le GR53 pour monter au Schneeberg. Attention à rester attentif au balisage : dans une petite pépinière sombre, le chemin bifurque brusquement à votre droite.
- Au Schneeberg, redescendez vers le col des Pandours en suivant les **croix rouges**. Vous croiserez un refuge, et c'est devant sa porte que vous devrez passer pour trouver les indications et reprendre le chemin (sans quoi on est assez facilement tenté de poursuivre tout droit vers un très beau point de vue, mais non prévu). De même peu après le refuge, le chemin se sépare en une patte d'oie serrée : avec votre élan, n'allez pas embrasser un épicéa, mais contentez-vous de suivre les croix rouges.
- Au Col des Pandours, empruntez ensuite les **rectangles jaunes et blancs** jusqu'au Carrefour Anlagen. Légèrement sur votre droite, vous trouverez les **cercles bleus** qui vous mèneront sur les deux Ringelfels (la montée démarre aussitôt).
- Après le château du Grand Ringelfels, poursuivez sur les cercles bleus jusqu'à croiser un chemin forestier. Vous devrez alors emprunter les **disques jaunes** sur votre gauche en contrebas du chemin. Vous les suivrez jusqu'au village.


## Conclusion

Il s'agit d'un parcours, comme d'habitude, assez touristique, mais qui permet de s'entraîner efficacement à la montée sur des sentiers. En général, le parcours compte moins de 30% de chemin forestier large et très peu de bitume (maximum 500 mètres), ce qui est plutôt appréciable dans les Vosges.

## Carte, profil et photos

<iframe src="https://umap.openstreetmap.fr/fr/map/oberhaslach-schneeberg_37723?scaleControl=false&amp;miniMap=false&amp;scrollWheelZoom=true&amp;zoomControl=true&amp;allowEdit=false&amp;moreControl=true&amp;datalayersControl=true&amp;onLoadPanel=none&amp;captionBar=true" frameborder="0" height="400px" width="100%"></iframe>

<a href="https://umap.openstreetmap.fr/fr/map/oberhaslach-schneeberg_37723">Voir en plein écran</a> | Cliquer sur « Plus » à gauche puis l'icône d'export pour télécharger le fichier du tracé (gpx, kml, geojson).
