---
title: "Dernières news"
date: 2021-11-11
author: "Christophe Masutti"
image: "/images/postimages/cm.png"
description: "Le temps passe et il fini par manquer. J'ai délaissé ce blog depuis la rentrée mais c'est pour livrer encore plus de lectures ! En vrac, voici quelques activités qui pourraient vous intéresser."
tags: ["Libres propos", "Politique", "surveillance"]
categories:
- Libres propos
---


Le temps passe et il fini par manquer. J'ai délaissé ce blog depuis la rentrée mais c'est pour livrer encore plus de lectures&nbsp;! En vrac, voici quelques activités qui pourraient vous intéresser.

<!--more-->

# Publications

Commençons d'abord par les publications. Voici deux nouvelles références. 

Un article court mais que j'ai voulu un peu percutant&nbsp;:

Christophe Masutti, «&nbsp;Encore une autre approche du capitalisme de surveillance&nbsp;», *[La Revue Européenne des Médias et du Numérique](https://la-rem.eu/)*, num. 59.

Beaucoup plus long, et qui complète mon ouvrage sur la question de l'histoire du courtage de données&nbsp;:

Christophe Masutti, «&nbsp;En passant par l’Arkansas. Ordinateurs, politique et marketing au tournant des années 1970&nbsp;», *[Zilsel -- Science, Technique, Société](https://editions-croquant.org/revue-zilsel/770-zilsel-n-9.html)*, num. 9.


Ces textes seront versés dans HAL-SHS dans quelques temps.

# Interventions

A part cela, je mentionne deux enregistrements. 

Le premier à [Radio Libertaire](https://radio-libertaire.org/accueil.php) où j'ai eu le plaisir d'être interviewé par Mariama dans l'émission *Pas de Quartier*, du groupe Louise Michel, le 2 novembre 2021. [On peut réécouter l'émission ici](http://www.groupe-louise-michel.org/?page=emission&id_document=1589).

Le second est une conférence débat qui s'est tenue à Bruxelles au *Festival des Libertés* où j'ai eu l'honneur d'être invité avec Olivier Tesquet. Un débat organisé et animé par Julien Chanet. [On peut l'écouter depuis le site ici](https://www.festivaldeslibertes.be/2021/fase6?event=21050&_Debat__Surveillance-numerique,-entre-realite-et-fictions__#21050).

Et une annonce.

Enfin, j'annonce mon intervention prochaine auprès des *Amis du Monde Diplomatique* dans le cadre de *CitéPhilo* à Lille, où j'ai le plaisir d'avoir été invité par Bertrand Bocquet le 22 novembre prochain. [Plus d'information ici](https://www.amis.monde-diplomatique.fr/Surveiller-et-vendre.html).

À bientôt&nbsp;!
