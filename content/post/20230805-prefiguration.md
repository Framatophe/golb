---
title: "Mouvements préfiguratifs"
date: 2023-05-08
author: "Christophe Masutti"
image: "/images/postimages/cm.png"
description: "Ce que portent en elles les politiques préfiguratives est éminemment dangereux pour le pouvoir en place : la démonstration en acte d’une autre vision du monde possible. Beaucoup d’études, souvent américaines (parce que le concept y est forgé) se sont penchées sur cette question et ont cherché à définir ce qu’est la préfigurativité dans les mouvements sociaux. Je propose ici d’en discuter, à partir de quelques lectures commentées et ponctuées de mon humble avis."
tags: ["Libres propos", "anarchie", "pouvoir", "Préfiguration"]
categories:
- Libres propos
---


Lorsque le ministre de l’Intérieur français déclare avec conviction début avril 2023 que « plus aucune ZAD ne s’installera dans notre pays » (version polie de « pas d’ça chez nous »), il est important d’en saisir le sens. Dans une ambiance où la question du maintien de l’ordre en France se trouve questionnée, y compris aux plus hauts niveaux des instances Européennes, détourner le débat sur les ZAD relève d’une stratégie assez tordue. Les ZAD, sont un peu partout en Europe et font partie de ces mouvements sociaux de défense environnementale qui s’opposent assez frontalement aux grands projets capitalistes à travers de multiples actions dont l’occupation de zones géographiques.  Or, dans la mentalité bourgeoise-réactionnaire, les modes de mobilisation acceptables sont les manifestations tranquilles et les pétitions, en d’autres termes, les ZAD souffrent (heureusement de moins en moins) du manque de lisibilité de leurs actions : car une ZAD est bien plus que la simple occupation d’une zone, c’est tout un ensemble d’actions coordonnées et de réflexions, de travaux collectifs et de processus internes de décision… en fait une ZAD est un exemple de *politique préfigurative*. Pour le ministre de l’Intérieur, ce manque de lisibilité est un atout : il est très facile de faire passer les ZAD pour ce qu’elles ne sont pas, c’est-à-dire des repères de gauchos-anarchiss’ qui ne respectent pas la propriété privée. Parler des ZAD, c’est renvoyer la balle aux autres pays Européens qui viendraient à critiquer le maintien de l’ordre à la Française : regardez d’abord chez vous.


Pourquoi cette caricature réactionnaire ? elle ne concerne pas seulement
les ZAD, mais aussi toutes les actions d’occupation, y compris les plus
petites comme le simple fait qu’un groupe d’étudiants ingénieurs se
mette à racheter une ferme pour y vivre sur un mode alternatif. Cette
caricature est sciemment maintenue dans les esprits parce que ce que portent
en elles les politiques préfiguratives est éminemment dangereux pour le
pouvoir en place : la démonstration en acte d’une autre vision du monde
possible. Beaucoup d’études, souvent américaines (parce que le concept y
est forgé) se sont penchées sur cette question et ont cherché à définir
ce qu’est la préfigurativité dans les mouvements sociaux. Ces approches
ont désormais une histoire assez longue, depuis la fin des années 1970.
On ne peut donc pas dire que le concept soit nouveau et encore moins le
mode d’action. Seulement voilà, depuis les années 1990, on en parle de
plus en plus (j’essaie d’expliquer pourquoi plus loin). Je propose donc
ici d’en discuter, à partir de quelques lectures commentées et ponctuées de mon humble
avis.

<!--more-->
{{< toctoc >}}

----

## Quotidien et transformation individuelle

Dans son chapitre « Exemplarité et mouvements sociaux » (Renou 2020), G.
Renou fait l’inventaire des types de mouvements sociaux dont
l’exemplarité joue un rôle stratégique. Le phénomène de
*quotidianisation revendiquée* recouvre ces mouvements dont le
raisonnement repose sur l’articulation entre changement personnel
(individuel mais on pourrait rajouter, je pense, aussi celui du collectif
comme personne « morale ») et changement sociopolitique. On peut les
appeler mouvements « d’exemplarité », « préfiguratifs » ou « de mode de
vie », ils proposent tous des alternatives à l’existant mais présentent
certaines nuances. On peut ainsi distinguer, en suivant G. Renou :

-   les mouvements d’exemplarité proprement dite : mouvements fondés sur
    l’imitation de pratiques, plus ou moins rigides et exigeantes,
-   les mouvements préfiguratifs autrement nommés « politiques
    préfiguratives » : ils incarnent par leurs pratiques et leurs moyens
    la société souhaitée, ces pratiques et ces moyens évoluent en
    fonction de la manière dont est conçu ce chevauchement des moyens et
    de la fin,
-   les mouvements dits de mode de vie (*lifestyle movements*) : c’est
    le mode de vie lui-même qui est censé changer la société, comme
    démonstration de la contestation, le mode de vie est en soi
    revendicateur.

Il y a trois caractéristiques importantes que l’on retrouve dans tous
ces mouvements :

-   la cohérence entre le discours et les pratiques (ce qui conduit
    parfois à prendre des positions radicalement opposées à l’ordre
    établi, comme c’est le cas avec des actions de désobéissance
    civile),
-   l’ancrage sur un territoire (« on est là ») : la transformation
    structurelle de la société implique une prise de position dans un
    espace. Ce peut être un pays ou un territoire complet (le Chiapas,
    le Rojava) ou une place (Nuit Debout, Occupy),
-   la transformation individuelle comme acte politique.

Sur ce troisième point, G. Renou explique :

> « Le troisième déplacement opéré par la problématique de l’exemplarité a
> trait à la subversion des articulations individuel/collectif,
> privé/public et à la définition même de l’action collective. Si on
> accepte que la transformation de l’existence personnelle devienne, aux
> yeux des activistes, un enjeu pleinement politique, au sens où
> celle-ci n’est plus rabattue sur la vie privée opposée à la vie
> citoyenne et publique (cadrage juridique), ni même sur le for
> intérieur ou la conscience (cadrage psychologique), un corollaire
> s’impose. L’action collective tend à ne plus être assimilable à la
> défense des intérêts d’un groupe identifié par la médiation d’une
> action sur l’appareil d’État, traditionnellement considéré comme le
> lieu d’impulsion de tout changement politique d’ampleur, ainsi que le
> présupposaient les grandes organisations militantes de masse, depuis
> la fin de la première guerre mondiale. »

Pour moi, ce point de vue est en partie discutable. Les mouvements
préfiguratifs ne cherchent pas à rompre avec une certaine tradition qui
ne serait incarnée que par les organisations « de masse » qui œuvraient
dans un cadre institutionnel de revendication « depuis la fin de la
Première Guerre Mondiale ». Cela situe la définition de la politique
préfigurative dans les présupposés où elle a été définie la première
fois. Et il semble que l’approche de G. Renou soit en quelque sorte
victime d’une vision quelque peu figée. On peut le comprendre, car son
article figure dans un dictionnaire (*Dictionnaire des mouvements
sociaux*) et à ce titre l’approche ne saurait être prospective.

Qui le premier a forgé ce concept de *prefigurative politics* ? Il
s’agit de Carl Boggs en 1977 (Boggs 1977a, 1977b). Boggs fait remonter
le concept à la tradition anarchiste et syndicaliste du XIXᵉ siècle et
sa définition se confronte avec le marxisme classique :

> « Par "préfiguration", j’entends l’incarnation, dans la pratique
> politique permanente d’un mouvement, des formes de relations sociales,
> de prise de décision, de culture et d’expérience humaine qui
> constituent l’objectif ultime. Développée principalement en dehors du
> marxisme, elle a produit une critique de la domination bureaucratique
> et une vision de la démocratie révolutionnaire que le marxisme n’avait
> généralement pas. »[^1]

Cette définition projette de manière évidente les mouvements sociaux qui
s’en réclament dans une dynamique de la quotidienneté. Mais cette
dynamique est historique. Elle va se chercher dans l’héritage
contre-institutionnel des soviets ou, mieux, celui de la Commune de
Paris en 1870. Dans le cas de la Commune, il s’agissait des institutions
d’enseignement scolaire ou des hôpitaux, d’un Conseil, ou encore des
institutions créatives (comme les Clubs), toutes placées sous le
contrôle du peuple et cherchant à supplanter celles du pouvoir en place
tout en les fondant sur un ensemble de pratiques démocratiques. La
préfiguration s’inscrit donc dans l’Histoire mais, de plus, dans une
tradition anarchiste, elle consiste à entrer en lutte contre le pouvoir
en opposant l’intérêt collectif à l’organisation du pouvoir (l’État, en
l’occurrence).

L’approche de C. Boggs est cependant elle-même inscrite dans la pensée
des années 1970 et du néo-marxisme ambiant : anti-institutionnel
(héritage de A. Gramsci[^2]), antipositiviste, et faisant la jonction
entre matérialisme historique et conscience de soi (on pense à J.-P.
Sartre ou H. Marcuse, d’où l’importance de la transformation
individuelle comme acte politique). Cela entre dans la « New Left »
américaine[^3], cela intègre une partie des études féministes ou encore
les *cultural studies*, et bien sûr l’anarchisme. Dans cette
perspective et dans le contexte des années 1970, le préfigurativisme est une forme de socialisme
décentralisé (Kann 1983) tout en prônant l’insurrection populaire. Du
point de vue d’aujourd’hui, on pourra néanmoins ajouter que ce furent
essentiellement des postures qui, comme le rappelle C. Malabou (Malabou
2022) sont surtout philosophiques et diffusées par des philosophes qui
jamais ne se reconnaissent anarchistes et encore moins dans les faits
(car ils sont détenteurs eux-mêmes de positions de pouvoir, à commencer
par leurs statuts d’universitaires et d’intellectuels).

Pour autant, si les moyens structurels et les pratiques jouent un rôle
fondamental dans les politiques préfiguratives, c’est parce que leur
tradition est d’essence anarchiste sans pour autant se reconnaître et se
théoriser comme telle (comme c’est souvent le cas dans les pratiques
anarchistes de beaucoup de mouvements). Je pense que les néo-marxistes
n’ont pas vraiment saisi les opportunités de la préfiguration comme mode
d’action directe et de revendication. Par action directe, on pense bien
sûr à ce que Boggs identifiait comme pratiques contre-institutionnelles
(ce qui est toujours d’actualité, voir (Murray 2014)), mais comme
revendication il s’agissait aussi de faire la démonstration des
alternatives à l’ordre établi, tout comme aujourd’hui nous reconnaissons
cette dimension aux mouvements dits « altermondialistes ». Il ne s’agit
donc pas tant de *remplacer* la défense et la revendication de l’intérêt
collectif par la mise en œuvre de pratiques quotidiennes (aussi
démonstratives qu’elles puissent être), mais *d’articuler* l’intérêt
collectif avec des moyens, des valeurs et des routines de manière
cohérente, et c’est cette cohérence qui fonde la légitimité de
l’alternative proposée. On peut illustrer cela, par exemple :

-   pour échapper à la dissonance cognitive permanente qu’un système
    capitaliste nous impose en nous obligeant à choisir entre sa
    justification permanente du progrès matériel et la défense de
    l’environnement soi-disant rétrograde, on peut proposer des modes de
    décision collective et mettre en pratique des systèmes de production
    et d’échanges économiques en dehors des concepts du capitalisme tout
    en montrant que les institutions classiques ne sont pas en mesure de
    défendre l’intérêt collectif,
-   à un bas niveau, on peut aussi démontrer que les mouvements sociaux
    (préfiguratifs ou non) ont tout intérêt à utiliser des logiciels
    libres en accord avec les valeurs qu’ils défendent (solidarité,
    partage…) plutôt que de se soumettre à la logique des plateformes
    proposées par les multinationales du numérique. C’est notamment le
    credo de [Framasoft](https://framasoft.org), une pierre à l’édifice
    de l’altermondialisation.

## Ne pas oublier Marx

Pour revenir au néo-marxistes, je pense que leur point de vue
pas-tout-à-fait-anarchiste vient de ce qu’ils sont eux-mêmes victimes de
la tension qui s’est déclarée dès la Première Internationale entre les
libertaires et les marxistes-collectivistes, et dont les résonances sont
identifiées dans le texte de C. Boggs comme les obstacles à la
préfiguration (cf. plus bas). Le premier élément qui vient en tête est
ce conflit larvé entre Marx et Bakounine, où (pour grossièrement
résumer[^4]) le second oppose à l’organisation Internationale cette
tendance à la bureaucratisation et à l’acceptation de fait de positions
de pouvoirs. C’est-à-dire une opposition de deux types de militance.
Celle des libertaires n'a pas pris le dessus, c’était celle de
l’autonomie des sections et l’exercice démocratique de la prise de
décision en assemblée et non par délégués interposés. En cela, les
positions marxistes ont fini par se structurer autour de la
revendication et la grève par une forme syndicaliste hiérarchisée et
centralisée. Si on considère que c’est là l’un des points de rupture
entre libertaires et marxistes, alors la préfiguration n’a effectivement
pas pu être saisie toute entière par les néo-marxistes. Un autre point
est qu’historiquement les anarchistes n’ont pas toujours cherché à
inscrire la préfiguration dans le cadre d’une lutte de classe, mais
souvent comme une présentation d’alternatives, au pluriel. Pourquoi au
pluriel ? parce que un bonne politique préfigurative avance par essai et
erreurs, « elle est expérimentale autant qu’expérientielle » (van de
Sande 2015) et doit toujours se réajuster pour être formulée comme
alternative aux formes d’injustices, d’inégalité et de répression que
porte notamment le capitalisme.

À cela on peut cependant opposer que la pensée pratique de Marx recèle
des éléments tout à fait pertinents pour une politique préfigurative.
C’est la thèse de Paul Raekstad (Raekstad 2018) qui explore la
philosophie de Marx dans ce sens. Selon Marx, pour mener une politique
révolutionnaire, il ne faut pas seulement des sujets révolutionnaires,
il faut aussi des sujets chez qui il y a un besoin révolutionnaire à
satisfaire, c’est-à-dire une conscience révolutionnaire. Ainsi, une
politique préfigurative est à même de produire cette conscience
révolutionnaire dans la mesure où elle permet à « l’éducateur de
s’éduquer ». Du point de vue de la transformation individuelle, si le
comportement et les choix de vie peuvent être préfiguratifs, ils
participent à une conscience de soi révolutionnaire et pour Marx, c’est
ce qui fait la pratique révolutionnaire.

Ainsi, dans les *Thèses sur Feuerbach* (numéro 3), Marx écrit :

> « La doctrine matérialiste du changement des circonstances et de
> l’éducation oublie que les circonstances sont changées par les hommes
> et que l’éducateur doit lui-même être éduqué. C’est pourquoi elle doit
> diviser la société en deux parties — dont l’une est élevée au-dessus
> d’elle.  
> La coïncidence du changement des circonstances et de l’activité
> humaine ou autochangement ne peut être saisie et rationnellement
> comprise que comme *pratique révolutionnaire*. »

Cette pratique s’accomplit aussi à travers l’assemblée qui n’est pas
seulement une assemblée d’ouvriers, mais une assemblée où s’exerce une
sorte de phénoménologie de la conscience collective révolutionnaire et
où les moyens deviennent aussi la fin. Se rassembler et discuter est
aussi important que le but du rassemblement :

Ainsi dans le *Troisième Manuscrit de 1844*, Marx écrit :

> « Lorsque les ouvriers communistes se réunissent, c’est d’abord la
> doctrine, la propagande, etc., qui est leur but. Mais en même temps
> ils s’approprient par là un besoin nouveau, le besoin de la société,
> et ce qui semble être le moyen est devenu le but. »

Pour autant, il y a d’autres philosophies pratiques, plus modernes et
plus à même de fonder une véritable philosophie préfigurativiste. Je
crois que non seulement elle à chercher dans les approches anarchistes,
mais aussi que Marx n’est pas le plus indiqué. Non pas que la
philosophie de Marx ne s’y prêterait absolument pas mais vouloir tirer
absolument de Marx des principes d’action qui à l’époque ne se posaient
carrément pas en ces termes, me semble assez anachronique. La question
est plutôt de savoir comment les actions des politiques préfiguratives
sont rendues visibles, quel est leur caractère performatif et quels
principes d’action on peut en tirer *aujourd’hui*.

## Rendre visibles les mouvements préfiguratifs

Nous avons vu que chercher à définir les politiques préfiguratives ne
les rend pas pour autant évidentes. Tantôt il s’agit de mouvements
sociaux « exemplaires », tantôt une dialectique entre moyen et fin,
tantôt une approche néo-marxiste anti-institutionnelle, tantôt une
résurgence des idées libertaires qui pourtant n’échappent pas à la
pensée pratique de Marx… L’erreur consiste peut-être à trop chercher à
en saisir le concept plutôt que la portée. N’est-ce pas en fonction du
caractère opérationnel de l’action directe que l’on peut en saisir le
sens ?

Retournons du coté des définitions. On peut se pencher sur celle que
donne Darcy Leach (Leach 2013), courte et élégante. Pour elle, la
préfigurativité est…

> « fondée sur la prémisse selon laquelle les fins qu’un mouvement social
> vise sont fondamentalement constituées par les moyens qu’il emploie,
> et que les mouvements doivent par conséquent faire de leur mieux pour
> incarner – ou "préfigurer" – le type de société qu’ils veulent voir
> advenir. »

En se reportant au texte (court) de Darcy Leach, on voit qu’elle ne
cherche pas tant à présenter en long et en large ce que sont les
« politiques préfiguratives ». En effet, un tel exercice n’aurait pas
d’autre choix que de s’adonner à une litanie d’exemples. Exactement
comme lorsqu’on présente des idées anarchistes et que, toujours
confronté au défi de démontrer que ces idées peuvent « fonctionner », on
se met invariablement à citer des exemples dont la démonstration est
aussi longue que laborieuse. C’est tout simplement parce que les
anarchistes théorisent l’anarchie mais l’anarchie, elle, est toujours en
pratique, elle est même parfois indicible. Au lieu de cela D. Leach
préfère résumer les trois raisons principales pour lesquelles elles
peuvent échouer, celles identifiées déjà par C. Boggs, ce qui revient à
définir par la négative :

> -   « le jacobinisme, dans lequel les forums populaires sont réprimés ou
>     leur souveraineté usurpée par une autorité révolutionnaire
>     centralisée ;
> -   le spontanéisme, une paralysie stratégique causée par des
>     inclinations paroissiales ou antipolitiques qui empêchent la
>     création de structures plus larges de coordination efficace ;
> -   et le corporatisme, qui se produit lorsqu’une strate oligarchique
>     d’activistes est cooptée, ce qui les conduit à abandonner les
>     objectifs initialement radicaux du mouvement afin de servir leurs
>     propres intérêts dans le maintien du pouvoir. »

Elle résume aussi pourquoi les mouvements préfiguratifs ont tant de mal
à être correctement identifiés : c’est parce que dans les
représentations courantes, ont s’attend toujours à voir dans les
collectifs une forme hiérarchique stable, avec un leader et des
suiveurs, ou avec un bureau (dans le cas d’une association), en somme,
une représentation du mouvement selon une division des rôles bien
rationnelle :

> « Comme les théories des mouvements sociaux ont souvent supposé des
> acteurs instrumentalement rationnels et une organisation
> bureaucratique, les mouvements préfiguratifs sont souvent mal
> interprétés ou apparaissent comme des cas anormaux dans la recherche
> sur les mouvements sociaux. »

Il n’en demeure pas moins que lorsqu’on s’intéresse aux mouvements
sociaux dans différents pays, les études sont loin d’être aussi
pusillanimes quant à la lecture de ces mouvements à travers le prisme de
la préfigurativité. Il y a même une tendance universaliste de ce point
de vue. Ainsi Marina Sitrin (Sitrin 2012), qui s’intéresse à l’Argentine
du début des années 2000 , affirme que la politique préfigurative est
une manière d’envisager les relations sociales et économiques comme nous
voudrions qu’elles soient. Si cette manière d’envisager les choses a des
sources historiques profondes, c’est aussi parce qu’elle est loin d’être
anecdotique :

> « Dans le monde entier, il ne s’agit pas de petites « expériences »,
> mais de communautés comprenant des centaines de milliers, voire des
> millions de personnes – des personnes et des communautés qui ouvrent
> des brèches dans l’histoire et créent quelque chose de nouveau et de
> beau dans cette brèche. »

Et on est bien tenté d’être d’accord avec ceci : pour peu que l’on
regarde les mouvements sociaux depuis les années 2000, ceux qui prennent
modèle sur les mouvements plus historiques ou les plus récents qu’ils
soient internationaux ou plus locaux (comme les Zad en France), il est
désormais très difficile de passer à côté de leur caractère préfiguratif
tant il est porté systématiquement à la connaissance des observateurs
comme une sorte d’identité revendicatrice et à portée universelle…
altermondialiste ! Pour M. Sitrin, il faut surtout prendre en compte les
mouvements apparemment spontanés mais qui se sont cristallisés sur un
mode préfiguratif (justement pour limiter ce spontanéisme,
cf. ci-dessus), pour répondre à une situation donnée, sans que pour
autant on en voie les conséquences immédiates :

> « Ces expériences à court terme vont de la France en mai 1968 à ce que
> l’on appelle aujourd’hui la [Comuna de Oaxaca](https://www.contretemps.eu/commune-oaxaca-mexique-greve-classe-democratie/), en référence aux
> quatre-vingt-dix jours de 2006 pendant lesquels la population a occupé
> le zócalo (parc central), mettant en place des formes alternatives de
> prise de décision et de survie (…) ; ces deux expériences constituent
> le type de rupture auquel les zapatistes font référence, **la création
> d’une pause dans une situation politiquement insoutenable**. Des
> assemblées et des démocraties de masse similaires ont été observées en
> 2011 en Égypte, en Grèce et en Espagne. Les résultats de ces
> rassemblements de masse et de ces formations politiques préfiguratives
> n’ont pas encore été déterminés, mais ce qui est certain, c’est que de
> nouvelles relations sont en train de se créer. Les ruptures provoquées
> par des événements « naturels » – ou du moins par un moment
> d’étincelle, comme un tremblement de terre ou un attentat terroriste –
> ont donné lieu à des versions à court terme de ces mêmes événements.
> C’est ce qui s’est passé à New York pendant et immédiatement après le
> 11 septembre. »

Dans les années 1990, de nombreux mouvements sociaux se sont dressés de
cette manière, sur un mode apparemment spontané et improvisé. En
apparence seulement car les racines conceptuelles sont profondes.

## L’avenir est contagieux : depuis les années 1990

Prenons par exemple ces deux évènements : la Conférence des Nations
Unies sur l’environnement et le Développement, autrement nommée Sommet
de la Terre à Rio en 1992, et la même année la publication du livre de
F. Fukuyama *La fin de l’histoire et le dernier homme*. Le Sommet de la
Terre proposait pour la première fois un texte fondateur de 27
principes, intitulé « Déclaration de Rio sur l’environnement et le
développement », précisait la notion de développement durable, et
proposait une série d’action pour le 21ᵉ siècle, l’« Agenda 21 ». Ce
faisant, la renommée de ce Sommet aidant, le message qui était alors
compris par bon nombre d’ONG et autres mouvements était celui d’une
projection générale de l’économie dans un 21ᵉ siècle dont on espérait
qu’il soit le moment d’un rééquilibrage des forces entre l’extraction
capitaliste de la nature et l’avenir de la Terre. La même année le livre
de Fukuyama proposait de voir dans la fin de la Guerre Froide la
victoire définitive de la démocratie libérale sur les idéologies,
c’est-à-dire que la politique était désormais réduite à n’être que le
bras de la nécessité économique instruite par le néolibéralisme
(j’extrapole un peu mais c’est l’idée reçue). Fukuyama n’était pas
célèbre, ce qui le rendit célèbre durant les deux ou trois années
suivantes, c’est la réception qu’a eu son livre dans les milieux
néolibéraux et la caisse de résonance de certains médias, alors même que
la plupart des philosophes regardaient cela d’un air plutôt goguenard
(je m’en souviens un peu, j’étais en fac de philo ces années-là).
Donc deux discours sur le futur s’affrontaient : le premier donnait de
l’espoir dans les politiques publiques, le second privait complètement
les populations de la construction de futurs alternatifs, et c’est ce
qui fut finalement rabâché à partir du slogan (certes un peu plus
ancien) thatchérien « There is no alternative », y compris jusqu’à
aujourd’hui.

La conséquence de cette dissonance touche tous les individus parce que
l’avenir est plus qu’incertain, il devient une menace pour chacun
lorsque la politique n’est plus en mesure de proposer de « plan B », ni
même de négociation, lorsque la « nécessité économique » sert de
justification systématique au recul des conquêtes sociales, là, toute
proposition progressiste d’un parti ou d’une idéologie n’est même plus
crédible (la longue chute du socialisme français en est l’illustration
éclatante).

Comme le philosophe italien Franco Berardi (Bifo) l’affirme dans *Dopo
il futuro* (Berardi 2011), cette lente « annulation de l’avenir » c’est
désormais bien plus que le vieux slogan punk, cela fait plus que toucher
les gens, cela touche la chair et le mental :

> « L’avenir devient une menace lorsque l’imagination collective devient
> incapable d’envisager des alternatives aux tendances qui conduisent à
> la dévastation, à l’augmentation de la pauvreté et de la violence.
> C’est précisément notre situation actuelle, car le capitalisme est
> devenu un système d’automatismes technico-économiques auxquels la
> politique ne peut se soustraire. La paralysie de la volonté
> (l’impossibilité de la politique) est le contexte historique de
> l’épidémie de dépression actuelle. »

Et c’est justement ce contre quoi les oppositions altermondialistes se
battent, à commencer par se réapproprier le futur, répondre à un message
négatif en proposant positivement un avenir en commun. Et c’est dans ce
cadre que les politiques préfiguratives évoluent.

Cette jonction entre la préfigurativité et l’aspiration à un autre
avenir est aux mouvements sociaux le fer de leur conscience
révolutionnaire et d’une liberté retrouvée. C’est pourquoi David Graeber
n’hésite pas à déclarer que la préfigurativité est une arme politique
des plus efficaces, si ce n’est la plus efficace. Elle se retrouve de
*Occupy* à la Zad de N-D des Landes, au Rojavas et au Chiapas, dans les
mouvements en Grèce, en Espagne, en Égypte (les Printemps..), dans les
mouvement indigènes comme au Brésil, etc. D. Graeber écrit dans la
préface à *Éloge des mauvaises herbes: ce que nous devons à la Zad*
(Lindgaard et al. 2018) :

> « La ZAD a gagné contre un très grand projet d’infrastructure. Elle a
> gagné en utilisant l’une des armes politiques les plus puissantes,
> celle de la préfiguration (…). La préfiguration est l’exact contraire
> de l’idée que la fin justifie les moyens. **Plutôt que de calculer
> comment renverser le régime actuel, en formulant l’hypothèse que d’une
> manière ou d’une autre quelque chose de neuf en surgira spontanément,
> vous essayez de faire de la forme de votre résistance un modèle de ce
> à quoi la société à laquelle vous aspirez pourrait ressembler**. Cela
> signifie aussi que vous ne pouvez pas reporter, disons, la question
> des droits des femmes, ou celle de la démocratie interne à "après la
> révolution" : ces questions doivent être traitées dès maintenant. À
> l’évidence, ce que vous obtiendrez ne sera jamais le modèle exact
> d’une future société libre – mais il s’agira au moins d’un ordre
> social qui pourrait exister en dehors de structures de coercition et
> d’oppression. Cela signifie que les gens peuvent avoir une expérience
> immédiate de la liberté, ici et maintenant. Si l’action directe
> consiste pour les activistes à relever avec constance le défi qui
> consiste à agir comme si l’on était déjà libre, la politique
> préfigurative consiste à relever avec constance le défi de se
> comporter les uns vis-à-vis des autres comme nous le ferions dans une
> société véritablement libre.(…) Bien évidemment, la ZAD est une
> expérience à beaucoup plus petite échelle, mais ce qu’elle nous a
> appris, c’est que même au cœur de l’Europe nous pouvons réussir à
> créer des espaces d’autonomie – et même si ce n’est que pour un temps
> limité. Être conscient que de tels lieux existent nous permet de voir
> tout ce que nous faisons sous un jour nouveau : nous sommes déjà des
> communistes lorsque nous travaillons sur un projet commun, nous sommes
> déjà des anarchistes lorsque nous trouvons des solutions aux problèmes
> sans le recours aux avocats ou à la police, nous sommes tous des
> révolutionnaires lorsque nous créons quelque chose de véritablement
> nouveau. »

Et comme il n’y a pas de meilleur argument que l’exemple, dans *Comme si
nous étions déjà libres*, D. Graeber envisage le mouvement Occupy Wall
Street comme une politique préfigurative, en ces termes :

> « Occupy Wall Street s’est d’abord inspiré des traditions de la
> démocratie directe et de l’action directe. Du point de vue anarchiste,
> la démocratie directe et l’action directe sont (ou devraient être)
> deux aspects d’une même idée : la forme de nos actions doit servir de
> modèle ou offrir un aperçu de la façon dont des gens libres peuvent
> s’organiser et de ce à quoi pourrait ressembler une société libre. Au
> début du XXᵉ siècle, c’est ce qu’on appelait "construire la nouvelle
> société dans la coquille de l’ancienne", et dans les années 1980 et
> 1990, on l’a appelé "politique préfigurative". Or, quand les
> anarchistes grecs déclarent : "nous sommes l’avenir", ou que les
> Américains affirment créer une civilisation d’insurgés, ils parlent en
> fait d’une même chose. Nous parlons de cette sphère où l’action
> devient elle-même prophétie. »

L’avenir est important : il est contagieux. Les mouvements
altermondialistes se sont construits en première intention sur l’idée
d’un « contagionnisme », c’est-à-dire l’idée que mettre en place des
dispositifs solidaires ne suffit pas et que les organisations fondées
sur la démocratie directe et des formats non-hiérarchisés peuvent être
des modèles contagieux. En d’autres termes, altermondialiste signifie
surtout que la vision du monde alternative qui est proposée est avant
tout celle où la démocratie s’exerce, ce qui sous-entend que la
mondialisation économique et politique contrevient à la démocratie. La
place laissée à la parole, à l’exercice de l’écoute, à la possibilité
d’obtenir des décisions collectives éclairées et consensuelles font
partie intégrante de l’organisation politique et changent la perception de
ce qu’il est possible de faire et donc d’opposer au système mondialiste
ou capitaliste. Ces formats non hiérarchisés marquent la fin d’une
époque révolue où les mouvements protestataires obéissaient à des jeux
internes de prise de parole, d’affichage et pour finir des jeux de
pouvoir et des têtes d’affiche. On le voit au niveau local où les
autorités et autres administrations ont tendance à rechercher dans une
association un président, un bureau, une forme hiérarchique pour
identifier non pas le mouvement mais les personnes sous prétexte de
responsabilisation de l’action. Une politique de préfiguration ne
cherche pas tant à dé-responsabiliser les individus mais à exercer la
démocratie directe dans les processus de prise de décision et d’action.

Pour citer encore D. Graeber :

> « Depuis le mouvement altermondialiste, l’époque des comités de
> direction est tout à fait révolue. La plupart des militants de la
> communauté en sont arrivés à l’idée de la politique "préfigurative",
> idée selon laquelle la forme organisationnelle qu’adopte un groupe
> doit incarner le type de société qu’il veut créer. »

Tout le reste consiste à créer des opportunités pour que ceux qui sont
ou ne sont pas (pas encore) dans une dynamique militante puissent
s’insérer dans ce type de démarche au lieu d’en rester à une attitude de
retrait ou dans de vieux concepts de la militance.

## Répondre à la question : comment faire ?

Pour le sociologue Luke Yates (Yates 2015), parler de politiques
préfiguratives consiste à porter l’attention sur le fait que les
militants *expriment* les objectifs politiques par les moyens des
actions qu’ils entreprennent. C’est une inversion de l’adage « la fin
justifie les moyens ». Alors que cet adage privilégie l’objectif au
détriment d’autres considérations tout en cherchant à remplacer un état
du monde par un autre, une politique préfigurative ajuste l’action à
l’opposition à un état du monde pour non pas remplacer mais proposer une
alternative dont l’expression et l’avènement est en quelque sorte
déjà-là puisqu’il est expérimenté par le groupe militant, ici et
maintenant, dans un espace géographique et au présent. On retrouve une
part du « on est là » des Gilets Jaunes en France par exemple, ou les
Zad, etc.

L. Yates prend l’exemple des centres sociaux autonomes de Barcelone.
Selon lui, une politique préfigurative n’a de sens qu’en définissant
correctement la logique des constructions d’alternatives qui doivent
elles-mêmes être évaluée au sein du mouvement pour s’assurer de leur
solidité et de leur opérationnalité dans l’avenir. Ainsi L. Yates
identifie cinq composantes de la préfiguration (il les nomme :
« expérimentation », « perspectives », « conduite », « consolidation »
et « diffusion ») :

1.  L’expérimentation collective : concernant les pratiques quotidiennes
    (prise de parole, processus de décision, discussions, partage de
    connaissances, etc.), l’expérimentation consiste aussi à envisager
    collectivement des moyens plus performants pour l’avenir et donc
    émettre une critique des processus en place, ce qui suppose une
    attention permanente et une forme de conscience de soi.
2.  Perspectives et imagination : des cadres de création (réunions,
    séminaires, avec principes de fonctionnement, slogan, éducation
    populaire, etc.) et des concepts politiques qui cherchent à définir
    le mouvement mais aussi à communiquer ces idées en interne comme
    vers l’extérieur.
3.  Conduite : des normes qui émanent des expérimentations et des
    perspectives politiques qui s’ouvrent. Alors qu’on retient souvent
    des mouvements sociaux leurs expérimentations et leurs cadres de
    gouvernance collective, il ne faut pas oublier qu’en réalité les
    résultats de ces expérimentations créent des nouvelles routines et
    de nouveaux cadres de gouvernance qui ne sont pas seulement des
    choix tactiques de militance mais peuvent définir une réorientation
    du mouvement (par exemple à Framasoft, notre orientation vers
    l’éducation populaire).
4.  Consolidation : le mouvement doit aussi consolider ces normes, idées
    et cadres dans l’infrastructure elle-même (division de l’espace,
    cuisine commune, partage de biens, systèmes de communication, etc.)
    et cette infrastructure est aussi le reflet des valeurs partagées
    autant qu’elles les illustrent.
5.  Diffusion : se projeter au-delà du présent et du lieu pour diffuser
    (manifestations publiques, médias, protestations, actions directes
    militantes, *happenings* etc.) et expliquer ces idées vers d’autres
    réseaux, groupes, collectifs, publics.

Pour ma part, je rejoins L. Yates sur ces composantes, mais je pense
plutôt la politique préfigurative comme une dialectique entre, d’un côté
les moyens et actions préfiguratifs eux-mêmes, et de l’autre côté, la
déconstruction systématique de la vision du monde contre laquelle on
oppose une alternative. En d’autres termes une politique préfigurative
n’a de sens aujourd’hui que si elle participe à la déconstruction autant
conceptuelle qu’opérationnelle du capitalisme. D’un point de vue
conceptuel, l’action directe démontre opérationnellement les travers du
capitalisme (ou du néolibéralisme) mais il faut encore conceptualiser
pour transformer l’action en discours qui entre alors en dialectique et
justifie l’action (tout comme le capitalisme crée des discours qui
l’auto-justifient, par exemple la notion de progrès). Du point de vue
opérationnel, en retour, l’opposition se fait frontalement et entre en
collision avec les forces capitalistes. Cela peut donner lieu à des
tensions et affrontements selon le degré que ressentent les autorités et
le pouvoir dans l’atteinte à l’ordre social qui se définit par rapport
au capitalisme (et non par rapport à des valeurs : si l’action ne
remettait en cause que des valeurs, alors la discussion serait toujours
possible).

## Questions en vrac pour finir

Et le vote ? Dans beaucoup de mouvements préfiguratifs, le vote est un
des instruments privilégiés de la démocratie directe, même s’il n’en est
qu’un des nombreux instruments. Ma crainte, toujours lorsqu’il s’agit de
vote, est d’y voir une tendance quelque peu paresseuse à accepter qu’un
pouvoir soit délégué à quelques-uns, ce qui est toujours une brèche
ouverte à des jeux de domination. Selon moi, le vote est à considérer
par défaut comme l’échec du consensus. Certes, un consensus est parfois
difficile à obtenir : peut-être que dans ce cas, il faut se poser la
bonne question, ou bien la poser autrement, c’est-à-dire se demander
pourquoi le consensus est absent ? Certains n’hésiteront pas à rétorquer
que si l’on veut préfigurer le monde futur, le vote est un instrument
efficace face à la difficulté d’obtenir consensus de la multitude des
individus et sur un temps raccourci. Je pense qu’il faut prendre le
temps lorsqu’il le faut parce qu’en réalité, une fois que la dynamique
consensuelle est installée seule les plus grandes décisions prennent du
temps. Par ailleurs, un vote est la réponse à une question qui est en
soi une réduction des enjeux qui ne peuvent alors plus être discutés.

La lente « annulation de l’avenir » que mentionne Franco Berardi,
n’est-elle pas aussi une conséquence de la dépossession systématique
dont procède le capitalisme ? Cela rejoint mes réflexions en cours sur
les justifications du capitalisme. Il faut entendre la dépossession de
deux manières : dépossession du travail et de l’expertise par la
dynamique techno-capitaliste, mais aussi la dépossession de nos
savoir-être et savoir-faire (ce que B. Stiegler appelait notre
prolétarisation). La préfiguration pourrait-elle être un remède ou
est-elle un pharmakon ?

Ce que nous vivons dans les mouvements préfiguratifs est à la fois riche
et intimidant. Dans les dynamiques internes des mouvements, se pose
souvent la question de la légitimité des personnes. Légitimité à prendre
la parole, à proposer son expertise et son savoir-faire, etc. Autant on
peut toujours se prévaloir d’une écoute partagée et bienveillante autant
il est difficile de garantir à chacun un espace où l’on peut s’exprimer
et se sentir légitime à le faire, au risque de l’exclusion du groupe.
Comment y remédier ? Quelles sont les conséquences : est-ce que la
portée préfigurative revendiquée n’est pas elle-même la source du
ressenti personnel d’un manque de légitimité chez certains
participant-es ?

Préfiguration et syndicalisme… Lors du mouvement contre la réforme des
retraites en France en 2023, le rythme des manifestations obéissait
essentiellement à l’Inter-syndicale. Au mois de mai, certains syndicats
(notamment la CFDT) ont finalement accepté le rendez-vous avec la
Première Ministre. Pire : ils ont accepté la règle imposée des
rendez-vous séparés, c’est-à-dire la fin de l’Inter-syndicale. Alors
même que la plupart des manifestants voyaient dans le fait de décliner
les rendez-vous successifs comme une preuve de résistance dans le
rapport de force entre gouvernement et syndicats. On comprend alors
beaucoup mieux pourquoi les étudiants, dans la plupart des
manifestations, cherchaient non seulement à défiler séparément, mais
aussi à ne pas obéir au coup de sifflet syndicaliste à la fin des
manifestations et poursuivre de manière apparemment désordonnée la
démonstration de leur mobilisation. Sur beaucoup de campus, c’est bien
la préfigurativité qui prévaut, ne serait-ce que dans les assemblées
générales. Le syndicalisme me semble malheureusement hors jeu de ce
point de vue : grèves et manifestations ne suffisent plus face à la
« nécessité » de l’ordre économique asséné par le pouvoir et ses
institutions. Après la grève, l’ordre du monde et des institutions est
rétabli, or ce qui est en jeu aujourd’hui (et l’urgence climatique n’en
est qu’un levier) c’est justement la recherche d’un nouvel ordre
économique, culturel et philosophique. Si les politiques préfiguratives
sont invisibilisées, il est à craindre que le nouvel ordre soit en fait
une radicalisation du pouvoir. L’adage n’est plus « faire et faire tout
de même », il est devenu : « faire, faire sans eux, faire contre eux ».

## Bibliographie

<div id="refs" class="references csl-bib-body hanging-indent">

<div id="ref-angaut2007" class="csl-entry">

Angaut, Jean-Christophe. 2007. “Le conflit Marx-Bakounine dans
l’internationale : une confrontation des pratiques politiques.” *Actuel
Marx* 41 (1): 112–29. <https://doi.org/10.3917/amx.041.0112>.

</div>

<div id="ref-berardi2011" class="csl-entry">

Berardi, Franco. 2011. *After the future*. Edited by Gary Genosko and
Nicholas Thoburn. Edinburgh, Royaume-Uni de Grande-Bretagne et d’Irlande
du Nord.

</div>

<div id="ref-boggs1977" class="csl-entry">

Boggs, Carl. 1977a. “Marxism, Prefigurative Communism, and the Problem
of Workers’ Control.” *Radical America* 11 (6): 99–122.
<https://theanarchistlibrary.org/library/carl-boggs-marxism-prefigurative-communism-and-the-problem-of-workers-control>.

</div>

<div id="ref-boggs1977a" class="csl-entry">

———. 1977b. “Revolutionary Process, Political Strategy, and the Dilemma
of Power.” *Theory and Society* 4 (3): 359–93.
<https://www.jstor.org/stable/656724>.

</div>

<div id="ref-breines1989" class="csl-entry">

Breines, Wini. 1989. *Community and Organization in the New Left,
1962-1968: The Great Refusal*. New edition. New Brunswick N.J.: Rutgers
University Press.

</div>

<div id="ref-eckhardt2016" class="csl-entry">

Eckhardt, Wolfgang. 2016. *First Socialist Schism: Bakunin vs. Marx in
the International Working Men’s Association*. Oakland: PM Press.
<https://theanarchistlibrary.org/library/wolfgang-eckhardt-the-first-socialist-schism>.

</div>

<div id="ref-kann1983" class="csl-entry">

Kann, Mark E. 1983. “The New Populism and the New Marxism: A Response to
Carl Boggs.” *Theory and Society* 12 (3): 365–73.
<https://www.jstor.org/stable/657443>.

</div>

<div id="ref-leach2013" class="csl-entry">

Leach, Darcy K. 2013. “Prefigurative Politics.” In *The Wiley-Blackwell
Encyclopedia of Social and Political Movements*. John Wiley & Sons, Ltd.
<https://doi.org/10.1002/9780470674871.wbespm167>.

</div>

<div id="ref-lindgaard2018" class="csl-entry">

Lindgaard, Jade, Olivier Abel, Christophe Bonneuil, Patrick Bouchain,
and David Graeber. 2018. *Éloge Des Mauvaises Herbes: Ce Que Nous Devons
à La ZAD*. Paris, France: Éditions les Liens qui libèrent.

</div>

<div id="ref-malabou2022" class="csl-entry">

Malabou, Catherine. 2022. *Au Voleur !: Anarchisme Et Philosophie*.
Paris, France: PUF.

</div>

<div id="ref-murray2014" class="csl-entry">

Murray, Daniel. 2014. “Prefiguration or Actualization? Radical Democracy
and Counter-Institution in the Occupy Movement.” *Berkeley Journal of
Sociology*, en ligne, November.
<https://berkeleyjournal.org/2014/11/03/prefiguration-or-actualization-radical-democracy-and-counter-institution-in-the-occupy-movement/>.

</div>

<div id="ref-raekstad2018" class="csl-entry">

Raekstad, Paul. 2018. “Revolutionary Practice and Prefigurative
Politics: A Clarification and Defense.” *Constellations* 25 (3): 359–72.
<https://doi.org/10.1111/1467-8675.12319>.

</div>

<div id="ref-renou2020" class="csl-entry">

Renou, Gildas. 2020. “Exemplarité et mouvements sociaux.” In
*Dictionnaire des mouvements sociaux*, 2e éd.:244–51. Références. Paris:
Presses de Sciences Po.
<https://doi.org/10.3917/scpo.filli.2020.01.0244>.

</div>

<div id="ref-vandesande2015" class="csl-entry">

Sande, Mathijs van de. 2015. “Fighting with Tools: Prefiguration and
Radical Politics in the Twenty-First Century.” *Rethinking Marxism* 27
(2): 177–94. <https://doi.org/10.1080/08935696.2015.1007791>.

</div>

<div id="ref-sitrin2012" class="csl-entry">

Sitrin, Marina A. 2012. *Everyday Revolutions: Horizontalism and
Autonomy in Argentina*. 1st edition. Zed Books.

</div>

<div id="ref-yates2015" class="csl-entry">

Yates, Luke. 2015. “Rethinking Prefiguration: Alternatives,
Micropolitics and Goals in Social Movements.” *Social Movement Studies*
14 (1): 1–21. <https://doi.org/10.1080/14742837.2013.870883>.

</div>

</div>

## Notes

[^1]: « By “prefigurative”, I mean the embodiment, within the ongoing
    political practice of a movement, of those forms of social
    relations, decision-making, culture, and human experience that are
    the ultimate goal. Developing mainly outside Marxism, it produced a
    critique of bureaucratic domination and a vision of revolutionary
    democracy that Marxism generally lacked. »

[^2]: Pour résumer, il s’agit de dire que la domination, ou l’hégémonie,
    du capitalisme n’est pas un fait qui est indépendant des organes de
    l’État, mais au contraire ce sont les institutions qui l’organisent
    et perpétuent les rapports de force, quels que soient les partis et
    leurs bonnes intentions idéologiques. On retrouve cela aussi chez L.
    Althusser. En d’autres termes, si Marx a pensée la société de
    manière totale, il a aussi montré que les rapports de force sont
    structurés de manière complexe et dans cette complexité, on
    reconnaît des dominantes (par exemple certaines organisations
    locales peuvent avoir du pouvoir tandis que l’institution centrale a
    tendance à l’écraser). Il y a aussi ce concept d’ « autonomie
    relative » des institutions : certaines peuvent pour un temps se
    saisir légitimement des questions sociales, notamment en raison du
    jeu de pouvoirs entre partis politiques, mais en définitive l’État
    impose la domination capitaliste.

[^3]: La plupart des auteurs s’accordent sur le fait que les nombreux
    mouvements qu’on regroupe sous le terme « Nouvelle gauche » des
    années 1960-1970 peuvent être analysés comme des politiques
    préfiguratives. Cela est surtout dû à l’analyse qu’en donne la
    sociologue Wini Breines dans l’étude de cas qu’elle a consacré sur
    le Free Speech Movement, et le Students for a Democratic Society
    (SDS) (Breines 1989). On peut cependant noter que si cet ouvrage
    porte essentiellement sur les États-Unis des années 1960, il a fait
    école. Ainsi Marina Sitrin reprend l’approche dans son étude sur les
    mouvements argentins (Sitrin 2012).

[^4]: On peut se reporter à cet excellent article de Jean-Christophe
    Angaut: « Le conflit Marx-Bakounine dans l’internationale : une
    confrontation des pratiques politiques » (Angaut 2007), et pour
    vraiment approfondir, le livre de Wolfgang Eckhardt, sur le conflit
    Marx *vs* Bakounine dans l’Association Internationale des
    Travailleurs (Eckhardt 2016).
