---
title: "Avant un nouvel été de catastrophes climatiques, parlons de vraies solutions"
date: 2024-06-02
author: "Christophe Masutti"
image: "/images/postimages/cm.png"
description: "Voici la traduction d'un texte de Peter Gelderloos paru sur Crimethinc le 08 mai 2024. J'ai déjà eu l'occasion de traduire un texte de P.G. en 2019. Celui-ci est court et même si je crois que l'auteur néglige des points importants (je pense à nos rapports avec les technologies de l'information et la surveillance), j'adhère en grande partie à son approche. Nous sommes actuellement en pleine période électorale Européenne, et pour ces mêmes raisons je suis convaincu qu'il n'y a guère d'espoir politique dans les logiques de partis, bien au contraire. Une attitude anarchiste (et préfigurative) me semble être une voie de sortie souhaitable."
tags: ["Libres propos", "anarchisme", "anticapitalisme", "environnement"]
categories:
- Libres propos
---

Voici la traduction d'un texte de Peter Gelderloos paru sur [Crimethinc](https://fr.crimethinc.com/2024/05/08/ahead-of-another-summer-of-climate-disasters-lets-talk-about-real-solutions) le 08 mai 2024. J'ai déjà eu l'occasion de traduire [un texte de P.G.](https://fr.crimethinc.com/2018/11/05/perspectives-davenir-entre-la-crise-de-la-democratie-et-la-crise-du-capitalisme-quelques-previsions) en 2019. Celui-ci est court et même si je crois que l'auteur néglige des points importants (je pense à nos rapports avec les technologies de l'information et la surveillance), j'adhère en grande partie à son approche. Nous sommes actuellement en pleine période électorale Européenne, et pour ces mêmes raisons je suis convaincu qu'il n'y a guère d'espoir politique dans les logiques de partis, institutionnelles ou de plaidoyer, bien au contraire. Un attitude anarchiste (et préfigurative) me semble être une voie de sortie souhaitable.

<!--more-->
<hr>

**Introduction sur Crimethinc :**

En coopération avec [Freedom](https://freedomnews.org.uk/), nous présentons un court texte de Peter Gelderloos explorant les raisons de l'échec des stratégies actuellement employées par les principaux mouvements environnementaux pour stopper le changement climatique d'origine industrielle, et ce que nous pourrions faire à la place. Pour un examen plus approfondi de ces questions, nous recommandons le nouveau livre de Peter, [_The Solutions are Already Here : Strategies for Ecological Revolution from Below_](https://www.plutobooks.com/9780745345116/the-solutions-are-already-here).

<hr>

## Avant un nouvel été de catastrophes climatiques, parlons de vraies solutions

Le mouvement climatique dominant part d'un postulat qui garantit l'échec.

Pas seulement l'échec. Une catastrophe. Et plus il sera efficace, plus il causera de dégâts.

Voyons pourquoi.

----

### Le réductionnisme climatique

Aujourd'hui, lorsque les gens pensent à l'environnement, ils se représentent généralement des actions de désobéissance civile dans les rues, un militantisme médiatique, un lobbying enthousiaste et des conférences visant à fixer des objectifs mondiaux en matière d'émissions de carbone, le tout sous la houlette d'organisations non gouvernementales, d'universitaires et de politiciens progressistes. Cependant, la lutte écologique a toujours inclus des courants anticapitalistes et anticoloniaux, et ces courants sont devenus plus forts, plus dynamiques et mieux connectés au cours des deux dernières décennies.

Cette évolution ne s'est toutefois pas faite sans revers, souvent en raison d'une intense répression qui laisse les mouvements épuisés et traumatisés, comme la « [peur verte](https://crimethinc.com/2008/02/22/green-scared) » (_green scare_[^0]) qui a débuté en 2005 ainsi que la répression de [Standing Rock](https://truthout.org/articles/settler-state-repression-standing-rock-battles-continue-in-the-courts/) et d'autres mouvements anti-pipelines menés par des populations indigènes dix ans plus tard. Systématiquement, au moment précis où les courants radicaux pansent leurs plaies, la vision de l'environnementalisme, majoritairement blanche et issue de la classe moyenne, prend le devant de la scène et entraîne le débat dans des directions réformistes[^1].

La crise à laquelle nous sommes confrontés est une crise écologique complexe, dans laquelle s'enchevêtrent les assassinats par les forces de police, les lois répressives, l'histoire du colonialisme et du suprématisme blanc, la dégradation de l'habitat, l'accaparement des terres, l'agriculture alimentaire, la santé humaine, l'urbanisme, les frontières et les guerres. Les principaux leaders du mouvement environnementaliste ont pris la décision stratégique de réduire tout cela à une question de climat ­— la crise climatique — et de positionner l'État en tant que protagoniste, en tant que sauveur potentiel. Cela signifie présenter l'Accord de Paris et les sommets de la COP comme les solutions au problème, et utiliser l'activisme performatif et la désobéissance civile pour exiger des changements de politique et des investissements en faveur de l'énergie verte.


<figure>
<img src="/images/billettradgelderloos20240602/7.jpg">
<figcaption><em>Le climat se réchauffe, indépendamment des sommets internationaux sur le climat qui ne sont d'aucune utilité</em>.</figcaption>
</figure>


### Un échec prévisible

Les deux piliers de leur stratégie pour résoudre la crise climatique sont, d'une part, l'augmentation de la production d'énergie verte et, d'autre part, la réduction des émissions de carbone.

Ils ont été très efficaces pour atteindre le premier objectif, mais totalement inefficaces pour le second. C'était tout à fait prévisible.

Quiconque comprend le fonctionnement de notre société, c'est-à-dire le fonctionnement du capitalisme, sait que la conséquence logique d'une augmentation des investissements dans les énergies vertes sera une augmentation de la production de combustibles fossiles. La raison principale en est que les centaines de milliards de dollars qui ont déjà été investis dans les pipelines, les mines de charbon, les raffineries de pétrole et les puits de forage sont du capital fixe. Ils valent beaucoup d'argent, mais ce n'est pas de l'argent sur un compte bancaire qui peut être rapidement investi ailleurs, transformé en actions ou en biens immobiliers ou encore converti dans une autre devise.

Une excavatrice à charbon de 14 000 tonnes, une plateforme pétrolière offshore : elles ne deviendront jamais quelque chose d'autre d'une valeur financière similaire. C'est de l'argent qui a été dépensé, un investissement qui n'est utile aux capitalistes que s'ils peuvent continuer à l'utiliser pour extraire du charbon ou forer du pétrole.

Cette règle économique prévaut, que l'entreprise capitaliste en question soit ExxonMobil, la compagnie pétrolière d'État saoudienne ou la China Petrochemical Corporation, propriété du parti communiste (qui a été classée plus grande entreprise énergétique du monde en 2021).

Le capitalisme (y compris celui pratiqué par tous les gouvernements socialistes du monde) est basé sur la croissance. Si les investissements dans les énergies vertes augmentent, entraînant une hausse de la production totale d'énergie, le prix de l'énergie diminuera, ce qui signifie que les grands fabricants produiront davantage de marchandises, quelles qu'elles soient, rendant leurs produits moins chers dans l'espoir que les consommateurs en achèteront davantage. Par conséquent, la consommation totale d'énergie augmentera. Cela s'applique à l'énergie provenant de toutes les sources disponibles, en particulier les plus traditionnelles, à savoir les combustibles fossiles.


<figure>
<img src="/images/billettradgelderloos20240602/6.jpg">
<figcaption> </figcaption>
</figure>


Après des décennies d'investissement, l'énergie verte deviendra enfin concurrentielle, voire moins chère que l'énergie produite à partir de combustibles fossiles. Cela n'a débuté qu'au cours des dernières années, bien que les prix fluctuent encore en fonction de la région et du type de production d'énergie. [L'industrie des combustibles fossiles](https://www.theguardian.com/us-news/2024/mar/04/exxon-chief-public-climate-failures) n'a pas abandonné ses activités ni diminué sa production. De nombreuses entreprises ne couvriront même pas leurs investissements entre les combustibles fossiles et les énergies vertes. En revanche, elles investiront davantage dans de nouveaux projets liés aux combustibles fossiles. C'est l'économie capitaliste de base : si le prix marginal d'un produit diminue, le seul moyen de maintenir ou d'augmenter ses bénéfices est d'accroître la production totale. Cela explique pourquoi 2023 a été une année record pour [les nouveaux projets de combustibles fossiles](https://phys.org/news/2023-11-hundreds-oil-gas-climate-crisis.html).

Il existe une autre façon d'augmenter les profits : en diminuant les coûts de production. Pour l'industrie des combustibles fossiles, cela se traduit par une réduction des normes de sécurité et environnementales, ce qui signifie plus d'accidents, plus de pollution, plus de morts.

Nous l'avons vu venir. Nous avons dit que cela arrivait. Et nous avons été exclus du débat, et dans de nombreux cas tués ou emprisonnés, parce que le besoin pathétique de croire que le gouvernement peut nous sauver est encore plus grand que l'addiction aux combustibles fossiles.

Mais le capitalisme n'a pas d'avenir sur cette planète. Nous aurons besoin d'une révolution de grande envergure pour faire face à cette crise.



<figure>
<img src="/images/billettradgelderloos20240602/3.jpg">
<figcaption><em>Pour chaque barre, la barre bleu clair à gauche représente l'énergie « propre » ; la barre bleu foncé à droite représente les combustibles fossiles</em>. Source : rapport <a href="https://www.iea.org/reports/world-energy-investment-2023">The World Energy Investment 2023]</a>.</figcaption>
</figure>

<figure>
<img src="/images/billettradgelderloos20240602/2.jpg">
<figcaption><em>Bien que les investissements annuels dans les énergies « vertes » <a href="https://about.bnef.com/blog/global-clean-energy-investment-jumps-17-hits-1-8-trillion-in-2023-according-to-bloombergnef-report/">augmentent significativement</a>, la production et la consommation de combustibles fossiles continuent également d'augmenter</em>. Source : <a href="https://ourworldindata.org/fossil-fuels">Statistical Review of World Energy 2023</a>.</figcaption>
</figure>


### Alors, que faire ?

Nous devons réorienter le débat. Nous devons **adopter une posture** qui nous permette d'être prêts pour le long terme. Nous devons **soutenir les luttes** qui peuvent apporter de petites victoires et accroître notre pouvoir collectif, et **approfondir notre relation avec le territoire qui peut nous soutenir**. Par-dessus tout, nous devons imaginer des **avenirs meilleurs** que celui qu'ils nous réservent.

#### Parler

Le type de transformation sociale — de révolution mondiale — qui peut guérir les blessures que nous avons infligées à la planète elle-même et à tous ses systèmes vivants devra être plus ambitieux que tout ce que nous avons connu jusqu'à présent. Cette crise nous prend tous au piège et nuit à tout un chacun ; la réponse devra être apportée par le plus grand nombre possible d'entre nous.

Imaginez toutes les personnes de votre entourage dont vous ne voulez pas qu'elles meurent de faim ou de cancer, qu'elles soient soumises à des conditions météorologiques extrêmes ou qu'elles soient abattues par la police ou autres suprémacistes blancs.

Vous n'avez pas besoin de convaincre toutes ces personnes de devenir des révolutionnaires anarchistes. Il suffirait d'en convaincre certaines de rompre leur loyauté envers les institutions dominantes et les mouvements réformateurs classiques et de sympathiser avec une approche révolutionnaire, ou du moins de comprendre pourquoi une telle approche a du sens.

Pour ce faire, vous pouvez poser une question dont la réponse est incontestable, une question qui a un rapport direct avec un sujet qui les affecte ou les motive. Par exemple :

- Combien de personnes meurent chaque année à cause du manque d'eau potable, de la famine, des conditions climatiques, de la pollution de l'air et autres causes liées à la crise écologique ? **Au moins 10 à 20 millions chaque année, et ce chiffre ne cesse d'augmenter**.
- Depuis 2017, les investissements dans les énergies renouvelables augmentent chaque année. En 2022, les investissements dans les énergies renouvelables seront 15 fois plus importants qu'en 2004. Cela a-t-il été rentable pour les investisseurs ? **Oui. Les investissements annuels s'élèvent à plus de mille milliards de dollars et les bénéfices à plus de cent milliards, même si les investisseurs ont montré qu'ils [retiraient rapidement leur argent des énergies vertes](https://www.reuters.com/sustainability/climate-energy/renewables-funds-see-record-outflows-rising-rates-costs-hit-shares-2023-10-09/) lorsque les marges bénéficiaires diminuaient**. Qu'est-il advenu des émissions mondiales de CO2 au cours de cette même période ? **Elles ont grimpé d'un tiers**. Et la production de combustibles fossiles au cours de la même période ? **Elle a augmenté de 40 %**. Ces chiffres correspondent-ils à peu près aux taux d'augmentation des émissions de carbone et de la production de combustibles fossiles au cours des décennies précédentes ? Oui. Et qu'est-ce que cela signifie ? **L'explosion des investissements dans les énergies vertes n'a en rien ralenti la production de combustibles fossiles et les émissions de carbone, même si les nouveaux projets d'extraction de combustibles fossiles deviennent plus difficiles et plus coûteux**.
- Notre eau, notre air et nos aliments sont chargés de produits chimiques toxiques. Nombre d'entre eux sont liés à la production de plastiques, de pesticides, de substances chimiques persistantes ([PFAS](https://fr.wikipedia.org/wiki/Substances_per-_et_polyfluoroalkyl%C3%A9es)), à l'exploitation minière et à la combustion de carburants fossiles. Nous connaissons les dangers de la plupart de ces composés depuis des décennies, et plusieurs d'entre eux sont interdits ou réglementés par divers gouvernements. Dans l'ensemble, les quantités de ces toxines présentes dans notre environnement augmentent-elles ou diminuent-elles ? **Elles augmentent**. Qu'ont fait de nombreuses grandes entreprises chimiques en réponse à l'interdiction de l'[APFO](https://fr.wikipedia.org/wiki/Acide_perfluorooctano%C3%AFque), un « produit chimique éternel » toxique ? **Elles se sont tournées vers la production d'autres PFAS dont on sait ou dont on pense qu'ils sont toxiques**. Savons-nous si ces interdictions sont effectivement respectées ? **Cinq ans après avoir accepté de retirer progressivement l'APFO sous la pression du gouvernement, les usines chimiques de DuPont continuaient à rejeter de l'APFO dans les eaux souterraines. C'est probablement encore le cas aujourd'hui, mais les populations concernées n'ont pas les moyens de s'en rendre compte et le gouvernement ne surveille pas ces rejets**.
- Examinons un sujet analogue pour voir si un tel réformisme a donné des résultats dans d'autres contextes. En 2020, les villes et les États américains ont cherché à apaiser le mouvement contre les meurtres commis par la police en adoptant des mesures visant à garantir la responsabilité de la police, qu'il s'agisse de formations à la sensibilité raciale, de commissions de surveillance citoyenne, de lignes directrices plus strictes sur l'usage de la force ou de caméras corporelles obligatoires. Le nombre d'homicides commis par la police a-t-il diminué depuis lors ? **Non, il a augmenté**.

Après avoir fait part des réponses à ces questions, vous pouvez insister sur le fait que la réforme du système existant est une stratégie qui a échoué, et demander à vos interlocuteurs s'ils comptent essayer la même stratégie encore et encore, en espérant des résultats différents.

Cela devrait vous permettre de déterminer quelles sont les personnes autour de vous qui sont capables de remettre en question le paradigme dans lequel elles vivent, et quelles sont celles qui sont attachées aux fausses croyances qui sous-tendent ce paradigme. Ne perdez pas votre temps avec ce dernier groupe. Quelles que soient les velléités de rédemption et les belles valeurs qu'elles peuvent avoir, essayer de dialoguer avec ces personnes par le biais de la raison, de l'éthique et de la logique, c'est passer à côté de l'essentiel. Lorsque des gens s'obstinent à croire des choses dont la fausseté a été démontrée, c'est soit parce que ces croyances les réconfortent, soit parce qu'elles leur apportent pouvoir et profit. Il est peu probable que le débat puisse changer cela.

Nous devons faire évoluer la discussion au niveau de la société dans son ensemble. Nous avons besoin que les gens comprennent nos arguments ; nous devons nous assurer que les orthodoxies dominantes soient considérées comme controversées et non acceptables.


Cela signifie qu'il faut discréditer l'Accord de Paris, les Nations unies, Extinction Rebellion et les grandes ONG, ainsi que toute la stratégie consistant à remplacer les combustibles fossiles par des énergies vertes tout en laissant le système économique mondial inchangé. La seule chose qu'ils arriveraient à faire, c'est de gagner beaucoup d'argent. De même, nous devrions promouvoir une compréhension plus claire de la [fonction de la police](https://www.akpress.org/our-enemies-in-blue.html) dans le contexte historique, de l'[impact](https://us.macmillan.com/books/9780374602529/inflamed) de la production économique basée sur la croissance sur notre santé et du fait qu'aucun gouvernement n'est susceptible de prendre des mesures pour atténuer l'un ou l'autre de ces méfaits.

Concentrons-nous sur les personnes qui sont capables de changer. Lorsque les gens commencent à changer d'avis, il est utile qu'ils puissent faire le lien avec un changement immédiat dans leurs actions. Aidez-les à trouver un petit geste à leur mesure. Par exemple :

- Réorienter les dons aux grandes ONG vers des fonds de défense juridique des défenseurs de la terre, vers des collectes pour des projets de défense de la terre, et pour des médias et éditeurs alternatifs qui présentent une vision objective de la crise ;
- Écrire une lettre à une personne emprisonnée pour sabotage dans un but écologique ou pour s'être défendue contre la police, ou à une personne qui cherche à obtenir un meilleur traitement et des moyens de survie à l'intérieur du système carcéral ;
- Diffuser sur les médias sociaux des informations sur les luttes pour la défense des terres Indigènes dans le monde entier ;
- Réagir aux campagnes conventionnelles de sensibilisation à l'environnement ou au cadre des Nations Unies sur le changement climatique, en soulignant qu'il s'agit d'une escroquerie et en renvoyant à des articles destinés à une large diffusion, comme [celui-ci](https://petergelderloos.substack.com/p/why-green-energy-wont-help-stop-climate) ;
- Demander aux bibliothèques et aux librairies locales de commander des livres qui présentent une vision honnête de la crise écologique ;
- Créer un groupe de lecture avec des amis ;
- Assister à une manifestation ;
- Soutenir un jardin communautaire local, un point de distribution de nourriture ou de vêtements gratuits, un groupe de réduction des risques ou une initiative de justice réparatrice ;
- Transformer une pelouse en un jardin de fleurs sauvages et de plantes comestibles autochtones ;
- Expérimenter la [guérilla jardinière](https://fr.wikipedia.org/wiki/Guerrilla_gardening).



<figure>
<img src="/images/billettradgelderloos20240602/5.jpg">
<figcaption><em>South Central Farm. Mentionné dans <a href="https://crimethinc.com/journals/rolling-thunder/4">Rolling Thunder #4</a>, ce jardin de Los Angeles a nourri des centaines de familles, préservant un espace vert dans la friche urbaine</em>.</figcaption>
</figure>


#### Être honnête

L'apocalypse a déjà commencé. Depuis des décennies, des millions d'humains — et maintenant des dizaines de millions d'humains — meurent chaque année des effets de cette crise écologique. Nous avons dépassé les taux de mortalité des pires années de la Seconde Guerre mondiale et de l'Holocauste, même si nous ne comptons pas les chiffres des victimes des guerres chaudes que les puissances suprématistes blanches mènent du Niger à la Palestine — bien que ces guerres soient également liées à cette crise.

En outre, un nombre inconnu d'espèces — probablement des milliers — sont condamnées à l'extinction chaque année. De nombreux habitats et écosystèmes disparaissent à jamais. La biomasse globale, c'est-à-dire la masse totale de tous les êtres vivants sur la planète, diminue considérablement. L'eau, l'air et le sol sont remplis de poisons. Les objectifs climatiques de réduction des émissions de carbone sont probablement trop optimistes ; nous avons déjà franchi de nombreux points de basculement à 26 ans de 2050 (l'objectif de l'ONU pour atteindre l'objectif « zéro émission nette »), et les projections des États les plus puissants et des plus grandes entreprises indiquent que nous ne parviendrons pas à respecter la date butoir tant souhaitée de 2050. La fin d'un monde est déjà en marche.

Pour faire ce que nous avons à faire, nous devons accepter cette réalité et nous y atteler. La souffrance est déjà là. La mortalité massive est déjà là. Mais après chaque mort, il y a une nouvelle vie, et il y aura encore de la vie sur cette planète jusqu'à la dilatation du soleil dans quelques milliards d'années. C'est une question de vie ou de mort pour nous, et nous devons donc la prendre au sérieux, faire des sacrifices, mais comme il est déjà « trop tard », nous pouvons nous concentrer sur des cadrages qualitatifs et à long terme, plutôt que de nous laisser guider par une urgence trop superficielle et épuisante.

Une chose au moins est certaine : les communautés vivantes de cette planète se porteront beaucoup mieux si nous abolissons l'État et le capitalisme. Si nous n'y parvenons pas de notre vivant, elles se porteront quand même mieux — nous nous porterons mieux — si nous érodons leur hégémonie, si la plupart des gens peuvent voir que les institutions dominantes sont responsables de ce qui se passe, si nous avons augmenté notre capacité de guérison et de survie collective.


<figure>
<img src="/images/billettradgelderloos20240602/1.jpg">
<figcaption> </figcaption>
</figure>



#### Commencer

Il existe de nombreuses façons de soutenir une lutte. Bien qu'il soit facile de se démoraliser lorsque la plupart des pipelines, bases militaires, mines et autres mégaprojets auxquels nous nous opposons sont néanmoins construits, il est vital de s'engager. La révolution n'est pas une progression linéaire — ce n'est pas un millier de petites victoires qui s'accumulent en une grande victoire. Oui, il est nécessaire de montrer que nous pouvons parfois gagner, mais il s'agit aussi de la joie et de l'expérience que nous emportons avec nous, des instincts tactiques et stratégiques que nous développons, du savoir-faire technique, des relations que nous construisons, de la jubilation à forcer la police à tourner les talons pour s'enfuir, de la conscience que les figures d'autorité à l'intérieur et à l'extérieur du mouvement ne font que nous entraver, de la façon dont, dans la lutte, il devient clair que sont liées entre elles toutes les questions qui sont cloisonnées et toutes les formes d'oppression.

Nous devons nous engager dans des luttes intermédiaires de manière à aider les gens à découvrir et à pratiquer les types de tactiques et de stratégies qui seront nécessaires pour un changement à long terme.

De nombreuses luttes menées au cours des dernières décennies nous ont donné de l'énergie et nous ont appris des leçons que nous ne devrions jamais oublier : les insurrections à [Oaxaca](https://crimethinc.com/2006/11/18/oaxaca-timeline), [en Grèce](https://crimethinc.com/2008/12/25/how-to-organize-an-insurrection), [en France](https://crimethinc.com/2018/12/14/the-yellow-vest-movement-showdown-with-the-state-reports-from-the-clashes-in-paris-around-france-and-across-europe), [à Hong Kong](https://crimethinc.com/2019/09/20/three-months-of-insurrection-an-anarchist-collective-in-hong-kong-appraises-the-achievements-and-limits-of-the-revolt) et [au Chili](https://crimethinc.com/2020/10/15/chile-looking-back-on-a-year-of-uprising-what-makes-revolt-spread-and-what-hinders-it), les assemblées décentralisées du [mouvement d'occupation des places](https://crimethinc.com/2020/06/17/snapshots-from-the-uprising-accounts-from-three-weeks-of-countrywide-revolt), l'antiracisme sans compromis des [rébellions anti-policières](https://crimethinc.com/2020/06/17/snapshots-from-the-uprising-accounts-from-three-weeks-of-countrywide-revolt), la joyeuse reconquête de l'espace public exprimée par [Reclaim the Streets](https://crimethinc.com/2021/11/30/epilogue-on-the-movement-against-capitalist-globalization-22-years-after-n30-what-it-can-teach-us-today), les occupations de forêts de [Hambach](https://crimethinc.com/2015/06/15/37-the-hambacher-forest-occupation) à [Khimki](https://crimethinc.com/2010/10/19/eco-defense-and-repression-in-russia), la ligne stratégique de [Stop Cop City](https://crimethinc.com/2023/02/28/balance-sheet-two-years-against-cop-city-evaluating-strategies-refining-tactics), et bien d'autres choses encore.


#### Creuser

La survie a commencé hier. Les habitants des pays qui ont déjà subi un effondrement, ainsi que les communautés autochtones et les communautés noires défavorisées du monde entier, ont déjà une longueur d'avance. Apprenez de ceux qui ont vécu ces expériences. Ensuite, apprenez à connaître intimement votre territoire. Apprenez d'où peut provenir la nourriture et quelles modifications devront être apportées aux habitations pendant les saisons les plus extrêmes en cas de panne du réseau électrique. Établissez des méthodes de communication et de coordination pour le cas où les téléphones et les connexions Internet ne fonctionneraient plus. Renseignez-vous sur les moyens d'accéder à de l'eau potable. Identifiez les lieux où le sol est le plus contaminé afin que personne ne puisse y cultiver de la nourriture. Apprenez à quel point les suprémacistes blancs sont coordonnés.

Et ensuite, mettez-vous au travail pour créer plus de ressources alimentaires communautaires, un meilleur accès au logement et plus de réseaux d'autodéfense collective. Soutenez tout projet qui vous inspire et qui nous rend tous plus forts, à la fois aujourd'hui et dans l'avenir probable, qu'il s'agisse d'un effondrement, d'une montée de l'autoritarisme ou d'une guerre civile révolutionnaire.

Se connecter à nos territoires spécifiques signifiera probablement rompre avec les idéologies homogénéisantes qui prétendent que nous sommes tous les mêmes, qui ne peuvent pas tenir compte du fait que nous avons tous des histoires et des besoins différents et que ces histoires sont parfois sources de conflits, ou qui basent leur idée de la transformation sociale sur un programme prédéterminé ou sur une certaine idée de l'unité forcée. L'avenir que nous devons créer est un écosystème sans centre.

### Rêver en grand

La révolution est encore possible. Nous pouvons l'affirmer avec conviction parce que l'histoire nous livre certains modèles au fil des siècles, et aussi parce que nous entrons dans une période sans précédent, où les institutions dominantes utilisent des plans et des modèles qui sont déjà obsolètes.

Toutes les révolutions des derniers siècles ont finalement été des échecs. Cela signifie que nous pouvons en tirer des leçons sans bloquer notre imagination ou présumer que nous savons à quoi ressemblera une transformation réussie de l'ensemble de la société.

Elle ne découlera pas d'un plan prédéfini. Elle ne sera pas le résultat du triomphe d'un parti. Elle sera le résultat d'innombrables rêves, plans, complots, espoirs fous et batailles que nous ne pouvons pas encore prévoir. Nous y parviendrons ensemble, en rêvant sans cesse, en tricotant sans cesse, parce que c'est cela, vivre libre.




[^0]: NdT : la « menace éco-terroriste », pourrait-on dire. L'expression est reprise de _red scare_, la peur rouge, celle du communisme. Voir la page Wikipédia [_Green Scare_](https://en.wikipedia.org/wiki/Green_Scare).

[^1]: J'examine des exemples de cette répression dans le monde et la manière dont elle est systématiquement liée au remplacement des mouvements radicaux par des courants réformistes dans [_The Solutions Are Already Here: Strategies for Ecological Revolution from Below_](http://www.plutobooks.com/9780745345116/the-solutions-are-already-here/) et [_They Will Beat the Memory Out of Us: Forcing Nonviolence on Forgetful Movements_](http://www.plutobooks.com/9780745349770/they-will-beat-the-memory-out-of-us/).

