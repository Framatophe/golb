---
title: "Manipuler des fichiers PDF"
date: 2014-10-20
author: "Christophe Masutti"
image: "/images/postimages/terminal.png"
tags: ["Logiciel libre", "PDF", "Fichier"]
description: "Quelques outils libres pour manipuler des fichiers PDF"
categories:
- Logiciel libre
---

La manipulation de fichiers PDF sous GNU/Linux fut longtemps un problème a priori insurmontable. Même sous les autres systèmes d'exploitation, l'achat de logiciels de la marque Adobe était nécessaire. Certes, ces derniers logiciels fort complets permettent bien d'autres choses, propres en particulier dans le monde du graphisme et de l'édition, pourtant il importe de se pencher sur certains besoins : annoter des PDF, voire procéder à de menues modifications, extraire un certain nombre de pages d'un long PDF pour les communiquer ensuite, fusionner plusieurs PDFs, etc.


Xournal
-------

Xournal est un logiciel de prise de note basé sur le même modèle que [Gournal](http://www.adebenham.com/old-stuff/gournal/), [NoteLab](http://java-notelab.sourceforge.net/) ou [Jarnal](http://levine.sscnet.ucla.edu/general/software/tc1000/jarnal.htm).

Ces logiciels (dont il existe des versions pour GNU/Linux, Mac et MSWindows) sont normalement destinés à être utilisés via une tablette graphique ou un écran tactile, afin d'imiter la pratique de prise de note sur un bloc papier.

Xournal a ceci de particulier qu'il demeure tout à fait fonctionnel sans dispositif tactile. L'organisation des pages de notes est très simple à l'utilisation et la prise en main est rapide.

Comme les autres logiciels cités plus haut, un avantage de Xournal est la possibilité d'ouvrir des fichiers PDF, les annoter, insérer du texte, surligner, et exporter le fichier ainsi édité (en fait il s'agit d'un calque que vous pouvez soit garder comme calque, soit exporter un nouveau PDF intégrant le calque sur le PDF original).

Vous rêviez de pouvoir éditer un PDF? Que votre voeu soit exaucé! Xournal est conçu pour une interface utilisant GTK, ce qui permet une excellente intégration au bureau Gnome. Dans le fichier de configuration, vous pourrez de même modifier quelques réglages usuels, selon les particularités de votre matériel ou vos envies. Une liste des options du fichier de configuration est disponible dans le manuel de Xournal (section "Configuration File").

Couper, extraire, fusionner : PdfTk et PdfMod
---------------------------------------------

Pour les autres tâches, obéissant à la logique Unix selon laquelle un logiciel se destine à une tâche ("write programs that do one thing and do it well"), deux logiciels méritent toute notre attention : [pdftk](http://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/) et [Pdf Mod](http://live.gnome.org/PdfMod).

Pdftk fait partie de ces programmes très efficaces mais dont l'utilisation rebute souvent le néophyte mal à l'aise avec la ligne de commande. Pourtant les commandes de pdftk sont extrêmement simples : concaténation, extraction, et sortie de nouveau pdf sont les actions les plus couramment demandées mais pdftk est
capable de bien des choses (la page de la [documentation Ubuntu consacrée à pdftk](http://doc.ubuntu-fr.org/pdftk) est assez éloquente sur ce point).

Un second programme est sans doute plus abordable puisqu'il utilise une interface graphique. Pdf Mod permet l'extraction et l'import d'éléments dans un fichier pdf en n'utilisant que la souris. Élaboré en un en temps record par Gabriel Burt, contributeur de talent aux logiciels plus connus tels que Banshee et F-Spot, nul doute que les futures amélioration de Pdf Mod répondront à la majorité des besoins des utilisateurs.

-   [Xournal](http://xournal.sourceforge.net/) (Site officiel)
-   [pdftk](http://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/) (Site officiel)
-   [Doc. Ubuntu](http://doc.ubuntu-fr.org/pdftk)(pdftk)
-   [Exemples d'utilisation de pdftk](http://www.pdflabs.com/docs/pdftk-cli-examples/)
-   [PdfMod](http://live.gnome.org/PdfMod) (site officiel)
-   [Blog de Gabriel Burt](http://gburt.blogspot.com/2009/07/pdf-mod.html)
-   [Article Pdf Mod](http://www.silicon.fr/pdf-mod-loutil-gratuit-qui-revolutionne-le-monde-des-pdf-36779.html) sur Silicon.fr

