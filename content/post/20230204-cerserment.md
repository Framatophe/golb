---
title: "Toute la France se tient sage... toute ?"
date: 2023-02-04
author: "Christophe Masutti"
image: "/images/postimages/cm.png"
description: "Il y a des torchons qu'on ne devrait pas signer et d'autres qui méritent franchement d'être brûlés. La « France qui se tient sage » est un concept qui a fait du chemin et il est vrai que bien peu l'ont vu venir. Il se décline aujourd'hui dans le monde associatif mais aussi -- on en parle moins -- dans le monde académique."
tags: ["Libres propos", "sciences", "contrôle", "morale", "autoritarisme"]
categories:
- Libres propos
---


Il y a des torchons qu'on ne devrait pas signer et d'autres qui méritent franchement d'être brûlés. La « France qui se tient sage » est un concept qui a fait du chemin et il est vrai que bien peu l'ont vu venir. Il se décline aujourd'hui dans le monde associatif mais aussi -- on en parle moins -- dans le monde académique.

<!--more-->

## Des associations qui se tiennent sages

En 2020, dans le cadre de la lutte contre le « séparatisme », l'argumentaire était judicieusement choisi. Surfant sur la vague des attentats (notamment [celui de Samuel Paty](https://fr.wikipedia.org/wiki/Assassinat_de_Samuel_Paty)) se revendiquant d'un certain islam, et sur celle de l'angoisse d'un monde en guerre et de l'écho trop large laissé aux discours de haine dans les médias, la lutte contre le séparatisme visait à prémunir la société française contre les tentatives anti-démocratiques de groupes identifiés souhaitant imposer leur propre ordre social. Ordres religieux, s'il en est, ou bien ordres néofascistes, bref, la Loi confortant le respect des principes de la République (dite « Loi séparatisme ») nous a été vendue comme un bouclier républicain. En particulier des dispositions ont été prises pour que toute association profitant des bienfaits institutionnels (versement de subventions ou commodités municipales) tout en livrant discours et actes à l'encontre de l'ordre républicain se verrait dissoute ou au moins largement contrecarrée dans ses projets.

On peut s'interroger néanmoins. L'arsenal juridique n'était-il pas suffisant&nbsp;? Et dans la surenchère d'amendements appelant à la censure de tous côtés, on pouvait déjà se douter que le gouvernement était désormais tombé dans le piège anti-démocratique grossièrement tendu&nbsp;: ordonner le lissage des discours, l'obligation au silence (des fonctionnaires, par exemple), contraindre la censure à tout bout de champ dans les médias... le front des atteintes aux libertés (surtout d'expression) marquait une avancée majeure, ajoutées au déjà très nombreuses mesures liberticides prises depuis le début des années 2000.

Le 31 décembre 2021, un décret mettait alors en place le *Contrat d'engagement républicain* (CER) à destination des associations qui bénéficient d'un agrément officiel ou de subventions publiques (au sens large&nbsp;: il s'agit aussi de commodités en nature qu'une collectivité locale pourrait mettre à disposition). Ce CER n'a pas fait couler beaucoup d'encre à sa sortie, et pour cause&nbsp;: les conséquences n'était pas évidentes à concevoir pour le grand public. Tout au plus pouvait-on se demander pourquoi il était pertinent de le signer pour toutes les associations qui sont déjà placées sous la loi de 1901 (ou sous le code civil local d'Alsace-Moselle). Après tout, celles accusées de séparatisme tombaient déjà sous le coup de la loi. Par exemple, pourquoi l'association des Joyeux Randonneurs de Chimoux-sur-la-Fluette, dont les liens avec le fascisme ou l'intégrisme religieux sont a priori inexistants, devait se voir contrainte de signer ce CER pour avoir le droit de demander à la municipalité quelques menus subsides pour l'organisation de la marche populaire annuelle&nbsp;?

Réponse&nbsp;: parce que l'égalité est républicaine. Soit ce CER est signé par tout le monde soit personne. D'accord. Et le silence fut brisé par quelques acteurs de qui, eux, l'avaient lu attentivement, ce contrat. Il y eut *Le Mouvement Associatif* qui dès [le 3 janvier 2022 publie un communiqué de presse](https://lemouvementassociatif-cvl.org/contrat-engagement-republicain/) et pointe les dangers de ce CER. On note par exemple pour une association l'obligation de surveillance des actions de ses membres, ou encore le fait que les « dirigeants » d'une association engagent la responsabilité de l'ensemble de l'association. Comprendre&nbsp;: si l'un d'entre vous fait le mariole, c'est toute l'asso qui en subit les conséquences (privé de subvention, na!).

Puis s'enclenche un mouvement général, comme le 02 févier 2022 cet [appel de la Ligue des Droits de l'Homme](https://www.ldh-france.org/contrat-dengagement-republicain-les-elus-locaux-doivent-proteger-la-liberte-associative/). Enfin [L.A. Coalition fut au premier plan](https://www.lacoalition.fr/Loi-separatisme-Contrat-d-engagement-republicain) contre le CER dès les premiers débats sur la loi séparatisme. Créé dès 2019 afin de « proposer des stratégies de riposte contre les répressions subies par le secteur associatif », L.A. Coalition montre combien sont nombreuses les entraves judiciaires ou administratives dressées dans le seul but de nuire aux libertés associatives, surtout lorsqu'elles sont teintés de militantisme... alors même qu'elles sont la matière première de la démocratie en France. L.A. Coalition illustre la contradiction notoire entre le discours gouvernemental prétendant défendre la République et la voix du peuple profondément attaché aux valeurs démocratiques. Elle se fait ainsi le thermomètre de l'[illibéralisme](https://fr.wikipedia.org/wiki/Illib%C3%A9ralisme) en France.

Lorsque les associations s'emparent de sujets de société tels l'écologie, les questions de genre, l'économie, les migration, la pauvreté, ou oeuvrent plus généralement pour une société plus juste et solidaire, elles sont [désormais amenées à engager une lutte](https://www.alternatives-economiques.fr/contrat-dengagement-republicain-associations-mises/00105932) à l'encontre des institutions qui, jusqu'à présent, leur garantissait un cadre d'action démocratique et juridique.

Mais cette lutte dépasse assez largement le cadre des actions associatives. En effet, le CER n'a pas pour seul objet de contraindre les citoyens à la censure. Il sert aussi à court-circuiter les cadres de l'action publique locale, surtout si les collectivités locales sont dirigées par des élus qui ne sont pas du même bord politique que la majorité présidentielle. C'est que qu'illustre notamment [l'affaire d'Alternatiba Poitiers](https://reporterre.net/Un-prefet-macroniste-s-attaque-a-Alternatiba) qui s'est vue (ainsi que d'autres associations) mettre en cause par le préfet de la Vienne en raison d'un atelier de formation à la désobéissance civile lors d'un évènement « Village des alternatives ». Invoquant le CER contre le concept même de désobéissance civile, le préfet Jean-Marie Girier (macroniste notoire) a obligé la mairie de Poitiers (dont le maire est EELV) à ne pas subventionner l'évènement. Il reçu aussitôt l'appui du Ministre de l'Intérieur (l'affaire est devant la justice). La France des droits et libertés contre la France de l'ordre au pouvoir, tel est l'essence même du CER.

## Des scientifiques qui se tiennent sages

On ne peut pas s'empêcher de rapprocher le CER du dernier avatar du genre&nbsp;: le *serment doctoral*.

Qu'est-ce que c'est&nbsp;?  Il s'agit d'un serment d'intégrité scientifique rendu obligatoire pour tout nouveau docteur et stipulé dans *L’arrêté du 26 août 2022 modifiant l'arrêté du 25 mai 2016 fixant le cadre national de la formation et les modalités conduisant à la délivrance du diplôme national de doctorat*. L'office français de l'intégrité scientifique (OFIS) en a fait [une fiche pratique](https://www.hceres.fr/sites/default/files/media/files/fiche-serment-doctoral-integrite-scientifique-pdf1.pdf).

Voici le texte ([Art. 19b de l'arrêté](https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000046237262))&nbsp;:

> «&nbsp;En présence de mes pairs.
>
> Parvenu(e) à l'issue de mon doctorat en [xxx], et ayant ainsi pratiqué, dans ma quête du savoir, l'exercice d'une recherche scientifique exigeante, en cultivant la rigueur intellectuelle, la réflexivité éthique et dans le respect des principes de l'intégrité scientifique, je m'engage, pour ce qui dépendra de moi, dans la suite de ma carrière professionnelle quel qu'en soit le secteur ou le domaine d'activité, à maintenir une conduite intègre dans mon rapport au savoir, mes méthodes et mes résultats.&nbsp;»

On notera que sous son apparente simplicité, le texte se divise en deux&nbsp;: ce que le doctorant a fait jusqu'à présent et ce que le nouveau docteur fera à l'avenir (même s'il quitte le monde académique car le grade de docteur valide compétences et savoirs d'un individu et non d'une fonction). Autrement dit, le serment est censé garantir « l'intégrité » du docteur, alors même que l'obtention du diplôme validé par des (désormais) pairs était déjà censé sanctionner cette intégrité.

Là encore, tout comme avec le CER, il y a une volonté de court-circuiter des acteurs institutionnels (ici les professeurs et l'Université qui valident le doctorat) qui sont désormais réputés ne plus être en mesure de détecter la fraude ou même de garantir les sciences contre les manipulations d'informations, de données et les arguments fallacieux (on pense évidemment aux informations douteuses qui ont circulé lors de l'épisode COVID). On peut aussi s'interroger sur la représentation de la Science que cela implique&nbsp;: une Science figée, obéissant à un ordre moral qui l'empêcherait d'évoluer à ses frontières, là où justement les sciences ont toujours évolué, souvent à l'encontre du pouvoir qu'il soit politique, religieux ou moral.

Plusieurs sections CNU (Conseil National des Universités) sont actuellement en train de réfléchir à l'adoption de motions à l'encontre de ce serment. Ainsi la section 22 (Histoire) a récemment publié sa motion: « Elle appelle les Écoles doctorales et la communauté universitaire dans son ensemble à résister collectivement, par tous les moyens possibles, à l’introduction de ce serment ». L'argument principal est que ce serment introduit un contrôle moral de la recherche. Ce en quoi la communauté universitaire a tout à fait raison de s'y opposer.

On peut aussi comparer ce serment avec un autre bien connu, le serment d'Hippocrate. Après tout, les médecins prêtent déjà un serment, pourquoi n'en serait-il pas de même pour tous les docteurs&nbsp;?

Hé bien pour deux raisons&nbsp;:

- Parce que le serment d'Hippocrate n'est pas défini par un arrêté et encore moins dicté par le législateur. Il s'agit d'un serment traditionnel dont la valeur symbolique n'est pas feinte&nbsp;: il est hautement moral et à ce titre n'a pas de rapport juridique avec le pouvoir.
- Il engage le médecin dans une voie de responsabilité vis-à-vis de ses pairs et surtout des patients qu'il est amené à soigner. Il appartient au Conseil de l'Ordre des médecins&nbsp;: c'est une affaire entre médecins et patients d'abord et entre médecins et médecins ensuite.


Reprenons les termes du serment doctoral imposé par le gouvernement&nbsp;: « je m'engage, pour ce qui dépendra de moi, dans la suite de ma carrière professionnelle quel qu'en soit le secteur ou le domaine d'activité, à maintenir une *conduite intègre* dans mon rapport au savoir, mes méthodes et mes résultats. »

Question&nbsp;: qui est censé juger de l'intégrité de la conduite&nbsp;? Les pairs&nbsp;? Ce n'est pas stipulé, alors que c'est clair dans le [serment d'Hippocrate de l'Ordre des Médecins](https://www.conseil-national.medecin.fr/medecin/devoirs-droits/serment-dhippocrate), dernier paragraphe&nbsp;:

> «&nbsp;Que les hommes et mes confrères m’accordent leur estime si je suis fidèle à mes promesses ; que je sois déshonoré(e) et méprisé(e) si j’y manque.&nbsp;»

Le « serment doctoral » stipule seulement qu'il est prêté « en présence de mes pairs », la belle affaire. Il est évident selon moi que l'intégrité scientifique d'un chercheur sera à l'avenir évaluée *moralement* par les institutions et non par les pairs. Cette tendance à la fongibilité entre morale et droit dans nos institutions républicaines est tout à fait malsaine.

Parlons des scandales pharmaceutiques. Parlons des scandales environnementaux. Si demain un chercheur ayant signé ce serment utilise son savoir et ses méthodes pour démontrer les conséquences néfastes (tant environnementales que sociales) d'un engagement gouvernemental dans l'ouverture d'une mine de charbon comme celle de [Hambach en Allemagne](https://reporterre.net/En-Allemagne-les-ecologistes-combattent-une-mine-de-charbon), à votre avis&nbsp;: pourra-t-il toujours dans son laboratoire essayer de prétendre à quelques subventions pour des recherches (même n'ayant aucun rapport avec son engagement personnel contre la mine)&nbsp;? En fait, sur le sujet climatique, la question de l'engagement des chercheurs est brûlante&nbsp;: ce qu'on veut surtout éviter c'est le profil du chercheur militant, car on ne sait jamais, il pourrait avoir raison...

*Là encore le militantisme et la démocratie sont mises en cause de manière frontale par le gouvernement*.

Après avoir bien transformé la dynamique de la recherche française en la noyant sous l'évaluation et la logique de projet, il est normal de faire en sorte que les chercheurs, tout comme les associations, puissent enfin rentrer dans le rang&nbsp;: c'est une France [qui se tient sage](https://blogs.mediapart.fr/jean-pierre-anselme/blog/071218/voila-une-classe-qui-se-tient-sage). Le concept a fait du [chemin](https://fr.wikipedia.org/wiki/Un_pays_qui_se_tient_sage).
