---
title: "Loi sécurité globale : analyse"
date: 2021-02-01
author: "Christophe Masutti"
image: "/images/postimages/z.jpg"
description: "La proposition de loi « Sécurité globale » fut déposée en octobre 2020 par deux députés du mouvement La République en Marche, Alice Thourot et Jean-Michel Fauvergue. Elle fut adoptée le 24 novembre 2020 par l'Assemblée Nationale. Le texte tout entier relève d'une volonté d'appliquer une idéologie extrêmiste et sécuritaire, opposée aux libertés comme aux principes fondamentaux de la justice. Cette idéologie conçoit le citoyen comme une menace et redéfini la démocratie dans ce que permet ou ne permet pas le pouvoir de la technopolice. L'un de ses piliers est un marché des technologies et du travail de la surveillance. Ce film de Karine Parrot et Stéphane Elmadjian en propose une analyse critique et pertinente."
tags: ["Libres propos", "Politique", "surveillance"]
categories:
- Libres propos
---

La proposition de loi « Sécurité globale » fut déposée en octobre 2020 par deux députés du mouvement *La République en Marche*, Alice Thourot et Jean-Michel Fauvergue. Elle fut adoptée le 24 novembre 2020 par l'Assemblée Nationale. Le texte tout entier relève d'une volonté d'appliquer une idéologie extrêmiste et sécuritaire, opposée aux libertés comme aux principes fondamentaux de la justice. Cette idéologie conçoit le citoyen comme une menace et redéfini la démocratie dans ce que permet ou ne permet pas le pouvoir de la technopolice. L'un de ses piliers est un marché des technologies et du travail de la surveillance. Ce film de Karine Parrot et Stéphane Elmadjian en propose une analyse critique et pertinente.

## Sept juristes décryptent la loi Sécurité Globale

— Synopsis —

> Novembre 2020. L’état d’urgence sanitaire est en vigueur. La population française est confinée, nul ne peut sortir de chez soi, sauf dans quelques cas « dérogatoires » et moyennant une attestation. C’est précisément ce moment hors du commun que le gouvernement choisit pour faire adopter – suivant la procédure d’urgence – une loi sur « la sécurité globale » qui vient accroître les dispositifs de contrôle et de surveillance.
> 
> Que signifie cette idée de « sécurité globale » et d’où vient-elle ? Quels sont les nouveaux systèmes de surveillance envisagés ? Qu’est-ce que le continuum de sécurité ? Que révèle le processus parlementaire d’adoption de la loi ? Pourquoi la liberté d’opinion est-elle menacée ?
> 
> Ce film croise les points de vue de sept universitaires, chercheuses et chercheurs en droit, spécialistes du droit pénal, de politique criminelle, des données personnelles et de l’espace public. Interrogées sur cette proposition de loi « Sécurité globale », ils et elles décryptent les dispositifs techno-policiers prévus par le texte et, au-delà, le projet politique qu’il recèle.

<div align="center">
<iframe width="560" height="315" src="https://tube.seditio.fr/videos/embed/d55a784a-6a76-4165-ba02-d71490402193" frameborder="0" allowfullscreen></iframe>
</div>

