---
title: "Trail : créez et partagez votre parcours en 5 minutes avec OpenStreetMap"
date: 2014-10-01
author: "Christophe Masutti"
description: "Travailler avec des fonds de cartes libres et pouvoir en faire à peu près ce qu'on en veut"
image: "/images/postimages/terminal.png"
tags: ["Logiciel libre", "Sport", "Cartographie"]
categories:
- Sport
- Logiciel Libre
---

Se livrer à son sport favori avec des outils libres, ce n'est pas
toujours évident. L'un des obstacles à franchir est de travailler avec
des fonds de cartes libres et pouvoir en faire à peu près ce qu'on en
veut.

Ce petit tutoriel rapide à propos de
[Umap](http://umap.openstreetmap.fr/fr/) va sûrement vous être utile!
Vous venez de faire votre parcours (ou vous en projetez un) et vous
brûlez d'envie de partager le tracé avec vos amis? Il existe pour cela
plusieurs solutions :

1.  Vous disposez d'un appareil qui se charge pour vous de téléverser
    votre tracé et une flopée de données personnelles sur un site de
    partage. Là, tant pis pour vous et la confidentialité de vos
    données, c'est un choix.
2.  Vous prenez une carte, vous scannez et dessinez ensuite le parcours
    par dessus... un peu laborieux, non?
3.  Vous téleversez votre tracé sur un site qui offre des fonds de carte
    et un éditeur prêts à l'emploi. Généralement un fond de carte IGN
    (issu de Géoportail) est accessible. C'est là aussi un choix, mais
    votre carte ne sera toujours pas libre.
4.  Vous travaillez avec [Umap](http://umap.openstreetmap.fr/fr/).

C'est quoi [Umap](http://umap.openstreetmap.fr/fr/) ? Il s'agit d'une
application en ligne utilisant plusieurs fonds de cartes issus du projet
OpenStreetMap. Comme il s'agit de cartographie libre vous pouvez alors
en disposer à votre guise pour afficher vos parcours sur votre site ou
partout ailleurs sans contrevenir à des questions de copyright. Vous
pouvez même choisir la licence sous laquelle votre tracé pourra être
partagé. Étape par étape, voici comment faire pour obtenir le tracé
ci-dessous, c'est à dire permettre à d'autres de visualiser une zone de
carte sur laquelle on a tracé quelque chose.

<iframe src="https://umap.openstreetmap.fr/fr/map/guirbaden-petit-parcours_18373?scaleControl=false&amp;miniMap=false&amp;scrollWheelZoom=false&amp;zoomControl=true&amp;allowEdit=false&amp;moreControl=true&amp;datalayersControl=true&amp;onLoadPanel=undefined" frameborder="0" height="400px" width="90%"></iframe>


[Voir en plein écran](https://umap.openstreetmap.fr/fr/map/guirbaden-petit-parcours_18373)

Étape 1
-------

1.  Rendez-vous à l'adresse
    [umap.openstreetmap.fr](http://umap.openstreetmap.fr) et ouvrez un
    compte (soit sur OpenStreetMap, ou en vous connectant avec vos
    comptes Github ou même Twitter si vous en avez).
2.  Cliquez sur "créez une carte".

![Accueil Umap](/images/umap1.png)

L'interface est relativement simple mais son aspect dépouillé (au profit de la cartographie) peut paraître déroutant.

![Umap](/images/aaaumap2.png)

En gros, à gauche vous disposez des outils vous
permettant d'interagir avec le site et l'interface. À droite, vous
disposez des outils d'édition. Ce sont ces derniers que nous allons voir
de près. 

![Boutons Umap](/images/boutonumap.png)

L'idée générale est de partager un ou plusieurs
calques sur un fond de carte. En termes clairs : je dessine quelque
chose sur un calque et je partage ce calque pour l'afficher avec le fond
de carte de mon choix. Les trois premiers boutons sont très faciles
**a)** insérer un marqueur, **b)** tracer une ligne ou une **c)** forme.
Pour chaque éléments que je dessine, qu'il s'agisse de ligne, de forme
ou de parcours, je pourrai lui assigner des propriétés : un nom, un
descriptif, des couleurs et d'autres propriétés (épaisseur de trait,
interactivité, etc.) Ensuite, dans l'ordre d'apparition des boutons :
**d)** importer des données, **e)** modifier les propriétés de la carte,
**f)** changer le fond de carte, **g)** centrer la carte sur la zone que
l'on est en train de travailler, **h)** gérer les permissions
(possibilité de rendre l'accès privé).

Étape 2
-------

1.  Importez vos données (outil **d**) soit depuis votre machine (bouton
    "parcourir") soit depuis une url. Choisissez le format utilisé (par
    exemple GPX) et cliquez sur **Importer**.
2.  Sitôt importé, le tracé se dessine sur la carte qui se centre
    automatiquement sur la zone concernée.
3.  En double-cliquant sur le tracé du parcours, chaque point pourra
    être édité.

![Umap Interface](/images/umap3.png)


Étape 3
-------

1.  Allez ensuite dans les paramètres (bouton **e**).
2.  Donnez un nom à votre carte et un descriptif.

![Umap Interface](/images/umap4.png)

Étape 4
-------

1.  Ajoutez un ou plusieurs marqueurs pour définir des points
    remarquables de votre parcours : le départ, l'arrivée, un point de
    vue, etc.
2.  À chaque marqueur vous pourrez définir son nom (s'affichera au
    survol de la souris), un descriptif. Dans les propriétés avancées,
    vous pouvez définir sa couleur, sa forme. Dans les coordonnées, vous
    pouvez affiner si besoin en modifiant la latitude et la longitude.

![Umap Interface](/images/umap5.png)

Étape 5
-------

Revenez aux paramètres de la carte (bouton **e**), puis:

1.  dans les **Options d'Interface**, cochez les boutons dont vous
    laisserez l'usage à vos lecteurs. Les paramètres par défaut peuvent
    suffire dans un premier temps.
2.  Les **propriétés par défaut** vous permettent de valoriser les
    éléments de votre carte en particulier le tracé. Choisissez par
    exemple une couleur bien tranchée (un rose fushia!)
3.  Dans notre exemple, nous avons choisi une opacité de 0.5 points et
    une épaisseur de 8 points.
4.  Bien d'autres options sont disponibles, vous pourrez y revenir plus
    tard.

Étape 6
-------

1.  Cliquez sur **enregistrer le zoom et le centre actuel** (bouton
    **g**). Cela permettra à vos lecteurs d'accéder directement à la
    zone sur laquelle vous travaillez.
2.  Dans la partie **crédit** : renseignez les crédits que donnez à
    votre carte. Ici, nous plaçons la carte sous licence libre CC-By-SA.

![Umap Interface](/images/umap6.png)

Étape 7
-------

1.  Cliquez sur le bouton **f** pour afficher les différents fonds de
    carte disponibles.
2.  Si vous courrez en ville, le fond de carte par défaut (OSM-Fr)
    devrait être suffisant. Si par contre vous courrez en nature
    (montagne, campagne, etc), et pour obtenir un affichage plus proche
    de ce que vous connaissez habituellement, choisissez le fond **Open
    Topo Map**
3.  Cliquez sur **Enregistrer**, en haut à droite.

![Umap Interface](/images/umap7.png)

Étape 8
-------

1.  Allez sur les outils de gauche et cliquez sur le bouton **exporter
    et partager la carte**.
2.  Vous avez alors votre disposition le code l'iframe que vous pourrez
    coller sur votre site web ou ailleurs. Les options d'export vous
    permettent de régler la hauteur, la largeur et autres options que
    vous laissez à l'usage de vos lecteurs (à savoir: le lien "plein
    écran" permettra au lecteur d'afficher la carte ne grand sur le site
    Umap).
3.  L'autre solution étant tout simplement de copier l'url de votre
    carte (dans la barre de votre navigateur) et de l'envoyer à vos
    correspondants.

![Umap Interface](/images/umap8.png)


