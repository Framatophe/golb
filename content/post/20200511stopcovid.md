---
title: "Revue articles et communications autour de Stop-Covid"
date: 2020-05-11
lastmod: 2020-06-23
author: "Christophe Masutti"
image: "/images/postimages/inspgadg.jpg"
description: "Afin de garder un trace (!) des débats qui enflamèrent l'opinion publique lors de la crise sanitaire du printemps 2020, voici une revue partielle d'articles parus dans la presse et quelques communications officielles autour de l'application Stop-Covid, entre discours réalistes et solutionnisme technologique (cette liste est mise à jour régulièrement, autant que possible).
"
tags: ["Libres propos", "Capitalisme de surveillance", "Libertés", "covid-19"]
categories:
- Libres propos
---

Afin de garder une trace (!) des débats qui enflamèrent l'opinion publique lors de la crise sanitaire du printemps 2020, voici une revue partielle d'articles parus dans la presse et quelques communications officielles autour de l'application Stop-Covid, entre discours réalistes et solutionnisme technologique (cette liste est mise à jour régulièrement, autant que possible).

<!--more-->






# Presse écrite et en ligne

Numerama (18/06/2020), « [StopCovid a été lancé alors que même la Cnil ne savait pas comment l’app fonctionnait](https://www.numerama.com/tech/631458-stopcovid-a-ete-lance-alors-que-meme-la-cnil-ne-savait-pas-comment-lapp-fonctionnait.html) »

> Un chercheur a montré que le fonctionnement technique de StopCovid n'était pas similaire à ce qui était annoncé par le gouvernement, ce qui est écrit dans le décret d'application et même ce qui a été validé par la Cnil.
> ...
> En résumé, la Cnil a validé un dispositif qui n’est pas celui qui est utilisé aujourd’hui dans l’application que tous les Français et Françaises sont fortement invités à utiliser, par le gouvernement et à grands coups de campagnes de communication. Comme on peut le voir au point numéro 37 qui était abordé dans l’avis du 25 mai, la Commission avait d’ailleurs elle-même « [pris] acte de ce que l’algorithme permettant de déterminer la distance entre les utilisateurs de l’application reste à ce stade en développement et pourra subir des évolutions futures ».

Le Monde - Pixel (16/06/2020), « [L’application StopCovid collecte plus de données qu’annoncé](https://www.lemonde.fr/pixels/article/2020/06/16/l-application-stopcovid-collecte-plus-de-donnees-qu-annonce_6043038_4408996.html) »

> Un chercheur a découvert que l’application collectait les identifiants de toutes les personnes croisées par un utilisateur, pas seulement celles croisées à moins d’un mètre pendant quinze minutes.


Mediapart (15/06/2020), « [StopCovid, l’appli qui en savait trop](https://www.mediapart.fr/journal/france/150620/stopcovid-l-appli-qui-en-savait-trop) »

> Loin de se limiter au recensement des contacts « à moins d’un mètre pendant au moins quinze minutes », l’application mise en place par le gouvernement collecte, et transfère le cas échéant au serveur central, les identifiants de toutes les personnes qui se sont « croisées » via l’appli. De près ou de loin.



L'Usine Digitale (12/06/2020), « [L'application StopCovid n'a été activée que par 2 % des Français](https://www.usine-digitale.fr/article/l-application-stopcovid-n-a-ete-activee-que-par-2-des-francais.N974786) »

> Échec pour StopCovid. Du 2 au 9 juin, seuls 2 % des Français ont téléchargé et activé l'application. Un chiffre beaucoup trop bas pour qu'elle soit utile. Au moins 60 % de la population d'un pays doit utiliser un outil de pistage numérique de ce type pour que celui-ci puisse détecter des chaînes de contamination du virus.


Dossier Familial (12/06/2020), « [Le flop de l’application StopCovid](https://www.dossierfamilial.com/actualites/vie-pratique/le-flop-de-lapplication-stopcovid-443374) »

> Alors même que son efficacité dépend de son succès, l’application StopCovid n’est utilisée effectivement que par environ 350 000 personnes. 



France Inter (11/06/2020), « [Coût et soupçon de favoritisme : l'appli StopCovid dans le collimateur d'Anticor](https://www.franceinter.fr/emissions/histoires-economiques/histoires-economiques-11-juin-2020) »

> L’association Anticor, qui lutte contre la corruption, a déposé un signalement auprès du Procureur de la République. Le coût de l’application StopCovid est en cause. L’inquiétude ne porte pas sur l’utilisation de nos données, mais sur son coût et… un soupçon de favoritisme. 


Liberation (11/06/2020), « [L’application StopCovid utilise-t-elle des services des Gafam ?](https://www.liberation.fr/amphtml/checknews/2020/06/11/l-application-stopcovid-utilise-t-elle-des-services-des-gafam_1790846) »

> Le logiciel développé par le gouvernement français a bien recours au «Captcha» de Google mais n’est pas hébergé sur des serveurs Microsoft (contrairement au projet, distinct, de Health Data Hub). 

Le Monde (10/06/2020) « [L’application StopCovid, activée seulement par 2 % de la population, connaît des débuts décevants](https://www.lemonde.fr/pixels/article/2020/06/10/l-application-stopcovid-connait-des-debuts-decevants_6042404_4408996.html) »

> Le coût de l’application de suivi de contacts, désormais supportée par l’Etat, pose question, une semaine après son lancement.


Slate.fr (03/06/2020), [« Les trois erreurs qui plombent l'application StopCovid »](http://www.slate.fr/story/191205/trois-erreurs-application-stopcovid-design-comite-choix-architecture-communication)

> Ce projet tout à fait louable s'est écrasé en rase campagne en quelques semaines alors que tous les feux étaient au vert.


Nouvel Observateur (02/06/2020), « [StopCovid, une application au coût salé](https://www.nouvelobs.com/economie/20200602.OBS29619/stopcovid-une-application-au-cout-sale.html?utm_medium=Social&utm_source=Twitter#Echobox=1591112235) »

> L’application de lutte contre le Covid-19 a été développée gratuitement par des chercheurs et partenaires privés. Toutefois, depuis son lancement mardi 2 juin, sa maintenance et son hébergement sont bel et bien facturés, entre 200 000 et 300 000 euros par mois.


TV5 Monde (01/06/2020), [StopCovid : « C’est le signe d’une société qui va très mal et qui est en train de devenir totalement paranoïaque »](https://information.tv5monde.com/info/stopcovid-c-est-le-signe-d-une-societe-qui-va-tres-mal-et-qui-est-en-train-de-devenir)

> A partir de ce mardi 2 juin, l’application StopCovid débarque en France. Son but : pister le Covid-19 pour éviter sa propagation. Alors cette application est-elle vraiment efficace ? Nos données personnelles sont-elles menacées ? Entretien avec Benjamin Bayart, ingénieur français, ancien président de French Data Network et co-fondateur de la Quadrature du Net.



The Conversation (25/05/2020) « [Données de santé : l’arbre StopCovid qui cache la forêt Health Data Hub](https://theconversation.com/amp/donnees-de-sante-larbre-stopcovid-qui-cache-la-foret-health-data-hub-138852?__twitter_impression=true) »

> Le projet de traçage socialement « acceptable » à l’aide des smartphones dit StopCovid, dont le lancement était initialement prévu pour le 2 juin, a focalisé l’intérêt de tous. Apple et Google se réjouissaient déjà de la mise en place d’un protocole API (interface de programmation d’application) qui serait commun pour de nombreux pays et qui confirmerait ainsi leur monopole.
> 
> Mais la forte controverse qu’a suscitée le projet en France, cumulée au fait que l’Allemagne s’en est retirée et à l’échec constaté de l’application à Singapour, où seulement 20 % des utilisateurs s’en servent, annoncent l’abandon prochain de StopCovid.
> 
> « Ce n’est pas prêt et ce sera sûrement doucement enterré. À la française », estimait un député LREM le 27 avril auprès de l’AFP.
> 
> Pendant ce temps-là, un projet bien plus large continue à marche forcée : celui de la plate-forme des données de santé Health Data Hub (HDHub).


The Guardian (23/05/2020), « [How did the Covidsafe app go from being vital to almost irrelevant?](https://www.theguardian.com/world/2020/may/24/how-did-the-covidsafe-app-go-from-being-vital-to-almost-irrelevant?__twitter_impression=true) »

> The PM told Australians in April the contact tracing app was key to getting back to normal but just one person has been identified using its data (En avril, le Premier ministre a déclaré aux Australiens que l'application de recherche des contacts était la clé du retour à la normale, mais seule une personne a été identifiée grâce à ses données)



Politico (20/05/2020) « [Meet the Dutchman who cried foul on Europe’s tracking technology](https://www.politico.eu/article/meet-the-dutchman-aleid-wolfsen-who-cried-foul-on-europe-coronavirus-covid19-tracking-technology/amp/?__twitter_impression=true) »

> The national privacy watchdog has repeatedly broken ranks with EU peers over tracing technology. His approach seems to be winning.
> 
> (...) Alors que les gouvernements européens s'empressent d'adopter la technologie pour lutter contre le coronavirus, un Néerlandais au franc-parler est apparu comme une épine dans leur pied.
> 
> Le message d'Aleid Wolfsen : Ne prétendez pas que vos solutions sont respectueuses de la vie privée.
> 
> "Nous devons éviter de déployer une solution dont on ne sait pas si elle fonctionne réellement, avec le risque qu'elle cause principalement d'autres problèmes", a déclaré en avril le responsable de l'autorité néerlandaise chargée de la protection de la vie privée, en référence aux applications de suivi des coronavirus pour smartphones en cours de développement dans la plupart des pays de l'UE.


Le Monde (18/05/2020), Tribune -- [Coronavirus : « Sur l’application StopCovid, il convient de sortir des postures dogmatiques »](https://www.lemonde.fr/idees/article/2020/05/18/coronavirus-sur-l-application-stopcovid-il-convient-de-sortir-des-postures-dogmatiques_6040038_3232.html)

> Chercheurs à l’Inria et travaillant sur l’application StopCovid, Claude Castelluccia et Daniel Le Métayer mettent en garde, dans une tribune au « Monde », contre les oppositions de principe, notamment à un système centralisé, qui ne reposent pas sur une analyse précise des risques potentiels et des bénéfices attendus. (...)
> 
> (...) Même si la valeur ajoutée d’une application comme StopCovid dans le processus plus global de traçage (qui repose sur des enquêtes menées par des humains) reste à déterminer et à évaluer précisément...



The Economist (16/05/2020) « [Don’t rely on contact-tracing apps](https://www.economist.com/leaders/2020/05/16/dont-rely-on-contact-tracing-apps) »

> Governments are pinning their hopes on a technology that could prove ineffective — and dangerous


Wired (13/05/2020), « [Secret NHS files reveal plans for coronavirus contact tracing app](https://www.wired.co.uk/article/nhs-covid-19-app-health-status-future) »


> Documents left unsecured on Google Drive reveal the NHS could in the future ask people to post their health status to its Covid-19 contact tracing app


Numerama (13/05/2020), « ['Il n’y a rien du tout' : la première publication du code source de StopCovid est inutile](https://www.numerama.com/tech/623866-il-ny-a-rien-du-tout-la-premiere-publication-du-code-source-de-stopcovid-est-inutile.html) »

> (...) les critiques et les moqueries ont fusé immédiatement sur ce qui a été publié le 12 mai. En effet, de l’avis de diverses personnes habituées à travailler avec du code informatique, ce qui est proposé ressemble surtout à une coquille vide. Impossible donc pour les spécialistes de passer aux travaux pratiques en analysant les instructions du logiciel.


Dalloz Actualité (12/05/2020) -- par Caroline Zorn -- « [État d’urgence pour les données de santé (I) : l’application StopCovid](https://www.dalloz-actualite.fr/flash/etat-d-urgence-pour-donnees-de-sante-i-l-application-stopcovid) »


> À travers deux exemples d’actualité que sont l’application StopCovid (première partie) et le Service intégré de dépistage et de prévention (SIDEP) (seconde partie), la question de la protection des données de santé à l’aune du pistage est confrontée à une analyse juridique (et parfois technique) incitant à une réflexion sur nos convictions en période d’état d’urgence sanitaire. Cette étude a été séparée en deux parties, la première consacrée à StopCovid est publiée aujourd’hui. 


Revue politique et parlementaire (11/05/2020), « [Stopcovid : une application problématique sur le plan éthique et politique](https://www.revuepolitique.fr/stopcovid-une-application-problematique-sur-le-plan-ethique-et-politique) »

> Pierre-Antoine Chardel, Valérie Charolles et Eric Guichard font le point sur les risques engendrés par la mise en œuvre d’une solution de type Stopcovid, solution techniciste de court terme qui viendrait renforcer une défiance des citoyens envers l’État et ses représentants, ou à l’inverse un excès de confiance dans le numérique, au détriment de projets technologiques soucieux de l’éthique et susceptibles de faire honneur à un État démocratique.


MIT Technology Review (11/05/2020) « [Nearly 40% of Icelanders are using a covid app — and it hasn’t helped much](https://www.technologyreview.com/2020/05/11/1001541/iceland-rakning-c19-covid-contact-tracing/) »

> The country has the highest penetration of any automated contact tracing app in the world, but one senior figure says it “wasn’t a game changer.”


Newstatesman -- NSTech (11/05/2020), « [The NHSX tracing app source code was released but privacy fears remain](https://tech.newstatesman.com/security/nhsx-tracing-app-source-code-was-released-privacy-fears) »

> In a move designed to create the impression of radical transparency, NHSX finally released the source code for the UK’s Covid-19 tracing app last week. However, among experts, the reception was mixed.
> 
> Aral Balkan, software developer at Small Technology Foundation, expressed criticism that the source code for the server hadn’t been released, only that of the iOS and Android clients. “Without that, it’s a meaningless gesture,” he wrote on Twitter. “Without the server source you have no idea what they’re doing with the data.”
> 
> In further analysis, Balkan said the iOS app was “littered with Google tracking” and the app was using Firebase (a Google mobile and web application development platform) to build the app. He pointed out that Microsoft analytics also tracked what information the person has provided, such as partial postcode and collected contact events. 


France info, (09/05/2020) -- [Nouveau monde. Refuser l’application StopCovid revient à remettre en question notre système de santé, selon le directeur du comité d’éthique du CNRS](https://www.francetvinfo.fr/sante/maladie/coronavirus/nouveau-monde-refuser-lapplication-stopcovid-revient-a-remettre-en-question-notre-systeme-de-sante-selon-le-directeur-du-comite-dethique-du-cnrs_3933943.html)

> « Déjà, nous sommes tracés tous les jours par notre téléphone qui enregistre énormément d'informations. Surtout, nous sommes tous satisfaits de notre système étatique de santé et, avec un système étatique, contrairement aux États-Unis, l'État est nécessairement au courant de tout. L’État sait chez quels médecins nous allons, quels médicaments nous sont prescrits, quels hôpitaux nous fréquentons, etc. Bien sûr, cela doit se faire avec une certaine confidentialité mais si l’on prenait au sérieux certains discours actuels, cela remettrait en cause le système de santé tel qu'il existe aujourd'hui. Et ça, je pense que ce n'est pas légitime. »


Le Monde (08/05/2020), « [Suivi des « cas contacts » : ce que contiendront les deux nouveaux fichiers sanitaires prévus par l’Etat Par Martin Untersinger](https://www.lemonde.fr/pixels/article/2020/05/08/suivi-des-cas-contacts-ce-que-contiendront-les-deux-nouveaux-fichiers-medicaux-prevus-par-l-etat_6039059_4408996.html) »

> La stratégie du gouvernement pour lutter contre une reprise de l’épidémie repose sur un suivi des « cas contacts ». Les autorités prévoient un « système d’information », reposant sur deux bases de données médicales : Sidep et Contact Covid.


Acteurs Publics, (07/05/2020) « [Aymeril Hoang, l’architecte discret du projet d’application StopCovid](https://www.acteurspublics.fr/evenement/aymeril-hoang-larchitecte-discret-du-projet-dapplication-stopcovid) »

> Membre du Conseil scientifique Covid-19 en tant qu’expert du numérique, l’ancien directeur de cabinet de Mounir Mahjoubi au secrétariat d’État au Numérique a eu en réalité un rôle central, en coulisses, dans l’élaboration du projet d’application de traçage numérique StopCovid. Récit.

Les Echos (07/05/2020), « [La Réunion lance sa propre appli de traçage… sans attendre StopCovid](https://www.lesechos.fr/tech-medias/hightech/la-reunion-lance-sa-propre-appli-de-tracage-sans-attendre-stopcovid-1201262) »

> Des entrepreneurs réunionnais ont mis au point Alertanoo, une application mobile permettant aux habitants de l'île testés positifs au coronavirus d'alerter les personnes qu'elles ont croisées récemment.

Le Temps (06/05/2020), « [A Singapour, le traçage par app dégénère en surveillance de masse](https://www.letemps.ch/economie/singapour-tracage-app-degenere-surveillance-masse) »

> Premier pays à avoir lancé le pistage du virus par smartphone de manière volontaire, Singapour lance un nouveau service liberticide, baptisé SafeEntry. La Suisse peut en tirer des leçons
> 
> (...) Ainsi, le système central obtiendra les coordonnées complètes – du nom au numéro de téléphone – des Singapouriens qui fréquentent ces lieux. SafeEntry diffère ainsi de TraceTogether sur deux points majeurs: d’abord, son caractère obligatoire, comme on vient de le voir – même si un haut responsable de la Santé vient de demander que TraceTogether devienne obligatoire. Ensuite, la qualité des données récoltées diffère: la première application lancée fonctionne de manière anonyme – ni le nom, ni la localisation des personnes n’étant révélés. SafeEntry ne semble pas avoir suscité, pour l’heure, de critiques.


Wired (05/06/2020), « [India's Covid-19 Contact Tracing App Could Leak Patient Locations](https://www.wired.com/story/india-covid-19-contract-tracing-app-patient-location-privacy/) »

> The system's use of GPS data could let hackers pinpoint who reports a positive diagnosis.
> 
> As countries around the world rush to build smartphone apps that can help track the spread of Covid-19, privacy advocates have cautioned that those systems could, if implemented badly, result in a dangerous mix of health data and digital surveillance. India's new contact tracing app may serve as a lesson in those privacy pitfalls: Security researchers say it could reveal the location of Covid-19 patients not only to government authorities but to any hacker clever enough to exploit its flaws.


Nextinpact (04/05/2020), « [SIDEP et Contact Covid, les deux fichiers du « contact tracing](https://www.nextinpact.com/news/108950-sidep-et-contact-covid-deux-fichiers-contact-tracing.htm) »

> Le Conseil d’État estime que les conditions de mise en œuvre des fichiers, qui reposeront notamment sur le traitement de données de santé non anonymisées, « le cas échéant sans le consentement des personnes intéressées », « ne portent pas une atteinte disproportionnée au droit au respect de la vie privée ». Le projet de loi est examiné aujourd'hui au Sénat.

Numerama (04/05/2020), « [Après l’échec du StopCovid local, Singapour passe à une solution beaucoup plus radicale](https://www.numerama.com/tech/622089-apres-lechec-du-stopcovid-local-singapour-passe-a-une-solution-beaucoup-plus-radicale.html) »

> À Singapour, l'application TraceTogether non fonctionnelle est accompagnée d'un nouveau mode de traçage des contacts numérique : le scan d'un QR Code. Et, dans les grandes lignes, il est obligatoire.

Medium (03/05/2020), par Cedric O. « [StopCovid ou encore ?](https://medium.com/@cedric.o/stopcovid-ou-encore-b5794d99bb12) »

> Le secrétaire d'Etat chargé du numérique Cedric O. se fend d'un long article sur Medium pour faire le point sur l'application Stop-Covid.

Liberation (02/05/2020), « [Fichiers médicaux, isolement, traçage… Le gouvernement précise son état d'urgence sanitaire](https://www.liberation.fr/politiques/2020/05/02/fichiers-medicaux-isolement-tracage-le-gouvernement-precise-son-etat-d-urgence-sanitaire_1787175) »

> A l’issue du Conseil des ministres de samedi, Olivier Véran a justifié l’utilisation prochaine de «systèmes d’information» pour «tracer» les personnes infectées par le nouveau coronavirus et la prolongation de l’état d’urgence sanitaire jusqu’à juillet.


France Info Tv/2 (02/05/2020), « [Déconfinement : le travail controversé des "brigades d’anges gardiens"](https://www.francetvinfo.fr/sante/maladie/coronavirus/deconfinement-le-travail-controverse-des-brigades-danges-gardiens_3945525.html) »

> Protéger, isoler, tester, pour mettre en place ce triptyque, la mise en place d’un nouvel outil, avec le suivi des patients, et la récolte des données personnelles est prônée par le gouvernement.
> 
> Allons-nous tous être fichés, et faut-il s’inquiéter ? Le gouvernement a dévoilé sa politique de traçage. Lorsqu’une personne est testée positive au Covid-19, elle apparaitra dans un fichier, appelé Sidep, les personnes qu’elle a côtoyé seront fichées dans un deuxième fichier nommé Contact Covid, afin d’être testées et si besoin isolées pour empêcher la propagation du virus. 3 à 4 000 personnes seront affectées à cette mission.


France Info (02/05/2020), « [Application StopCovid : "Nous avons absolument besoin de ce regard", affirme le président du syndicat de médecins généralistes MG France](https://www.francetvinfo.fr/sante/maladie/coronavirus/application-stopcovid-nous-avons-absolument-besoin-de-ce-regard-affirme-le-president-du-syndicat-de-medecins-generalistes-mg-france_3945531.html) »

> Selon Jacques Battistoni, les généralistes effectuent déjà une forme de surveillance en mentionnant les contaminations au coronavirus sur les arrêts de travail. Pour lui, l'application envisagé par le gouvernement donnera une "vision d'ensemble".


Numerama (02/05/2020), « [StopCovid : Olivier Véran confirme les incertitudes autour de l’application de contact tracing](https://www.numerama.com/politique/622048-stopcovid-olivier-veran-confirme-le-destin-incertain-de-lapplication-de-contact-tracing.html) »

> L'application StopCovid ne sera pas prête pour le 11 mai. Le ministre de la Santé Olivier Véran prépare déjà la France à ce qu'elle ne voit jamais le jour.


Reuters (02/05/2020), « [India orders coronavirus tracing app for all workers](https://www.reuters.com/article/us-health-coronavirus-india-app/india-makes-government-tracing-app-mandatory-for-all-workers-idUSKBN22E07K) »

> NEW DELHI (Reuters) - India has ordered all public and private sector employees use a government-backed contact tracing app and maintain social distancing in offices as it begins easing some of its lockdown measures in districts less affected by the coronavirus.
> (...) “Such a move should be backed by a dedicated law which provides strong data protection cover and is under the oversight of an independent body,” said Udbhav Tiwari, Public Policy Advisor for internet browser company Mozilla.  Critics also note that about 400 million of India’s population do not possess smartphones and would not be covered.

Capital (01/05/2020), « [L'application "Stop Covid" dans le top de l'AppStore, sauf que ce n'est pas la bonne](https://www.capital.fr/lifestyle/lapplication-stop-covid-dans-le-top-de-lappstore-sauf-que-ce-nest-pas-la-bonne-1368985) »

> Des milliers de personnes ont téléchargé cette application sur Google Play ou l’AppStore, sauf qu’il s’agit de celle lancée par le gouvernement géorgien.

Charlie Hebdo (30/04/2020), « [Stop Covid est un danger pour nos libertés](https://charliehebdo.fr/2020/04/societe/stop-covid-est-un-danger-pour-nos-libertes/) »

> Le Covid-19 va-t-il nous faire basculer vers une société de surveillance ? Bernard E. Harcourt, professeur de droit à Columbia University et directeur d’études à l’EHESS nous alerte notamment sur les risques de l’application Stop Covid. Il avait publié en janvier dernier un ouvrage intitulé « La société d’exposition », (Ed Seuil), dans lequel il montrait de manière saisissante comment les nouvelles technologies exploitent notre désir d’exhibition et de mise en avant de soi.

Buzzfeed (29/04/2020) « [We Need An “Army” Of Contact Tracers To Safely Reopen The Country. We Might Get Apps Instead](https://www.buzzfeednews.com/amphtml/carolinehaskins1/coronavirus-contact-tracing-google-apple) »

> Without enough human contact tracers to identify infected people, the US is barreling toward a digital solution, and possible disaster.
> 
> (...) “My problem with contact tracing apps is that they have absolutely no value,” Bruce Schneier, a privacy expert and fellow at the Berkman Klein Center for Internet & Society at Harvard University, told BuzzFeed News. “I’m not even talking about the privacy concerns, I mean the efficacy. Does anybody think this will do something useful? … This is just something governments want to do for the hell of it. To me, it’s just techies doing techie things because they don’t know what else to do.”

Dalloz Actualités (28/04/2020) « [Coronavirus : StopCovid, les avis de la CNIL et du CNNum](https://www.dalloz-actualite.fr/flash/coronavirus-stopcovid-avis-de-cnil-et-du-cnnum) »

> Quoi que favorables à l’application de traçage StopCovid telle qu’elle est actuellement présentée, la Commission nationale informatique et libertés et le Conseil national du numérique relèvent cependant les risques et les garanties qui devront être discutées par le législateur.

Mediapart (28/04/2020) « [Le gouvernement s’embourbe dans son projet d’application «StopCovid»](https://www.mediapart.fr/journal/france/280420/le-gouvernement-s-embourbe-dans-son-projet-d-application-stopcovid) »

> Édouard Philippe a reconnu « les incertitudes » pesant sur le projet d’application de traçage des contacts des personnes contaminées. Celui-ci suscite l’opposition de défenseurs des libertés publiques comme de chercheurs.


Numerama (27/04/2020), « [Application StopCovid : que sait-on du projet de contact tracing du gouvernement ?](https://www.numerama.com/politique/616687-application-stopcovid-de-pistage-des-malades-que-sait-on-du-projet-du-gouvernement.html) »

> StopCovid est un projet d'application servant à suivre la propagation de la maladie dans la population. Présentée début avril, elle doit être prête lorsque la phase de déconfinement débutera. Mais cette stratégie est l'objet d'un intense débat. Que sait-on d'elle ? Numerama fait le point.

Le Monde (27/04/2020) « [Coronavirus : « Substituons temporairement au terme “liberté” de notre devise française celui de “responsabilité”](https://www.lemonde.fr/idees/article/2020/04/27/coronavirus-substituons-temporairement-au-terme-liberte-de-notre-devise-francaise-celui-de-responsabilite_6037911_3232.html?utm_medium=Social&utm_source=Twitter) »

> Trois spécialistes en épidémiologie de l’université de Toulouse, associée à l’Inserm, présentent dans une tribune au « Monde » un éclairage éthique sur le traçage numérique de la propagation du Covid-19, entre intérêt public et libertés individuelles.

France Culture (27/04/2020), « [Traçage : "Ce n'est pas le numérique qui pose question, mais notre capacité à penser le bien commun"](https://www.franceculture.fr/amp/numerique/tracage-ce-nest-pas-le-numerique-qui-pose-question-mais-notre-capacite-a-penser-le-bien-commun?__twitter_impression=true)

> Entretien | À travers le monde, les différentes solutions technologiques imaginées pour répondre à la pandémie via les applications de traçage "StopCovid" dessinent une carte assez précise de la façon dont le politique se saisit - ou pas - des questions numériques. Analyse avec Stéphane Grumbach, de l'INRIA.



Numerama (27/04/2020), « [StopCovid dans l’impasse : Cédric O confirme que la France se lance dans un bras de fer avec Apple](https://www.numerama.com/tech/620777-stopcovid-dans-limpasse-cedric-o-confirme-que-la-france-se-lance-dans-un-bras-de-fer-avec-apple.html/amp?%E2%80%A6) »

> Cédric O a confirmé que la France choisissait le bras de fer avec Apple pour obtenir un passe-droit sur le Bluetooth, au risque de mettre en danger la vie privée des utilisatrices et utilisateurs d'iPhone. Sans cela, StopCovid ne fonctionnera pas.

Libération (26/04/2020). Tribune par Didier Sicard et al. « [Pour faire la guerre au virus, armons numériquement les enquêteurs sanitaires](https://www.liberation.fr/debats/2020/04/26/pour-faire-la-guerre-au-virus-armons-numeriquement-les-enqueteurs-sanitaires_1786298) »

> Pourquoi se focaliser sur une application qu'il faudra discuter à l'Assemblée nationale et qui risque de ne jamais voir le jour, alors que nous devrions déjà nous concentrer sur la constitution et l'outillage numérique d'une véritable armée d'enquêteurs en épidémiologie ?

Le Figaro (26/04/2020). « [Mobilisation générale pour développer l’application StopCovid](https://www.lefigaro.fr/secteur/high-tech/mobilisation-generale-pour-developper-l-application-stopcovid-20200426) »

> Les récents avis rendus par la Cnil, le CNNum et le Conseil scientifique ont sonné le top départ pour les équipes de développement d’une application de «contact tracing».

Les Echos (26/04/2020), « [StopCovid : le gouvernement passe en force et supprime le débat à l'Assemblée](https://www.lesechos.fr/tech-medias/hightech/stopcovid-le-gouvernement-passe-en-force-et-supprime-le-debat-a-lassemblee-1198202) »

> En intégrant le débat sur le projet d'application StopCovid à celui, général, sur le déconfinement, l'exécutif est accusé de passer en force. Une situation dénoncée à gauche comme au sein même de la majorité, où les réticences contre cette application sont fortes.

Politis (26/04/2020). « [Covid-19 : la solution ne sera pas technologique](https://www.politis.fr/articles/2020/04/covid-19-la-solution-ne-sera-pas-technologique-41787/) »

> Les débats autour de l’application StopCovid, souhaitée par le gouvernement français, révèlent la grande obsession de notre temps. Face aux périls qui nous menacent, nous devrions nous jeter à corps perdu dans la technologie. Surtout, ne rien remettre en cause, mais opter pour un monde encore et toujours plus numérisé. Au mépris de nos libertés, de l’environnement, de l’humain et, au final, de notre avenir.

Reuters (26/04/2020), « [Germany flips to Apple-Google approach on smartphone contact tracing](https://www.reuters.com/article/us-health-coronavirus-europe-tech/germany-flips-on-smartphone-contact-tracing-backs-apple-and-google-idUSKCN22807J) »

> Germany changed course on Sunday over which type of smartphone technology it wanted to use to trace coronavirus infections, backing an approach supported by Apple and Google along with a growing number of other European countries.


Le Monde (26/04/2020), « [Application StopCovid : la CNIL appelle le gouvernement « à une grande prudence](https://www.lemonde.fr/pixels/article/2020/04/26/stopcovid-etre-prets-le-11-mai-sera-un-defi-selon-cedric-o_6037789_4408996.html) » »

> Dans son avis, la Commission nationale de l’informatique et des libertés donne un satisfecit au gouvernement tout en pointant les « risques » liés à ce projet d’application.

Le Monde (25/04/2020). Tribune par Antonio Casilli et al. « [StopCovid est un projet désastreux piloté par des apprentis sorciers](https://www.lemonde.fr/idees/article/2020/04/25/stopcovid-est-un-projet-desastreux-pilote-par-des-apprentis-sorciers_6037721_3232.html) »

> Il faut renoncer à la mise en place d’un outil de surveillance enregistrant toutes nos interactions humaines et sur lequel pèse l’ombre d’intérêts privés et politiques, à l’instar du scandale Cambridge Analytica, plaide un collectif de spécialistes du numérique dans une tribune au « Monde ».

La Dépêche du Midi (25/04/2020), « [Coronavirus : la société toulousaine Sigfox propose des bracelets électroniques pour gérer le déconfinement](https://www.ladepeche.fr/2020/04/25/coronavirus-la-societe-toulousaine-sigfox-propose-des-bracelets-electroniques-pour-gerer-le-deconfinement,8862285.php) »

> Tracer les malades du Covid-19 via une application pour alerter tous ceux qui les ont croisés et qui ont été touchés par le nouveau coronavirus, c'est l'une des options étudiées par le gouvernement pour le déconfinement. La société toulousaine Sigfox a proposé ses services : un bracelet électronique au lieu d'une application. Plus sûr selon son patron.

Le Monde (24/04/2020), Tribune de Thierry Klein, entrepreneur du numérique, « [Déconfinement : « Nous nous sommes encore tiré une balle dans le pied avec le RGPD »](https://www.lemonde.fr/idees/article/2020/04/24/deconfinement-nous-nous-sommes-encore-tire-une-balle-dans-le-pied-avec-le-rgpd_6037643_3232.html)

> L’entrepreneur Thierry Klein estime, dans une tribune au « Monde », que le Règlement général sur la protection des données est un monstre juridique dont la France subira les conséquences quand il s’agira de mettre en place des outils numériques de suivi de la contagion.
>
> Extrait :
>
> Le pompon de la bêtise, celle qui tue, est en passe d’être atteint avec le traitement du coronavirus. Au moment où nous déconfinerons, il devrait être possible de mettre une application traceuse sur chaque smartphone (« devrait », car il ne faut pas préjuger des capacités techniques d’un pays qui n’a su procurer ni gel ni masques à ses citoyens). Cette application, si vous êtes testé positif, va être capable de voir quels amis vous avez croisés et ils pourront être eux-mêmes testés. Ainsi utilisée, une telle application réduit significativement la contagion, sauve des vies (par exemple en Corée du Sud), mais réduirait selon certains nos libertés et, drame national, enfreindrait le RGPD.
>
> Une telle interprétation est un détournement de la notion de citoyenneté, au bénéfice d’une liberté individuelle mal comprise – de fait, la liberté de tuer.

NextInpact (24/04/2020), « [« Contact tracing » : la nécessité (oubliée) de milliers d'enquêteurs](https://www.nextinpact.com/news/108906-la-necessite-oubliee-milliers-denqueteurs.htm?r?skipua=1) » (Par Jean-Marc Manach )

> La focalisation du débat autour des seules applications Bluetooth de « contact tracing » fait complètement l'impasse sur le fait que, en termes de santé publique, le « contact tracing » repose d'abord et avant tout sur le facteur humain. Et qu'il va falloir en déployer des centaines de milliers, dans le monde entier.



Numerama (21/04/2020), « »[StopCovid vs Apple : pourquoi la France s’est mise dans une impasse](https://www.numerama.com/tech/619446-stopcovid-vs-apple-pourquoi-la-france-sest-mise-dans-une-impasse.html/) »

> D'après Cédric O, secrétaire d'État au Numérique, l'application StopCovid ne sortira pas dans les temps à cause d'Apple. Cette politisation simpliste du discours autour de l'app de contact tracing cache un désaccord technique entre la méthode nationale de l'Inria et le développement international de la solution de Google et Apple. Résultat : la France est dans l'impasse.

Corriere Della Sera (21/04/2020), « [Coronavirus, l’App (quasi) obbligatoria e l’ipotesi braccialetto per gli anziani](https://www.corriere.it/cronache/cards/coronavirus-l-app-quasi-obbligatoria-braccialetto-gli-anziani/scaricare-immuni.shtml) »

> Une idée pour l'incitation pourrait être de laisser la possibilité de télécharger l'application, ou de porter le bracelet, rester volontaire. Comme le gouvernement l'a déjà indiqué clairement. Mais prévoir que pour ceux qui choisissent de ne pas le télécharger, des restrictions de mobilité subsistent. Il reste à préciser ce que l'on entend exactement par restrictions à la liberté de circulation. Pas l'obligation de rester à la maison, ce ne serait pas possible. Mais il pourrait y avoir un resserrement du mouvement qui, pour tous les autres, dans la phase 2, sera autorisé progressivement. La proposition, encore en phase d'élaboration, pourrait être formalisée dans les prochains jours par la commission technico-scientifique, en accord avec Domenico Arcuri, le commissaire extraordinaire qui a signé l'ordre précisément pour l'application, et en accord également avec le groupe de travail dirigé par Vittorio Colao. La décision finale, bien sûr, appartient au gouvernement.

Korii-slate (20/04/2020), « [Peut-on déjà se fier à une app de passeport immunitaire?](https://korii.slate.fr/tech/sortie-confinement-app-passeport-immunitaire-coronapass-donnees-fiabilite) » (voir [original sur Quartz](https://qz.com/1838764/is-it-too-soon-for-immunity-passports/) paru le 15/04/2020)

> Une start-up propose déjà son CoronaPass aux États, sur des bases scientifiques pourtant balbutiantes.

Le Temps (18/04/2020), « [L’EPFL se distancie du projet européen de traçage du virus via les téléphones](https://www.letemps.ch/economie/lepfl-se-distancie-projet-europeen-tracage-virus-via-telephones) »

> L’EPFL, mais aussi l’EPFZ se distancient du projet européen de traçage des smartphones pour lutter contre le coronavirus. A l’origine, les deux établissements faisaient partie de l’ambitieux projet de recherche européen Pan-European Privacy-Preserving Proximity Tracing (PEPP-PT), regroupant 130 organismes de huit pays. Mais vendredi, les deux partenaires suisses ont décidé d’explorer une autre solution, davantage respectueuse de la vie privée.

Le Figaro (17/04/2020). Interview de Shoshana Zuboff « [Shoshana Zuboff: «Les applications de contrôle de la pandémie doivent être obligatoires, comme les vaccins»](https://www.lefigaro.fr/international/shoshana-zuboff-les-applications-de-controle-de-la-pandemie-doivent-etre-obligatoires-comme-les-vaccins-20200417) » Traduction d'une interview parue originellement dans La Repubblica [«Shoshana Zuboff: 'Le app per il controllo della pandemia possono essere obbligatorie come i vaccini' »](https://rep.repubblica.it/pwa/intervista/2020/04/09/news/shoshana_zuboff_altro_che_privacy_le_app_per_il_controllo_della_pandemia_devono_essere_obbligatorie_come_i_vaccini_-253587046/)

> VU D’AILLEURS - L’auteure du livre L’ère du capitalisme de surveillance nous explique pourquoi les systèmes numériques de contrôle de la pandémie devraient relever du secteur public et donc devenir obligatoires.


L'Usine Nouvelle (16/04/2020) « [Sanofi et Luminostics s'associent pour développer un autotest du Covid-19 sur smartphone](https://www.usine-digitale.fr/article/covid-19-sanofi-et-luminostics-s-associent-pour-developper-un-autotest-sur-smartphone.N954366) »

> Le français Sanofi et la start-up californienne Luminostics font alliance pour le développement d'un test de dépistage du Covid-19 à faire soi-même sur son smartphone. Les travaux doivent débuter dans quelques semaines et la solution pourra voir le jour d'ici à la fin 2020 sous réserve de l'obtention des autorisations réglementaires.


WIRED (14/04/2020), « [The Apple-Google Contact Tracing Plan Won't Stop Covid Alone](https://www.wired.com/story/apple-google-contact-tracing-wont-stop-covid-alone/) »

> Putting the privacy of these smartphone apps aside, few Americans have been tested—and there's a risk of false positives.
> 
> Apple and Google are offering a bold plan using signals from more than 3 billion smartphones to help stem the spread of the deadly coronavirus while protecting users’ privacy. But epidemiologists caution that—privacy concerns aside—the scheme will be of limited use in combating Covid-19 because of the technology’s constraints, inadequate testing for the disease, and users’ reluctance to participate.


Le Monde (10/04/2020), « [Coronavirus : Apple et Google proposent un outil commun pour les applications de traçage des malades](https://www.lemonde.fr/pixels/article/2020/04/10/coronavirus-apple-et-google-proposent-un-outil-commun-pour-les-applications-de-tracage-des-malades_6036278_4408996.html) »

> Les deux géants de la téléphonie vont mettre en place une solution technique mise à disposition des gouvernements.


Wired (05/04/2020), « [Google and Apple Reveal How Covid-19 Alert Apps Might Look](https://www.wired.com/story/apple-google-covid-19-contact-tracing-interface/) »

> As contact tracing plans firm up, the tech giants are sharing new details for their framework—and a potential app interface.
> 
> Finding out that you've been exposed to a serious disease through a push notification may still seem like something out of dystopian science fiction. But the ingredients for that exact scenario will be baked into Google and Apple's operating system in as soon as a matter of days. Now the two companies have shown not only how it will work, but how it could look—and how it'll let you know if you're at risk.


France Inter (04/04/2020) « [Google et le Covid-19 ou le Big data au service des gouvernement](https://www.franceinter.fr/google-et-le-covid-19-ou-le-big-data-au-service-des-gouvernements) »

> Google a mis en ligne vendredi l’ensemble des données de déplacements des utilisateurs (qui ont activé l’option localisation) de son outil de cartographie, Google Maps. Des données qui doivent, selon le géant du web américain, permettre d’aider les dirigeants à améliorer l’efficacité de leur politique de confinement.


La Tribune (24/03/2020), « [Covid-19 et contagion : vers une géolocalisation des smartphones en France ?](https://www.latribune.fr/technos-medias/covid-19-et-contagion-vers-une-geolocalisation-des-smartphones-en-france-843173.html) »

> Un comité de douze chercheurs et médecins va être mis en place à l'Élysée à partir de ce mardi pour conseiller le gouvernement sur les moyens d'endiguer la propagation du coronavirus. Le numérique figure parmi les thèmes à l'étude, notamment pour identifier des personnes ayant été potentiellement exposées au virus. De nombreux pays ont déjà opté pour la géolocalisation des smartphones afin de traquer les déplacements des individus.


# Avis et infos experts

CNIL (25/05/2020) -- [Délibération n° 2020-056 du 25 mai 2020 portant avis sur un projet de décret relatif à l’application mobile dénommée «StopCovid» (demande d’avis n° 20008032)](https://www.cnil.fr/sites/default/files/atoms/files/deliberation-2020-056-25-mai-2020-avis-projet-decret-application-stopcovid.pdf)

> Feu vert de la CNIL.

SI Lex (01/05/2020), par Calimaq : [StopCovid, la subordination sociale et les limites au « Consent Washing »](https://scinfolex.com/2020/05/01/stopcovid-la-subordination-sociale-et-les-limites-au-consent-washing/?print)

> J’ai déjà eu l’occasion dans un précédent billet de dire tout le mal que je pensais de StopCovid et je ne vais pas en rajouter à nouveau dans ce registre. Je voudrais par contre prendre un moment sur la délibération que la CNIL a rendue en fin de semaine dernière à propos du projet d’application, car elle contient à mon sens des éléments extrêmement intéressants, en particulier sur la place du consentement en matière de protection des données personnelles. 


CNIL (26/04/2020), [Publication de l’avis de la CNIL sur le projet d’application mobile « StopCovid »](https://www.cnil.fr/fr/publication-de-lavis-de-la-cnil-sur-le-projet-dapplication-mobile-stopcovid)

> Dans le cadre de l’état d’urgence sanitaire lié à l’épidémie de COVID-19, et plus particulièrement de la stratégie globale de « déconfinement », la CNIL a été saisie d’une demande d’avis par le secrétaire d’État chargé du numérique. Celle-ci concerne l’éventuelle mise en œuvre de « StopCovid » : une application de suivi de contacts dont le téléchargement et l’utilisation reposeraient sur une démarche volontaire. Les membres du collège de la CNIL se sont prononcés le 24 avril 2020.
>
> Dans le contexte exceptionnel de gestion de crise, la CNIL estime le dispositif conforme au Règlement général sur la protection des données (RGPD) si certaines conditions sont respectées. Elle relève qu’un certain nombre de garanties sont apportées par le projet du gouvernement, notamment l’utilisation de pseudonymes.
>
> La CNIL appelle cependant à la vigilance et souligne que l’application ne peut être déployée que si son utilité est suffisamment avérée et si elle est intégrée dans une stratégie sanitaire globale. Elle demande certaines garanties supplémentaires. Elle insiste sur la nécessaire sécurité du dispositif, et fait des préconisations techniques.
>
> Elle demande à pouvoir se prononcer à nouveau après la tenue du débat au Parlement, afin d’examiner les modalités définitives de mise en œuvre du dispositif, s’il était décidé d’y recourir.

Conseil National du Numérique (24/04/2020), « [Stop-Covid. Avis du Conseil National du Numérique](https://cnnumerique.fr/StopCOVID-Avis) »

> Dans le cadre de la réponse à la crise du COVID-19, ​le secrétaire d’État chargé du Numérique a saisi le Conseil national du numérique pour émettre un avis sur les modalités de fonctionnement et de déploiement de l’application pour téléphones mobiles StopCOVID​, dont le développement a été annoncé le 8 avril 2020.


Ligue des Droits de l'Homme (24/04/2020), « [Lettre ouverte concernant le vote sur la mise en œuvre de l’application StopCovid](https://www.ldh-france.org/lettre-ouverte-concernant-le-vote-sur-la-mise-en-oeuvre-de-lapplication-stopcovid/?print) »

> Les risques d’atteinte au respect de la vie privée et au secret médical, les risques de surveillance généralisée au regard d’une efficacité tout à fait incertaine conduisent la Ligue des droits de l’Homme (LDH) à vous demander instamment de vous opposer au projet StopCovid.


Washington Post (23/04/2020). Tribune de Bill Gates (24/04/2020) « [Here are the innovations we need to reopen the economy](https://www.washingtonpost.com/opinions/2020/04/23/bill-gates-here-are-innovations-we-need-reopen-economy/?arc404=true). Version lisible aussi sur son blog en [version courte](The first modern pandemic (short read) ) et en [version longue](https://www.gatesnotes.com/Health/Pandemic-Innovation). On peut voir qu'un informaticien devient « spécialiste » en épidémiologie et comment il promeut du solutionnisme technologique (tout en affirmant que ce n'est pas non plus la panacée). Il propose le contact tracing (tracage de contact)


> Le deuxième domaine dans lequel nous avons besoin d'innovation est le contact tracing. Lorsqu'une personne est testée positive, les professionnels de santé publique doivent savoir qui d'autre cette personne pourrait avoir été infectée.
>
> Pour l'instant, les États-Unis peuvent suivre l'exemple de l'Allemagne : interroger toutes les personnes dont le test est positif et utiliser une base de données pour s'assurer que quelqu'un assure le suivi de tous leurs contacts. Cette approche est loin d'être parfaite, car elle repose sur la déclaration exacte des contacts par la personne infectée et nécessite un suivi personnel important. Mais ce serait une amélioration par rapport à la manière sporadique dont le contact tracing se fait actuellement aux États-Unis.
>
> Une solution encore meilleure serait l'adoption généralisée et volontaire d'outils numériques. Par exemple, il existe des applications qui vous aideront à vous rappeler où vous avez été ; si jamais vous êtes déclaré positif, vous pouvez revoir l'historique ou choisir de le partager avec la personne qui vient vous interroger sur vos contacts. Et certaines personnes ont proposé de permettre aux téléphones de détecter d'autres téléphones qui se trouvent à proximité en utilisant le Bluetooth et en émettant des sons que les humains ne peuvent pas entendre. Si une personne était testée positive, son téléphone enverrait un message aux autres téléphones, et son propriétaire pourrait se faire tester. Si la plupart des gens choisissaient d'installer ce genre d'application, cela en aiderait probablement certains.

Risque-tracage.fr (21/04/2020). [Le traçage anonyme, dangereux oxymore. Analyse de risques à destination des non-spécialistes](https://risques-tracage.fr).

> Des chercheurs de l'INRIA, du LORIA etc. se regroupent pour diffuser un note de vulgarisation sur les risques liés au traçage par téléphone. Ils produisent [un PDF](https://risques-tracage.fr/docs/risques-tracage.pdf) qui circule beaucoup.

Privacy International (20/04/2020) « [There's an app for that: Coronavirus apps](https://privacyinternational.org/long-read/3675/theres-app-coronavirus-apps) »

> Increased trust makes every response to COVID-19 stronger. Lack of trust and confidence can undermine everything. Should we trust governments and industry with their app solutions at this moment of global crisis?
>
> Key findings:
>
> - There are many apps across the world with many different uses.
> - Some apps won't work in the real world; the ones that do work best with more virus testing.
> - They all require trust, and that has yet to be built after years of abuses and exploitation.
> - Apple and Google's contributions are essential to making contact-tracing work for their users; these are positive steps into privacy-by-design, but strict governance is required for making, maintaining and removing this capability.



La Quadrature du Net (14/04/2020), « [Nos arguments pour rejeter StopCovid
Posted](https://www.laquadrature.net/2020/04/14/nos-arguments-pour-rejeter-stopcovid/) »

> Utilisation trop faible, résultats vagues, contre-efficacité sanitaire, discriminations, surveillance, acclimatation sécuritaire...


Ross Anderson (chercheur Univ. Cambridge), (12/04/2020) « [Contact Tracing in the Real World](https://www.lightbluetouchpaper.org/2020/04/12/contact-tracing-in-the-real-world/) »

> Voici le véritable compromis entre la surveillance et la santé publique. Pendant des années, une pandémie a figuré en tête du registre des risques de la Grande-Bretagne, mais on a beaucoup moins dépensé pour s'y préparer que pour prendre des mesures antiterroristes, dont beaucoup étaient plus ostentatoires qu'efficaces. Pire encore, la rhétorique de la terreur a gonflé les agences de sécurité au détriment de la santé publique, prédisposant les gouvernements américain et britannique à ignorer la leçon du SRAS en 2003 et du MERS en 2015 - contrairement aux gouvernements de la Chine, de Singapour, de Taïwan et de la Corée du Sud, qui y ont prêté au moins quelque attention. Ce dont nous avons besoin, c'est d'une redistribution radicale des ressources du complexe industriel de surveillance vers la santé publique.
>
> Nos efforts devraient porter sur l'extension des tests, la fabrication de ventilateurs, le recyclage de tous ceux qui ont une formation clinique, des infirmières vétérinaires aux physiothérapeutes, et la construction d'hôpitaux de campagne. Nous devons dénoncer des conneries quand nous en voyons et ne pas donner aux décideurs politiques le faux espoir que la techno-magie pourrait leur permettre d'éviter les décisions difficiles. Sinon, nous pouvons mieux être utiles en nous tenant à l'écart. La réponse ne doit pas être dictée par les cryptographes mais par les épidémiologistes, et nous devons apprendre ce que nous pouvons des pays qui ont le mieux réussi jusqu'à présent, comme la Corée du Sud et Taïwan.


Union Européenne (08/04/2020) - Rapport technique sur le contact tracing. European Centre for Disease Prevention and Control . « Contact tracing: public health management of persons, including healthcare workers, having had contact with COVID-19cases in the European Union. » [Lien vers le pdf](https://www.ecdc.europa.eu/sites/default/files/documents/Contact-tracing-Public-health-management-persons-including-healthcare-workers-having-had-contact-with-COVID-19-cases-in-the-European-Union%E2%80%93second-update_0.pdf) , [Lien vers la page](https://www.ecdc.europa.eu/en/covid-19-contact-tracing-public-health-management).

> This document aims to helpEU/EEA public health authorities in the tracing and management of persons,including healthcare workers, who had contact with COVID-19 cases. It should be implemented in combination with non-pharmaceutical measures as appropriate.
>
> The purpose of identifying and managing the contacts ofprobable or confirmedCOVID-19casesisto rapidly identify secondary cases that may arise after transmission from the primary known cases in order tointervene andinterruptfurther onward transmission. This is achieved through:
>
> - the prompt identification of contacts of a probable or confirmedcase of COVID-19;
> - providing contacts withinformation on self-quarantine, proper hand hygiene and respiratory etiquette measures,and advice around what to do if they develop symptoms;
> - timely laboratory testingfor all those withsymptoms.


CEVIPOF (01/04/2020), Martial Foucault « [Note 3 - Opinons sur l'usage du téléphone portable en période de crise du Coronavirus](https://www.sciencespo.fr/cevipof/sites/sciencespo.fr.cevipof/files/Note3_COVID-19_Martial_Foucault_VF.pdf) »

> En France, aucune décision n’a encore été prise. Comme le démontrent les exemples étrangers précédents, plusieurs modalités d’usage des données cellulaires sont envisageables, allant d’une politique de santé publique partagée (Singapour) à une politique de surveillance sans consentement (Iran). Dans le cadre de l’enquête comparée «Attitudes sur le COVID-19», nous avons interrogé un échantillon représentatif de Français sur «la possibilité de mobiliser les opérateurs téléphoniques à des fins de contrôle des déplacements». Àcette question, une majorité de personnes exprime une opinion fortement défavorable (48%). Seules 33% des personnes interrogées (échantillon de 2000 personnes) y sont favorables. Le niveau d’indécision sesitue à 18,4%.L’évolution des réponses à cette question posée dans les mêmes termes entre les 16-17 mars et les 24-25 mars montre une dynamique fortement positive. Les raisons de cette progression sont à rechercher  autour  de  l’aggravation  réelle  de  la  pandémie  et  de  la  perception  par  l’opinion  de l’urgence de l’ensemble des politiques sanitaires, technologiques et maintenant numériques.  Le cadrage médiatique, fondé sur des expériences étrangères, du possible recours aux téléphones ne précise pas la position que pourraient prendre les autorités françaises en la matière. C’est pourquoi, ici, la question posée dans les deux enquêtesporte volontairement sur la dimension la plus intrusive sur la vie privée et la plus liberticide (la formulation insiste sur une politique de surveillance).

Chaos Computer Club (06/04/2020), « [10 requirements for the evaluation of "Contact Tracing" apps](https://www.ccc.de/en/updates/2020/contact-tracing-requirements) »

> Loin d'être fermé à la mise en oeuvre d'application mobile de contact tracing le CCC émet 10 recommandation hautement exigeantes pour rendre ces applications acceptables.

Facebook (06/04/2020), « [De nouveaux outils pour aider les chercheurs en santé à suivre et à combattre le COVID-19](https://about.fb.com/fr/news/2020/04/de-nouveaux-outils-pour-aider-les-chercheurs-en-sante-a-suivre-et-a-combattre-la-covid-19/) »

> Dans le cadre du programme Data for Good de Facebook, nous proposons des cartes sur les mouvements de population que les chercheurs et les organisations à but non lucratif utilisent déjà pour comprendre la crise du coronavirus, en utilisant des données agrégées pour protéger la vie privée des personnes. Ils nous ont dit combien ces informations peuvent être précieuses pour répondre au COVID-19, et aujourd’hui nous annonçons de nouveaux outils pour soutenir leur travail :...

Google (blog), (03/04/2020), « [Helping public health officials combat COVID-19](https://www.blog.google/technology/health/covid-19-community-mobility-reports/) »

> Starting today we’re publishing an early release of our COVID-19 Community Mobility Reports to provide insights into what has changed in response to work from home, shelter in place, and other policies aimed at flattening the curve of this pandemic. These reports have been developed to be helpful while adhering to our stringent privacy protocols and policies.



# Officiel

Anti Cor (12/11/2020) « [Application StopCovid : Anticor saisit le parquet national financier](https://www.anticor.org/2020/06/12/application-stopcovid-anticor-saisit-le-parquet-national-financier/) »

> Anticor a déposé un signalement auprès du Procureur de la République, mercredi 10 juin, ayant pour objet l’attribution du contrat de maintenance de l’application StopCovid, qui n’aurait été soumis à aucune procédure de passation de marché public.

FRA (European Union Agency For Fundamentals Rights) - 28/05/2020 « [Les réponses technologiques à la pandémie de COVID-19 doivent également préserver les droits fondamentaux](https://fra.europa.eu/fr/news/2020/les-reponses-technologiques-la-pandemie-de-covid-19-doivent-egalement-preserver-les-droits) »

> Protection des données, respect de la vie privée et nouvelles technologiesAsile, migration et frontièresAccès à l’asileProtection des donnéesEnfants, jeunes et personnes âgéesPersonnes handicapéesOrigine raciale et ethniqueRomsDroits des victimes 
> 
> De nombreux gouvernements sont à la recherche de technologies susceptibles de les aider à suivre et tracer la diffusion de la COVID-19, comme le montre un nouveau rapport de l’Agence des droits fondamentaux de l’UE (FRA). Les gouvernements qui utilisent la technologie pour protéger la santé publique et surmonter la pandémie se doivent de respecter les droits fondamentaux de chacun.


Office parlementaire d'évaluation des choix scientifiques et technologiques, 12/05/2020. [Audition de M. Guillaume Poupard, directeur général de l'Agence nationale de la sécurité des systèmes d'information](https://videos.senat.fr/video.1608918_5ebaa04d6b8cd.audition-de-m-guillaume-poupard-directeur-general-de-l-agence-nationale-de-la-securite-des-systeme?timecode=1840000) (ANSSI) -- Vidéos Sénat.

> Explications sur Stop-Covid, nécessité de conserver la souveraineté de l'Etat dans ce domaine, inquiétudes au sujet de la progression des GAFAM dans le secteur de la santé. Inquiétudes au sujet de la montée en puissance du tracage de contacts dans les dispositifs Apple / Google, et l'éventualité d'un futur développement d'une économie dans ce secteur avec tous les biais que cela suppose.

ANSSI (27/04/2020), « [Application StopCovid –L‘ANSSI apporte à Inria son expertise technique sur le volet sécurité numérique du projet](https://www.ssi.gouv.fr/uploads/2020/04/anssi-communique_presse-20200427-application_stopcovid.pdf) »

> Pilote  du  projet  d’application  StopCovid,  à  la  demande  du  Secrétaire  d’Etat  chargé  du  numérique, Cédric  O,  l’Institut  national  de  recherche  en  sciences  et  technologies  du  numérique  (Inria)  est accompagné par l’Agence nationale de la sécurité des systèmes d’information (ANSSI) sur les aspects de sécurité numérique.

INRIA (26/04/2020). [Communiqué de presse annonçant officiellement l'équipe-projet de l'application Stop-Covid](https://www.inria.fr/fr/stopcovid).

> Inria, ANSSI, Capgemini, Dassault Systèmes, Inserm, Lunabee Studio, Orange, Santé Publique France et Withings  (et Beta.gouv a été débarqué du projet, qui contiendra finalement peu de briques open-source).


# Protocoles

Les protocoles et applications suivants, développés dans le cadre de la lutte contre Covid-19 utilisent en particulier le Bluetooth.

[BlueTrace](https://en.wikipedia.org/wiki/BlueTrace) est le protocole open source inventé au sein du gouvernement de Singapour dans le cadre de la lutte contre la propagation du virus Covid-19. Le protocole est utilisé dans l'application [TraceTogether](https://en.wikipedia.org/wiki/TraceTogether). À son tour le gouvernemetn Australien implémente ce protocole dans l'application [COVIDSafe](https://en.wikipedia.org/wiki/COVIDSafe) distribuée en Australie.

En Europe, le protocole ouvert [Pan-European Privacy-Preserving Proximity Tracing](https://en.wikipedia.org/wiki/Pan-European_Privacy-Preserving_Proximity_Tracing) (PEPP-PT/PEPP) utilise le bluetooth pour rechercher des contacts mais centralise des rapports sur un serveur centralisé. Son concurrent open source [Decentralized Privacy-Preserving Proximity Tracing](https://en.wikipedia.org/wiki/Decentralized_Privacy-Preserving_Proximity_Tracing) ne permet pas au serveur central de signalement d'accéder aux journaux de contacts.

Pour la France, le protocole ROBERT (ROBust and privacy-presERving proximity Tracing) a été [publié](https://github.com/ROBERT-proximity-tracing/documents) par l'INRIA le 18 avril 2020

Listes d'applications Covid-19 : [fr.wikipedia.org/wiki/Applications_Covid-19](https://fr.wikipedia.org/wiki/Applications_Covid-19)

