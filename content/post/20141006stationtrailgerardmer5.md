---
title: "Station de trail de Gérardmer - Parcours 5"
date: 2014-10-06
author: "Christophe Masutti"
description: "Une nouvelle station avec plusieurs parcours pour tous les niveaux"
image: "/images/postimages/hikebike.png"
tags: ["Sport", "Trail", "Vosges", "Gérardmer"]
categories:
- Sport
---

Le réseau des stations de trail est une initiative conjointe de la
société Raidlight et d'acteurs territoriaux (voir
l'[historique](http://www.stationdetrail.com/Le-concept-de-Station-de-Trail-c.html)).
Depuis le printemps 2014, Gérardmer dispose de sa station de Trail et
fait désormais partie du réseau. Au moins deux événements réguliers et
célèbres se déroulent sur la station : l'[Ultra-montée du
Tetras](http://www.stationdetrail.com/L-Ultra-Montee-du-Tetras.html) et
le [Trail de la Vallée des Lacs](http://trailvalleedeslacs.com/). Mais
la station propose plusieurs parcours et ateliers qui présentent un
grand intérêt pour les coureurs, de passage ou habitant des environs, et
permettent de découvrir la région de Gérardmer d'une manière
originale...

Description
-----------

Se rendre à la station de trail est très
simple. Arrivé à Gérardmer, suivez simplement les indications des pistes
de ski alpin de la Mauselaine. Allez tout au bout du parking, c'est là
que se situe le départ

Si toutefois vous souhaitez profiter des
dispositifs de la station de trail (casier, douche, salle de repos) pour
une somme modique (5 euros la journée), il vous faudra passer à l'Office
du tourisme au préalable. Tout est expliqué sur le [site de la
sation](http://www.stationdetrail.com/La-Base-d-accueil-de-la-Station,388.html).

Par ailleurs, [cet article de Fredéric
Flèche](http://www.guide-restaurants-et-voyages-du-monde.com/bonus.php?article_id=1247&titre=Test%20de%20la%20Station%20Trail%20de%20Gerardmer&item=44)
montre très bien, avec des photos, la configuration des lieux.

Une fois
à la Mauselaine, rendez-vous au niveau des caisses de la station de ski,
poursuivez le chemin en direction du télésiège sur une centaine de
mètres. Vous trouverez un mur en béton sur lequel sont affichés les
différents parcours et ateliers.

La signalétique est simple : vous
choisissez votre parcours et vous suivez son numéro inscrit sur les
balises. Plusieurs parcours empruntent parfois les mêmes balises, ce qui
explique que celles-ci puissent être composées de plusieurs chiffres
(par exemple 57 pour les parcours 5 et 7). Un code couleur (niveau de
difficulté) est aussi disponible. Le mieux est de choisir votre parcours
depuis chez vous sur le site de la station : vous pourrez ainsi
consulter la météo (parfois fort capricieuse en ces lieux) et regarder
de plus près le profil altimétrique.

Nous allons nous pencher plus
précisément sur l'un des parcours de la station la **Boucle des
Écarts**, le numéro 5.

Le parcours 5
-------------

Les parcours de la station n'ont pas seulement été élaborés en fonction
des aspects techniques des entraînements. Ils ont aussi été imaginés par
des amoureux de la Vallée des Lacs, soucieux de procurer au traileur les
meilleurs souvenirs.

Au programme : des points de vues enchanteurs, une
végétation changeante et des lieux parfois même trop peu connus des
habitants.

Si le soleil est au rendez-vous, bien que discret dans les
sous-bois, votre course sera d'autant plus agréable.

Avant tout, il faut
expliquer pourquoi la **Boucle des Écarts** porte ce nom. Un écart, dans
les Hautes Vosges, est une petite prairie isolée sur les hauteurs d'une
vallée, souvent dotée d'une ou plusieurs fermes. Au fil du parcours,
vous allez donc croiser des lieux forts anciens. La plupart du temps, en
l'absence d'élevage, vous ne verrez que des épicéas. Mais à l'époque,
tous ces endroits étaient fortement défrichés et servaient de pâturages.

Dans l'ordre du parcours, les noms des principaux écarts sont les
suivants : La Mauselaine (avant d'être une station de ski), la Rayée, le
Grand Étang, Les Bas Rupts, La Poussière, la Basse du Rôle, l'Urson, la
Grande Mougeon, Le Phény, Mérelle (étang), Ramberchamp, Le Costet.

Passons au vif du sujet. Le parcours est dans l'ensemble un bon
entraînement aux montées, bien que celles-ci soient assez courtes. La
distance est de 16 km pour 762m+, ce qui ne demande pas un haut niveau
technique.

La morphologie du terrain comporte du sentier (50%, dont deux
passages pierreux en descente), du chemin forestier (35%) du bitume (15%
surtout en ville). Les bâtons ne sont pas du tout nécessaires. Sur
l'ensemble du parcours le balisage est très clair et les balises ne
manquent pas (même si on peut toujours améliorer l'existant déjà de très
bonne qualité). Par ailleurs, l'état du parcours est normalement indiqué
sur le site de la station de Trail qui insiste bien sur ce point : se
renseigner avant de partir&nbsp;!

Si vous partez depuis le mur des parcours, à
la Mauselaine, redescendez quelques foulées en direction du parking : le
parcours débute par l'ascension d'une piste de ski (là où se situent les
petits téleskis)...

Retournez-vous de temps en temps pour admirer le
point de vue sur Gérardmer. Sur la totalité du parcours on peut recenser
quelques montées et descentes remarquables (plus ou moins techniques).

Je vous conseille d'imprimer [le topo](http://www.stationdetrail.com/sites/stationdetrail/IMG/pdf/stationvosges_Rando_trail_-_n_5_Bleu_-_La_Boucle_des_Ecarts.pdf)
disponible sur le site de la station, avec le profil altimétrique. Il y
quatre passages qui selon moi méritent d'être mentionnés :

1.  La première montée, sur une piste de ski herbeuse et parfumée...
    Cette manière de débuter un parcours n'est pas forcément
    ultra-confortable, alors on y va doucement car la montée est assez
    longue. Cette partie du parcours est néanmoins originale, et aucune
    pierre ne vient vous gâcher la foulée.
2.  La montée depuis le Col du Haut de la côte (km 4 à 6): il s'agit
    d'un chemin forestier très roulant en faux-plat qui se termine par
    une côte raide à travers les épicéas. Là, il faut savoir monter sans
    précipitation, mais cette difficulté reste assez courte.
3.  La descente depuis Mérelle jusqu'au lac de Gérardmer (km 11 à 12):
    un sentier assez pierreux sur lequel il faut absolument rester
    attentif aux promeneurs. Attention par temps de pluie aux pierres
    glissantes.
4.  La dernière montée, depuis les rues de Gérardmer jusqu'à la
    Mauselaine en passant par la Roche du Rain et le Costet (km 14 à
    16): sachez garder un peu d'énergie sous la semelle car il est
    facile de se laisser aller à quelques accélérations en fin de
    parcours et se retrouver à sec lorsqu'il faut dépenser encore de
    l'énergie jusqu'à l'arrivée.

Lorsque vous arrivez au pied de la [Tour de Mérelle](http://fr.wikipedia.org/wiki/G%C3%A9rardmer#mediaviewer/File:Tour_de_M%C3%A9relle2.jpg),
et si vous ne connaissez pas cet endroit, je vous conseille fortement
d'appuyer sur la touche STOP de votre chrono et de gravir la centaine de
marches de la tour : une splendide vue sur la vallée de Gérardmer vous y
attend! S'il n'y a aucun visiteur, ne vous arrêtez pas et considérez
cette ascension comme une partie du parcours, cela ajoutera un peu de
dénivelé (mais ne descendez pas les marches en courant, c'est trop
dangereux).

Enfin, vous aurez noté que le parcours passe sur les bords
du lac de Gérardmer puis dans le centre ville... Suivant l'heure de la
journée, vous risquez de vous retrouver à slalomer entre les passants,
ce qui peut ne pas être agréable, surtout en fin de parcours.

Le bord du
lac, en particulier au niveau de la passerelle du complexe nautique,
figure parmi les endroits les plus fréquentés en saison et les week-end:
enfants en vélo, poussettes, sorties familiales, etc. Essayez donc
plutôt de passer derrière le complexe nautique pour rejoindre
directement la D486, même si cela vous oblige à arpenter du bitume.

Vous pouvez aussi éviter les trottoirs du centre ville de la manière suivante&nbsp;: si vous passez par la D486, vous tomberez sur un rond-point. Prenez à
droite (Rue du 29e BCP), montez et prenez la deuxième rue à gauche (Rue
Haute) : vous évitez le centre ville et arriverez derrière la mairie.
Vous pouvez alors rattraper le parcours à cet endroit.

Le tracé en lien ci-dessous **n'est pas** le parcours normal : pour moi, le
début se situe à Gérardmer. Mais vous pouvez voir comment j'évite le
centre ville.

[Tracé du parcours](https://umap.openstreetmap.fr/fr/map/carte-sans-nom_18663)
