---
title: "Entraînement trail, autour de Gérardmer"
date: 2014-07-17
author: "Christophe Masutti"
image: "/images/postimages/hikebike.png"
tags: ["Sport", "Trail", "Entraînement", "Parcours"]
description: "Une balade typiquement vosgienne autour de la Perle des Vosges"
categories:
- Sport
---

Au cas où parmi les lecteurs de ce blog certains ne sauraient pas où aller courir, je compte proposer quelques parcours de trail que j'ai testé au moins deux ou trois fois. Aujourd'hui, une cuvée personnelle sur les hauteurs de [Gérardmer](http://fr.wikipedia.org/wiki/G%C3%A9rardmer). Vous aurez de même l'occasion de croiser les pistes de la [station de trail gérômoise](http://www.stationdetrail.com/-Station-de-Trail-Vosges-Gerardmer-.html), dont j'aurai l'occasion de parler ultérieurement. Ce parcours a une vocation à la fois touristique et sportive. Il est adapté au traileurs débutants qui, sans forcer dans les montées où les passages peuvent être techniques, pourront assez facilement arriver au bout. Il vous mènera vers les « écarts » gérômois. Vous admirerez quelques anciennes fermes remarquables ainsi que des points de vues originaux et peu fréquentés.


## Commentaires

<dl>
- **Km 0 - 2,5—** Le parcours débute doucement en guise d'échauffement. Depuis le Square Briffaut, on longe la rive du lac de Gérardmer, en passant par le sentier du Tour du Lac. Toutefois, attention aux promeneurs au km 2 : le passage est rocheux et étroit. Si cette solution ne vous tente pas, il est possible de passer par la route, légèrement en amont.
- **Km 2,5 - 3,5—** Montée de la Cascade de Mérelle. Sur un chemin romantique (assez humide et glissant par moment) on remonte le ruisseau de Mérelle, jusque sa cascade. Au delà de cette dernière, il est possible de reprendre la course progressivement pour enchaîner jusqu'au sommet. Cette section est assez technique : sol rocailleux et humide.
- **Km 3,5 - 6—** Une section typique des hauts gérômois avec trois écarts remarquables : Frémont, Le Pré Chaussotte, Noirupt. Le chemin monte progressivement jusqu'au premier sommet du parcours. Aucune difficulté de terrain, chemin large.
- **km 6 - 9,5—** Descente jusqu'au Col de Sapois, puis jonction jusqu'au Col du Haut de la Côte. Une section très rapide au début, si on pousse un peu le rythme de la descente, il sera possible ensuite de récupérer doucement sur le chemin de l'Urson. Pas de difficulté, on peut en profiter pour se nourrir un peu.
- **km 9,5 - 12,7—** L’endurance est la clé de cette section longue et progressive. On emprunte les pistes de ski nordique de Gérardmer jusqu'au lieu-dit les Hautes Vannes. Garder de l'énergie sous la semelle pour enchaîner la section suivante.
- **km 12,7 - 13,9—** La montée vous mènera jusqu'au chalet-refuge de la Croix Claudé. Le chemin est tantôt rocailleux, herbeux et humide. Les plus aguerris pourront courir, au moins par moment, mais attention à la longueur... et le parcours n'est pas fini...
- **km 13,9 - 15,6—** La crête la plus sympathique du parcours puisque, en cas de beau temps et à partir de la Roche des Bioqués, on pourra admirer la superbe vue sur la vallée du Chajoux, l'une des deux vallées de la commune de La Bresse. Le sentier est terreux et rocailleux, et se termine en pente douce jusqu'à la reprise vers le sommet de Grouvelin
- **km 15,6 - 17,3—** Montée puis descente du sommet de Grouvelin vers la ferme de Grouvelin. N'hésitez pas à marquer un arrêt au sommet, à la table d'orientation (1137m)!
- **km 17,3 - 21,3—** Sur chemin large, descente vers la Mauselaine à travers les piste de ski alpin de Gérardmer. Vue sur la vallée de Gérardmer... Attention aux cuisses!
- **km 17,3 - 22—** Après un peu de bitume (inévitable), descente assez technique par la Roche du Rain. Attention aux promeneurs en sens inverse (ou aux traileurs qui terminent l'un des parcours de la station de Trail!). Sentier rocailleux et terreux.
- **km 22 - fin—** Retour dans la ville de Gérardmer, en évitant le centre, jusqu'au Square Briffaut.


## Infos


- Longueur : 23,56 km
- Denivelé positif cumulé : 998 m
- Denivelé négatif cumulé : 991 m
- Altitude maxi : 1129 m
- Altitude mini : 659 m
- [Voir la carte du parcours](http://umap.openstreetmap.fr/fr/map/les-hauts-23_14504)


