---
title: "Confessions d’un voleur"
subtitle: "Internet : la liberté confisquée"
date: 2002-03-31
image: "/images/biblio/laurent-chemla.jpg"
author: "Christophe Masutti"
description: "Chemla, Laurent. Confessions d’un voleur. Internet : la liberté confisquée. Denoël impacts, 2002"
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Chemla"]
---

L'essor des intelligences artificielles réactualise une prophétie lancinante : avec le remplacement des êtres humains par les machines, le travail serait appelé à disparaître. Si certains s'en alarment, d'autres voient dans la « disruption numérique » une promesse d'émancipation fondée sur la participation, l'ouverture et le partage. Les coulisses de ce théâtre de marionnettes (sans fils) donnent cependant à voir un tout autre spectacle. Celui des usagers qui alimentent gratuitement les réseaux sociaux de données personnelles et de contenus créatifs monnayés par les géants du Web. Celui des prestataires des start-ups de l'économie collaborative, dont le quotidien connecté consiste moins à conduire des véhicules ou à assister des personnes qu'à produire des flux d'informations sur leur smartphone. Celui des microtravailleurs rivés à leurs écrans qui, à domicile ou depuis des « fermes à clics », propulsent la viralité des marques, filtrent les images pornographiques et violentes ou saisissent à la chaîne des fragments de textes pour faire fonctionner des logiciels de traduction automatique. En dissipant l'illusion de l'automation intelligente, Antonio Casilli fait apparaître la réalité du digital labor : l'exploitation des petites mains de l'intelligence « artificielle », ces myriades de tâcherons du clic soumis au management algorithmique de plateformes en passe de reconfigurer et de précariser le travail humain.

----

Chemla, Laurent. *Confessions d’un voleur. Internet : la liberté confisquée*. Denoël impacts, 2002.


**Lien vers le site :** http://www.confessions-voleur.net

----
