---
title: "Digital disconnect"
subtitle: "How capitalism is turning the internet against democracy"
date: 2014-02-01
image: "/images/biblio/Robert-McChesney.jpg"
author: "Christophe Masutti"
description: "McChesney, Robert Waterman. Digital disconnect. How capitalism is turning the internet against democracy. The New Press, 2014."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["McChesney"]
---

Celebrants and skeptics alike have produced valuable analyses of the Internet’s effect on us and our world, oscillating between utopian bliss and dystopian hell. But according to Robert W. McChesney, arguments on both sides fail to address the relationship between economic power and the digital world.

McChesney’s award-winning *Rich Media, Poor Democracy* skewered the assumption that a society drenched in commercial information is a democratic one. In *Digital Disconnect*, McChesney returns to this provocative thesis in light of the advances of the digital age. He argues that the sharp decline in the enforcement of antitrust violations, the increase in patents on digital technology and proprietary systems and massive indirect subsidies and other policies have made the internet a place of numbing commercialism. A handful of monopolies now dominate the political economy, from Google, which garners a 97 percent share of the mobile search market, to Microsoft, whose operating system is used by over 90 percent of the world’s computers. Capitalism’s colonization of the Internet has spurred the collapse of credible journalism and made the Internet an unparalleled apparatus for government and corporate surveillance and a disturbingly antidemocratic force.

In *Digital Disconnect*, Robert McChesney offers a groundbreaking critique of the Internet, urging us to reclaim the democratizing potential of the digital revolution while we still can.

----

McChesney, Robert Waterman. *Digital disconnect. How capitalism is turning the internet against democracy*. The New Press, 2014.


**Lien vers le site de l'éditeur :** https://thenewpress.com/books/digital-disconnect


----
