---
title: "L’utopie déchue"
subtitle: "Une contre-histoire d’Internet, XVe-XXIe siècle"
date: 2019-12-21
image: "/images/biblio/felix-treguer.jpg"
author: "Christophe Masutti"
description: "Treguer, Félix. L’utopie déchue. Une contre-histoire d’Internet, XVe-XXIe siècle. Fayard, 2019."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Treguer"]
---

Ce livre est écrit comme un droit d'inventaire. Alors qu'Internet a été à ses débuts perçu comme une technologie qui pourrait servir au développement de pratiques émancipatrices, il semble aujourd'hui être devenu un redoutable instrument des pouvoirs étatiques et économiques.

Pour comprendre pourquoi le projet émancipateur longtemps associé à cette technologie a été tenu en échec, il faut replacer cette séquence dans une histoire longue : celle des conflits qui ont émergé chaque fois que de nouveaux moyens de communication ont été inventés. Depuis la naissance de l'imprimerie, les stratégies étatiques de censure, de surveillance, de propagande se sont sans cesse transformées et sont parvenues à domestiquer ce qui semblait les contester. Menacé par l'apparition d'Internet et ses appropriations subversives, l'État a su restaurer son emprise sous des formes inédites au gré d'alliances avec les seigneurs du capitalisme numérique tandis que les usages militants d'Internet faisaient l'objet d'une violente répression.

Après dix années d'engagement en faveur des libertés sur Internet, Félix Tréguer analyse avec lucidité les fondements antidémocratiques de nos régimes politiques et la formidable capacité de l'État à façonner la technologie dans un but de contrôle social. Au-delà d'Internet, cet ouvrage peut se lire comme une méditation sur l'utopie, les raisons de nos échecs passés et les conditions de l'invention de pratiques subversives. Il interpelle ainsi l'ensemble des acteurs qui luttent pour la transformation sociale.


----

Tréguer, Félix. *L’utopie déchue. Une contre-histoire d’Internet, XVe-XXIe siècle*. Fayard, 2019.



**Lien vers le site de l'éditeur :** https://www.fayard.fr/sciences-humaines/lutopie-dechue-9782213710044


----
