---
title: "Richard Stallman et la révolution du logiciel libre"
subtitle: "Une biographie autorisée"
date: 2010-10-21
image: "/images/biblio/Stallman-revol-lbre.jpg"
author: "Christophe Masutti"
description: "Williams, Sam, Masutti, Christophe et Stallman, Richard. Richard Stallman et la révolution du logiciel libre. Une biographie autorisée. Eyrolles / Framasoft, 2010."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["stallman", "williams", "masutti"]
---

Né en 1953, Richard Stallman est un programmeur américain hors pair considéré comme le « père » du logiciel libre.

Son héritage est unanimement reconnu et son influence toujours plus grande sur nos sociétés actuelles de l’information et de la communication. Ses conférences en français débutent invariablement ainsi : « Je puis résumer le logiciel libre en trois mots : liberté, égalité, fraternité… ».

Cette biographie éclaire sans complaisance la vie de ce personnage autant décrié qu’encensé qui a révolutionné l’histoire du logiciel en particulier en initiant le projet GNU. À travers cet ouvrage, nous pouvons mieux connaître le parcours et les combats de cet homme hors du commun.


----

Williams, Sam, Masutti, Christophe et Stallman, Richard. *Richard Stallman et la révolution du logiciel libre. Une biographie autorisée*. Eyrolles / Framasoft, 2010.


**Lien vers le site de l'éditeur :** https://framabook.org/richard-stallman-et-la-revolution-du-logiciel-libre-2/


----
