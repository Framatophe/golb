---
title: "La renaissance des communs"
subtitle: "Pour une société de coopération et de partage"
date: 2014-03-31
image: "/images/biblio/David-Bollier.jpg"
author: "Christophe Masutti"
description: "Bollier, David. La renaissance des communs: pour une société de coopération et de partage. Traduit par Olivier Petitjean, C. L. Mayer, 2014."
categories: ["bibliographie"]
auteurs: ["Bollier"]
---

De nombreux domaines de notre patrimoine commun sont actuellement en état de siège : l’eau, la terre, les forêts, les pêcheries, les organismes vivants, mais aussi les œuvres créatives, l’information, les espaces publics, les cultures indigènes… Pour proposer une réponse aux multiples crises, économiques, sociales et environnementales, que connaît la notre société actuelle, David Bollier invite à revenir sur cette notion de « communs », un ensemble de pratiques sociales collectives que la modernité industrielle a fait progressivement disparaître. Aujourd’hui, les communs doivent être appréhendés non comme des ressources dont tout le monde aurait la libre jouissance, mais comme un système de coopération et de gouvernance permettant de préserver et de créer des formes de richesse partagée. L’auteur montre comment ils peuvent remédier à nos maux économiques en. Car Cette approche, mettant en avant une théorie plus riche de la valeur que l’économie conventionnelle, implique de nouveaux modèles de production, des formes plus ouvertes et responsables de participation des citoyens ainsi qu’une culture d’innovation sociale. C’est ce dont témoignent les actions et initiatives des différents mouvements des « commoneurs » à travers le monde, déterminés à construire des alternatives vivantes et fonctionnelles à l’étau des grandes technocraties publiques et privées.

Cet ouvrage devrait permettre d’éclairer et de promouvoir l’enjeu des communs aussi bien auprès des universitaires et des élus que des militants associatifs et autres citoyens engagés.

----

Bollier, David. *La renaissance des communs: pour une société de coopération et de partage*. Traduit par Olivier Petitjean, C. L. Mayer, 2014.


**Lien vers le site de l'éditeur:** http://www.eclm.fr/ouvrage-364.html

----
