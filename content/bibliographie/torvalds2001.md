---
title: "Il était une fois Linux"
subtitle: "L’Extraordinaire Histoire d’une révolution accidentelle"
date: 2001-05-21
image: "/images/biblio/linus-torvalds.jpg"
author: "Christophe Masutti"
description: "Torvalds, Linus, et David Diamond. Il était une fois Linux. L’extraordinaire Histoire d’une révolution accidentelle. Eyrolles Multimédia, 2001."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Torvalds", "Diamond"]
---

Il y a aujourd'hui dix ans, un étudiant finlandais nommé Linus Torvalds s'enfermait plusieurs mois, dans sa chambre, rideaux tirés, pour un long tête-à-tête avec son ordinateur. Le résultat : un nouveau système d'exploitation. Qu'allait-il en faire ? Le garder pour son usage personnel ? Le vendre à une société de logiciels? Rien de tout cela. Linus décide de rendre le fruit de son travail librement accessible sur Internet en invitant toute personne intéressée à l'améliorer. L'UNIX libre de Linus, baptisé Linux, était né et avec lui, une nouvelle manière de concevoir les logiciels qui allait bouleverser le monde de l'informatique.

La suite des événements fera date dans l'histoire. Linus Torvalds est devenu la figure emblématique du monde du logiciel libre. Son puissant système d'exploitation est aujourd'hui un acteur de tout premier plan de l'industrie informatique. La méthode de conception utilisée, nourrie de passion volontaire, fait de Linux le plus vaste projet de création collective qui ait jamais existé.

Pour la première fois, Linus Torvalds raconte, dans ce livre, son étonnant parcours : sa fascination, tout enfant, pour la calculatrice de son grand-père, professeur de statistiques à l'Université d'Helsinki, sa première rencontre en 1981 avec un ordinateur, un Commodore VIC-20 et bien sûr les circonstances de la création du noyau Linux, le composant essentiel du système GNU.


----

Torvalds, Linus, et David Diamond. *Il était une fois Linux. L’extraordinaire Histoire d’une révolution accidentelle*. Eyrolles Multimédia, 2001.


**Lien vers le site de l'éditeur :** https://www.eyrolles.com/Informatique/Livre/il-etait-une-fois-linux-9782746403215/


----
