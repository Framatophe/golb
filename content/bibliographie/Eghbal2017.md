---
title: "Sur quoi reposent nos infrastructures numériques ?"
date: 2018-03-31
image: "/images/biblio/Nadia-Eghbal.jpg"
author: "Christophe Masutti"
description: "Eghbal, Nadia. Sur quoi reposent nos infrastructures numériques ? Lyon, Framasoft & Open Editions, 2017."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Eghbal"]
---

Aujourd’hui, la quasi-totalité des logiciels couramment utilisés sont tributaires de code dit « open source », créé et maintenu par des communautés composées de développeurs et d’autres talents. Ce code peut être repris, modifié et utilisé par n’importe qui, entreprise ou particulier, pour créer ses propres logiciels. Partagé, ce code constitue ainsi l’infrastructure numérique de la société d’aujourd’hui… dont les fondations menacent cependant de céder sous la demande !


En effet, dans un monde régi par la technologie, qu’il s’agisse des entreprises du Fortune 500, du Gouvernement, des grandes entreprises de logiciel ou des startups, nous sommes en train d’accroître la charge de ceux qui produisent et entretiennent cette infrastructure partagée. Or, comme ces communautés sont assez discrètes, les utilisateurs ont mis longtemps à en prendre conscience.

Tout comme l’infrastructure matérielle, l’infrastructure numérique nécessite pourtant une maintenance et un entretien réguliers. Face à une demande sans précédent, si nous ne soutenons pas cette infrastructure, les conséquences seront nombreuses.

L’entretien de notre infrastructure numérique est une idée nouvelle pour beaucoup, et les problèmes que cela pose sont mal cernés. Dans cet ouvrage, Nadia Eghbal met au jour les défis uniques auxquels sont confrontées les infrastructures numériques et comment l’on peut œuvrer pour les relever.

----

Eghbal, Nadia. *Sur quoi reposent nos infrastructures numériques ?*, Lyon, Framasoft & Open Editions, 2017.


**Lien vers le site de l'éditeur :** https://framabook.org/sur-quoi-reposent-nos-infrastructures-numeriques/

----
