---
title: "Culture numérique"
date: 2019-02-02
image: "/images/biblio/Dominique-Cardon.jpg"
author: "Christophe Masutti"
description: "Cardon, Dominique. Culture numérique. SciencesPo les presses, 2019"
categories: ["bibliographie"]
auteurs: ["Cardon"]
---

La révolu­tion digitale est venue insérer des connaissances et des informations dans tous les aspects de nos vies. Jusqu'aux machines, qu'elle est en train de rendre intelligentes. Si nous fabri­quons le numérique, il nous fabrique aussi. Voilà pourquoi il est indispensable que nous nous forgions une culture numérique. Lire la suite

L'entrée du numérique dans nos sociétés est souvent comparée aux grandes ruptures technologiques des révolutions industrielles.

En réalité, c'est avec l'invention de l'imprimerie que la comparaison s’impose, car la révolu­tion digitale est avant tout d’ordre cognitif. Elle est venue insérer des connaissances et des informations dans tous les aspects de nos vies. Jusqu’aux machines, qu’elle est en train de rendre intelligentes.

Si nous fabri­quons le numérique, il nous fabrique aussi. Voilà pourquoi il est indispensable que nous nous forgions une culture numérique.

----

Cardon, Dominique. *Culture numérique*. SciencesPo les presses, 2019.

**Lien vers le site de l'éditeur :** http://www.pressesdesciencespo.fr/fr/book/?gcoi=27246100540390

----
