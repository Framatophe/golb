---
title: "Obfuscation"
subtitle: "La vie privée, mode d’emploi"
date: 2019-08-02
image: "/images/biblio/helen-nissenbaum.jpg"
author: "Christophe Masutti"
description: "Nissenbaum, Helen Fay, et Finn Brunton. *Obfuscation. La vie privée, mode d’emploi*. Traduit par Elena Marconi, C&F éditions, 2019."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Nissenbaum", "Brunton"]
---

« Où le Sage cache-t-il une feuille ? Dans la forêt. Mais s'il n'y a pas de forêt, que fait-t-il ?… Il fait pousser une forêt pour la cacher. »
L'obfuscation, magistralement illustrée par l'auteur de roman G. K. Chesterton.

Dans ce monde de la sélection par des algorithmes, de la publicité ciblée et du marché des données personnelles, rester maîtres de nos actions, de nos relations, de nos goûts, de nos navigations et de nos requêtes implique d'aller au delà de la longue tradition de l'art du camouflage. Si on peut difficilement échapper à la surveillance numérique, ou effacer ses données, il est toujours possible de noyer nos traces parmi de multiples semblables, de créer nous-mêmes un brouillard d'interactions factices.

Quels en sont alors les enjeux et les conséquences ? Finn Brunton et Helen Nissenbaum ayant constaté l'asymétrie de pouvoir et d'information entre usagers et plateformes dressent le bilan, proposent des actions et prennent le temps de la réflexion : pourquoi et comment reconquérir son autonomie personnelle ? Comment résister éthiquement avec les armes du faible ? Comment réfléchir ensemble à ce que l'obfuscation nous fait découvrir sur l'influence mentale exercée par les puissants du numérique ?

----

Nissenbaum, Helen Fay, et Finn Brunton. *Obfuscation. La vie privée, mode d’emploi*. Traduit par Elena Marconi, C&F éditions, 2019.


**Lien vers le site de l'éditeur :** https://cfeditions.com/obfuscation/ 


----
