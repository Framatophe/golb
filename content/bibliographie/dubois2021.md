---
title: "Contrôler les assistés"
subtitle: "Genèses et usages d’un mot d’ordre"
date: 2021-04-15
image: "/images/biblio/vincent-dubois.jpg"
author: "Christophe Masutti"
description: "Dubois, Vincent. Contrôler les assistés. Genèses et usages d’un mot d’ordre. Raisons d'Agir, 2021."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Dubois"]
---

Contrôler les assistés s’est imposé à partir des années 1990 en France comme un mot d’ordre politique, bureaucratique et moral. Jamais les bénéficiaires d’aides sociales, et parmi eux les plus précaires, n’avaient été aussi rigoureusement surveillés, ni leurs illégalismes ou leurs erreurs si sévèrement sanctionnés. Ce renforcement du contrôle n’est cependant pas réductible à des préoccupations financières. Ainsi, moins sévèrement réprimés, l’évasion fiscale ou les défauts de paiement des cotisations sociales par les employeurs atteignent des montants sans commune mesure avec ceux qui concernent les erreurs ou abus des bénéficiaires d’aides sociales, traqués sans relâche.

Un mécanisme implacable à plusieurs facettes sous-tend cette spirale rigoriste à l’égard des assistés : des leaders politiques qui pourfendent la fraude sociale et qui parviennent à stigmatiser leurs contradicteurs comme naïfs ou complices ; des administrations qui surenchérissent dans des technologies de contrôle toujours plus performantes ; une division du travail bureaucratique qui déréalise et déshumanise le traitement des cas ; le fonctionnement interne de commissions où la clémence est toujours plus difficile à défendre que la sévérité ; le point d’honneur professionnel du contrôleur de la caisse locale qui traque la moindre erreur au nom de l’exactitude des dossiers.

Au nom de la responsabilisation individuelle, de la lutte contre l’abus, de la maîtrise des dépenses, un service public fondamental qui vise à garantir des conditions de vie dignes à tous les citoyens contribue désormais à un gouvernement néopaternaliste des conduites qui stigmatise et précarise les plus faibles.

----

Dubois, Vincent. *Contrôler les assistés. Genèses et usages d’un mot d’ordre*. Raisons d'Agir, 2021.


**Lien vers le site de l'éditeur :** https://www.raisonsdagir-editions.org/catalogue/controler-les-assistes/


----
