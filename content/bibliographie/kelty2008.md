---
title: "Two bits"
subtitle: "The cultural significance of free software"
date: 2008-04-21
image: "/images/biblio/Christopher-Kelty-two-bits.jpg"
author: "Christophe Masutti"
description: "Kelty, Christopher. Two bits. The cultural significance of free software. Duke University Press, 2008."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Kelty"]
---

In Two Bits, Christopher M. Kelty investigates the history and cultural significance of Free Software, revealing the people and practices that have transformed not only software, but also music, film, science, and education.

Free Software is a set of practices devoted to the collaborative creation of software source code that is made openly and freely available through an unconventional use of copyright law. Kelty shows how these specific practices have reoriented the relations of power around the creation, dissemination, and authorization of all kinds of knowledge after the arrival of the Internet. Two Bits also makes an important contribution to discussions of public spheres and social imaginaries by demonstrating how Free Software is a "recursive public" public organized around the ability to build, modify, and maintain the very infrastructure that gives it life in the first place.

Drawing on ethnographic research that took him from an Internet healthcare start-up company in Boston to media labs in Berlin to young entrepreneurs in Bangalore, Kelty describes the technologies and the moral vision that binds together hackers, geeks, lawyers, and other Free Software advocates. In each case, he shows how their practices and way of life include not only the sharing of software source code but also ways of conceptualizing openness, writing copyright licenses, coordinating collaboration, and proselytizing for the movement. By exploring in detail how these practices came together as the Free Software movement from the 1970s to the 1990s, Kelty also shows how it is possible to understand the new movements that are emerging out of Free Software: projects such as Creative Commons, a nonprofit organization that creates copyright licenses, and Connexions, a project to create an online scholarly textbook commons.

----

Kelty, Christopher. *Two bits. The cultural significance of free software*. Duke University Press, 2008.


**Lien vers le site de l'éditeur :** https://twobits.net/

----
