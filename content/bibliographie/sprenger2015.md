---
title: "The Politics of Micro-Decisions"
subtitle: "Edward Snowden, Net Neutrality, and the Architectures of the Internet"
date: 2015-04-21
image: "/images/biblio/micro-decision-politic.jpg"
author: "Christophe Masutti"
description: "Sprenger, Florian. The Politics of Micro-Decisions. Edward Snowden, Net Neutrality, and the Architectures of the Internet. Meson Press, 2015."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Sprenger"]
---

Be it in the case of opening a website, sending an email, or high-frequency trading, bits and bytes of information have to cross numerous nodes at which micro-decisions are made. These decisions concern the most efficient path through the network, the processing speed, or the priority of incoming data packets. Despite their multifaceted nature, micro-decisions are a dimension of control and surveillance in the twenty-first century that has received little critical attention. They represent the smallest unit and the technical precondition of a contemporary network politics - and of our potential opposition to it. The current debates regarding net neutrality and Edward Snowden's revelation of NSA surveillance are only the tip of the iceberg. What is at stake is nothing less than the future of the Internet as we know it.


----

Sprenger, Florian. *The Politics of Micro-Decisions. Edward Snowden, Net Neutrality, and the Architectures of the Internet*. Meson Press, 2015.



**Lien vers le site de l'éditeur :** https://meson.press/books/the-politics-of-micro-decisions/

Disponible aussi ici : https://mediarep.org/handle/doc/1124

----
