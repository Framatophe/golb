---
title: "Propaganda"
subtitle: "Comment manipuler l’opinion en démocratie"
date: 2009-03-31
image: "/images/biblio/edward-bernays.jpg"
author: "Christophe Masutti"
description: "Bernays, E. L., & Baillargeon, N. (2009). Propaganda : Comment manipuler l’opinion en démocratie (O. Bonis, Trad.). Lux."
categories: ["bibliographie"]
auteurs: ["Bernays","Baillargeon"]
---

LE manuel classique de l’industrie des relations publiques » selon Noam Chomsky. Véritable petit guide pratique écrit en 1928 par le neveu américain de Sigmund Freud, ce livre expose cyniquement et sans détour les grands principes de la manipulation mentale de masse ou de ce que Bernays appelait la « fabrique du consentement ».  Comment imposer une nouvelle marque de lessive ? Comment faire élire un président ? Dans la logique des « démocraties de marché », ces questions se confondent.  Bernays assume pleinement ce constat : les choix des masses étant déterminants, ceux qui parviendront à les influencer détiendront réellement le pouvoir. La démocratie moderne implique une nouvelle forme de gouvernement, invisible : la propagande. Loin d’en faire la critique, l’auteur se propose d’en perfectionner et d’en systématiser les techniques, à partir des acquis de la psychanalyse.  Un document édifiant où l’on apprend que la propagande politique au XX<sup>e</sup> siècle n’est pas née dans les régimes totalitaires, mais au cœur même de la démocratie libérale américaine.

----

Bernays, E. L., & Baillargeon, N. (2009). *Propaganda : Comment manipuler l’opinion en démocratie* (O. Bonis, Trad.). Lux. 


**Lien vers le site de l'éditeur:** https://www.editions-zones.fr/lyber?propaganda

----
