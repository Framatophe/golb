---
title: "La société automatique 1"
subtitle: "L’avenir du travail"
date: 2015-01-21
image: "/images/biblio/bernard-stieglersocieteautomat.jpg"
author: "Christophe Masutti"
description: "Stiegler, Bernard. La société automatique 1. L’avenir du travail. Fayard, 2015."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Stiegler"]
---

Le 19 juillet 2014, le journal Le Soir révélait à Bruxelles que selon des estimations américaines, britanniques et belges, la France, la Belgique, le Royaume-Uni, l’Italie, la Pologne et les États-Unis pourraient perdre entre 43 et 50 % de leurs emplois dans les dix à quinze prochaines années. Trois mois plus tard, le Journal du dimanche soutenait que trois millions d’emplois seraient condamnés à disparaître en France au cours des dix prochaines années.

L’automatisation intégrée est le principal résultat de ce que l’on appelle « l’économie des data ». Organisant des boucles de rétroactions à la vitesse de la lumière (à travers les réseaux sociaux, objets communicants, puces RFID, capteurs, actionneurs, calcul intensif sur données massives appelées big data, smart cities et robots en tout genre) entre consommation, marketing, production, logistique et distribution, la réticulation généralisée conduit à une régression drastique de l’emploi dans tous les secteurs – de l’avocat au chauffeur routier, du médecin au manutentionnaire – et dans tous les pays.

Pourquoi le rapport remis en juin 2014 au président de la République française par Jean Pisani-Ferry occulte-t-il ces prévisions ? Pourquoi le gouvernement n’ouvre-t-il pas un débat sur l’avenir de la France et de l’Europe dans ce nouveau contexte ?
L’automatisation intégrale et généralisée fut anticipée de longue date – notamment par Karl Marx en 1857, par John Maynard Keynes en 1930, par Norbert Wiener et Georges Friedmann en 1950, et par Georges Elgozy en 1967. Tous ces penseurs y voyaient la nécessité d’un changement économique, politique et culturel radical.

Le temps de ce changement est venu, et le présent ouvrage est consacré à en analyser les fondements, à en décrire les enjeux et à préconiser des mesures à la hauteur d’une situation exceptionnelle à tous égards – où il se pourrait que commence véritablement le temps du travail.

----

Stiegler, Bernard. *La société automatique 1. L’avenir du travail*. Fayard, 2015.



**Lien vers le site de l'éditeur :** https://www.fayard.fr/sciences-humaines/la-societe-automatique-9782213685656


----
