---
title: "Cyberstructure"
subtitle: "L’internet, un espace politique"
date: 2018-07-31
image: "/images/biblio/Stephane-bortzmeyer.jpg"
author: "Christophe Masutti"
description: "Bortzmeyer, Stéphane. Cyberstructure: l’internet, un espace politique. C&F éditions, 2018."
categories: ["bibliographie"]
auteurs: ["Bortzmeyer"]
---

Les outils de communication ont d'emblée une dimension politique : ce sont les relations humaines, les idées, les échanges commerciaux ou les désirs qui s'y expriment. L'ouvrage de Stéphane Bortzmeyer montre les relations subtiles entre les décisions techniques concernant l'Internet et la réalisation — ou au contraire la mise en danger — des droits fondamentaux. Après une description précise du fonctionnement de l'Internet sous les aspects techniques, économiques et de la prise de décision, l'auteur évalue l'impact des choix informatiques sur l'espace politique du réseau. Un ouvrage pour appuyer une citoyenneté informée, adaptée aux techniques du XXI<sup>e</sup> siècle et en mesure de défendre les droits humains

----

Bortzmeyer, Stéphane. *Cyberstructure: l’internet, un espace politique*. C&F éditions, 2018.



**Lien vers le site de l'éditeur :** https://cyberstructure.fr/

----
