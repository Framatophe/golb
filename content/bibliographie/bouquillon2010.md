---
title: "Le Web collaboratif"
subtitle: "Mutations des industries de la culture et de la communication"
date: 2010-03-31
image: "/images/biblio/philippe-bouquillion.jpg"
author: "Christophe Masutti"
description: "Bouquillion, Philippe, et Jacob Thomas Matthews. Le Web collaboratif: mutations des industries de la culture et de la communication. Presses universitaires de Grenoble, 2010."
categories: ["bibliographie"]
auteurs: ["Bouquillion", "Matthews"]
---

LeWeb 2.0 – ou Web collaboratif – est aujourd’hui présenté comme une évolution culturelle majeure, voire même comme le fondement d’une nouvelle ère politique et sociétale. Dans les versions les plus optimistes, on assisterait à l’émergence d’une nouvelle culture participative, basée sur les interactions libres entre « usagers générateurs de contenus » sur le réseau Internet, et à l’effacement du rôle central occupé jusqu’à présent par les industries de la culture et de la communication.

Face à ces représentations dominantes, l’ouvrage propose une analyse critique du phénomène, à la fois sur le plan socio-économique et en termes de production idéologique. Il interroge tout d’abord la notion même deWeb 2.0 et les questions qu’il pose aux catégories d’analyse des industries de la culture et de la communication. Les auteurs illustrent comment les discours et les dispositifs propres au Web collaboratif s’inscrivent dans le cadre de profondes transformations des rapports entre médias, économie et politique. Cette approche permet de mieux comprendre la place spécifique du Web collaboratif au sein des mutations du capitalisme, dans les industries de la culture et de la communication.

----

Bouquillion, Philippe, et Jacob Thomas Matthews. *Le Web collaboratif: mutations des industries de la culture et de la communication*. Presses universitaires de Grenoble, 2010.


**Lien vers le site de l'éditeur :** https://www.pug.fr/produit/756/9782706115936/le-web-collaboratif

----
