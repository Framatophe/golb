---
title: "Surveillance://"
subtitle: "Les libertés au défi du numérique : comprendre et agir"
date: 2016-08-03
image: "/images/biblio/tristan-nitot.jpg"
author: "Christophe Masutti"
description: "Nitot, Tristan. Surveillance:// Les libertés au défi du numérique : comprendre et agir. C&F éditions, 2016."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Nitot"]
---

Tous nos pas dans le cyberespace sont suivis, enregistrés, analysés, et nos profils se monnayent en permanence. Comment en est-on arrivé là ? Les évolutions techniques ont permis à plus de quatre milliards d'internautes de communiquer, de rechercher de l'information ou de se distraire. Dans le même temps, la concentration des acteurs et les intérêts commerciaux ont développé une industrie mondiale des traces. Les États se sont engouffrés dans cette logique et ont mis en œuvre partout dans le monde des outils de surveillance de masse. Le livre de Tristan Nitot porte un regard lucide et analytique sur la situation de surveillance ; il nous offre également des moyens de reprendre le contrôle de notre vie numérique. Comprendre et agir sont les deux faces de cet ouvrage, qui le rendent indispensable à celles et ceux qui veulent défendre les libertés dans un monde numérique.


----

Nitot, Tristan. Surveillance:// Les libertés au défi du numérique : comprendre et agir. C&F éditions, 2016.


**Lien vers le site de l'éditeur :** https://cfeditions.com/surveillance/


----
