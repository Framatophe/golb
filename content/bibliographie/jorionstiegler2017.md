---
title: "La toile que nous voulons"
subtitle: "Le web néguentropique"
date: 2017-04-21
image: "/images/biblio/Bernard-stiegler-la-toile-que-nous-voulons.jpg"
author: "Christophe Masutti"
description: "Jorion, Paul, et Stiegler, Bernard et al. La toile que nous voulons. Le web néguentropique. Édité par Bernard Stiegler, FYP éditions, 2017."
type: bibliographie
categories: ["bibliographie"]
auteurs: ["Jorion", "Stiegler"]
---

Depuis son origine, et sous la pression d’un secteur économique désormais hégémonique, le web a évolué en un sens qui l’a profondément dénaturé, au point d’en faire un instrument d’hypercontrôle et d’imposition d’une gouvernance purement computationnelle de toutes choses. Privilégiant à outrance l’automatisation mise au service de modèles économiques devenus la plupart du temps ravageurs pour les structures sociales, cette évolution a affaibli toujours plus gravement les conditions d’une pratique réflexive, délibérative, outre les aspects révélés par Edward Snowden. Cet ouvrage présente les principaux aspects théoriques et pratiques d’une refondation indispensable du web, dans lequel et par lequel aujourd’hui nous vivons. L’automatisation du web ne peut être bénéfique que si elle permet d’organiser des plateformes contributives et des processus délibératifs, notamment à travers la conception d’un nouveau type de réseaux sociaux. Bernard Stiegler, Julian Assange, Paul Jorion, Dominique Cardon, Evgeny Morozov, François Bon,Thomas Bern, Bruno Teboul, Ariel Kyrou, Yuk Hui, Harry Halpin, Pierre Guéhénneux, David Berry, Christian Claude, Giuseppe Longo, balayent les aspects et les enjeux économiques, politiques, militaires et épistémologiques de cette rénovation nécessaire et avance des hypothèses pour l’élaboration d’un avenir meilleur.

----

Jorion, Paul, et Stiegler, Bernard et al. *La toile que nous voulons. Le web néguentropique*. Édité par Bernard Stiegler, FYP éditions, 2017. 

**Lien vers le site de l'éditeur :** https://www.fypeditions.com/toile-voulons-bernard-stiegler-evgeny-morozov-julian-assange-dominique-cardon/

----
